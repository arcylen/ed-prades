<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Tbl_unilevel_point_log extends Model
{
	protected $table = 'tbl_unilevel_point_log';
	protected $primaryKey = 'unilevel_id';
	public $timestamps = false;
	
	 public function scopeSlot($query)
    {
        return $query->join('tbl_slot', 'tbl_slot.slot_id', '=', 'tbl_unilevel_point_log.unilevel_sponsor');
    }
    public function scopeSlotregistree($query)
    {
        return $query->join('tbl_slot', 'tbl_slot.slot_id', '=', 'tbl_unilevel_point_log.unilevel_sponsor');
    }
    public function scopeAccount($query)
    {
        return $query->leftJoin('tbl_account', 'tbl_account.account_id', '=', 'tbl_slot.slot_owner');
    }
    public function scopeMembership($query)
    {
        return $query->leftJoin('tbl_membership', 'tbl_membership.membership_id', '=', 'tbl_slot.slot_membership');
    }
    public function scopeProduct($query)
    {
        return $query->leftJoin('tbl_product_code', 'tbl_product_code.product_pin', '=', 'tbl_unilevel_point_log.unilevel_product_pin')
        ->leftJoin('tbl_voucher_has_product', 'tbl_voucher_has_product.voucher_item_id', '=', 'tbl_product_code.voucher_item_id')
        ->leftJoin('tbl_product', 'tbl_product.product_id', '=', 'tbl_voucher_has_product.product_id');
    }
    
}

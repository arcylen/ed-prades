<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_pipeline_report extends Model
{
	protected $table = 'tbl_pipeline_report';
	protected $primaryKey = "pipeline_report_id";
	public $timestamps = false;

    public function scopeSlot($query)
    {
        return $query->leftJoin('tbl_slot', 'tbl_slot.slot_id', '=', 'tbl_pipeline_report.pipeline_slot_id');
    }
    public function scopeAccount($query)
    {
        return $query->leftJoin('tbl_account', 'tbl_account.account_id', '=', 'tbl_slot.slot_owner');
    }
    public function scopeSettings($query)
    {
        return $query->leftJoin('tbl_pipeline_settings', 'tbl_pipeline_settings.pipeline_id', '=', 'tbl_pipeline_report.pipeline_settings_id');
    }
}
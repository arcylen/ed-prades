<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_direct_unilevel_setting extends Model
{
	protected $table = 'tbl_direct_unilevel_setting';
	protected $primaryKey = 'set_id';
}
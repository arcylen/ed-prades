<?php namespace App\Classes;   
use App\Tbl_slot;
use App\Tbl_tree_placement;
use App\Tbl_tree_sponsor;
use App\Tbl_tree_pipeline;
use App\Tbl_binary_pairing;
use App\Tbl_binary_report;
use App\Tbl_direct_sb;
use App\Tbl_indirect_sb;
use App\Tbl_binary_breakdown;
use App\Tbl_binary_admin_report;
use App\Tbl_direct_report;
use App\Tbl_indirect_report;
use App\Tbl_indirect_admin_report;
use App\Tbl_direct_admin_report;
use App\Tbl_indirect_setting;
use App\Tbl_gsb_report;
use App\Tbl_total_gsb;
use App\Tbl_direct_unilevel;
use App\Tbl_indirect_unilevel_setting;
use App\Tbl_direct_unilevel_setting;
use App\Tbl_sequence_setting;
use App\Tbl_sequence;
use App\Tbl_unilevel_setting;
use App\Tbl_pipeline;
use App\Tbl_pipeline_settings;
use App\Tbl_pipeline_admin_report;
use App\Tbl_royalty_setting;
use App\Tbl_royalty_bonus;
use App\Tbl_royalty_promotion_setting;
use App\Tbl_pipeline_report;
use App\Tbl_pay_cheque;
use App\Tbl_pay_cheque_batch;
use App\Tbl_membership;
use App\Classes\Log;
use Carbon\Carbon;
use App\Tbl_matching_bonus;
use DB;
use App\Tbl_voucher;
use App\Tbl_wallet_logs;
use App\Tbl_travel_reward;
use Session;
use App\Tbl_travel_qualification;
use App\Tbl_pay_cheque_schedule;
use App\Tbl_account;
use DateTime;
use App\Tbl_unilevel_point_log;


use App\Tbl_stairstep;
use App\Tbl_stairstep_settings;
use App\Classes\Compute;
use DateInterval;
use App\Tbl_ladder_rebates;

class Compute
{
    public static $total_binary_payout = 0;
    public static $total_binary_pairing = 0;
    public static $total_binary_flushout = 0;
    public static $total_pipeline_payout = 0;
    public static $total_indirect_payout = 0;

    public static function update_account_pay_cheque($account_id)
    {
        $amount = Tbl_pay_cheque::where("pay_cheque_requested", 1)->where("pay_cheque_processed", 0)->slot()->where("slot_owner", $account_id)->sum("pay_cheque_amount");
        $update_account["account_paycheque_total"] = $amount;
        
        Tbl_account::where("account_id", $account_id)->update($update_account);
    }
    public static function update_pay_cheque_schedule($key)
    {
        $update["pay_schedule_last"] = Carbon::now();
        Tbl_pay_cheque_schedule::where("pay_schedule_key", $key)->update($update);
    }
    public static function tree($new_slot_id)
    {
    	$slot_info = Tbl_slot::id($new_slot_id)->first();
    	Compute::insert_tree_placement($slot_info, $new_slot_id, 1); /* TREE RECORD FOR BINARY GENEALOGY */
    	Compute::insert_tree_sponsor($slot_info, $new_slot_id, 1); /* TREE RECORD FOR SPONSORSHIP GENEALOGY */
    }
    public static function entry($new_slot_id, $method = "SLOT CREATION")
    {
        // Compute::binary_daily($new_slot_id, $method);
        Compute::direct_cutoff();
        
        /*Soon */
        // Compute::dsb_cutoff();
        
    }
    
    public static function dsb_cutoff()
    {
        $_slot = Tbl_slot::where("slot_direct_report", 0)->membership()->get();

        $total_payout = 0;
        $referral_count = 0;
        foreach($_slot as $slot)
        {
            $bonus = $slot->membership_direct_sponsorship_bonus;
            $sponsor_info = Tbl_slot::where("slot_id", $slot->slot_sponsor)->membership()->first();

            if(!empty($sponsor_info))
            {
                // echo "SLOT NO. " . $slot->slot_sponsor . " HAS SPONSORED SLOT NO. " . $slot->slot_id . "<br>";
                // echo "SLOT NO. " . $slot->slot_sponsor . " earned <b>" . currency($bonus) . "</b>";
                // echo "<br><br>";
                $total_payout += $bonus;
                $referral_count++;
            }

        }
        
        if($total_payout != 0)
        {
            $insert_admin_report["admin_report_amount"] = $total_payout;
            $insert_admin_report["admin_report_date"] = Carbon::now();
            $insert_admin_report["admin_referral_count"] = $referral_count;
            $admin_report_id = Tbl_direct_admin_report::insertGetId($insert_admin_report);
            
            foreach($_slot as $key => $slot)
            {
                $ss = $slot;    
                //luke additional 1k if pipeline2open
                $additional = Compute::add1kpipeline2open($slot->slot_sponsor);
                $slot->membership_direct_sponsorship_bonus = $slot->membership_direct_sponsorship_bonus + $additional;
                //
                $insert_report["direct_sponsor"] = $slot->slot_sponsor;
                $insert_report["direct_child"] = $slot->slot_id;
                $insert_report["direct_amount"] = $slot->membership_direct_sponsorship_bonus;
                $insert_report["direct_date"] = Carbon::now();
                $insert_report["direct_admin_report"] = $admin_report_id;
                $insert_report["direct_pay_cheque"] = 0;
                $report_id = Tbl_direct_report::insertGetId($insert_report);

                $update_slot["slot_direct_report"] = $report_id;
                Tbl_slot::where("slot_direct_report", 0)->update($update_slot);
                

                /* UPDATE SLOT SPONSOR COUNT */
                $slot_sponsor = Tbl_slot::where("slot_id", $slot->slot_sponsor)->first();
                
                if($slot_sponsor)
                {
                    $update_slot_count["slot_direct_count"] = $slot_sponsor->slot_direct_count + 1;
                    Tbl_slot::where("slot_id", $slot->slot_sponsor)->update($update_slot_count);

                    /* PIPELINE */
                    // Compute::insert_pipeline_record($slot->slot_sponsor, $slot->slot_id, $slot->slot_pipeline_level);

                    /* INDIRECT */
                    // Compute::indirect_referral_cutoff($slot->slot_sponsor, $slot->slot_id, 1);
                }
            }
            // Compute::insert_pipeline_record_admin_report();
            Compute::insert_indirect_record_admin_report();
        }  
    }
    public static function direct_cutoff()
    {
        
        $_slot = Tbl_slot::where("slot_direct_report", 0)->membership()->get();

        $total_payout = 0;
        $referral_count = 0;
        foreach($_slot as $slot)
        {
            $bonus = $slot->membership_direct_sponsorship_bonus;
            $sponsor_info = Tbl_slot::where("slot_id", $slot->slot_sponsor)->membership()->first();

            if(!empty($sponsor_info))
            {
                // echo "SLOT NO. " . $slot->slot_sponsor . " HAS SPONSORED SLOT NO. " . $slot->slot_id . "<br>";
                // echo "SLOT NO. " . $slot->slot_sponsor . " earned <b>" . currency($bonus) . "</b>";
                // echo "<br><br>";
                $total_payout += $bonus;
                $referral_count++;
            }

        }
        
        if($total_payout != 0)
        {
            $insert_admin_report["admin_report_amount"] = $total_payout;
            $insert_admin_report["admin_report_date"] = Carbon::now();
            $insert_admin_report["admin_referral_count"] = $referral_count;
            $admin_report_id = Tbl_direct_admin_report::insertGetId($insert_admin_report);
            
            foreach($_slot as $key => $slot)
            {
                $ss = $slot;    
                //luke additional 1k if pipeline2open
                $additional = Compute::add1kpipeline2open($slot->slot_sponsor);
                $slot->membership_direct_sponsorship_bonus = $slot->membership_direct_sponsorship_bonus + $additional;
                //
                $insert_report["direct_sponsor"] = $slot->slot_sponsor;
                $insert_report["direct_child"] = $slot->slot_id;
                $insert_report["direct_amount"] = $slot->membership_direct_sponsorship_bonus;
                $insert_report["direct_date"] = Carbon::now();
                $insert_report["direct_admin_report"] = $admin_report_id;
                $insert_report["direct_pay_cheque"] = 0;
                $report_id = Tbl_direct_report::insertGetId($insert_report);

                $update_slot["slot_direct_report"] = $report_id;
                Tbl_slot::where("slot_direct_report", 0)->update($update_slot);
                

                /* UPDATE SLOT SPONSOR COUNT */
                $slot_sponsor = Tbl_slot::where("slot_id", $slot->slot_sponsor)->first();
                
                if($slot_sponsor)
                {
                    $update_slot_count["slot_direct_count"] = $slot_sponsor->slot_direct_count + 1;
                    Tbl_slot::where("slot_id", $slot->slot_sponsor)->update($update_slot_count);

                    /* PIPELINE */
                    // Compute::insert_pipeline_record($slot->slot_sponsor, $slot->slot_id, $slot->slot_pipeline_level);

                    /* INDIRECT */
                    // Compute::indirect_referral_cutoff($slot->slot_sponsor, $slot->slot_id, 1);
                }
            }
            // Compute::insert_pipeline_record_admin_report();
            Compute::insert_indirect_record_admin_report();
        }  
    }
    public static function indirect_referral_cutoff($parent_id, $registree_id, $level)
    {
        $registree_info = Tbl_slot::where("slot_id", $registree_id)->first();
        $parent_info = Tbl_slot::where("slot_id", $parent_id)->first();
        $indirect_setting = Tbl_indirect_setting::where("level", $level)->where("membership_id", $registree_info->slot_membership)->first();
    
        if($indirect_setting)
        {
            if($parent_info)
            {
                if($indirect_setting->value != 0)
                {
                    /* INSERT INCOME REPORT */
                    $insert_report["indirect_slot"] = $parent_info->slot_id;
                    $insert_report["indirect_registree"] = $registree_info->slot_id;
                    $insert_report["indirect_level"] = $level;
                    $insert_report["indirect_date"] = Carbon::now();
                    $insert_report["indirect_admin_report"] = 0;
                    $insert_report["indirect_pay_cheque"] = 0;
                    $insert_report["indirect_amount"] = $indirect_setting->value;
                    $insert_report["indirect_membership"] = $registree_info->slot_membership;
                    Tbl_indirect_report::insert($insert_report);

                    Compute::$total_indirect_payout += $indirect_setting->value;
                }

                Compute::indirect_referral_cutoff($parent_info->slot_sponsor, $registree_id, $level + 1);
            }
        }
    }
    public static function binary_cutoff()
    {
        $_slot = Tbl_slot::where("slot_binary_affected", 1)->membership()->get();
        foreach($_slot as $slot)
        {
            Compute::binary_cutoff_compute($slot);
        }

        if(Compute::$total_binary_pairing != 0)
        {
            /* INSERT ADMIN REPORT */
            $insert_binary_admin["binary_admin_date"] = Carbon::now();
            $insert_binary_admin["binary_admin_flushout"] = Compute::$total_binary_flushout;
            $insert_binary_admin["binary_admin_amount"] = Compute::$total_binary_payout;
            $insert_binary_admin["binary_admin_pairing_count"] = Compute::$total_binary_pairing;
            $binary_admin_id = Tbl_binary_admin_report::insertGetId($insert_binary_admin);

            $update_binary_report["binary_admin_id"] = $binary_admin_id;
            Tbl_binary_report::where("binary_admin_id", 0)->update($update_binary_report);
        }

        echo "SUPER TOTAL OF PAYOUT IS <b> " . currency(Compute::$total_binary_payout) . "</b><br>";
        echo "SUPER TOTAL OF FLUSHOUT IS <b> " . currency(Compute::$total_binary_flushout) . "</b><br>";
        echo "SUPER TOTAL PAIRING: <b>" . Compute::$total_binary_pairing . "</b> <br>";
        //die();
    }
    public static function binary_cutoff_compute($slot_info)
    {
        $max_pair = $slot_info->max_pairs_per_day;
        $number_of_pairs = 0;
        $earnings = 0;
        $flushout = 0;

        $_pairing = Tbl_binary_pairing::orderBy("pairing_point_l", "desc")->where('membership_id',$slot_info->slot_membership)->get();

        $initial["left"] = $binary["left"] = $slot_info->slot_binary_left;
        $initial["right"] = $binary["right"] = $slot_info->slot_binary_right; 

        echo "<span style='color: red'>------------------------------------------ COMPUTE SLOT NO. " . str_pad($slot_info->slot_id, 6, '0', STR_PAD_LEFT) . "------------------------------------------<br></span>";

        foreach($_pairing as $pairing)
        {   
            if($pairing->membership_id == $slot_info->slot_membership)
            {
                while($binary["left"] >= $pairing->pairing_point_l && $binary["right"] >= $pairing->pairing_point_r)
                {
                    $number_of_pairs++;

                    if($number_of_pairs > $max_pair) //FLUSHOUT
                    {
                        echo "(" . $binary["left"] . ") - (" . $binary["right"] . ") FLUSHOUT - <b>" . currency($pairing->pairing_income) . "</b><br>";
                        Compute::$total_binary_flushout += $pairing->pairing_income;
                        Compute::$total_binary_pairing++;
                        $flushout+= $pairing->pairing_income;

                    }
                    else //PAIRING
                    {
                        echo "(" . $binary["left"] . ") - (" . $binary["right"] . ") PAIRING COMBINATION (EARNED) - <b>" . currency($pairing->pairing_income) . "</b><br>";
                        $earnings += $pairing->pairing_income;
                        Compute::$total_binary_payout += $pairing->pairing_income;
                        Compute::$total_binary_pairing++;
                    }

                    $binary["left"] = $binary["left"] - $pairing->pairing_point_l;
                    $binary["right"] = $binary["right"] - $pairing->pairing_point_r;
                }
            }
        }


        /* INSERT BINARY REPORT */
        $last_binary_report = Tbl_binary_report::where("binary_slot_id", $slot_info->slot_id)->orderBy("binary_id", "desc")->first();
        
        if($last_binary_report)
        {
            $insert_binary_report["binary_start_left"] = $last_binary_report->binary_end_left;
            $insert_binary_report["binary_start_right"] = $last_binary_report->binary_end_right;
        }
        else
        {
            $insert_binary_report["binary_start_left"] = 0;
            $insert_binary_report["binary_start_right"] = 0;
        }

        /* INSERT BINARY REPORT */
        $insert_binary_report["binary_date"] = Carbon::today()->format("Y-m-d");
        $insert_binary_report["binary_slot_id"] = $slot_info->slot_id;
        $insert_binary_report["binary_today_left"] = $initial["left"];
        $insert_binary_report["binary_today_right"] = $initial["right"];
        $insert_binary_report["binary_end_left"] = $binary["left"];
        $insert_binary_report["binary_end_right"] = $binary["right"];
        $insert_binary_report["binary_flushout"] = $flushout;
        $insert_binary_report["binary_earned"] = $earnings;
        $binary_report_id = Tbl_binary_report::insertGetId($insert_binary_report);

        /* UPDATE BREAKDOWN REPORT */
        $update_breakdown["binary_report_id"] = $binary_report_id;
        Tbl_binary_breakdown::where("binary_slot_id", $slot_info->slot_id)->where("binary_report_id", 0)->update($update_breakdown);

        /* UPDATE SLOT */
        $update_slot_info["slot_binary_left"] = $binary["left"];
        $update_slot_info["slot_binary_right"] = $binary["right"];
        $update_slot_info["slot_binary_affected"] = 0;
        Tbl_slot::where("slot_id", $slot_info->slot_id)->update($update_slot_info);

        echo "TOTAL INCOME: <b>" . currency($earnings) . "</b> <br>";
        echo "TOTAL FLUSHOUT: <b>" . currency($flushout) . "</b> <br>";
        echo "<br><br>";
    }
    public static function insert_pipeline_record_admin_report()
    {
        $insert["pipeline_admin_amount"] = Compute::$total_pipeline_payout;
        $insert["pipeline_admin_date"] = Carbon::now();
        $pipeline_admin_report = Tbl_pipeline_admin_report::insertGetId($insert);

        $update["pipeline_admin_report"] = $pipeline_admin_report;
        Tbl_pipeline_report::where("pipeline_admin_report", 0)->update($update);
    }
    public static function insert_indirect_record_admin_report()
    {
        $insert["admin_report_amount"] = Compute::$total_indirect_payout;
        $insert["admin_report_date"] = Carbon::now();
        $indirect_admin_report = Tbl_indirect_admin_report::insertGetId($insert);

        $update["indirect_admin_report"] = $indirect_admin_report;
        Tbl_indirect_report::where("indirect_admin_report", 0)->update($update);
    }
    public static function insert_pipeline_record($upline, $downline, $level)
    {
        $upline_info = Tbl_slot::where("slot_id", $upline)->first();
        $level = $upline_info->slot_pipeline_level;
        $insert["pipeline_upline"] = $upline;
        $insert["pipeline_downline"] = $downline;
        $insert["pipeline_level"] = $level;
        Tbl_pipeline::insert($insert);

        /*  COUNT DONWLINE OF CURRENT UPLINE IN CURRENT LEVEL */
        $downline_count = Tbl_pipeline::where("pipeline_upline", $upline)->where("pipeline_level", $level)->count();

        /* CHECK LEVEL */
        if($upline_info)
        {
            if($upline_info->slot_pipeline_level <= $level)
            {
                /* GET GRADUATION REQUIREMENT */
                $pipeline_settings = Tbl_pipeline_settings::where("pipeline_id", $upline_info->slot_pipeline_settings)->orderBy("pipeline_order", "asc")->first();

                /* CHECK IF QUALIFIED FOR GRADUATION */
                if($pipeline_settings->pipeline_requirement <= $downline_count)
                {
                    /* INSERT INCOME FOR PIPELINE */
                    $insert_report["pipeline_slot_id"] = $upline_info->slot_id;
                    $insert_report["pipeline_settings_id"] = $pipeline_settings->pipeline_id;
                    $insert_report["pipeline_earned_amount"] = $pipeline_settings->pipeline_graduate_amount;
                    $insert_report["pipeline_date"] = Carbon::now();
                    $insert_report["pipeline_admin_report"] = 0;
                    Tbl_pipeline_report::insert($insert_report);
                    

                    /* CHECK IF WILL GIVE BINARY POINTS */
                    if($pipeline_settings->pipeline_binary_points != 0)
                    {
                        Compute::binary_daily($upline_info->slot_id, "PIPELINE GRADUATE OF SLOT", $pipeline_settings->pipeline_binary_points);
                    }

                    Compute::$total_pipeline_payout += $pipeline_settings->pipeline_graduate_amount;

                    /* UPDATE SLOT PIPELINE SETTINGS */
                    $update_slot_pipe["slot_pipeline_settings"] = $pipeline_settings->pipeline_id + 1;
                    Tbl_slot::where("slot_id", $upline_info->slot_id)->update($update_slot_pipe);

                    /* CHECK TO FILL UP NEXT */
                    if($pipeline_settings->pipeline_graduate_trigger_next == 1)
                    {
                        /* UPDATE SLOT PIPELINE LEVEL */
                        $update_slot_graduate["slot_pipeline_level"] = $upline_info->slot_pipeline_level + 1;
                        Tbl_slot::where("slot_id", $upline_info->slot_id)->update($update_slot_graduate);


                        /* SET PIPELINE DOWNLINE AS A GRADUATE */
                        $graduate["pipeline_graduate"] = 1;
                        Tbl_pipeline::where("pipeline_level", $level)->where("pipeline_downline", $upline_info->slot_id)->update($graduate);
                        $current_graduate = Tbl_pipeline::where("pipeline_level", $level)->where("pipeline_downline", $upline_info->slot_id)->first();
                        $current_slot = $current_graduate->pipeline_upline;

                        /* INSERT TO NEXT PIPELINE */
                        $qualified_upline = false;

                        /* CHECK QUALIFIED UPLINE */
                        while($qualified_upline == false)
                        {
                            $check = Tbl_pipeline::where("pipeline_level", $level)->where("pipeline_downline", $current_slot)->first();

                            if($check)
                            {
                                if($check->pipeline_graduate == 1)
                                {
                                    $qualified_upline = true;
                                }
                                else
                                {
                                    $current_slot = $check->pipeline_upline;
                                }
                            }
                            else
                            {
                                $qualified_upline = true;
                            }
                        }

                        if($check)
                        {
                            $new_upline = $check->pipeline_downline;
                        }
                        else
                        {
                            $new_upline = 2147483647;
                        }

                        Compute::insert_pipeline_record($new_upline, $upline_info->slot_id, $level+1);
                    }
                }
            }
        }


    }
    public static function insert_tree_pipeline($slot_info, $new_slot_id, $level, $pipeline_level)
    {
    }
    public static function unilevel_paycheque()
    {
       
        
        $data['slot'] = Tbl_slot::account()->membership()
		->where('slot_personal_points', '>=', DB::raw('membership_required_pv'))
		->get();
		foreach($data['slot'] as $key => $value){
		   $data['sum_perslot'] = Tbl_unilevel_point_log::where("unilevel_distributed", 0)->where('unilevel_type', 1)->where('unilevel_sponsor',$value->slot_id)->sum('unilevel_points');
		   $data['sum_perslot_multiplier'] = $data['sum_perslot'] * $value->multiplier;
		   $data['sum_perslot_batch'] = Tbl_unilevel_point_log::where("unilevel_distributed", 0)->where('unilevel_type', 1)->where('unilevel_sponsor',$value->slot_id)->get();
		   
        //   dd($data);
           foreach($data['sum_perslot_batch'] as $unilevel_report)
            {
                /* INSERT PAYCHECK */
                $insert_batch["pay_cheque_batch_amount"] = $unilevel_report->unilevel_points * $value->multiplier;
                $insert_batch["pay_cheque_batch_date"] = Carbon::now();
                $insert_batch["pay_cheque_batch_reference"] = "unilevel";
                $pay_cheque_batch_id = Tbl_pay_cheque_batch::insertGetId($insert_batch);  
            }
            if(isset($pay_cheque_batch_id)){
              $insert_pay_cheque["pay_cheque_amount"] = $data['sum_perslot_multiplier'];
            $insert_pay_cheque["pay_cheque_slot"] = $value->slot_id;
            $insert_pay_cheque["pay_cheque_batch_id"] = $pay_cheque_batch_id;
            $pay_cheque_id = Tbl_pay_cheque::insertGetId($insert_pay_cheque); 
            
            /* UPDATE DIRECT REPORT */
            $update_unilevel_report["unilevel_distributed"] = $pay_cheque_id;
            $update_unilevel_reports["unilevel_distributed"] = $pay_cheque_id;
            Tbl_unilevel_point_log::where("unilevel_distributed", 0)->where("unilevel_sponsor", $unilevel_report->unilevel_sponsor)->update($update_unilevel_reports);
            
            
            // $gspv =Tbl_slot::where('slot_id', $value->slot_id)->pluck("total_group_spv");
            // $pspv =Tbl_slot::where('slot_id', $value->slot_id)->pluck("total_personal_spv");
            
            // $update_unilevel_points["total_group_spv"] = $gspv + $update_unilevel_points['slot_group_points'];
            // $update_unilevel_points["total_personal_spv"] = $pspv + $update_unilevel_points['slot_personal_points'];
            
            // $update_unilevel_points['slot_personal_points'] = 0;
            // $update_unilevel_points['slot_group_points'] = 0;
            // Tbl_slot::where('slot_id', $value->slot_id)->update($update_unilevel_points);
            
            
            /* UPDATE SLOT OWNER'S PAYCHEQUE */
            Compute::update_account_pay_cheque($value->slot_id);
            }
            
            
		}
        // dd($data);
        Compute::update_pay_cheque_schedule("unilevel");
    }
    public static function rb_paycheque()
    {
         $_direct_report = Tbl_direct_report::selectRaw('*, sum(direct_amount) as earned')->where("direct_pay_cheque", 0)->slot()->groupBy('tbl_slot.slot_id')->get();
        $total_pay_cheque = 0;
        foreach($_direct_report as $direct_report)
        {
            $total_pay_cheque += $direct_report->earned;
        }

        /* INSERT BATCH */
        $insert_batch["pay_cheque_batch_amount"] = $total_pay_cheque;
        $insert_batch["pay_cheque_batch_date"] = Carbon::now();
        $insert_batch["pay_cheque_batch_reference"] = "Referral Bonus";
        $pay_cheque_batch_id = Tbl_pay_cheque_batch::insertGetId($insert_batch);

        foreach($_direct_report as $direct_report)
        {
            /* INSERT PAYCHECK */
            $insert_pay_cheque["pay_cheque_amount"] = $direct_report->earned;
            $insert_pay_cheque["pay_cheque_slot"] = $direct_report->direct_sponsor;
            $insert_pay_cheque["pay_cheque_batch_id"] = $pay_cheque_batch_id;
            $pay_cheque_id = Tbl_pay_cheque::insertGetId($insert_pay_cheque);

            /* UPDATE DIRECT REPORT */
            $update_direct_report["direct_pay_cheque"] = $pay_cheque_id;
            Tbl_direct_report::where("direct_pay_cheque", 0)->where("direct_sponsor", $direct_report->direct_sponsor)->update($update_direct_report);
        
            /* UPDATE SLOT OWNER'S PAYCHEQUE */
            Compute::update_account_pay_cheque($direct_report->direct_sponsor);
        }

        Compute::update_pay_cheque_schedule("rb");
    }
    public static function dsb_paycheque()
    {
        $_dsb_report = Tbl_direct_sb::selectRaw('*, sum(dsb_amount) as earned')->where("dsb_paycheque", 0)->slot()->groupBy('tbl_slot.slot_id')->get();
        $total_pay_cheque = 0;
        foreach($_dsb_report as $dsb_report)
        {
            $total_pay_cheque += $dsb_report->earned;
        }

        /* INSERT BATCH */
        $insert_batch["pay_cheque_batch_amount"] = $total_pay_cheque;
        $insert_batch["pay_cheque_batch_date"] = Carbon::now();
        $insert_batch["pay_cheque_batch_reference"] = "Direct Sales Bonus";
        $pay_cheque_batch_id = Tbl_pay_cheque_batch::insertGetId($insert_batch);

        foreach($_dsb_report as $dsb_report)
        {
            /* INSERT PAYCHECK */
            $insert_pay_cheque["pay_cheque_amount"] = $dsb_report->earned;
            $insert_pay_cheque["pay_cheque_slot"] = $dsb_report->dsb_sponsor;
            $insert_pay_cheque["pay_cheque_batch_id"] = $pay_cheque_batch_id;
            $pay_cheque_id = Tbl_pay_cheque::insertGetId($insert_pay_cheque);

            /* UPDATE DIRECT REPORT */
            $update_direct_report["dsb_paycheque"] = $pay_cheque_id;
            Tbl_direct_sb::where("dsb_paycheque", 0)->where("dsb_sponsor", $dsb_report->dsb_sponsor)->update($update_direct_report);
        
            /* UPDATE SLOT OWNER'S PAYCHEQUE */
            Compute::update_account_pay_cheque($dsb_report->dsb_sponsor);
        }

        Compute::update_pay_cheque_schedule("dsb");
    }
    public static function isb_paycheque()
    {
        $_isb_report = Tbl_indirect_sb::selectRaw('*, sum(isb_amount) as earned')->where("isb_paycheque", 0)->slot()->groupBy('tbl_slot.slot_id')->get();
        $total_pay_cheque = 0;
        foreach($_isb_report as $isb_report)
        {
            $total_pay_cheque += $isb_report->earned;
        }

        /* INSERT BATCH */
        $insert_batch["pay_cheque_batch_amount"] = $total_pay_cheque;
        $insert_batch["pay_cheque_batch_date"] = Carbon::now();
        $insert_batch["pay_cheque_batch_reference"] = "Indirect Sales Bonus";
        $pay_cheque_batch_id = Tbl_pay_cheque_batch::insertGetId($insert_batch);

        foreach($_isb_report as $isb_report)
        {
            /* INSERT PAYCHECK */
            $insert_pay_cheque["pay_cheque_amount"] = $isb_report->earned;
            $insert_pay_cheque["pay_cheque_slot"] = $isb_report->isb_sponsor;
            $insert_pay_cheque["pay_cheque_batch_id"] = $pay_cheque_batch_id;
            $pay_cheque_id = Tbl_pay_cheque::insertGetId($insert_pay_cheque);

            /* UPDATE DIRECT REPORT */
            $update_direct_report["isb_paycheque"] = $pay_cheque_id;
            Tbl_indirect_sb::where("isb_paycheque", 0)->where("isb_sponsor", $isb_report->isb_sponsor)->update($update_direct_report);
        
            /* UPDATE SLOT OWNER'S PAYCHEQUE */
            Compute::update_account_pay_cheque($isb_report->isb_sponsor);
        }
        Compute::update_pay_cheque_schedule("isb");
    }
    // public static function royalty_bonus_paycheque()
    // {
        // Compute::royalty_bonus();

    // }
    public static function royalty_bonus()
    {
        // dd("dfghjk");
        $royalty_level = Tbl_royalty_setting::get();

        //select all levels
        foreach ($royalty_level as $key => $value) {
            $royalty_level[$key] = $value;
            //check if slots is counted in royalty level
            Compute::compute_rb($royalty_level[$key]->rb_id);
        }


        $_all_slot = Tbl_slot::get();
        foreach ($_all_slot as $key_5 => $value_5) {
            $_all_slot[$key_5] = $value_5;
            $up_slot["total_group_spv"] = 0;
            $up_slot["total_personal_spv"] = 0;
            Tbl_slot::where("slot_id",$_all_slot[$key_5]->slot_id)->update($up_slot);
        }
        dd("success");
    }

    public static function compute_rb($rb_id)
    { 
        $royalty_level = Tbl_royalty_setting::where("rb_id",$rb_id)->first();
          
         $_slots = Tbl_slot::where("royalty_level",$royalty_level->rb_id)->orderBy("slot_id","DESC")->get(); 
            // dd($_slots);
            if($_slots)
            {
                foreach ($_slots as $key_1 => $value_1) {
                    $_slots[$key_1] = $value_1;
                    $qualified = Compute::check_if_qualified($_slots[$key_1]->slot_id);
                    if($qualified == 1)
                    {
                        // dd($_slots[$key_1]->slot_id." ".$_slots[$key_1]->royalty_level);

                        //owner SPV
                          $owner_slot = Tbl_slot::where("slot_id",$_slots[$key_1]->slot_id)->first();
                          $owner_spv = $owner_slot->total_group_spv + $owner_slot->total_personal_spv;

                          // dd($owner_spv);
                         $_childs = Tbl_tree_sponsor::where("sponsor_tree_parent_id",$_slots[$key_1]->slot_id)->get();
                         // dd($_childs);
                         $child = null;
                         foreach($_childs as $key_3 => $value_3)
                         {
                           $child[$key_3] = $value_3;
                           $rb_child[$key_3] = Tbl_slot::where("slot_id",$child[$key_3]->sponsor_tree_child_id)
                                                       ->get();
                         }

                          //GET SPV of THEIR CHILD
                          $rb = null;
                          $spv = 0;
                          $total_spv =0;
                          $net_total_spv = 0;
                          foreach ($rb_child as $key_2 => $value_2) 
                          {
                            $rb[$key_2] = $value_2;
                            $spv = $rb[$key_2]->sum("total_personal_spv") + $rb[$key_2]->sum("total_group_spv");
                            $net_total_spv += $spv;
                          }

                          //total SPV of the GROUP
                          $total_spv = $net_total_spv + $owner_spv;
                        //check if he/she have upline with the same level
                        $_slot_upline = Tbl_tree_sponsor::join("tbl_slot","tbl_slot.slot_id","=","tbl_tree_sponsor.sponsor_tree_parent_id")
                                                        ->where("tbl_slot.royalty_level",$royalty_level->rb_id)
                                                        ->where("sponsor_tree_child_id",$_slots[$key_1]->slot_id)
                                                        ->get();

                        $count_upline = Tbl_tree_sponsor::join("tbl_slot","tbl_slot.slot_id","=","tbl_tree_sponsor.sponsor_tree_parent_id")
                                                            ->where("tbl_slot.royalty_level",$royalty_level->rb_id)
                                                            ->where("sponsor_tree_child_id",$_slots[$key_1]->slot_id)
                                                            ->count();
                        
                        $ctr = 1;
                        if($_slot_upline)
                        {
                            $slot_upline = null;
                             foreach ($_slot_upline as $key_5 => $value_5) {
                                    $slot_upline[$key_5] = $value_5;
                                    $qualified_1 = Compute::check_if_qualified($slot_upline[$key_5]->slot_id);
                                    if($qualified_1 == 1)
                                    {                                     
                                        $ctr = $ctr + 1;   
                                    }
                            }
                        }
                        // dd($ctr);
                        // if($count_upline == 0 )
                        // {
                        //     $count_upline = 1 ;
                        // }
                        // else
                        // {
                        //     $count_upline = $count_upline + 1;
                        // }
                        $profit_sharing = 0 ;
                        $profit_sharing = ($royalty_level->rb_profitsharing/100) * $total_spv;
                       // dd($profit_sharing);
                        $final_spv = (( $royalty_level->rb_value/100) * $profit_sharing) / $ctr;
                        // dd($final_spv);
                        if($_slot_upline != null)
                        {
                            foreach ($_slot_upline as $key_4 => $value_4) {
                                $_slot_upline[$key_4] = $value_4;
                                $qualified_1 = Compute::check_if_qualified($_slot_upline[$key_4]->slot_id);
                                    if($qualified_1 == 1)
                                    {  
                                        $ins_rb["rb_bonus"] = $final_spv;
                                        $ins_rb["rb_sponsor_id"] = $_slot_upline[$key_4]->sponsor_tree_child_id;
                                        $ins_rb["rb_id"] = $_slot_upline[$key_4]->royalty_level;
                                        $ins_rb["rb_slot_id"] = $_slot_upline[$key_4]->slot_id;
                                        $ins_rb["rb_date"] = Carbon::now();

                                        Tbl_royalty_bonus::insert($ins_rb);
                                    }    
                            }
                            $ins_rb["rb_bonus"] = $final_spv;
                            $ins_rb["rb_sponsor_id"] = $_slots[$key_1]->slot_id;
                            $ins_rb["rb_id"] =  $_slots[$key_1]->royalty_level;
                            $ins_rb["rb_slot_id"] =  $_slots[$key_1]->slot_id;
                            $ins_rb["rb_date"] = Carbon::now();
                            // dd($ins_rb);
                            Tbl_royalty_bonus::insert($ins_rb);
                        }
                        else
                        {
                                $ins_rb["rb_bonus"] = $final_spv;
                                $ins_rb["rb_sponsor_id"] = $_slots[$key_1]->slot_id;
                                $ins_rb["rb_id"] =  $_slots[$key_1]->royalty_level;
                                $ins_rb["rb_slot_id"] =  $_slots[$key_1]->slot_id;
                                $ins_rb["rb_date"] = Carbon::now();
                                // dd($ins_rb);
                                Tbl_royalty_bonus::insert($ins_rb);
                        }
                    }
                }
            }
    }
    public static function check_if_qualified($child_id)
    {       
        $rb_child = Tbl_slot::where("slot_id",$child_id)->first();
        $rb = Tbl_royalty_setting::where("rb_id",$rb_child->royalty_level)->first();

        if($rb)
        {
            $spv_total = $rb_child->total_personal_spv + $rb_child->total_group_spv;
            

               $count_assoc_director = Tbl_tree_sponsor::join("tbl_slot",'tbl_tree_sponsor.sponsor_tree_child_id','=','tbl_slot.slot_id')
                                                    ->where("tbl_slot.active_member",1)
                                                    ->where("tbl_slot.royalty_level",0) 
                                                    ->where("tbl_slot.sequence_level_id",4) // associate director_id
                                                    ->where("tbl_tree_sponsor.sponsor_tree_parent_id",$child_id)
                                                    ->count();
                    $count_director = Tbl_tree_sponsor::join("tbl_slot",'tbl_tree_sponsor.sponsor_tree_child_id','=','tbl_slot.slot_id')
                                                    ->where("tbl_slot.active_member",1)
                                                    ->where("tbl_slot.royalty_level",0) 
                                                    ->where("tbl_slot.sequence_level_id",5) // Director_id
                                                    ->where("tbl_tree_sponsor.sponsor_tree_parent_id",$child_id)
                                                    ->count();


                    $count_avp = Tbl_tree_sponsor::join("tbl_slot",'tbl_tree_sponsor.sponsor_tree_child_id','=','tbl_slot.slot_id')
                                                    ->where("tbl_slot.active_member",1)
                                                    ->where("tbl_slot.royalty_level",1) // AVP
                                                    ->where("tbl_tree_sponsor.sponsor_tree_parent_id",$child_id)
                                                    ->count();
                    $count_vp = Tbl_tree_sponsor::join("tbl_slot",'tbl_tree_sponsor.sponsor_tree_child_id','=','tbl_slot.slot_id')
                                                    ->where("tbl_slot.active_member",1)
                                                    ->where("tbl_slot.royalty_level",2) // VP
                                                    ->where("tbl_tree_sponsor.sponsor_tree_parent_id",$child_id)
                                                    ->count();
                    $count_svp = Tbl_tree_sponsor::join("tbl_slot",'tbl_tree_sponsor.sponsor_tree_child_id','=','tbl_slot.slot_id')
                                                    ->where("tbl_slot.active_member",1)
                                                    ->where("tbl_slot.royalty_level",3) // SVP
                                                    ->where("tbl_tree_sponsor.sponsor_tree_parent_id",$child_id)
                                                    ->count();
                    $count_evp = Tbl_tree_sponsor::join("tbl_slot",'tbl_tree_sponsor.sponsor_tree_child_id','=','tbl_slot.slot_id')
                                                    ->where("tbl_slot.active_member",1)
                                                    ->where("tbl_slot.royalty_level",4) // EVP
                                                    ->where("tbl_tree_sponsor.sponsor_tree_parent_id",$child_id)
                                                    ->count();
                    $count_president = Tbl_tree_sponsor::join("tbl_slot",'tbl_tree_sponsor.sponsor_tree_child_id','=','tbl_slot.slot_id')
                                                    ->where("tbl_slot.active_member",1)
                                                    ->where("tbl_slot.royalty_level",5) // P
                                                    ->where("tbl_tree_sponsor.sponsor_tree_parent_id",$child_id)
                                                    ->count();

           if($spv_total >= $rb->rb_required_spv)
           {
                if($count_assoc_director >= $rb->rb_active_assoc_director)
                {
                    if($count_director >= $rb->rb_active_director)
                    {
                        if($count_avp >= $rb->rb_active_avp)
                        {
                         if($count_vp >= $rb->rb_active_vp)
                            {
                                if($count_svp >= $rb->rb_active_svp)
                                {
                                 if($count_evp >= $rb->rb_active_evp)
                                    {
                                     if($count_president >= $rb->rb_active_president)
                                        {
                                            return 1;
                                        }
                                    }
                                }
                            }
                         }
                    }
                }
            }

        }
        else
        {
            return 0;
        }

    }
    public static function royalty_bonus_paycheque()
    {
        $_rb_report = Tbl_royalty_bonus::where("rb_paycheque",0)->slot()->get();
        $total_rb_paycheque = 0;
        foreach ($_rb_report as $key => $value) {
            $_rb_report[$key] = $value;
            $total_rb_paycheque += $_rb_report[$key]->rb_bonus;
        }

        /* INSERT BATCH */
        $insert_batch["pay_cheque_batch_amount"] = $total_rb_paycheque;
        $insert_batch["pay_cheque_batch_date"] = Carbon::now();
        $insert_batch["pay_cheque_batch_reference"] = "Royalty Bonus";
        $pay_cheque_batch_id = Tbl_pay_cheque_batch::insertGetId($insert_batch);

        $_rb_report = Tbl_royalty_bonus::selectRaw('*, sum(rb_bonus) as earned')->where("rb_paycheque", 0)->slot()->groupBy('tbl_slot.slot_id')->get();
        foreach ($_rb_report as $rb_report) 
        {
            /* INSERT PAYCHECK */
            $insert_pay_cheque["pay_cheque_amount"] = $rb_report->earned;
            $insert_pay_cheque["pay_cheque_slot"] = $rb_report->rb_slot_id;
            $insert_pay_cheque["pay_cheque_batch_id"] = $pay_cheque_batch_id;
            $pay_cheque_id = Tbl_pay_cheque::insertGetId($insert_pay_cheque);

            /* UPDATE Royalty REPORT */
            $update_rb_report["rb_paycheque"] = $pay_cheque_id;
            Tbl_royalty_bonus::where("rb_paycheque", 0)->where("rb_slot_id", $rb_report->rb_slot_id)->update($update_rb_report);
        
            /* UPDATE SLOT OWNER'S PAYCHEQUE */
            Compute::update_account_pay_cheque($rb_report->rb_slot_id);
        }

        Compute::update_pay_cheque_schedule("royalty_bonus");
    }
    public static function gsb_paycheque()
    {
        //selectRaw('*, sum(total_spv) as earned')->
        $_gsb_report = Tbl_total_gsb::where("gsb_paycheque", 0)->groupsalesbonus()->slot()->get();
        $total_pay_cheque = 0;
        $gsb_report = null;
        foreach($_gsb_report as $key => $value)
        {
            $gsb_report[$key]= $value;
            $total_pay_cheque += ($gsb_report[$key]->gsb_percentage/100) * $gsb_report[$key]->total_spv;
        }
        /* INSERT BATCH */
        $insert_batch["pay_cheque_batch_amount"] = $total_pay_cheque;
        $insert_batch["pay_cheque_batch_date"] = Carbon::now();
        $insert_batch["pay_cheque_batch_reference"] = "Group Sales Bonus";
        $pay_cheque_batch_id = Tbl_pay_cheque_batch::insertGetId($insert_batch);

        $_gsb_report = null;
        $_gsb_report = Tbl_total_gsb::where("gsb_paycheque",0)->get();
        // dd($_gsb_report);
        $gsb_report = null;
        foreach($_gsb_report as $key => $value)
        {
            $gsb_report[$key] = $value;

            $up_up["total_earned"] = ($gsb_report[$key]->gsb_percentage/100) * $gsb_report[$key]->total_spv;

            Tbl_total_gsb::where("total_gsb_id",$gsb_report[$key]->total_gsb_id)->update($up_up);

        }
        $_gsbreport = Tbl_gsb_report::where("gsb_distributed",0)->get();
        // dd($_gsb_report);
        $gsbreport = null;
        foreach($_gsbreport as $key => $value)
        {
            $gsbreport[$key] = $value;

            $upup["gsb_earned"] = ($gsbreport[$key]->percentage/100) * $gsbreport[$key]->gsb_spv;

            Tbl_gsb_report::where("gsb_id",$gsbreport[$key]->gsb_id)->update($upup);

        }

        $_gsb_report = Tbl_total_gsb::selectRaw('*, sum(total_earned) as earned')->where("gsb_paycheque", 0)->slot()->groupBy('tbl_slot.slot_id')->get();
        // dd($_gsb_report);
         foreach($_gsb_report as $gsb_report)
        {
               /* INSERT PAYCHECK */
            $insert_pay_cheque["pay_cheque_amount"] =  $gsb_report->earned;
            $insert_pay_cheque["pay_cheque_slot"] = $gsb_report->gsb_slot_id;
            $insert_pay_cheque["pay_cheque_batch_id"] = $pay_cheque_batch_id;
            $insert_pay_cheque["pay_cheque_processed_date"] = Carbon::now();
            $pay_cheque_id = Tbl_pay_cheque::insertGetId($insert_pay_cheque);

            /* UPDATE GSB REPORT */
            $update_direct_report["gsb_paycheque"] = $pay_cheque_id;
            $update_direct_report["total_gsb_distributed"] = $pay_cheque_id;
            Tbl_total_gsb::where("gsb_paycheque", 0)->where("gsb_slot_id", $gsb_report->gsb_slot_id)->update($update_direct_report);
        
            $up_rep["gsb_distributed"] = $pay_cheque_id;
            Tbl_gsb_report::where("gsb_distributed",0)->where("gsb_sponsor",$gsb_report->gsb_slot_id)->update($up_rep);

            /* UPDATE SLOT OWNER'S PAYCHEQUE */
            Compute::update_account_pay_cheque($gsb_report->gsb_slot_id);
        }

        Compute::update_pay_cheque_schedule("gsb");
    }
    public static function binary_paycheque()
    {
        $_binary_report = Tbl_binary_report::selectRaw('*, sum(binary_earned) as earned')->where("binary_payout_paycheck", 0)->slot()->groupBy('tbl_slot.slot_id')->get();
        $total_pay_cheque = 0;
        foreach($_binary_report as $binary_report)
        {
            $total_pay_cheque += $binary_report->earned;
        }

        /* INSERT BATCH */
        $insert_batch["pay_cheque_batch_amount"] = $total_pay_cheque;
        $insert_batch["pay_cheque_batch_date"] = Carbon::now();
        $insert_batch["pay_cheque_batch_reference"] = "binary";
        $pay_cheque_batch_id = Tbl_pay_cheque_batch::insertGetId($insert_batch);

        foreach($_binary_report as $binary_report)
        {
            /* INSERT PAYCHECK */
            $insert_pay_cheque["pay_cheque_amount"] = $binary_report->earned;
            $insert_pay_cheque["pay_cheque_slot"] = $binary_report->slot_id;
            $insert_pay_cheque["pay_cheque_batch_id"] = $pay_cheque_batch_id;
            $pay_cheque_id = Tbl_pay_cheque::insertGetId($insert_pay_cheque);

            /* UPDATE BINARY REPORT */
            $update_binary_report["binary_payout_paycheck"] = $pay_cheque_id;
            Tbl_binary_report::where("binary_payout_paycheck", 0)->where("binary_slot_id", $binary_report->slot_id)->update($update_binary_report);
        
            /* UPDATE SLOT OWNER'S PAYCHEQUE */
            Compute::update_account_pay_cheque($binary_report->slot_owner);
        }

        Compute::update_pay_cheque_schedule("binary");
    }
    public static function direct_paycheque()
    {
        $_direct_report = Tbl_direct_report::selectRaw('*, sum(direct_amount) as earned')->where("direct_pay_cheque", 0)->slot()->groupBy('tbl_slot.slot_id')->get();
        $total_pay_cheque = 0;
        foreach($_direct_report as $direct_report)
        {
            $total_pay_cheque += $direct_report->earned;
        }

        /* INSERT BATCH */
        $insert_batch["pay_cheque_batch_amount"] = $total_pay_cheque;
        $insert_batch["pay_cheque_batch_date"] = Carbon::now();
        $insert_batch["pay_cheque_batch_reference"] = "direct";
        $pay_cheque_batch_id = Tbl_pay_cheque_batch::insertGetId($insert_batch);

        foreach($_direct_report as $direct_report)
        {
            /* INSERT PAYCHECK */
            $insert_pay_cheque["pay_cheque_amount"] = $direct_report->earned;
            $insert_pay_cheque["pay_cheque_slot"] = $direct_report->direct_sponsor;
            $insert_pay_cheque["pay_cheque_batch_id"] = $pay_cheque_batch_id;
            $pay_cheque_id = Tbl_pay_cheque::insertGetId($insert_pay_cheque);

            /* UPDATE DIRECT REPORT */
            $update_direct_report["direct_pay_cheque"] = $pay_cheque_id;
            Tbl_direct_report::where("direct_pay_cheque", 0)->where("direct_sponsor", $direct_report->direct_sponsor)->update($update_direct_report);
        
            /* UPDATE SLOT OWNER'S PAYCHEQUE */
            Compute::update_account_pay_cheque($direct_report->direct_sponsor);
        }

        Compute::update_pay_cheque_schedule("direct");
    }
    public static function indirect_paycheque()
    {
        $_indirect_report = Tbl_indirect_report::selectRaw('*, sum(indirect_amount) as earned')->where("indirect_pay_cheque", 0)->slot()->groupBy('tbl_slot.slot_id')->get();
        $total_pay_cheque = 0;

        foreach($_indirect_report as $indirect_report)
        {
            $total_pay_cheque += $indirect_report->earned;
        }

        /* INSERT BATCH */
        $insert_batch["pay_cheque_batch_amount"] = $total_pay_cheque;
        $insert_batch["pay_cheque_batch_date"] = Carbon::now();
        $insert_batch["pay_cheque_batch_reference"] = "indirect";
        $pay_cheque_batch_id = Tbl_pay_cheque_batch::insertGetId($insert_batch);

        foreach($_indirect_report as $indirect_report)
        {
            /* INSERT PAYCHECK */
            $insert_pay_cheque["pay_cheque_amount"] = $indirect_report->earned;
            $insert_pay_cheque["pay_cheque_slot"] = $indirect_report->indirect_slot;
            $insert_pay_cheque["pay_cheque_batch_id"] = $pay_cheque_batch_id;
            $pay_cheque_id = Tbl_pay_cheque::insertGetId($insert_pay_cheque);

            /* UPDATE DIRECT REPORT */
            $update_indirect_report["indirect_pay_cheque"] = $pay_cheque_id;
            Tbl_indirect_report::where("indirect_pay_cheque", 0)->where("indirect_slot", $indirect_report->indirect_slot)->update($update_indirect_report);
        
            /* UPDATE SLOT OWNER'S PAYCHEQUE */
            Compute::update_account_pay_cheque($indirect_report->indirect_slot);
        }

        Compute::update_pay_cheque_schedule("indirect");
    }
    public static function pipeline_paycheque()
    {
        $_pipeline = Tbl_pipeline_report::selectRaw('*, sum(pipeline_earned_amount) as earned')->where("pipeline_pay_cheque", 0)->slot()->groupBy('tbl_slot.slot_id')->get();
        $total_pay_cheque = 0;

        foreach($_pipeline as $pipeline)
        {
            $total_pay_cheque += $pipeline->earned;
        }

        /* INSERT BATCH */
        $insert_batch["pay_cheque_batch_amount"] = $total_pay_cheque;
        $insert_batch["pay_cheque_batch_date"] = Carbon::now();
        $insert_batch["pay_cheque_batch_reference"] = "pipeline";
        $pay_cheque_batch_id = Tbl_pay_cheque_batch::insertGetId($insert_batch);

        foreach($_pipeline as $pipeline)
        {
            /* INSERT PAYCHECK */
            $insert_pay_cheque["pay_cheque_amount"] = $pipeline->earned;
            $insert_pay_cheque["pay_cheque_slot"] = $pipeline->pipeline_slot_id;
            $insert_pay_cheque["pay_cheque_batch_id"] = $pay_cheque_batch_id;
            $pay_cheque_id = Tbl_pay_cheque::insertGetId($insert_pay_cheque);

            /* UPDATE DIRECT REPORT */
            $update_pipeline_report["pipeline_pay_cheque"] = $pay_cheque_id;
            Tbl_pipeline_report::where("pipeline_pay_cheque", 0)->where("pipeline_slot_id", $pipeline->pipeline_slot_id)->update($update_pipeline_report);
        
            /* UPDATE SLOT OWNER'S PAYCHEQUE */
            Compute::update_account_pay_cheque($pipeline->pipeline_slot_id);
        }

        Compute::update_pay_cheque_schedule("pipeline");
    }

    public static function binary_daily($new_slot_id, $method = "SLOT CREATION", $points = null)
    {
        $new_slot_info = Tbl_slot::id($new_slot_id)->account()->membership()->first();

        /* GET THE TREE */
        $_tree = Tbl_tree_placement::child($new_slot_id)->level()->distinct_level()->get();

        /* UPDATE BINARY POINTS */
        foreach($_tree as $tree)
        {
            /* GET SLOT INFO FROM DATABASE */
            $slot_recipient = Tbl_slot::id($tree->placement_tree_parent_id)->membership()->first();

            /* RETRIEVE LEFT & RIGHT POINTS */
            $initial["left"] = $binary["left"] = $slot_recipient->slot_binary_left;
            $initial["right"] = $binary["right"] = $slot_recipient->slot_binary_right; 
            $flushpoints = $slot_recipient->pair_flush_out_wallet;

            /* ADD NECESARRY POINTS */
            if($points == null)
            {
                $earned_points = $new_slot_info->membership_binary_points;
            }
            else
            {
                $earned_points = $points;
            }
            

            /* CHECK POINTS EARNED */
            if($earned_points != 0)
            {
                $binary[$tree->placement_tree_position] = $binary[$tree->placement_tree_position] + $earned_points; 
                $check_wallet = Tbl_wallet_logs::id($new_slot_info->slot_id)->wallet()->sum('wallet_amount');

                /* UPDATE POINTS */
                $update_recipient["slot_binary_left"] = $binary["left"];
                $update_recipient["slot_binary_right"] = $binary["right"];
                $update_recipient["slot_binary_affected"] = 1;

                /* INSERT BINARY LOG */
                $insert_breakdown["binary_start_left"] = $initial["left"];
                $insert_breakdown["binary_start_right"] = $initial["right"];
                $insert_breakdown["binary_earned_left"] = $binary["left"] - $initial["left"];
                $insert_breakdown["binary_earned_right"] = $binary["right"] - $initial["right"];
                $insert_breakdown["binary_end_left"] = $binary["left"];
                $insert_breakdown["binary_end_right"] = $binary["right"];
                $insert_breakdown["binary_earned_reason"] = $method;
                $insert_breakdown["binary_slot_id_cause"] = $new_slot_info->slot_id;
                $insert_breakdown["binary_report_id"] = 0;
                $insert_breakdown["binary_slot_id"] = $slot_recipient->slot_id;
                $insert_breakdown["breakdown_time"] = Carbon::now();
                Tbl_binary_breakdown::insert($insert_breakdown);

                if($new_slot_info->slot_type != "FS" && $check_wallet >= 0)
                {
                    Tbl_slot::id($tree->placement_tree_parent_id)->update($update_recipient);
                }

                $update_recipient = null;
            }
        
        }       
    }
    public static function repurchase($buyer_slot_id, $binary_pts, $unilevel_pts,$upgrade_pts ,$product_pin, $method = "REPURCHASE")
    {
        
        Compute::unilevel_repurchase($buyer_slot_id, $unilevel_pts, $method, $upgrade_pts, $product_pin );
        
        // Compute::class_sequence($buyer_slot_id, $unilevel_pts, $method, $upgrade_pts, $product_pin );
        
        //Compute::binary_repurchase($buyer_slot_id, $binary_pts, $method);
        // Compute::binary_daily($buyer_slot_id, "PRODUCT REPURCHASE", $binary_pts);
    }
    // public static function class_sequence($buyer_slot_id, $unilevel_pts, $method, $upgrade_pts, $product_pin)
    // {
    //     $buyer_slot_info = Tbl_slot::id($buyer_slot_id)->account()->membership()->classsequence()->first();
        
    
    //       /* ----- COMPUTATION OF PERSONAL PV */
    //     $update_recipient["slot_personal_points"] = $buyer_slot_info->slot_personal_points + $unilevel_pts;

    //     /* INSERT LOG */
    //     $log = "Your slot #" . $buyer_slot_info->slot_id . " earned <b>" . number_format($unilevel_pts, 2) . " personal pv</b>.";
    //     Log::slot($buyer_slot_info->slot_id, $log, 0,$method,$buyer_slot_id);

    //     /* UPDATE SLOT CHANGES TO DATABASE */
    //     Tbl_slot::id($buyer_slot_info->slot_id)->update($update_recipient);
    //     $update_recipient = null;
        
       
    // }
    public static function unilevel_repurchase($buyer_slot_id, $unilevel_pts, $method, $upgrade_pts, $product_pin)
    {
        $buyer_slot_info = Tbl_slot::id($buyer_slot_id)->account()->groupsalesbonus()->membership()->first();
            
           /* ----- COMPUTATION OF PERSONAL PV */
        $update_recipient["slot_personal_points"] = $buyer_slot_info->slot_personal_points + $unilevel_pts;
        $update_recipient["total_personal_spv"] = $buyer_slot_info->total_personal_spv + $unilevel_pts;
        $update_recipient["active_member"] = 1;
        
        /* INSERT LOG */
        $log = "Your slot #" . $buyer_slot_info->slot_id . " earned <b>" . number_format($unilevel_pts, 2) . " personal pv</b>.";
        Log::slot($buyer_slot_info->slot_id, $log, 0,$method,$buyer_slot_id);


        if($buyer_slot_info->sequence_level_id > 2)
        {
            $personal_percentage = $buyer_slot_info->personal_bonus;

            
             $ctr = Tbl_total_gsb::where("total_gsb_level", 0)
                                    ->where("gsb_slot_id",$buyer_slot_id)
                                    ->where("total_gsb_distributed",0)
                                    ->count();
                $t_id = 0;
                if($ctr != 0)
                {
                    $p_gsb = Tbl_total_gsb::where("total_gsb_level", 0)
                                    ->where("gsb_slot_id",$buyer_slot_id)
                                    ->where("total_gsb_distributed",0)
                                    ->first();
                                    
                    $up_ptgsb["total_spv"] = $p_gsb->total_spv + $unilevel_pts;
                    $up_ptgsb["gsb_percentage"] = $p_gsb->gsb_percentage;
                    // $up_ptgsb["total_earned"] = $p_gsb->total_earned + (($personal_percentage/100) * $unilevel_pts);
                    $up_ptgsb["updated_at"] = Carbon::now();
                    
                    Tbl_total_gsb::where("total_gsb_level",0)
                                    ->where("gsb_slot_id",$buyer_slot_id)
                                    ->where("total_gsb_distributed",0)
                                    ->update($up_ptgsb);
                                    
                    $t_id = $p_gsb->total_gsb_id;
                }
                else
                {
                    $p_ins["total_gsb_level"] =  0;
                    $p_ins["total_spv"] =  $unilevel_pts;
                    $p_ins["gsb_percentage"] = $buyer_slot_info->personal_bonus;
                    // $p_ins["total_earned"] =  ($personal_percentage/100) * $unilevel_pts;
                    $p_ins["created_at"] = Carbon::now();
                    $p_ins["sequence_level_id"] = $buyer_slot_info->sequence_level_id;
                    $p_ins["gsb_slot_id"] =  $buyer_slot_id;
                    $p_ins["personal_true"] = 1;
                    
                    $t_id = Tbl_total_gsb::insertGetId($p_ins);
                }



            $insert_personal_gsb["personal_true"] = 1;
            $insert_personal_gsb["level"] = 0;
            $insert_personal_gsb["percentage"] = $personal_percentage;
            $insert_personal_gsb["no_of_slots"] = 1;
            $insert_personal_gsb["gsb_sponsor"] = $buyer_slot_id;
            $insert_personal_gsb["earned_from"] = $buyer_slot_id;
            $insert_personal_gsb["gsb_spv"] = $unilevel_pts;
            $earned["gsb_earned"] = ($personal_percentage/100) * $unilevel_pts;
            $insert_personal_gsb["gsb_date"] =Carbon::now();
            $insert_personal_gsb["gsb_level"] =0;
            $insert_personal_gsb["sequence_level_id"] = $buyer_slot_info->sequence_level_id;
            $insert_personal_gsb["t_gsb_id"] = $t_id;

             $log = "Your slot #" . $buyer_slot_info->slot_id . " earned <b>" . number_format($earned["gsb_earned"], 2) . " personal Boss Coin</b>.";
             Log::slot($buyer_slot_info->slot_id, $log, 0,$method,$buyer_slot_id);


            
            DB::table("tbl_gsb_report")->insert($insert_personal_gsb);
        }
        /* UPDATE SLOT CHANGES TO DATABASE */
        Tbl_slot::id($buyer_slot_info->slot_id)->update($update_recipient);



        $update_recipient = null;
        
        //luke
            $insertunilevelog['unilevel_type'] = 0;
            $insertunilevelog['unilevel_points'] = $unilevel_pts;
            $insertunilevelog['unilevel_level'] = 0;
            $insertunilevelog['unilevel_date'] = Carbon::now();
            $insertunilevelog['unilevel_recipeint'] =   $buyer_slot_info->slot_id;
            $insertunilevelog['unilevel_sponsor'] = $buyer_slot_info->slot_id;
            $insertunilevelog['unilevel_product_pin'] = $product_pin;
            Tbl_unilevel_point_log::insert($insertunilevelog);
        //luke
        
        /* ----- COMPUTATION OF GROUP PV ----- */
        $_unilevel_setting = Tbl_unilevel_setting::get();
        $_tree = Tbl_tree_sponsor::child($buyer_slot_id)->level()->distinct_level()->get();
            
        /* RECORD ALL INTO A SINGLE VARIABLE */
        $unilevel_level = null;
        foreach($_unilevel_setting as $key => $level)
        {
            $unilevel_level[$level->membership_id][$level->level] =  $level->value;
        }
            /* CHECK IF LEVEL EXISTS */
            if($unilevel_level)
            {
                foreach($_tree as $key => $tree)
                {
                    /* GET SLOT INFO FROM DATABASE */
                    $slot_recipient = Tbl_slot::id($tree->sponsor_tree_parent_id)->groupsalesbonus()->membership()->first();
                    if($slot_recipient->sequence_level_id == 1)
                    {
                        $update_recipient["slot_group_points"] = $slot_recipient->slot_group_points;
                        $update_recipient["total_group_spv"] = $slot_recipient->total_group_spv;
                        $update_recipient["slot_upgrade_points"] = $slot_recipient->slot_upgrade_points;
                        
                        /* COMPUTE FOR BONUS */
                        if(isset($unilevel_level[$slot_recipient->membership_id][$tree->sponsor_tree_level]))
                        {
                            $unilevel_bonus = ($unilevel_level[$slot_recipient->membership_id][$tree->sponsor_tree_level]/100) * $unilevel_pts;  
                            $upgrade_bonus = ($unilevel_level[$slot_recipient->membership_id][$tree->sponsor_tree_level]/100) * $upgrade_pts;  
                            
                            //luke
                            $insertunilevelog['unilevel_points'] = ($unilevel_level[$slot_recipient->membership_id][$tree->sponsor_tree_level]/100) * $unilevel_pts;
                            $insertunilevelog['unilevel_type'] = 1;
                            $insertunilevelog['unilevel_level'] = $tree->sponsor_tree_level;
                            $insertunilevelog['unilevel_date'] = Carbon::now();
                            $insertunilevelog['unilevel_recipeint'] = $tree->sponsor_tree_child_id;
                            $insertunilevelog['unilevel_sponsor'] = $tree->sponsor_tree_parent_id;
                            $insertunilevelog['unilevel_product_pin'] = $product_pin;
                            //luke
                        }
                        else
                        {
                            $unilevel_bonus = 0;
                            $upgrade_bonus = 0;
                            //luke
                            // $insertunilevelog['unilevel_type'] = 0;
                            // $insertunilevelog['unilevel_points'] = $update_recipient["slot_personal_points"];
                            // $insertunilevelog['unilevel_level'] = 0;
                            // $insertunilevelog['unilevel_date'] = Carbon::now();
                            // $insertunilevelog['unilevel_recipeint'] =   $buyer_slot_info->slot_id;
                            // $insertunilevelog['unilevel_sponsor'] = $buyer_slot_info->slot_id;
                            // $insertunilevelog['unilevel_sponsor'] = $product_pin;
                            //luke
                        }
                        
                        /* CHECK IF BONUS IS ZERO */
                        if($unilevel_bonus != 0 || $upgrade_bonus != 0)
                        {
                            /* UPDATE WALLET */
                            $update_recipient["slot_group_points"] = $update_recipient["slot_group_points"] + $unilevel_bonus;
                            $update_recipient["total_group_spv"] = $update_recipient["total_group_spv"] + $unilevel_bonus;
                            $update_recipient["slot_upgrade_points"] = $update_recipient["slot_upgrade_points"] + $upgrade_bonus;
                            
                            // $update_recipient["total_group_spv"] = $slot_recipient->total_group_spv + $update_recipient["slot_group_points"];
                            // $update_recipient["total_personal_spv"] = $slot_recipient->total_personal_spv + $update_recipient["slot_upgrade_points"];
                            
                            // if($update_recipient["slot_group_points"] > $slot_recipient->slot_highest_pv)
                            // {
                            //     $update_recipient["slot_highest_pv"] = $update_recipient["slot_group_points"];
                            // }
    
                            /* INSERT LOG */
                            $log = "Your slot #" . $slot_recipient->slot_id . " earned <b> " . number_format($unilevel_bonus, 2) . " group pv and ". $upgrade_bonus ." promotion points</b>. You earned it when slot #" . $buyer_slot_id . " uses a code worth " . number_format($unilevel_pts, 2) . " PV. That slot is located on the Level " . $tree->sponsor_tree_level . " of your sponsor genealogy. Your current membership is " . $slot_recipient->membership_name . " MEMBERSHIP.";
                            // Log::account($slot_recipient->slot_owner, $log);
                            Log::slot($slot_recipient->slot_id, $log, 0,$method,$buyer_slot_id);
                            /* UPDATE SLOT CHANGES TO DATABASE */
                            
                            // dd($update_recipient);
                            Tbl_slot::id($slot_recipient->slot_id)->update($update_recipient);
                            
                            //luke unilevel_points_log
                            Tbl_unilevel_point_log::insert($insertunilevelog);
                            //endluke
                            
                            /* CHECK IF QUALIFIED FOR PROMOTION */
                            // Compute::check_promotion_qualification($slot_recipient->slot_id);
                        }
                    }
                }
            }            

         /*start arcy*/
        /* BOSS COIN --- DIRECT ================================================================   */
        
            
                /* ----- COMPUTATION OF GROUP PV ----- */
                $_seq_tree = Tbl_tree_sponsor::child($buyer_slot_id)->level()->distinct_level()->get();
        
                 $_sequence_setting = Tbl_direct_unilevel_setting::get();
                    $sequence_level = null;
                    foreach($_sequence_setting as $key => $level_seq)
                    {
                        $sequence_level[$level_seq->sequence_id][$level_seq->seq_level] =  $level_seq->seq_value;
                    }
                    
                 $_indirect_setting = Tbl_indirect_unilevel_setting::get();
                    $indirect_level = null;
                    foreach($_indirect_setting as $indirect_key => $level_indirect)
                    {
                        $indirect_level[$level_indirect->sequence_id][$level_indirect->seq_level] =  $level_indirect->seq_value;
                    }
                    
                    
                /* RECORD ALL INTO A SINGLE VARIABLE */
                   if($_sequence_setting )
                   {
                        // dd($_seq_tree);
                        foreach($_seq_tree as $key => $seq_tree)
                        {
                            /* GET SLOT INFO FROM DATABASE=============================================== DIRECT*/
                            $seq_slot_recipient = Tbl_slot::id($seq_tree->sponsor_tree_parent_id)->directunilevel()->membership()->first();
                            // dd($seq_slot_recipient);
                           
                            if($sequence_level)
                            {
                                $update_recipient["slot_group_points"] = $seq_slot_recipient->slot_group_points;
                                $update_recipient["slot_upgrade_points"] = $seq_slot_recipient->slot_upgrade_points;
                                
                                $personal_point = $seq_slot_recipient->slot_personal_points;
                                $group_point = $seq_slot_recipient->slot_group_points;
                                
                                // dd($sequence_level)
                                $total_points = $personal_point + $group_point;            
                                $sequence_level_cq = $seq_slot_recipient->required_spv;
                                // dd($total_points." ".$sequence_level_cq);
                                if($total_points > $sequence_level_cq)
                                {
                                    /* COMPUTE FOR BONUS */
                                  if(isset($sequence_level[$seq_slot_recipient->membership_id][$seq_tree->sponsor_tree_level]))
                                    {
                                        $unilevel_bonus = ($sequence_level[$seq_slot_recipient->membership_id][$seq_tree->sponsor_tree_level]/100) * $unilevel_pts;  
                                        $upgrade_bonus = ($sequence_level[$seq_slot_recipient->membership_id][$seq_tree->sponsor_tree_level]/100) * $upgrade_pts;
                                        
                                        //arcy
                                        $insert_dsb['dsb_amount'] = ($sequence_level[$seq_slot_recipient->membership_id][$seq_tree->sponsor_tree_level]/100) * $unilevel_pts;
                                        $insert_dsb['dsb_date'] = Carbon::now();
                                        $insert_dsb['dsb_child'] = $seq_tree->sponsor_tree_child_id;
                                        $insert_dsb['dsb_sponsor'] = $seq_tree->sponsor_tree_parent_id;
                                        //arcy
                                    }
                                    else
                                    {
                                        $unilevel_bonus = 0;
                                        $upgrade_bonus = 0;
                                       
                                    }
                                    /* CHECK IF BONUS IS ZERO */
                                    if($unilevel_bonus != 0 || $upgrade_bonus != 0)
                                    {
                                        /* UPDATE WALLET */
                                        // $update_recipient["slot_group_points"] = $update_recipient["slot_group_points"] + $unilevel_bonus;
                                        // $update_recipient["slot_upgrade_points"] = $update_recipient["slot_upgrade_points"] + $upgrade_bonus;
                                        
                                        // if($update_recipient["slot_group_points"] > $slot_recipient->slot_highest_pv)
                                        // {
                                        //     $update_recipient["slot_highest_pv"] = $update_recipient["slot_group_points"];
                                        // }
                
                                        /* INSERT LOG */
                                        $log = "Your slot #" . $seq_slot_recipient->slot_id . " earned <b> " . number_format($unilevel_bonus, 2) . " group boss coin and ". $upgrade_bonus ." DSB </b>. You earned it when slot #" . $buyer_slot_id . " uses a code worth " . number_format($unilevel_pts, 2) . " PV. That slot is located on the Level " . $seq_tree->sponsor_tree_level . " of your sponsor genealogy. Your current membership is " . $seq_slot_recipient->membership_name . " MEMBERSHIP.";
                                        // Log::account($slot_recipient->slot_owner, $log);
                                        Log::slot($seq_slot_recipient->slot_id, $log, 0,$method,$buyer_slot_id);
                                        /* UPDATE SLOT CHANGES TO DATABASE */
                                        // Tbl_slot::id($seq_slot_recipient->slot_id)->update($update_recipient);
                                        
                                        //luke unilevel_points_log
                                        DB::table("tbl_direct_sb")->insert($insert_dsb);
                                        //endluke
                                        
                                        
                                        /* CHECK IF QUALIFIED FOR PROMOTION */
                                        // Compute::check_promotion_qualification($slot_recipient->slot_id);
                                    }
                                  
                                }
                            }
                            
                            
                            /* GET SLOT INFO FROM DATABASE=============================================== INDIRECT*/
                            $indirect_slot_recipient = Tbl_slot::id($seq_tree->sponsor_tree_parent_id)->indirectunilevel()->membership()->first();
                            // dd($seq_slot_recipient);
                           if($_indirect_setting)
                           {
                            if($indirect_level)
                            {
                                $in_update_recipient["slot_group_points"] = $indirect_slot_recipient->slot_group_points;
                                $in_update_recipient["slot_upgrade_points"] = $indirect_slot_recipient->slot_upgrade_points;
                                
                                $in_personal_point = $indirect_slot_recipient->slot_personal_points;
                                $in_group_point = $indirect_slot_recipient->slot_group_points;
                                
                                // dd($sequence_level)
                                $in_total_points = $in_personal_point + $in_group_point;            
                                $in_sequence_level_cq = $indirect_slot_recipient->required_spv;
                                // dd($in_total_points." ".$in_sequence_level_cq);
                                
                                if($in_total_points > $in_sequence_level_cq)
                                {
                                    /* COMPUTE FOR BONUS */
                                  if(isset($indirect_level[$indirect_slot_recipient->membership_id][$seq_tree->sponsor_tree_level]))
                                    {
                                        $inunilevel_bonus = ($indirect_level[$indirect_slot_recipient->membership_id][$seq_tree->sponsor_tree_level]/100) * $unilevel_pts;  
                                        $inupgrade_bonus = ($indirect_level[$indirect_slot_recipient->membership_id][$seq_tree->sponsor_tree_level]/100) * $upgrade_pts;
                                        
                                        //luke
                                        $insert_isb['isb_amount'] = ($indirect_level[$indirect_slot_recipient->membership_id][$seq_tree->sponsor_tree_level]/100) * $unilevel_pts;
                                        $insert_isb['isb_date'] = Carbon::now();
                                        $insert_isb['isb_child'] = $seq_tree->sponsor_tree_child_id; 
                                        $insert_isb['isb_sponsor'] = $seq_tree->sponsor_tree_parent_id;
                                        $insert_isb['isb_level'] = $seq_tree->sponsor_tree_level;
                                        //luke
                                    }
                                    else
                                    {
                                        $inunilevel_bonus = 0;
                                        $inupgrade_bonus = 0;
                                       
                                    }
                                    // dd($inunilevel_bonus);
                                    /* CHECK IF BONUS IS ZERO */
                                    if($inunilevel_bonus != 0 || $inupgrade_bonus != 0)
                                    {
                
                                        /* INSERT LOG */
                                        $log = "Your slot #" . $indirect_slot_recipient->slot_id . " earned <b> " . number_format($inunilevel_bonus, 2) . " group boss coin and ". $inupgrade_bonus ." promotion points</b>. You earned it when slot #" . $buyer_slot_id . " uses a code worth " . number_format($unilevel_pts, 2) . " PV. That slot is located on the Level " . $seq_tree->sponsor_tree_level . " of your sponsor genealogy. Your current membership is " . $indirect_slot_recipient->membership_name . " MEMBERSHIP.";
                                        // Log::account($slot_recipient->slot_owner, $log);
                                        Log::slot($indirect_slot_recipient->slot_id, $log, 0,$method,$buyer_slot_id);
                                        /* UPDATE SLOT CHANGES TO DATABASE */
                                        // Tbl_slot::id($seq_slot_recipient->slot_id)->update($update_recipient);
                                    
                                        DB::table("tbl_indirect_sb")->insert($insert_isb);
                                      
                                        
                                    }
                                  
                                }
                            }
                           }
                           
                                /* GET SLOT INFO FROM DATABASE=============================================== CONSULTANT SPV*/ /* INFINTE SPV TO THIER DOWNLINE */
                          $con_gsb_slot_recipient = Tbl_slot::id($seq_tree->sponsor_tree_parent_id)->consultantspv()->membership()->first();
                        


                          $_con_gsb_setting = Tbl_sequence_setting::where("sequence_id",$con_gsb_slot_recipient->consultant_unilevel)->get();
                            $con_gsb_level = null;
                            foreach($_con_gsb_setting as $congsb_key => $conlevel_gsb)
                            {
                                $con_gsb_level[$conlevel_gsb->sequence_id][$conlevel_gsb->seq_level] =  $conlevel_gsb->seq_value;
                            }
                          if($_con_gsb_setting)
                          {
                            if($con_gsb_level)
                            {
                                if($con_gsb_slot_recipient->consultant_unilevel == 2)
                                {
                                    $con_update_recipient["slot_group_points"] = $con_gsb_slot_recipient->slot_group_points;
                                    $con_update_recipient["total_group_spv"] = $con_gsb_slot_recipient->total_group_spv;
                                    $con_update_recipient["slot_upgrade_points"] = $con_gsb_slot_recipient->slot_upgrade_points;
                                    
                                    /* COMPUTE FOR BONUS */
                                    if(isset($con_gsb_level[$con_gsb_slot_recipient->consultant_unilevel][$seq_tree->sponsor_tree_level]))
                                    {
                                        $congsbunilevel_bonus = ($con_gsb_level[$con_gsb_slot_recipient->consultant_unilevel][$seq_tree->sponsor_tree_level]/100) * $unilevel_pts;  
                                        $congsbupgrade_bonus = ($con_gsb_level[$con_gsb_slot_recipient->consultant_unilevel][$seq_tree->sponsor_tree_level]/100) * $upgrade_pts;  
                                        
                                        // dd($congsbunilevel_bonus ." ".$congsbupgrade_bonus);
                                        //luke
                                        $insertunilevelog['unilevel_points'] = ($con_gsb_level[$con_gsb_slot_recipient->consultant_unilevel][$seq_tree->sponsor_tree_level]/100) * $unilevel_pts;
                                        $insertunilevelog['unilevel_type'] = 1;
                                        $insertunilevelog['unilevel_level'] = $seq_tree->sponsor_tree_level;
                                        $insertunilevelog['unilevel_date'] = Carbon::now();
                                        $insertunilevelog['unilevel_recipeint'] = $seq_tree->sponsor_tree_child_id;
                                        $insertunilevelog['unilevel_sponsor'] = $seq_tree->sponsor_tree_parent_id;
                                        $insertunilevelog['unilevel_product_pin'] = $product_pin;
                                        //luke
                                    }
                                    else
                                    {
                                        $congsbunilevel_bonus = 0;
                                        $congsbupgrade_bonus = 0;
                                        //luke
                                        // $insertunilevelog['unilevel_type'] = 0;
                                        // $insertunilevelog['unilevel_points'] = $update_recipient["slot_personal_points"];
                                        // $insertunilevelog['unilevel_level'] = 0;
                                        // $insertunilevelog['unilevel_date'] = Carbon::now();
                                        // $insertunilevelog['unilevel_recipeint'] =   $buyer_slot_info->slot_id;
                                        // $insertunilevelog['unilevel_sponsor'] = $buyer_slot_info->slot_id;
                                        // $insertunilevelog['unilevel_sponsor'] = $product_pin;
                                        //luke
                                    }
                                    /* CHECK IF BONUS IS ZERO */
                                    if($congsbunilevel_bonus != 0 || $congsbupgrade_bonus != 0)
                                    {
                                        /* UPDATE WALLET */
                                        $con_update_recipient["slot_group_points"] = $con_update_recipient["slot_group_points"] + $congsbunilevel_bonus;
                                        $con_update_recipient["total_group_spv"] = $con_update_recipient["total_group_spv"] + $congsbunilevel_bonus;
                                        $con_update_recipient["slot_upgrade_points"] = $con_update_recipient["slot_upgrade_points"] + $congsbupgrade_bonus;
                                        
                                        // $update_recipient["total_group_spv"] = $slot_recipient->total_group_spv + $update_recipient["slot_group_points"];
                                        // $update_recipient["total_personal_spv"] = $slot_recipient->total_personal_spv + $update_recipient["slot_upgrade_points"];
                                        
                                        // if($update_recipient["slot_group_points"] > $slot_recipient->slot_highest_pv)
                                        // {
                                        //     $update_recipient["slot_highest_pv"] = $update_recipient["slot_group_points"];
                                        // }
                
                                        /* INSERT LOG */
                                        $log = "Your slot #" . $con_gsb_slot_recipient->slot_id . " earned <b> " . number_format($congsbunilevel_bonus, 2) . " group pv and ". $congsbupgrade_bonus ." promotion points</b>. You earned it when slot #" . $buyer_slot_id . " uses a code worth " . number_format($unilevel_pts, 2) . " PV. That slot is located on the Level " . $seq_tree->sponsor_tree_level . " of your sponsor genealogy. Your current membership is " . $con_gsb_slot_recipient->membership_name . " MEMBERSHIP. ";
                                        // Log::account($slot_recipient->slot_owner, $log);
                                        Log::slot($con_gsb_slot_recipient->slot_id, $log, 0,$method,$buyer_slot_id);
                                        /* UPDATE SLOT CHANGES TO DATABASE */
                                        
                                        // dd($update_recipient);
                                        Tbl_slot::id($con_gsb_slot_recipient->slot_id)->update($con_update_recipient);
                                        
                                        //luke unilevel_points_log
                                        Tbl_unilevel_point_log::insert($insertunilevelog);
                                        //endluke
                                        
                                        /* CHECK IF QUALIFIED FOR PROMOTION */
                                        // Compute::check_promotion_qualification($slot_recipient->slot_id);
                                    }
                                }
                               
                                
                            }
                          }
                           
                           
                            /* GET SLOT INFO FROM DATABASE=============================================== GSB*/
                          $gsb_slot_recipient = Tbl_slot::id($seq_tree->sponsor_tree_parent_id)->groupsalesbonus()->membership()->first();
                          
                          $_gsb_setting = Tbl_sequence_setting::where("sequence_id",$gsb_slot_recipient->sequence_level_id)->get();
                            $gsb_level = null;
                            foreach($_gsb_setting as $gsb_key => $level_gsb)
                            {
                                $gsb_level[$level_gsb->sequence_id][$level_gsb->seq_level] =  $level_gsb->seq_value;
                            }
                          
                            // dd($seq_slot_recipient);
                          if($_gsb_setting)
                          {
                            if($gsb_level)
                            {
                                if($gsb_slot_recipient->sequence_level_id >= 3)
                                {
                                    
                                    $gsb_update_recipient["slot_group_points"] = $gsb_slot_recipient->slot_group_points;
                                    $gsb_update_recipient["slot_upgrade_points"] = $gsb_slot_recipient->slot_upgrade_points;
                                    
                                    $gsb_personal_point = $gsb_slot_recipient->slot_personal_points;
                                    $gsb_group_point = $gsb_slot_recipient->slot_group_points;
                                    
                                    $gsb_total_points = $gsb_personal_point + $gsb_group_point;            
                                    $gsb_sequence_level_cq = $gsb_slot_recipient->required_spv;
                                    // dd($gsb_total_points." ".$gsb_sequence_level_cq);
                                    if($gsb_total_points > $gsb_sequence_level_cq)
                                    {
                                        /* COMPUTE FOR BONUS */
                                      if(isset($gsb_level[$gsb_slot_recipient->sequence_level_id][$seq_tree->sponsor_tree_level]))
                                        {
                                            $gsb_unilevel_bonus = ($gsb_level[$gsb_slot_recipient->sequence_level_id][$seq_tree->sponsor_tree_level]/100) * $unilevel_pts;  
                                            $gsb_upgrade_bonus = ($gsb_level[$gsb_slot_recipient->sequence_level_id][$seq_tree->sponsor_tree_level]/100) * $upgrade_pts;
                                            
                                              $ctr = Tbl_total_gsb::where("total_gsb_level", $seq_tree->sponsor_tree_level)
                                                                ->where("gsb_slot_id",$seq_tree->sponsor_tree_parent_id)
                                                                ->where("total_gsb_distributed",0)
                                                                ->count();
                                                $id = 0;
                                                if($ctr != 0)
                                                {
                                                    $b_gsb = Tbl_total_gsb::where("total_gsb_level",$seq_tree->sponsor_tree_level)
                                                                    ->where("gsb_slot_id",$seq_tree->sponsor_tree_parent_id)
                                                                    ->where("total_gsb_distributed",0)
                                                                    ->first();
                                                      
                                                    $ctr_slots = Tbl_tree_sponsor::where("sponsor_tree_parent_id",$seq_tree->sponsor_tree_parent_id)
                                                                                ->where("sponsor_tree_level",$seq_tree->sponsor_tree_level)
                                                                                ->count();
                                                    
                                                    $up_tgsb["total_spv"] = $b_gsb->total_spv + $unilevel_pts;
                                                    // $up_tgsb["total_earned"] = $b_gsb->total_earned + (($gsb_level[$gsb_slot_recipient->sequence_level_id][$seq_tree->sponsor_tree_level]/100) * $unilevel_pts);
                                                    $up_tgsb["gsb_percentage"] = $gsb_level[$gsb_slot_recipient->sequence_level_id][$seq_tree->sponsor_tree_level];
                                                    $up_tgsb["updated_at"] = Carbon::now();
                                                    $up_tgsb["total_no_of_slots"] = $ctr_slots;
                                                    
                                                    Tbl_total_gsb::where("total_gsb_level",$seq_tree->sponsor_tree_level)
                                                                    ->where("gsb_slot_id",$seq_tree->sponsor_tree_parent_id)
                                                                    ->where("total_gsb_distributed",0)
                                                                    ->update($up_tgsb);
                                                                    
                                                    $id = $b_gsb->total_gsb_id;
                                                }
                                                else
                                                {
                                                    $ins["total_gsb_level"] =  $seq_tree->sponsor_tree_level;
                                                    $ins["total_spv"] =  $unilevel_pts;
                                                    $ins['gsb_percentage'] = $gsb_level[$gsb_slot_recipient->sequence_level_id][$seq_tree->sponsor_tree_level];
                                                    // $ins["total_earned"] =  ($gsb_level[$gsb_slot_recipient->sequence_level_id][$seq_tree->sponsor_tree_level]/100) * $unilevel_pts;
                                                    $ins["created_at"] = Carbon::now();
                                                    $ins["sequence_level_id"] = $gsb_slot_recipient->sequence_level_id;
                                                    $ins["gsb_slot_id"] =  $seq_tree->sponsor_tree_parent_id;
                                                    $ins["total_no_of_slots"] = 1;
                                                    $id = Tbl_total_gsb::insertGetId($ins);
                                                }
                                            
                                            
                                            //arcy_gsb
                                            $insert_gsb['gsb_spv'] = $unilevel_pts;
                                            $insert_gsb['level'] = $seq_tree->sponsor_tree_level;
                                            $insert_gsb['percentage'] = $gsb_level[$gsb_slot_recipient->sequence_level_id][$seq_tree->sponsor_tree_level];
                                            // $insert_gsb['gsb_earned'] =($gsb_level[$gsb_slot_recipient->sequence_level_id][$seq_tree->sponsor_tree_level]/100) * $unilevel_pts;
                                            $insert_gsb['gsb_date'] = Carbon::now();
                                            $insert_gsb['gsb_level'] = 0;
                                            $insert_gsb['no_of_slots'] = 1;
                                            $insert_gsb['gsb_sponsor'] = $seq_tree->sponsor_tree_parent_id;
                                            $insert_gsb['earned_from'] = $seq_tree->sponsor_tree_child_id;
                                            $insert_gsb['sequence_level_id'] = $gsb_slot_recipient->sequence_level_id;
                                            $insert_gsb["t_gsb_id"] = $id;
                                            //arcy_gsb
                                            
                                          
                                        }
                                        else
                                        {
                                            $gsb_unilevel_bonus = 0;
                                            $gsb_upgrade_bonus = 0;
                                           
                                        }
                                        // dd($inunilevel_bonus);
                                        /* CHECK IF BONUS IS ZERO */
                                        if($gsb_unilevel_bonus != 0 || $gsb_upgrade_bonus != 0)
                                        {
                    
                                            /* INSERT LOG */
                                            $log = "Your slot #" . $gsb_slot_recipient->slot_id . " earned <b> " . number_format($gsb_unilevel_bonus, 2) . " group boss coin and ". $gsb_upgrade_bonus ." promotion points</b>. You earned it when slot #" . $buyer_slot_id . " uses a code worth " . number_format($unilevel_pts, 2) . " PV. That slot is located on the Level " . $seq_tree->sponsor_tree_level . " of your sponsor genealogy. Your current membership is " . $gsb_slot_recipient->membership_name . " MEMBERSHIP. And your position is " . $gsb_slot_recipient->sequence_name;
                                            // Log::account($slot_recipient->slot_owner, $log);
                                            Log::slot($gsb_slot_recipient->slot_id, $log, 0,$method,$buyer_slot_id);
                                            /* UPDATE SLOT CHANGES TO DATABASE */
                                            // Tbl_slot::id($seq_slot_recipient->slot_id)->update($update_recipient);
                                            
                                            DB::table("tbl_gsb_report")->insert($insert_gsb);
                                          
                                            
                                        }
                                      
                                    }
                                }
                                
                            }
                          }
                            
                                
                        }
                            
                            
                    }
                    
        
        
        
        
            /*end arcy*/
        Compute::check_ifrank_up();
        Compute::royalty_rank_up();
    }
    public static function check_if_active()
    {
        $_slots = Tbl_slot::where("active_member",1)->get();
        $slots = null;
        foreach($_slots as $key => $value)
        {
            $slots[$key] = $value;
            $_product = Tbl_voucher::where("slot_id",$slots[$key]->slot_id)->orderby("voucher_id","DESC")->first();
            // dd($_product);
            if($_product)
            {
                $created = new Carbon($_product->created_at);
                $now = Carbon::now();
                $difference = $created->diffInDays($now);
                
                if($difference > 30)
                {
                    $up["active_member"] = 0;
                    Tbl_slot::where("slot_id",$slots[$key]->slot_id)->update($up);
                }
            }
            //DATE DIFFERENCE
           
            // dd($created->diffForHumans($now));
            //  dd($difference);          
        }
        // dd($result);
        //lastarcy
    }
    public static function check_ifrank_up()
    {
        $_all_slot = Tbl_slot::get();
        
        
        $all_slot = null;
        foreach($_all_slot as $key_1 => $value_1)
        {  

            $all_slot["all"][$key_1] = $value_1;
            
            $personal_point[$key_1] = $all_slot["all"][$key_1]->slot_personal_points;
            $group_point[$key_1]  = $all_slot["all"][$key_1]->slot_group_points;
        
            $total_points[$key_1] = $personal_point[$key_1] + $group_point[$key_1];
            
            $required = Tbl_sequence::where("sequence_id",($all_slot["all"][$key_1]->sequence_level_id + 1))->first();
            
            if($required)
            {
                $spv = $required->required_spv;
                $id = $required->sequence_id;
            
                if($total_points[$key_1] >= $spv)
                {
                    $count_consultant = Tbl_tree_sponsor::join("tbl_slot",'tbl_tree_sponsor.sponsor_tree_child_id','=','tbl_slot.slot_id')
                                                    ->where("tbl_slot.active_member",1)
                                                    ->where("tbl_slot.sequence_level_id",2) //consultant_id
                                                    ->where("tbl_tree_sponsor.sponsor_tree_parent_id",$all_slot["all"][$key_1]->slot_id)
                                                    ->count();
                    $count_manager = Tbl_tree_sponsor::join("tbl_slot",'tbl_tree_sponsor.sponsor_tree_child_id','=','tbl_slot.slot_id')
                                                    ->where("tbl_slot.active_member",1)
                                                    ->where("tbl_slot.sequence_level_id",3) //manager_id
                                                    ->where("tbl_tree_sponsor.sponsor_tree_parent_id",$all_slot["all"][$key_1]->slot_id)
                                                    ->count();
                    $count_assoc_director = Tbl_tree_sponsor::join("tbl_slot",'tbl_tree_sponsor.sponsor_tree_child_id','=','tbl_slot.slot_id')
                                                    ->where("tbl_slot.active_member",1)
                                                    ->where("tbl_slot.sequence_level_id",4) // associate director_id
                                                    ->where("tbl_tree_sponsor.sponsor_tree_parent_id",$all_slot["all"][$key_1]->slot_id)
                                                    ->count();
                    $count_director = Tbl_tree_sponsor::join("tbl_slot",'tbl_tree_sponsor.sponsor_tree_child_id','=','tbl_slot.slot_id')
                                                    ->where("tbl_slot.active_member",1)
                                                    ->where("tbl_slot.sequence_level_id",5) // Director_id
                                                    ->where("tbl_tree_sponsor.sponsor_tree_parent_id",$all_slot["all"][$key_1]->slot_id)
                                                    ->count();
                    
                   if($personal_point[$key_1] >= $required->personal_personal_spv)
                   {
                        if($count_consultant >= $required->active_consultant)
                        {
                         if($count_manager >= $required->active_manager)
                            {
                                if($count_assoc_director >= $required->active_assoc_director)
                                {
                                    if($count_director >= $required->active_director)
                                    {
                                        $up_level["sequence_level_id"] =  $all_slot["all"][$key_1]->sequence_level_id + 1;
                                        $up_level["consultant_unilevel"] = 2;
                                        
                                        //UPDATE IF HAVE CURRENT GSB
                                        //new 
                                        
                                        $_new = Tbl_sequence_setting::where("tbl_sequence_setting.sequence_id", $up_level["sequence_level_id"])->get();
                                      
                                        $_old = Tbl_gsb_report::where("gsb_sponsor",$all_slot["all"][$key_1]->slot_id)
                                                             ->where("gsb_distributed",0)
                                                             ->where("personal_true",0)
                                                             ->get();
                                       // dd($_old);                 
                                        if(isset($_old))
                                        {
                                            $old = null;
                                            foreach ($_old as $key_2 => $value_2) 
                                            {
                                                $old[$key_2] = $value_2;
                                                $new = null;
                                                foreach ($_new as $key_3 => $value_3) 
                                                {
                                                    $new[$key_3] = $value_3;
                                                    
                                                    if($old[$key_2]->level == $new[$key_3]->seq_level)
                                                    {
                                                        $up["percentage"] = $new[$key_3]->seq_value;
                                                        // $up["gsb_earned"] = ($new[$key_3]->seq_value/100) * $old[$key_2]->gsb_spv;
                                                        $up["sequence_level_id"] = $up_level["sequence_level_id"];
                                                        
                                                        Tbl_gsb_report::where("gsb_sponsor",$old[$key_2]->gsb_sponsor)
                                                                        ->where("personal_true",0)
                                                                        ->update($up);
                                                    }
                                                }                                  
                                                
                                            }
                                        }
                                        
                                        $_new_personal = Tbl_sequence::where("sequence_id", $up_level["sequence_level_id"])->first();
                                        
                                                    
                                        //PERSONAL
                                        // $_new_personal = Tbl_sequence::where("sequence_id",$up_level["sequence_level_id"])->pluck("personal_bonus");
                                        $_old_personal = Tbl_gsb_report::where("gsb_sponsor",$all_slot["all"][$key_1]->slot_id)
                                                             ->where("gsb_distributed",0)
                                                             ->where("personal_true",1)
                                                             ->get();

                                        if(isset($_old_personal))
                                        {
                                            $old_personal = null;
                                            foreach ($_old_personal as $key_6 => $value_6) 
                                            {
                                              $old_personal[$key_6] = $value_6;
                                                
                                                $up_["sequence_level_id"] = $up_level["sequence_level_id"];

                                                $up["percentage"] = $_new_personal->personal_bonus;
                                                $up["sequence_level_id"] = $up_level["sequence_level_id"];
                                                    
                                                    Tbl_gsb_report::where("gsb_sponsor",$old_personal[$key_6]->gsb_sponsor)
                                                                    ->where("personal_true",1)
                                                                    ->update($up);
                                            }
                                        }
                                        
                                        
                                        $_new_all = Tbl_gsb_report::where("gsb_sponsor",$all_slot["all"][$key_1]->slot_id)
                                                                ->where("gsb_distributed",0)
                                                                ->get();
                                        $_old_total = Tbl_total_gsb::where("gsb_slot_id",$all_slot["all"][$key_1]->slot_id)
                                                                    ->where("total_gsb_distributed",0)
                                                                    ->get();
                                        if($_old_total)
                                        {
                                            $old_total = null;
                                            foreach ($_old_total as $key_5 => $value_5) 
                                            {
                                              $old_total[$key_5] = $value_5;
                                                
                                                $up_["sequence_level_id"] = $up_level["sequence_level_id"];
                                                

                                                Tbl_total_gsb::where("gsb_slot_id",$all_slot["all"][$key_1]->slot_id)
                                                                        ->where("total_gsb_level",$old_total[$key_5]->total_gsb_level)
                                                                        ->where("total_gsb_distributed",0)
                                                                        ->update($up_);


                                                foreach ($_new_all as $key => $value)
                                                {
                                                    $new_all[$key] = $value;
                                                    if($new_all[$key]->t_gsb_id == $old_total[$key_5]->total_gsb_id)
                                                    {
                                                        $up_yp["gsb_percentage"] = $_new_all[$key]->percentage;
                                                          Tbl_total_gsb::where("total_gsb_id",$new_all[$key]->t_gsb_id)
                                                                        ->where("total_gsb_level",$old_total[$key_5]->total_gsb_level)
                                                                        ->where("total_gsb_distributed",0)
                                                                        ->update($up_yp);
                                                    }
                                                }
                                            }
                                        }                       
                                        
                                        Tbl_slot::where("slot_id", $all_slot["all"][$key_1]->slot_id)->update($up_level);
                                        
                                        $new = Tbl_slot::join("tbl_sequence","tbl_sequence.sequence_id",'=',"tbl_slot.sequence_level_id")
                                                        ->where("slot_id", $all_slot["all"][$key_1]->slot_id)
                                                        ->first();
                                        
                                         $log = "Your slot # " .  $all_slot["all"][$key_1]->slot_id . " earned: Total " . number_format($total_points[$key_1], 2) . "SPV and Personal ". number_format($personal_point[$key_1],2) ." SPV .You have ". $count_consultant . " active Consultant " . $count_manager . " active Manager ". $count_assoc_director . " active Associate Director ". $count_director . " active Director. And You've rank up to " . $new->sequence_name;
                                         Log::rank_up($log);
                                    }
                                }
                            }   
                        }
                   }
                }
            }
        }
        
    }
    public static function royalty_rank_up()
    {
        $_all_slot = Tbl_slot::where("sequence_level_id",5)->get();
        // dd($_all_slot);
        
        $all_slot = null;
        foreach($_all_slot as $key_1 => $value_1)
        {
            $all_slot["all"][$key_1] = $value_1;
            
            $personal_point[$key_1] = $all_slot["all"][$key_1]->slot_personal_points;
            $group_point[$key_1]  = $all_slot["all"][$key_1]->slot_group_points;
        
            $total_points[$key_1] = $personal_point[$key_1] + $group_point[$key_1];
            
            $required = Tbl_royalty_promotion_setting::where("rb_id",($all_slot["all"][$key_1]->royalty_level + 1))->first();
            if($required)
            {
                 $spv = $required->required_spv;
                $id = $required->sequence_id;
            
                if($total_points[$key_1] >= $spv)
                {
                    $count_assoc_director = Tbl_tree_sponsor::join("tbl_slot",'tbl_tree_sponsor.sponsor_tree_child_id','=','tbl_slot.slot_id')
                                                    ->where("tbl_slot.active_member",1)
                                                    ->where("tbl_slot.sequence_level_id",4) // associate director_id
                                                    ->where("tbl_tree_sponsor.sponsor_tree_parent_id",$all_slot["all"][$key_1]->slot_id)
                                                    ->count();
                    $count_director = Tbl_tree_sponsor::join("tbl_slot",'tbl_tree_sponsor.sponsor_tree_child_id','=','tbl_slot.slot_id')
                                                    ->where("tbl_slot.active_member",1)
                                                    ->where("tbl_slot.sequence_level_id",5) // Director_id
                                                    ->where("tbl_tree_sponsor.sponsor_tree_parent_id",$all_slot["all"][$key_1]->slot_id)
                                                    ->count();


                    $count_avp = Tbl_tree_sponsor::join("tbl_slot",'tbl_tree_sponsor.sponsor_tree_child_id','=','tbl_slot.slot_id')
                                                    ->where("tbl_slot.active_member",1)
                                                    ->where("tbl_slot.royalty_level",1) // AVP
                                                    ->where("tbl_tree_sponsor.sponsor_tree_parent_id",$all_slot["all"][$key_1]->slot_id)
                                                    ->count();
                    $count_vp = Tbl_tree_sponsor::join("tbl_slot",'tbl_tree_sponsor.sponsor_tree_child_id','=','tbl_slot.slot_id')
                                                    ->where("tbl_slot.active_member",1)
                                                    ->where("tbl_slot.royalty_level",2) // VP
                                                    ->where("tbl_tree_sponsor.sponsor_tree_parent_id",$all_slot["all"][$key_1]->slot_id)
                                                    ->count();
                    $count_svp = Tbl_tree_sponsor::join("tbl_slot",'tbl_tree_sponsor.sponsor_tree_child_id','=','tbl_slot.slot_id')
                                                    ->where("tbl_slot.active_member",1)
                                                    ->where("tbl_slot.royalty_level",3) // SVP
                                                    ->where("tbl_tree_sponsor.sponsor_tree_parent_id",$all_slot["all"][$key_1]->slot_id)
                                                    ->count();
                    $count_evp = Tbl_tree_sponsor::join("tbl_slot",'tbl_tree_sponsor.sponsor_tree_child_id','=','tbl_slot.slot_id')
                                                    ->where("tbl_slot.active_member",1)
                                                    ->where("tbl_slot.royalty_level",4) // EVP
                                                    ->where("tbl_tree_sponsor.sponsor_tree_parent_id",$all_slot["all"][$key_1]->slot_id)
                                                    ->count();
                    $count_president = Tbl_tree_sponsor::join("tbl_slot",'tbl_tree_sponsor.sponsor_tree_child_id','=','tbl_slot.slot_id')
                                                    ->where("tbl_slot.active_member",1)
                                                    ->where("tbl_slot.royalty_level",5) // P
                                                    ->where("tbl_tree_sponsor.sponsor_tree_parent_id",$all_slot["all"][$key_1]->slot_id)
                                                    ->count();
                    
                   if($personal_point[$key_1] >= $required->personal_personal_spv)
                   {
                        if($count_assoc_director >= $required->rbp_active_assoc_director)
                        {
                            if($count_director >= $required->rbp_active_director)
                            {
                                if($count_avp >= $required->rbp_active_avp)
                                {
                                 if($count_vp >= $required->rbp_active_vp)
                                    {
                                        if($count_svp >= $required->rbp_active_svp)
                                        {
                                         if($count_evp >= $required->rbp_active_evp)
                                            {
                                             if($count_president >= $required->rbp_active_president)
                                                {
                                                    // dd("rank up.TRIAL SUCCESS");
                                                    $up_level["royalty_level"] =  $all_slot["all"][$key_1]->royalty_level + 1;
                                                    Tbl_slot::where("slot_id", $all_slot["all"][$key_1]->slot_id)->update($up_level);
                                        
                                                    $new = Tbl_slot::join("tbl_royalty_promotion_setting","tbl_royalty_promotion_setting.rb_id",'=',"tbl_slot.royalty_level")
                                                                    ->join("tbl_royalty_setting","tbl_royalty_setting.rb_id",'=',"tbl_slot.royalty_level")
                                                                    ->where("slot_id", $all_slot["all"][$key_1]->slot_id)
                                                                    ->first();
                                                    
                                                     $log = "Your slot # " .  $all_slot["all"][$key_1]->slot_id . " earned: Total " . number_format($total_points[$key_1], 2) . "SPV and Personal ". number_format($personal_point[$key_1],2) ." SPV .You have ". $count_assoc_director . " active Associate Director " . $count_director . " active Director ". $count_avp . " active AVP ". $count_vp . " active VP ". $count_svp . " active SVP ".$count_evp . " active EVP ".$count_president . " active President. And You've rank up to " . $new->rb_position;
                                                     Log::rank_up($log);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
            }

        }
    }
}
    public static function binary_repurchase($buyer_slot_id, $binary_pts, $method)
    {
        $new_slot_info = Tbl_slot::id($buyer_slot_id)->account()->membership()->first();
        $_pairing = Tbl_binary_pairing::orderBy("pairing_point_l", "desc")->where('membership_id',$new_slot_info->slot_membership)->get();

        /* GET SETTINGS */
        $required_pairing_points = 100;

        /* GET THE TREE */
        $_tree = Tbl_tree_placement::child($buyer_slot_id)->level()->distinct_level()->get();
        

                /* UPDATE BINARY POINTS */
                foreach($_tree as $tree)
                {
                    /* GET SLOT INFO FROM DATABASE */
                    $slot_recipient = Tbl_slot::id($tree->placement_tree_parent_id)->membership()->first();
                    // $update_recipient["slot_wallet"] = $slot_recipient->slot_wallet;
                    // $update_recipient["slot_total_earning"] = $slot_recipient->slot_total_earning;

                    /* RETRIEVE LEFT & RIGHT POINTS */
                    $binary["left"] = $slot_recipient->slot_binary_left;
                    $binary["right"] = $slot_recipient->slot_binary_right; 
                    $flushpoints = $slot_recipient->pair_flush_out_wallet;
                    /* ADD NECESARRY POINTS */
                    $earned_points = $binary_pts;

                    /* CHECK POINTS EARNED */
                    if($earned_points != 0)
                    {
                        $binary[$tree->placement_tree_position] = $binary[$tree->placement_tree_position] + $earned_points; 
                        
                        $slot_recipient_gc = Tbl_slot::id($tree->placement_tree_parent_id)->membership()->first();
                        $member            = Tbl_membership::where('membership_id',$slot_recipient->slot_membership)->first();
                        $date              = Carbon::now()->toDateString();
                        $count             = Tbl_slot::id($tree->placement_tree_parent_id)->first();
                        $count             = $count->pairs_today;
                        $points_to_earned  = $earned_points;
                        /* Check if date is equal today's date*/
                        if($slot_recipient_gc->pairs_per_day_date == $date)
                        {
                            if($member->max_pairs_per_day < $count)
                            {
                                /* Already exceeded */
                                $binary["left"]   = 0;
                                $binary["right"]  = 0;
                                $points_to_earned = 0;
                            }
                        }

                        /* INSERT LOG FOR EARNED POINTS IN ACCOUNT */
                        $log = "Your slot #" . $slot_recipient->slot_id . " earned <b> " . number_format($points_to_earned, 2) . " match points</b> on " . $tree->placement_tree_position . " when " . $new_slot_info->account_name . " with " . $new_slot_info->membership_name . " MEMBERSHIP created a new slot (#" . $new_slot_info->slot_id . ").";
                        Log::slot($slot_recipient->slot_id, $log, 0,"Binary Earn",$buyer_slot_id);
                        // Log::account($slot_recipient->slot_owner, $log);    

                        /* CHECK PAIRING */
                        foreach($_pairing as $pairing)
                        {
                            if($pairing->membership_id == $slot_recipient->slot_membership)
                            {
                                while($binary["left"] >= $pairing->pairing_point_l && $binary["right"] >= $pairing->pairing_point_r)
                                {
                                    $binary["left"] = $binary["left"] - $pairing->pairing_point_l;
                                    $binary["right"] = $binary["right"] - $pairing->pairing_point_r;

                                    /* GET PAIRING BONUS */
                                    $pairing_bonus = $pairing->pairing_income;

                                    /* CHECK IF PAIRING BONUS IS ZERO */
                                    if($pairing_bonus != 0)
                                    {

                                                /* Check if entry per day is exceeded already */
                                                $count =  Tbl_slot::id($slot_recipient->slot_id)->first();
                                                $member = Tbl_membership::where('membership_id',$slot_recipient->slot_membership)->first();
                                                $count = $count->pairs_today;
                                                $date = Carbon::now()->toDateString(); 
                                                $condition = null;
                                                $gc = false;
                                                $slot_recipient_gc = Tbl_slot::id($tree->placement_tree_parent_id)->membership()->first();
                                                $count_gc_times    = Tbl_wallet_logs::id($slot_recipient->slot_id)->where("keycode","binary")->where("logs","!=","%Im sorry! Max pairing per day already exceed your%")->where("flushed_out",0)->count() + 1; 

                                                /* Check if date is equal today's date*/
                                                if($slot_recipient_gc->pairs_per_day_date == $date)
                                                {
                                                    if($member->max_pairs_per_day <= $count)
                                                    {
                                                        /* Already exceeded */
                                                        $update['pairs_today'] = $count + 1;
                                                        $condition = false;
                                                    }
                                                    else
                                                    {
                                                        /* Go Ahead */
                                                        $count = $count + 1;
                                                        $update['pairs_today'] = $count;
                                                        $condition = true;

                                                        if($slot_recipient_gc->every_gc_pair != 0)
                                                        {
                                                            if($count_gc_times%$slot_recipient_gc->every_gc_pair == 0)
                                                            {
                                                                $gc = true;
                                                            }                                                        
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    /* Do this when date is new */
                                                    $update['pairs_per_day_date'] = $date;
                                                    $count = 1;
                                                    $update['pairs_today'] = $count;
                                                    $condition = true;

                                                    //IF GC EVERY PAIR IS IS NOT EQuAL TO 0  
                                                    if($slot_recipient_gc->every_gc_pair != 0)
                                                    {
                                                        /* CHECK IF GC */
                                                        if($count_gc_times%$slot_recipient_gc->every_gc_pair == 0)
                                                        {
                                                            $gc = true;
                                                        }                                                        
                                                    }

                                                }
                                                $check_wallet = Tbl_wallet_logs::id($new_slot_info->slot_id)->wallet()->sum('wallet_amount');
                                                if($new_slot_info->slot_type != "FS" && $check_wallet >= 0)
                                                {                                        
                                                    /* Insert Count */
                                                    Tbl_slot::where('slot_id',$slot_recipient_gc->slot_id)->update($update);
                                                }

                                                /* Proceed when entry is okay */
                                                if($condition == true)
                                                {
                                                        /* UPDATE WALLET */
                                                        // $update_recipient["slot_wallet"] = $update_recipient["slot_wallet"] + $pairing_bonus;
                                                        // $update_recipient["slot_total_earning"] = $update_recipient["slot_total_earning"] + $pairing_bonus;
                                                        $log = "Congratulations! Your slot #" . $slot_recipient->slot_id . " earned <b>" . number_format($pairing_bonus, 2) . " wallet</b> from <b>MATCHING BONUS</b> due to matching combination (" . $pairing->pairing_point_l .  ":" . $pairing->pairing_point_r . "). Your slot's remaining match points is " . $binary["left"] . " point(s) on left and " . $binary["right"] . " point(s) on right. This combination was caused by a repurchase of one of your downlines."; 
                                                        /* CHECK IF NOT FREE SLOT */
                                                        $check_wallet = Tbl_wallet_logs::id($new_slot_info->slot_id)->wallet()->sum('wallet_amount');

                                                            //CHECK IF CONVERT TO GC OR NOT
                                                            if($gc == false)
                                                            {
                                                                Compute::income_per_day($slot_recipient->slot_id,$pairing_bonus,'binary_repurchase',$slot_recipient->slot_owner,$log,$buyer_slot_id);
                                                            }
                                                            elseif($gc == true)
                                                            {
                                                                $gcbonus = $pairing_bonus;
                                                                // Tbl_slot::where('slot_id',$slot_recipient->slot_id)->update(["slot_gc"=>$gcbonus]);

                                                                $log = "This is your ".$slot_recipient->every_gc_pair." MSB, Your ".$pairing_bonus." Income converted to GC (SLOT #".$slot_recipient->slot_id.") due to matching combination (" . $pairing->pairing_point_l .  ":" . $pairing->pairing_point_r . "). Your slot's remaining match points is " . $binary["left"] . " point(s) on left and " . $binary["right"] . " point(s) on right. This combination was caused by a repurchase of one of your downlines.";
                                                                Log::slot($slot_recipient->slot_id, $log, $gcbonus,"binary_repurchase",$buyer_slot_id,1);
                                                                // Log::account($slot_recipient->slot_owner, $log);
                                                            }     
                                                            
                                                            if($new_slot_info->slot_type != "FS" && $check_wallet >= 0)
                                                            {
                                                                  /* MATCHING SALE BONUS */
                                                                  Compute::matching($buyer_slot_id, "REPURCHASE", $slot_recipient, $pairing_bonus);                                           
                                                            }  


                                                        /* INSERT LOG */
                                                        // Log::account($slot_recipient->slot_owner, $log);
                                                        // Log::slot($slot_recipient->slot_id, $log, $pairing_bonus, "BINARY PAIRING");


                                                }
                                                else
                                                {   
                                                        $binary["left"]   = 0;
                                                        $binary["right"]  = 0;          
                                                        $make_it_zero["slot_binary_left"]  = $binary["left"];
                                                        $make_it_zero["slot_binary_right"] = $binary["right"];
                                                        Tbl_slot::id($tree->placement_tree_parent_id)->update($make_it_zero);
                                                        $log = "Im sorry! Max matching per day already exceed your slot #" . $slot_recipient->slot_id . " flushed out <b>" . number_format($pairing_bonus, 2) . " wallet</b> from <b>MATCHING BONUS</b> due to matching combination (" . $pairing->pairing_point_l .  ":" . $pairing->pairing_point_r . "). Your slot's remaining match points is " . $binary["left"] . " point(s) on left and " . $binary["right"] . " point(s) on right. This combination was caused by a repurchase of one of your downlines.";          
                                                        // Log::account($slot_recipient->slot_owner, $log);
                                                        Log::slot($slot_recipient->slot_id, $log, 0,$method,$buyer_slot_id);
                                                        $flushpoints =  $flushpoints+$pairing_bonus;
                                                }
                                    }
                                }
                            }
                        } 

                        /* UPDATE POINTS */
                        $update_recipient["slot_binary_left"] = $binary["left"];
                        $update_recipient["slot_binary_right"] = $binary["right"];
                        $update_recipient["pair_flush_out_wallet"] = $flushpoints;
                        $check_wallet = Tbl_wallet_logs::id($new_slot_info->slot_id)->wallet()->sum('wallet_amount');
                        /* CHECK IF NOT FREE SLOT */
                        Tbl_slot::id($tree->placement_tree_parent_id)->update($update_recipient);                           

                        /* UPDATE SLOT CHANGES TO DATABASE */

                        $update_recipient = null;
                    }
                }            
    }

    public static function check_promotion_qualification($slot_id)
    {
        $slot_info = Tbl_slot::membership()->id($slot_id)->first();

        if($slot_info->slot_group_points > $slot_info->slot_highest_pv)
        {
            $checkupdate["slot_highest_pv"] = $slot_info->slot_group_points;
            Tbl_slot::id($slot_id)->update($checkupdate);
        }

        $slot_info = Tbl_slot::membership()->id($slot_id)->first();
        $count = Tbl_tree_sponsor::where('sponsor_tree_parent_id',$slot_id)->where('sponsor_tree_level',1)->count();


            $data["next_membership"] = Tbl_membership::where("membership_required_pv_sales", ">",  $slot_info->membership_required_pv_sales)->orderBy("membership_required_pv_sales", "asc")->first();

            if($data["next_membership"])
            {
                $d = $data["next_membership"];
                if($d->membership_required_unilevel_leg == 0)
                {
                    if($d->membership_required_pv_sales != 0)
                    {   
                        if($count >= $d->membership_required_direct && $slot_info->slot_highest_pv >= $d->membership_required_pv_sales && $slot_info->slot_maintained_month_count >= $d->membership_required_month_count)
                        {
                            $update_slot["slot_upgrade_points"] = 0;
                            $update_slot["slot_membership"] = $data["next_membership"]->membership_id;
                            $update_slot["slot_highest_pv"] = 0;

                            $log = "Congratulation! Slot #" . $slot_id . " has been promoted from " . $slot_info->membership_name . " to " . $data["next_membership"]->membership_name;
                            Tbl_slot::id($slot_id)->update($update_slot);

                            Log::slot($slot_id, $log, 0,"Promoted",$slot_id);

                            // Log::account($slot_info->slot_owner, $log);                  
                        }                    
                    }
                }
                else
                {

                    if($d->membership_required_pv_sales != 0)
                    {   
                        if($slot_info->slot_highest_pv >= $d->membership_required_pv_sales && $slot_info->slot_maintained_month_count >= $d->membership_required_month_count)
                        {
                            $promote = false;
                            $get_direct = Tbl_tree_sponsor::where('sponsor_tree_parent_id',$slot_id)->where('sponsor_tree_level',1)->get();
                            $count_direct = 0;
                            foreach($get_direct as $g)
                            {
                                $check = Tbl_tree_sponsor::where('tbl_sponsor_tree',$g->tbl_sponsor_tree)->join('tbl_slot','tbl_tree_sponsor.sponsor_tree_child_id','=','tbl_slot.slot_id')->where('slot_membership','=',$d->membership_unilevel_leg_id)->where('sponsor_tree_level',1)->first();
                                $count = Tbl_tree_sponsor::where('sponsor_tree_parent_id',$g->sponsor_tree_child_id)->join('tbl_slot','tbl_tree_sponsor.sponsor_tree_child_id','=','tbl_slot.slot_id')->where('slot_membership','=',$d->membership_unilevel_leg_id)->count();
                            
                                if($check || $count > 0)
                                {
                                    $count_direct++;
                                }
                            }
                            if($count_direct >=  $d->membership_required_direct)
                            {
                                    $update_slot["slot_upgrade_points"] = 0;
                                    $update_slot["slot_membership"] = $data["next_membership"]->membership_id;
                                    $update_slot["slot_highest_pv"] = 0;

                                    $log = "Congratulation! Slot #" . $slot_id . " has been promoted from " . $slot_info->membership_name . " to " . $data["next_membership"]->membership_name;
                                    Tbl_slot::id($slot_id)->update($update_slot);

                                    Log::slot($slot_id, $log, 0,"Promoted",$slot_id);
                            }
                        }                    
                    }
                }
            }
    }

    public static function insert_tree_placement($slot_info, $new_slot_id, $level)
    {
    	$upline_info = Tbl_slot::id($slot_info->slot_placement)->first();

        if($upline_info)
        {
          
            $insert["placement_tree_parent_id"] = $upline_info->slot_id;
            $insert["placement_tree_child_id"] = $new_slot_id;
            $insert["placement_tree_position"] = $slot_info->slot_position;
            $insert["placement_tree_level"] = $level;
            Tbl_tree_placement::insert($insert);
            $level++;
            Compute::insert_tree_placement($upline_info, $new_slot_id, $level);
        }  
        
    }

    public static function insert_tree_sponsor($slot_info, $new_slot_id, $level)
    {
    	$upline_info = Tbl_slot::id($slot_info->slot_sponsor)->first();

        if($upline_info)
        {
            $insert["sponsor_tree_parent_id"] = $upline_info->slot_id;
            $insert["sponsor_tree_child_id"] = $new_slot_id;
            $insert["sponsor_tree_level"] = $level;
            Tbl_tree_sponsor::insert($insert);
            $level++;
            Compute::insert_tree_sponsor($upline_info, $new_slot_id, $level);  
        }
        $max_level = Tbl_tree_sponsor::max("sponsor_tree_level");

        $up_lvl["level_rep"] = $max_level;
        Tbl_sequence::where("sequence_id",2)->update($up_lvl);

        Tbl_sequence_setting::where("sequence_id",2)->delete();
        for($i = $max_level ; $i > 0  ; $i-- )
        {
            $ins["seq_level"] = $i;
            $ins["seq_value"] = 100;
            $ins["sequence_id"] = 2;
            Tbl_sequence_setting::insert($ins);
        }
    }

    public static function binary($new_slot_id, $method = "SLOT CREATION")
    {
        $new_slot_info = Tbl_slot::id($new_slot_id)->account()->membership()->first();
        $_pairing = Tbl_binary_pairing::orderBy("pairing_point_l", "desc")->get();
        
    	/* GET SETTINGS */
    	$required_pairing_points = 100;

    	/* GET THE TREE */
    	$_tree = Tbl_tree_placement::child($new_slot_id)->level()->distinct_level()->get();

            /* UPDATE BINARY POINTS */
            foreach($_tree as $tree)
            {
                /* GET SLOT INFO FROM DATABASE */
                $slot_recipient = Tbl_slot::id($tree->placement_tree_parent_id)->membership()->first();
                // $update_recipient["slot_wallet"] = $slot_recipient->slot_wallet;
                // $update_recipient["slot_total_earning"] = $slot_recipient->slot_total_earning;

                /* RETRIEVE LEFT & RIGHT POINTS */
                $initial["left"] = $binary["left"] = $slot_recipient->slot_binary_left;
                $initial["right"] = $binary["right"] = $slot_recipient->slot_binary_right; 
                $flushpoints = $slot_recipient->pair_flush_out_wallet;

                /* ADD NECESARRY POINTS */
                $earned_points = $new_slot_info->membership_binary_points;

                /* CHECK POINTS EARNED */
                if($earned_points != 0)
                {
                    $binary[$tree->placement_tree_position] = $binary[$tree->placement_tree_position] + $earned_points; 
                    $check_wallet = Tbl_wallet_logs::id($new_slot_info->slot_id)->wallet()->sum('wallet_amount');
                    if($new_slot_info->slot_type != "FS" && $check_wallet >= 0)
                    {
                        $slot_recipient_gc = Tbl_slot::id($tree->placement_tree_parent_id)->membership()->first();
                        $member            = Tbl_membership::where('membership_id',$slot_recipient->slot_membership)->first();
                        $date              = Carbon::now()->toDateString();
                        $count             = Tbl_slot::id($tree->placement_tree_parent_id)->first();
                        $count             = $count->pairs_today;
                        $points_to_earned  = $earned_points;
                        /* Check if date is equal today's date*/
                        if($slot_recipient_gc->pairs_per_day_date == $date)
                        {
                            /* FLUSH OUT TRIGGER */
                            if($member->max_pairs_per_day < $count)
                            {
                                /* Already exceeded */
                                $binary["left"]   = 0;
                                $binary["right"]  = 0;
                                $points_to_earned = 0;
                            }
                        }

                        /* INSERT LOG FOR EARNED POINTS IN ACCOUNT */
                        $log = "Your slot #" . $slot_recipient->slot_id . " earned <b> " . number_format($points_to_earned, 2) . " match points</b> on " . $tree->placement_tree_position . " when " . $new_slot_info->account_name . " with " . $new_slot_info->membership_name . " MEMBERSHIP created a new slot (#" . $new_slot_info->slot_id . ").";
                        Log::slot($slot_recipient->slot_id, $log, 0,"Binary Earn",$new_slot_info->slot_id);
                        // Log::account($slot_recipient->slot_owner, $log);        
               
                    }


                    /* CHECK PAIRING */
                    foreach($_pairing as $pairing)
                    {   
                        if($pairing->membership_id == $slot_recipient->slot_membership)
                        {
                                while($binary["left"] >= $pairing->pairing_point_l && $binary["right"] >= $pairing->pairing_point_r)
                                {
                                            $binary["left"] = $binary["left"] - $pairing->pairing_point_l;
                                            $binary["right"] = $binary["right"] - $pairing->pairing_point_r;
                                            
                                            /* GET PAIRING BONUS */
                                            $pairing_bonus = $pairing->pairing_income;

                                            /* CHECK IF PAIRING BONUS IS ZERO */
                                            if($pairing_bonus != 0)
                                            {

                                                /* Check if entry per day is exceeded already */
                                                $count =  Tbl_slot::id($tree->placement_tree_parent_id)->first();
                                                $member = Tbl_membership::where('membership_id',$slot_recipient->slot_membership)->first();
                                                $count = $count->pairs_today;
                                                $date = Carbon::now()->toDateString();
                                                $condition = null;
                                                $gc = false;
                                                $slot_recipient_gc = Tbl_slot::id($tree->placement_tree_parent_id)->membership()->first();
                                                $count_gc_times    = Tbl_wallet_logs::id($slot_recipient->slot_id)->where("keycode","binary")->where("logs","!=","%Im sorry! Max pairing per day already exceed your%")->where("flushed_out",0)->count() + 1; 

                                                  /* Check if date is equal today's date*/
                                                if($slot_recipient_gc->pairs_per_day_date == $date)
                                                {
                                                    if($member->max_pairs_per_day <= $count)
                                                    {
                                                        /* Already exceeded */
                                                        $update['pairs_today'] = $count + 1;
                                                        $condition = false;
                                                    }
                                                    else
                                                    {
                                                        /* Go Ahead */
                                                        $count = $count + 1;
                                                        $update['pairs_today'] = $count;
                                                        $condition = true;

                                                        if($slot_recipient_gc->every_gc_pair != 0)
                                                        {
                                                            /* CHECK IF GC */
                                                            if($count_gc_times%$slot_recipient_gc->every_gc_pair == 0)
                                                            {
                                                                $gc = true;
                                                            }                                                        
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    /* Do this when date is new */
                                                    $update['pairs_per_day_date'] = $date;
                                                    $count = 1;
                                                    $update['pairs_today'] = $count;
                                                    $condition = true;

                                                    if($slot_recipient_gc->every_gc_pair != 0)
                                                    {
                                                        /* CHECK IF GC */
                                                        if($count_gc_times%$slot_recipient_gc->every_gc_pair == 0 && $count != 0)
                                                        {
                                                            $gc = true;
                                                        }                                                        
                                                    }
                                                }
                                                $check_wallet = Tbl_wallet_logs::id($new_slot_info->slot_id)->wallet()->sum('wallet_amount');
                                                if($new_slot_info->slot_type != "FS" && $check_wallet >= 0)
                                                {  
                                                    /* Insert Count */
                                                    Tbl_slot::where('slot_id',$slot_recipient_gc->slot_id)->update($update);
                                                }

                                                /* Proceed when entry is okay */
                                                if($condition == true)
                                                {
                                                    /* UPDATE WALLET */
                                                    // $update_recipient["slot_wallet"] = $update_recipient["slot_wallet"] + $pairing_bonus;
                                                    // $update_recipient["slot_total_earning"] = $update_recipient["slot_total_earning"] + $pairing_bonus;

                                                    /* INSERT LOG */
                                                    $log = "Congratulations! Your slot #" . $slot_recipient->slot_id . " earned <b> " . number_format($pairing_bonus, 2) . " wallet</b> from <b>MATCHING BONUS</b> due to matching combination (" . $pairing->pairing_point_l .  ":" . $pairing->pairing_point_r . "). Your slot's remaining match points is " . $binary["left"] . " point(s) on left and " . $binary["right"] . " point(s) on right.";
                                                    // Log::account($slot_recipient->slot_owner, $log);
                                                    // Log::slot($slot_recipient->slot_id, $log, $pairing_bonus, "BINARY PAIRING");
                                                    $check_wallet = Tbl_wallet_logs::id($new_slot_info->slot_id)->wallet()->sum('wallet_amount');
                                                            if($gc == false && $new_slot_info->slot_type != "FS" && $check_wallet >= 0)
                                                            {
                                                                 Compute::income_per_day($slot_recipient->slot_id,$pairing_bonus,'binary',$slot_recipient->slot_owner,$log,$new_slot_id); 
                                                            }
                                                            elseif($gc == true && $new_slot_info->slot_type != "FS" && $check_wallet >= 0)
                                                            {
                                                                    $gcbonus = $pairing_bonus;
                                                                    // Tbl_slot::where('slot_id',$slot_recipient->slot_id)->update(["slot_gc"=>$gcbonus]);
                                                                    $log = $log = "This is your ".$slot_recipient->every_gc_pair." MSB, Your ".$pairing_bonus." Income converted to GC (SLOT #".$slot_recipient->slot_id.") due to matching combination (" . $pairing->pairing_point_l .  ":" . $pairing->pairing_point_r . "). Your slot's remaining match points is " . $binary["left"] . " point(s) on left and " . $binary["right"] . " point(s) on right.";
                                                                    Log::slot($slot_recipient->slot_id, $log, $gcbonus,"binary",$new_slot_id,1);
                                                                    // Log::account($slot_recipient->slot_owner, $log);       
                                                            } 

                                                            if($new_slot_info->slot_type != "FS" && $check_wallet >= 0)
                                                            {
                                                                       /* MATCHING SALE BONUS */
                                                                      Compute::matching($new_slot_id, $method, $slot_recipient, $pairing_bonus);  
                                                            }                                          
                                                }
                                                else
                                                {   
                                                        $binary["left"]   = 0;
                                                        $binary["right"]  = 0;
                                                        $make_it_zero["slot_binary_left"]  = $binary["left"];
                                                        $make_it_zero["slot_binary_right"] = $binary["right"];
                                                        Tbl_slot::id($tree->placement_tree_parent_id)->update($make_it_zero);
                                                        $log = "Im sorry! Max pairing per day already exceed your slot #" . $slot_recipient->slot_id . " flushed out <b>" . number_format($pairing_bonus, 2) . " wallet</b> from <b>MATCHING BONUS</b> due to matching combination (" . $pairing->pairing_point_l .  ":" . $pairing->pairing_point_r . "). Your slot's remaining match points is " . $binary["left"] . " point(s) on left and " . $binary["right"] . " point(s) on right.";          
                                                        Log::slot_with_flush($slot_recipient->slot_id, $log, 0,"binary",$new_slot_id,$pairing_bonus); 
                                                        // Log::account($slot_recipient->slot_owner, $log);
                                                        $flushpoints =  $flushpoints+$pairing_bonus;
                                                }

                                            }                                                      
                                }                                
              
                        }
                    } 

                    /* UPDATE POINTS */
                    $update_recipient["slot_binary_left"] = $binary["left"];
                    $update_recipient["slot_binary_right"] = $binary["right"];
                    $update_recipient["pair_flush_out_wallet"] = $flushpoints;
                    $check_wallet = Tbl_wallet_logs::id($new_slot_info->slot_id)->wallet()->sum('wallet_amount');
                    /* UPDATE SLOT CHANGES TO DATABASE */
                    if($new_slot_info->slot_type != "FS" && $check_wallet >= 0)
                    {
                        Tbl_slot::id($tree->placement_tree_parent_id)->update($update_recipient);
                    }

                    $update_recipient = null;
                }
            
            }         
    }

    public static function matching($new_slot_id, $method, $slot_recipient_for_binary, $pairing_bonus)
    {
        $slot_recipient_id = $slot_recipient_for_binary->slot_sponsor;

        /* GET SLOT INFO FROM DATABASE */
        // $slot_recipient = Tbl_slot::id($slot_recipient_id)->membership()->first();
           $slot_recipient = null;

        $_tree = Tbl_tree_sponsor::child($slot_recipient_for_binary->slot_id)->level()->distinct_level()->get();
        $matching_setting = Tbl_matching_bonus::get();

        $matching_bonus = null;

        foreach($matching_setting as $key => $level)
        {
            $matching_bonus[$level->membership_id][$level->level]['percent'] =  $level->matching_percentage;
            $matching_bonus[$level->membership_id][$level->level]['count'] =  $level->matching_requirement_count;
            // $matching_bonus[$level->membership_id][$level->level]['member'] =  $level->matching_requirement_membership_id;
            $matching_bonus[$level->membership_id][$level->level]['level'] =  $level->level;
        }
        if($matching_bonus)
        {
            foreach($_tree as $key => $tree)
            {
                    // $update_recipient["slot_wallet"] = $slot_recipient->slot_wallet;
                    // $update_recipient["slot_total_earning"] = $slot_recipient->slot_total_earning;

                    /* GET SLOT INFO */
                    $slot_recipient = Tbl_slot::id($tree->sponsor_tree_parent_id)->membership()->first();
                    if(isset($matching_bonus[$slot_recipient->membership_id][$tree->sponsor_tree_level]['percent']))
                    { 
                        // $count = 0;
                        $count = Tbl_tree_sponsor::where('sponsor_tree_parent_id',$slot_recipient->slot_id)->where('sponsor_tree_level',1)->count();
                        // foreach($check_requirement as $check)
                        // {
                        //     $check = Tbl_slot::id($tree->sponsor_tree_parent_id)->membership()->first();
                        //     if($check->membership_id == $matching_bonus[$slot_recipient->membership_id][$tree->sponsor_tree_level]['member'])
                        //     {
                        //         $count++;
                        //     }
                        // }
                        if($count >= $matching_bonus[$slot_recipient->membership_id][$tree->sponsor_tree_level]['count'])
                        {
                          $matching_income = ($matching_bonus[$slot_recipient->membership_id][$tree->sponsor_tree_level]['percent']/100)*$pairing_bonus;      
                        }
                        else
                        {
                            $matching_income = 0;
                        }
                    }
                    else
                    {
                            $matching_income = 0;
                    }

                    /* GET INFO OF REGISTREE */
                    // $new_slot_info = Tbl_slot::id($new_slot_id)->account()->membership()->first();
                    /* COMPUTE FOR THE MATCHING INCOME */


                    if($matching_income != 0)
                    {
                        /* UPDATE WALLET */
                        // $update_recipient["slot_wallet"] = $update_recipient["slot_wallet"] + $matching_income;
                        // $update_recipient["slot_total_earning"] = $update_recipient["slot_total_earning"] + $matching_income;
                        $log = "Congratulations! Your slot #" . $slot_recipient->slot_id . " earned <b> " . number_format($matching_income, 2) . " wallet</b> from <b>MENTOR BONUS</b>  due to pairing bonus earned by SLOT #" . $slot_recipient_for_binary->slot_id . ". You current membership is " . $slot_recipient->membership_name . " MEMBERSHIP which has " . number_format($matching_bonus[$slot_recipient->membership_id][$tree->sponsor_tree_level]['percent'], 2) . "% bonus for every income of your level ".$matching_bonus[$slot_recipient->membership_id][$tree->sponsor_tree_level]['level']." of your direct sponsor. This bonus is required ".$matching_bonus[$slot_recipient->membership_id][$tree->sponsor_tree_level]['count']." direct.";
                        Compute::income_per_day($slot_recipient->slot_id,$matching_income,'matching',$slot_recipient->slot_owner,$log,$new_slot_id);
                        /* INSERT LOG */
                        // Log::account($slot_recipient->slot_owner, $log);
                        // Log::slot($slot_recipient->slot_id, $log, $matching_income, "MATCHING BONUS");

                        /* UPDATE SLOT CHANGES TO DATABASE */
                        // Tbl_slot::id($slot_recipient->slot_id)->update($update_recipient);
                    }       
            }            
        }
    }
    public static function direct($new_slot_id, $method = "SLOT CREATION")
    {
        $new_slot_info = Tbl_slot::id($new_slot_id)->account()->membership()->first();

        /* GET SLOT INFO FROM DATABASE */
        $slot_recipient = Tbl_slot::id($new_slot_info->slot_sponsor)->membership()->first();
        $check_wallet = Tbl_wallet_logs::id($new_slot_info->slot_id)->wallet()->sum('wallet_amount');
        /* ONLY PAID SLOT */
        if($new_slot_info->slot_type != "FS" && $check_wallet >= 0)
        {
            /* CHECK IF SLOT RECIPIENT EXIST */
            if($slot_recipient)
            {
                // $update_recipient["slot_wallet"] = $slot_recipient->slot_wallet;
                // $update_recipient["slot_total_earning"] = $slot_recipient->slot_total_earning;
                $update_recipient["slot_upgrade_points"] = $slot_recipient->slot_upgrade_points;

                /* GET INFO OF REGISTREE */
                $new_slot_info = Tbl_slot::id($new_slot_id)->account()->membership()->first();

                /* COMPUTE FOR THE DIRECT INCOME */

                /* Check if percentage or not */
                if($new_slot_info->if_matching_percentage == 1)
                {
                   $direct_income = ($slot_recipient->membership_direct_sponsorship_bonus/100) * $new_slot_info->membership_price;                    
                }
                else
                {
                   $direct_income = $slot_recipient->membership_direct_sponsorship_bonus;        
                }
               

                if($direct_income != 0)
                {
                    /* UPDATE WALLET */
                    // $update_recipient["slot_wallet"] = $update_recipient["slot_wallet"] + $direct_income;
                    // $update_recipient["slot_total_earning"] = $update_recipient["slot_total_earning"] + $direct_income;
                    $update_recipient["slot_upgrade_points"] = $slot_recipient->slot_upgrade_points + $slot_recipient->member_upgrade_pts;
                  
                    
                    /* INSERT LOG */
                    $log = "Congratulations! Your slot #" . $slot_recipient->slot_id . " earned <b>" . number_format($direct_income, 2) . " wallet</b> through <b>DIRECT SPONSORSHIP BONUS</b> because you've invited SLOT #" . $new_slot_info->slot_id . " to join. Your current membership is " . $slot_recipient->membership_name . " MEMBERSHIP. Your slot #" . $slot_recipient->slot_id . " also earned <b>" . $slot_recipient->member_upgrade_pts . " Promotion Points</b>.";
                    Compute::income_per_day($slot_recipient->slot_id,$direct_income,'direct',$slot_recipient->slot_owner,$log,$new_slot_id);
                    // Log::account($slot_recipient->slot_owner, $log);
                    // Log::slot($slot_recipient->slot_id, $log, $direct_income, "DIRECT SPONSORSHIP BONUS (DSB)");

                    /* UPDATE SLOT CHANGES TO DATABASE */
                    Tbl_slot::id($slot_recipient->slot_id)->update($update_recipient);
                }
            }            
        }
                    /* CHECK IF QUALIFIED FOR PROMOTION */                
                    // Compute::check_promotion_qualification($slot_recipient->slot_id);
    }
    public static function indirect($new_slot_id, $method = "SLOT CREATION")
    {
        $new_slot_info = Tbl_slot::id($new_slot_id)->account()->membership()->first();
        $_indirect_setting = Tbl_indirect_setting::get();
        $_tree = Tbl_tree_sponsor::child($new_slot_id)->level()->distinct_level()->get();

        /* RECORD ALL INTO A SINGLE VARIABLE */
        $indirect_level = null;
        foreach($_indirect_setting as $key => $level)
        {
            $indirect_level[$level->membership_id][$level->level] =  $level->value;
        }

        /* ONLY PAID SLOT */
        if($new_slot_info->slot_type != "FS" && $new_slot_info->slot_wallet >= 0)
        {
            /* CHECK IF LEVEL EXISTS */
            if($indirect_level)
            {
                foreach($_tree as $key => $tree)
                {
                    /* GET SLOT INFO FROM DATABASE */
                    $slot_recipient = Tbl_slot::id($tree->sponsor_tree_parent_id)->membership()->first();
                    // $update_recipient["slot_wallet"] = $slot_recipient->slot_wallet;
                    // $update_recipient["slot_total_earning"] = $slot_recipient->slot_total_earning;

                    /* COMPUTE FOR BONUS */
                    if(isset($indirect_level[$slot_recipient->membership_id][$tree->sponsor_tree_level]))
                    {
                        $indirect_bonus = $indirect_level[$slot_recipient->membership_id][$tree->sponsor_tree_level];    
                    }
                    else
                    {
                        $indirect_bonus = 0;
                    }
                    
                    /* CHECK IF BONUS IS ZERO */
                    if($indirect_bonus != 0)
                    {
                        /* UPDATE WALLET */
                        // $update_recipient["slot_wallet"] = $update_recipient["slot_wallet"] + $indirect_bonus;
                        // $update_recipient["slot_total_earning"] = $update_recipient["slot_total_earning"] + $indirect_bonus;
                        $log = "Congratulations! Your slot #" . $slot_recipient->slot_id . " earned <b>" . number_format($indirect_bonus, 2) . " wallet</b> from <b>INDIRECT LEVEL BONUS</b>. You earned it when slot #" . $new_slot_id . " creates a new slot on the Level " . $tree->sponsor_tree_level . " of your sponsor genealogy. Your current membership is " . $slot_recipient->membership_name . " MEMBERSHIP.";
                        $check = Compute::income_per_day($slot_recipient->slot_id,$indirect_bonus,'indirect',$slot_recipient->slot_owner,$log,$new_slot_id);
                        /* INSERT LOG */




                        /* UPDATE SLOT CHANGES TO DATABASE */
                        // Tbl_slot::id($slot_recipient->slot_id)->update($update_recipient);
                    }
                }
            }            
        }
    }
    public static function income_per_day($slot_id,$income,$method,$owner,$log,$cause)
    {
                $date = Carbon::now()->toDateString();
                $getslot = Tbl_slot::where('slot_id',$slot_id)->membership()->first();
                $ifnegative = Tbl_wallet_logs::id($slot_id)->wallet()->sum('wallet_amount');

                        if($getslot->slot_today_date == $date)
                        {
                           $total = ($getslot->slot_today_income + $income);

                           if($getslot->max_income < $total && $getslot->max_income <= $getslot->slot_today_income)
                           {
                             // $update['slot_today_income']  =  $getslot->slot_today_income;
                             // $update['slot_flushout']      =  $getslot->slot_flushout + $income;
                             // $update['slot_total_earning'] =  $getslot->slot_total_earning;
                             // $update['slot_wallet'] =  $getslot->slot_wallet;
                             Compute::method_with_flush($method,$slot_id,$income,$owner,$log,$cause);
                             // Tbl_slot::where('slot_id',$slot_id)->update($update);
                           }
                           else if($getslot->max_income < $total)
                           {
                              $total  = $total + (($getslot->max_income - $total)- $getslot->slot_today_income);
                              $total2 = $income - $total;
                              // $update['slot_today_income']  = $getslot->slot_today_income + $total;
                              // $update['slot_total_earning'] = $getslot->slot_total_earning + $total;
                              // $update['slot_flushout']      =  $getslot->slot_flushout + $total2;
                              // $update['slot_wallet'] =  $getslot->slot_wallet + $total;
                              Compute::method_reduced_flush($method,$slot_id,$total,$owner,$log,$total2,$cause);
                              // Compute::put_income_summary($slot_id,$method,$total);
                              // Tbl_slot::where('slot_id',$slot_id)->update($update);
                           }
                           else
                           {
                             // $update['slot_today_income'] = $total; 
                             // $update['slot_total_earning'] = $getslot->slot_total_earning + $income;
                             // $update['slot_wallet'] =  $getslot->slot_wallet + $income;
                             Compute::method_no_flush($method,$slot_id,$income,$owner,$log,$cause);
                             // Compute::put_income_summary($slot_id,$method,$income);
                             // Tbl_slot::where('slot_id',$slot_id)->update($update);
                           }
                        }
                        else
                        {
                           // $update['slot_today_date'] = $date;
                           // $update['slot_today_income'] = $income;
                           // $update['slot_total_earning'] = $getslot->slot_total_earning + $income;
                           // $update['slot_wallet'] =  $getslot->slot_wallet + $income;
                           // Compute::method_no_flush($method,$slot_id,$income,$owner,$log); 

                           $total = $income;

                           if($getslot->max_income < $total && $getslot->max_income <= $getslot->slot_today_income)
                           {
                             // $update['slot_today_income']  =  0;
                             // $update['slot_flushout']      =  $getslot->slot_flushout + $income;
                             // $update['slot_total_earning'] =  $getslot->slot_total_earning;
                             // $update['slot_wallet'] =  $getslot->slot_wallet;
                             Compute::method_with_flush($method,$slot_id,$income,$owner,$log,$cause);
                             // Tbl_slot::where('slot_id',$slot_id)->update($update);
                           }
                           else if($getslot->max_income < $total)
                           {
                              $total  = $total + (($getslot->max_income - $total)- $getslot->slot_today_income);
                              $total2 = $income - $total;
                              // $update['slot_today_income']  = $total;
                              // $update['slot_total_earning'] = $getslot->slot_total_earning + $total;
                              // $update['slot_flushout']      =  $getslot->slot_flushout + $total2;
                              // $update['slot_wallet'] =  $getslot->slot_wallet + $total;
                              Compute::method_reduced_flush($method,$slot_id,$total,$owner,$log,$total2,$cause);
                              // Compute::put_income_summary($slot_id,$method,$total);
                              // Tbl_slot::where('slot_id',$slot_id)->update($update);
                           }
                           else
                           {
                             // $update['slot_today_income'] = $total; 
                             // $update['slot_total_earning'] = $getslot->slot_total_earning + $income;
                             // $update['slot_wallet'] =  $getslot->slot_wallet + $income;
                             Compute::method_no_flush($method,$slot_id,$income,$owner,$log,$cause);
                             // Compute::put_income_summary($slot_id,$method,$income);
                             // Tbl_slot::where('slot_id',$slot_id)->update($update);
                           }
                        }                        
                     
                 if($getslot->slot_type == "CD")
                 {
                     if($ifnegative >= 0)
                     {
                        $update = null;
                        $update["slot_type"] = "PS";
                        $message = "Your slot #".$slot_id." becomes a paid slot.";
                        Log::slot($slot_id, $message, 0, "CD to PS",$slot_id);
                        $vouch = DB::table('tbl_voucher')->where('slot_id',$slot_id)->first();
                        if($vouch)
                        {
                            $check = Tbl_voucher::where('tbl_voucher.voucher_id',$vouch->voucher_id)->update(["status"=>"unclaimed"]);
                        }
                        Compute::binary($slot_id, "CD TO PS");
                        Compute::direct($slot_id, "CD TO PS");
                        Tbl_slot::where('slot_id',$slot_id)->update($update);
                     }                   
                 }                    
                
    }
    public static function method_no_flush($method,$slot_id,$income,$owner,$log,$cause)
    {
                    Log::slot($slot_id, $log, $income, $method,$cause);
                    // Log::account($owner, $log);            
    }
    public static function method_with_flush($method,$slot_id,$income,$owner,$log,$cause)
    {
                    $log = "Im sorry! You have already reach the max income for today, Your slot #" . $slot_id.' flushed out '.number_format($income, 2) . " wallet.";
                    // Log::account($owner, $log);
                    Log::slot_with_flush($slot_id, $log,0, $method,$cause,$income);             
    }
    public static function method_reduced_flush($method,$slot_id,$income,$owner,$log,$flush,$cause)
    {
                    $log = $log." Max income is reached, wallet earned reduced to <b>".$income."</b> and flushed out <b>".$flush.'</b>.';
                    // Log::account($owner, $log);
                    Log::slot_with_flush($slot_id, $log, $income, $method,$cause,$flush);             
    }
    public static function put_income_summary($slot_id,$method,$amount)
    {
            if($method == "direct")
            {
                $get = Tbl_slot::where('slot_id',$slot_id)->first();                
                $update['total_earned_direct'] = $get->total_earned_direct + $amount;
                Tbl_slot::where('slot_id',$slot_id)->update($update); 
                $update = null;
            }
            else if($method == "indirect")
            {
                $get = Tbl_slot::where('slot_id',$slot_id)->first();                
                $update['total_earned_indirect'] = $get->total_earned_indirect + $amount;
                Tbl_slot::where('slot_id',$slot_id)->update($update); 
                $update = null;
            }
            else if($method == "binary")
            {
                $get = Tbl_slot::where('slot_id',$slot_id)->first();                
                $update['total_earned_binary'] = $get->total_earned_binary + $amount;
                Tbl_slot::where('slot_id',$slot_id)->update($update); 
                $update = null;
            }
            else if($method == "matching")
            {
                $get = Tbl_slot::where('slot_id',$slot_id)->first();                
                $update['total_earned_matching'] = $get->total_earned_matching + $amount;
                Tbl_slot::where('slot_id',$slot_id)->update($update); 
                $update = null;
            }
    }

    public static function compute_travel($slot)
    {
        $reward = null;
        $points = 0 ;


        $required = Tbl_travel_qualification::where('archived',0)->get();

        foreach($required as $key => $r)
        {
            if($r->travel_qualification_name == "Direct Referral")
            {
                $count = Tbl_tree_sponsor::where('sponsor_tree_parent_id',$slot->slot_id)->where('sponsor_tree_level',1)->count();
                if($count >= $r->item)
                {
                    $points = $points + $r->points;
                }
            }
            elseif($r->travel_qualification_name == "Total Spent")
            {
                $count = Tbl_wallet_logs::id($slot->slot_id)->wallet()->where('wallet_amount','<=',0)->sum('wallet_amount');
                $count = $count * (-1);

                if($count >= $r->item)
                {
                    $points = $points + $r->points;
                }          
            }
            elseif($r->travel_qualification_name == "Total Income")
            {
                $count = Tbl_wallet_logs::id($slot->slot_id)->wallet()->where('wallet_amount','>=',0)->sum('wallet_amount');
                if($count >= $r->item)
                {
                    $points = $points + $r->points;
                }  
            }
            elseif($r->travel_qualification_name == "Total Downline")
            {
                $count = Tbl_tree_placement::where('placement_tree_parent_id',$slot->slot_id)->count();
                if($count >= $r->item)
                {
                    $points = $points + $r->points;
                }  
            }
        }

        $reward = Tbl_travel_reward::where('archived',0)->where('required_points','<=',$points)->orderBy('required_points','DESC')->first();

        $data['points'] = $points;
        $data['reward'] = $reward;

        return $data;
    }

    public static function delete_slot($slot_id)
    {

        $delete_slot = Tbl_slot::id($slot_id)->first();

        if($delete_slot->slot_type != 'CD' && $delete_slot->slot_type != 'FS')
        {
                $c = Carbon::now()->toDateString();

                $d = Carbon::parse($delete_slot->created_at)->toDateString();

                if($d == $c)
                {
                    $condition = true;
                }
                else
                {
                    $condition = false;
                }

                $get_membership = DB::table('tbl_binary_pairing')->where('membership_id',$delete_slot->slot_membership)->first();
                $binary_l = $get_membership->pairing_point_l;
                $binary_r = $get_membership->pairing_point_r;

                $get = Tbl_wallet_logs::where('cause_id',$slot_id)->where('keycode','binary')->get();
  
                foreach($get as $g)
                {
                    $slot_info = Tbl_slot::id($g->slot_id)->first();

                    $update['slot_binary_left']  = $slot_info->slot_binary_left  + $binary_l;
                    $update['slot_binary_right'] = $slot_info->slot_binary_right + $binary_r;
                    if($condition == true)
                    {
                        $update['pairs_today'] = $slot_info->pairs_today - 1;            
                    }

                    Tbl_slot::id($g->slot_id)->update($update);
                    $update = null;
                }

                $get = Tbl_tree_placement::where('placement_tree_child_id',$slot_id)->get();
                foreach($get as $g)
                {
                    $slot_info = Tbl_slot::id($g->placement_tree_parent_id)->first();   

                    if($g->placement_tree_position == "left")
                    {
                        $update['slot_binary_left']  = $slot_info->slot_binary_left  - $binary_l;
                        Tbl_slot::id($g->placement_tree_parent_id)->update($update); 
                    }
                    elseif($g->placement_tree_position =="right")
                    {
                        $update['slot_binary_right'] = $slot_info->slot_binary_right - $binary_r; 
                        Tbl_slot::id($g->placement_tree_parent_id)->update($update);               
                    }

                    $update = null; 
                }            
        }
       
    }
    public static function add1kpipeline2open($sponsorid){
        //check if pipeline 2 open
        $pipelinegraduate = Tbl_pipeline::where('pipeline_level', 2)->where('pipeline_downline', $sponsorid)->count();
        // additional if pipeline 2 open
        $additional = 1000;
        if($pipelinegraduate >= 1){
            return $additional;
        }
        else
        {
          return 0;  
        }
    }
      public static function staistep_checkif_rank_up($slot_id){;
        // $data['stair'] = Tbl_tree_placement::where('placement_tree_level', 1)
        // ->where('placement_tree_parent_id', $slot_id)
        // ->where('placement_tree_position', $tree_position)
        // ->join('tbl_stairstep', 'tbl_stairstep.stairstep_slot_id', '=', 'tbl_tree_placement.placement_tree_child_id')
        // ->first();
        $count = Tbl_stairstep::settings()->where('stairstep_slot_id', $slot_id)->count();
        if($count == 0)
        {
            $insert['stairstep_slot_id'] = $slot_id;
            $insert['stairstep_gv'] =  0;
            $insert['stairstep_pv'] =  0;
            $insert['stairstep_settings_id'] = 1;
            Tbl_stairstep::insert($insert);
        }
        $data['stair'] = Tbl_tree_placement::where('tbl_tree_placement.placement_tree_level', 1)
        ->where('tbl_tree_placement.placement_tree_parent_id', $slot_id)
        ->first();
        if($data['stair'] != null){
            $count = Tbl_stairstep::settings()->where('stairstep_slot_id', $data['stair']['placement_tree_child_id'] )->count();
            if($count == 0)
            {
                $insert['stairstep_slot_id'] = $data['stair']['placement_tree_child_id'];
                $insert['stairstep_gv'] =  0;
                $insert['stairstep_pv'] =  0;
                $insert['stairstep_settings_id'] = 1;
                Tbl_stairstep::insert($insert);
            }
            $data['stair_up'] = Tbl_tree_placement::where('tbl_tree_placement.placement_tree_level', 1)
            ->where('tbl_tree_placement.placement_tree_parent_id', $slot_id)
            ->join('tbl_stairstep', 'tbl_stairstep.stairstep_slot_id', '=', 'tbl_tree_placement.placement_tree_parent_id')
            ->first();
             $data['stair_down'] = Tbl_tree_placement::where('placement_tree_level', 1)
            // ->where('placement_tree_child_id', $data['stair']['placement_tree_child_id'])
            // ->where('placement_tree_position', $tree_position)
            ->join('tbl_stairstep', 'tbl_stairstep.stairstep_slot_id', '=', 'tbl_tree_placement.placement_tree_child_id')
            ->get();
            $next_level = $data['stair_up']['stairstep_settings_id'] + 1;
            $data['requirements_settings'] = Tbl_stairstep_settings::where('stairstep_level', $next_level)->first();
            $leg = 0;
            foreach($data['stair_down'] as $key => $value)
            {
                if($value->stairstep_gv >= $data['requirements_settings']['stairstep_special'])
                {
                    $leg = $leg + 1;
                }
            }
            if($leg >= $data['requirements_settings']['stairstep_special_qty'])
            {
                if($data['stair_up']['stairstep_pv'] >= $data['requirements_settings']['stairstep_required_pv']  ){
                $update['stairstep_settings_id'] = $data['requirements_settings']['stairstep_level'];
                Tbl_stairstep::where('stairstep_slot_id', $data['stair_up']['stairstep_slot_id'])->update($update);
                Compute::add_stairstep_report($data['stair_up']['stairstep_slot_id'], $data['requirements_settings']['stairstep_level'], $data['requirements_settings']['stairstep_bonus']);
                }
            }
        }
       
        // dd ($data);
        //////////////////////////////////////////////////////////////////////////
        // $stair = Tbl_stairstep::settings()->where('stairstep_slot_id', $slot_id)->first();
        // if($stair->stairstep_gv >= $stair->stairstep_required_gv)
        // {
        //     if($stair->stairstep_pv >= $stair->stairstep_required_pv)
        //     {
        //         $count = Tbl_stairstep::settings()->where('stairstep_level', '>', $stair->stairstep_settings_id)->count();
        //         if($count >= 1)
        //         {
        //             $update['stairstep_settings_id'] = $stair->stairstep_settings_id + 1;
        //             Tbl_stairstep::where('stairstep_slot_id', $slot_id)->update($update);
        //         }
        //     }
        // }
        ///////////////////////////////////////////////////////////////////////////
    }
    public static function add_stairstep($slot_id){
        $insert['stairstep_slot_id'] = $slot_id;
        $insert['stairstep_gv'] =  0;
        $insert['stairstep_pv'] =  0;
        $insert['stairstep_settings_id'] = 1;
        Tbl_stairstep::insert($insert);
    }
    public static function add_stairstep_report($stairstep_slot_id, $stairstep_settings_id, $stairstep_earned_amount){
        $insert['stairstep_slot_id']  = $stairstep_slot_id;
        $insert['stairstep_settings_id']  = $stairstep_settings_id;
        $insert['stairstep_earned_amount']  = $stairstep_earned_amount;
        $insert['stairstep_date']  = Carbon::now();
        $insert['stairstep_admin_report']  = 1;
        $insert['stairstep_pay_cheque']  = 0;
        Tbl_stairstep_report::insert($insert);
        
    }
}
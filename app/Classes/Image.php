<?php namespace App\Classes;
use Config;
use File;
use ImageLib;

class Image
{
    public static function view($filename, $size="150x150", $mode="crop")
    {
        $image_server = Config::get('app.image_server');
        $domain = $_SERVER['SERVER_NAME'];
        return $filename;
        // return $image_server . "view?source=$domain&filename=$filename&size=$size&mode=$mode";
    }
    public static function view_v2($filename, $size="150x150", $module="product")
    {
        if (!$filename) 
        {
            $filename = '/resources/assets/img/1428733091.jpg';
        }
        $file_explode = explode("/", $filename);
        $file = end($file_explode);
        $path = "/resources/assets/img/".$module."/".$size."/".$file;
        $explode_size = explode("x", $size);
        if (!File::exists(public_path() . $path)) 
        {
            if (!File::exists(public_path() . "/resources/assets/img/".$module."/".$size)) 
            {
                File::makeDirectory(public_path() . "/resources/assets/img/".$module."/".$size, 0775, true);
            }
            //EXIST
            if (File::exists(public_path().$filename)) 
            {
                $img = ImageLib::make(public_path().$filename);
                // crop the best fitting 5:3 (600x360) ratio and resize to 600x360 pixel
                $img->fit($explode_size[0], $explode_size[1]);
                
                $img->save(public_path() . $path);
            }
            else
            {
                $filename = '/resources/assets/img/1428733091.jpg';

                $img = ImageLib::make(base_path().$filename);
                // crop the best fitting 5:3 (600x360) ratio and resize to 600x360 pixel
                $img->fit($explode_size[0], $explode_size[1]);
                
                $img->save(base_path() . $path);
            }
        }

        
        return $path;
    }
    public static function get_path()
    {
        $image_server = Config::get('app.image_server');
        $domain = Image::giveHost($_SERVER['SERVER_NAME']);
        return $image_server . "uploads/$domain/image/";
    }
    public static function view_main($filename)
    {
        $image_server = Config::get('app.image_server');
        $domain = Image::giveHost($_SERVER['SERVER_NAME']);
        return $image_server . "uploads/$domain/image/$filename/$filename";
    }
    public static function giveHost($host_with_subdomain)
    {
        $array = explode(".", $host_with_subdomain);

        return (array_key_exists(count($array) - 2, $array) ? $array[count($array) - 2] : "").".".$array[count($array) - 1];
    }
}
<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_pipeline extends Model
{
    protected $table = 'tbl_pipeline';
    protected $primaryKey = 'pipeline_id';
    public $timestamps = false;

    public function scopeSlot($query)
    {
        return $query->leftJoin("tbl_slot", "tbl_slot.slot_id", "=", "tbl_pipeline.pipeline_downline");
    }
    public function scopeMembership($query)
    {
        return $query->leftJoin("tbl_membership", "tbl_membership.membership_id", "=", "tbl_slot.slot_membership");
    }
    public function scopeAccount($query)
    {
        return $query->leftJoin("tbl_account", "tbl_account.account_id", "=", "tbl_slot.slot_owner");
    }
}
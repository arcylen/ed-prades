<?php namespace App\Http\Controllers;
use App\Tbl_binary_report;
use App\Tbl_binary_breakdown;
use App\Tbl_direct_report;
use App\Tbl_pipeline_settings;
use App\Tbl_pipeline_report;
use App\Tbl_pipeline;
use App\Tbl_membership;
use App\Tbl_indirect_setting;
use App\Tbl_sequence;
use App\Tbl_total_gsb;
use App\Tbl_gsb_report;
use App\Tbl_indirect_report;
use Request;
use DB;
use Carbon\Carbon;
use App\Tbl_unilevel_setting;
use App\Tbl_unilevel_point_log;

use App\Tbl_stairstep;
use App\Tbl_stairstep_settings;
use App\Classes\Compute;
use DateTime;
use DateInterval;
use App\Tbl_ladder_rebates;
use App\Tbl_slot;

class DistributorReportController extends DistributorController
{
	public function binary()
	{
		$data["page"] = "Binary Report";
		if(isset($this->slotnow->slot_id)){
		$data["_binary"] = Tbl_binary_report::where("binary_slot_id", $this->slotnow->slot_id)->orderBy("binary_id", "desc")->get();
		}
		return view('distributor.report.binary', $data);
	}
	public function binarydetail()
	{
		$id = Request::input('id');
		$data['_detail'] = Tbl_binary_breakdown::where('binary_report_id', $id)->get();
		// dd($data);
		return view('distributor.report.detail', $data);
	}
	public function direct()
	{
		$data["page"] = "Direct Referral Report";
		if(isset($this->slotnow->slot_id)){
		$data["_direct"] = Tbl_direct_report::where("direct_sponsor", $this->slotnow->slot_id)->slotchild()->account()->orderBy("direct_id", "desc")->get();
		}
		return view('distributor.report.direct', $data);
	}
	public function gsb()
	{
		$data["page"] = "Group Sales Bonus";
		if(isset($this->slotnow->slot_id)){
			$data["_position"] = Tbl_sequence::where("sequence_id",$this->slotnow->sequence_level_id)->first();
		$data["_gsb"] = DB::table("tbl_total_gsb")->where("gsb_slot_id",$this->slotnow->slot_id)
		->where("personal_true",0)->where("total_gsb_distributed",0)->sum("total_earned");
			$data["_personal_gsb"] = DB::table("tbl_total_gsb")->where("gsb_slot_id",$this->slotnow->slot_id)->where("personal_true",1)->sum("total_earned");
			
		$data["get_total"] = Tbl_total_gsb::where("gsb_slot_id",$this->slotnow->slot_id)
															->where("total_gsb_distributed",0)
															->get();
		
		$total = 0;
			if($data["get_total"])
			{
				$total_all = null;
				foreach ($data["get_total"] as $key => $value) {
					$total_all[$key] = $value;
					$total += (($total_all[$key]->gsb_percentage/100) * $total_all[$key]->total_spv);
				}
			}
 		$data["total_gsb"] = $total;
 			
			$data["personal_gsb"] = DB::table("tbl_total_gsb")
										->join("tbl_sequence","tbl_sequence.sequence_id","=","tbl_total_gsb.sequence_level_id")
										->where("tbl_total_gsb.gsb_slot_id",$this->slotnow->slot_id)
										->where("tbl_total_gsb.personal_true",1)
										->where("tbl_total_gsb.total_gsb_distributed",0)
										->first();
									
			$data["gsb_report"] = DB::table("tbl_total_gsb")->join("tbl_sequence","tbl_sequence.sequence_id","=","tbl_total_gsb.sequence_level_id")
								->join("tbl_sequence_setting","tbl_sequence_setting.sequence_id","=","tbl_total_gsb.sequence_level_id")
								->where("gsb_slot_id",$this->slotnow->slot_id)
								->orderby("total_gsb_level","ASC")
								->where("tbl_total_gsb.personal_true",0)
								->groupby("total_gsb_level")
								->where("total_gsb_distributed",0)
								->get();

			// $data["gsb_report"]->count_no_of_slots = DB::table("tbl_gsb_report")
			// 										->join("tbl_total_gsb","tbl_total_gsb.total_gsb_id","=","tbl_gsb_report.t_gsb_id")
			// 										->where("tbl_total_gsb.personal_true",0)


		}
		return view('distributor.report.gsb', $data);
	}
	public function select_level()
	{
		$id = Request::input("id");
		$data = Tbl_gsb_report::where("t_gsb_id", $id)->where("gsb_distributed",0)->orderby("level","ASC")->get();
		// dd($data);
		return json_encode($data);
	}
	public function indirect()
	{
		$data["page"] = "Indirect Referral Report";
		$data["_level"] = Tbl_indirect_setting::max('level');
		$data["_membership"] = $_membership = Tbl_membership::where("tbl_membership.archived", 0)->orderBy("membership_price")->get()->toArray();
		$data["_indirect"] = null;
		if(isset($this->slotnow->slot_id)){
			foreach($_membership as $key => $membership)
			{
				$data["_indirect"][$membership["membership_id"]] = null;
				$_indirect_setting = Tbl_indirect_setting::where("membership_id", $membership["membership_id"])->get()->toArray();
				foreach($_indirect_setting as $key => $indirect_setting)
				{
					$data["_indirect"][$membership["membership_id"]][$indirect_setting["level"]] = null;
					$data["_indirect"][$membership["membership_id"]][$indirect_setting["level"]]["income"] = $indirect_setting["value"];
					$data["_indirect"][$membership["membership_id"]][$indirect_setting["level"]]["count"] = $count = Tbl_indirect_report::where("indirect_slot", $this->slotnow->slot_id)->where("indirect_level", $indirect_setting["level"])->count();
					$data["_indirect"][$membership["membership_id"]][$indirect_setting["level"]]["total"] = Tbl_indirect_report::where("indirect_slot", $this->slotnow->slot_id)->where("indirect_level", $indirect_setting["level"])->sum('indirect_amount');
					$data["_indirect"][$membership["membership_id"]][$indirect_setting["level"]]["total_processed"] = Tbl_indirect_report::where("indirect_slot", $this->slotnow->slot_id)->where("indirect_level", $indirect_setting["level"])->where('indirect_pay_cheque', "<>", 0)->sum('indirect_amount');
				}
			}
		}
		
		return view('distributor.report.indirect', $data);
	}
	public function unilevel()
	{
		$data["page"] = "Unilevel Report";
		$data['unilevelsettings'] = Tbl_unilevel_setting::get();
		foreach($data['unilevelsettings'] as $key => $value)
		{
			$data['unilevel_log'][$key] = Tbl_unilevel_point_log::where('unilevel_distributed', 0)
			->where('unilevel_type', 1)
			->where('unilevel_sponsor', $this->slotnow->slot_id)
			->where('unilevel_level', $value->level)
			->sum('unilevel_points');
			$data['unilevel_log_old'][$key] = Tbl_unilevel_point_log::where('unilevel_distributed','!=', 0)
			->where('unilevel_type', 1)
			->where('unilevel_sponsor', $this->slotnow->slot_id)
			->where('unilevel_level', $value->level)
			->sum('unilevel_points');
			
		}
		$data['unilevel_log_personal'] = Tbl_unilevel_point_log::where('unilevel_distributed', 0)
			->where('unilevel_type', 0)
			->where('unilevel_sponsor', $this->slotnow->slot_id)
			->where('unilevel_level', 0)
			->get();
		$data['unilevel_log_personal_old'] = Tbl_unilevel_point_log::where('unilevel_distributed', '!=', 0)
			->where('unilevel_type', 0)
			->where('unilevel_sponsor', $this->slotnow->slot_id)
			->where('unilevel_level', 0)
			->paginate(20);
		// dd($data);
		// return $this->slotnow->slot_personal_points;
		return view('distributor.report.unilevel', $data);
	}
	public function unilevelget($id)
	{
			$data['unilevel_log_get'] = Tbl_unilevel_point_log::where('unilevel_distributed', 0)
			->where('unilevel_type', 1)
			->where('unilevel_sponsor', $this->slotnow->slot_id)
			->where('unilevel_level', $id)->product()
			->get();
			foreach($data['unilevel_log_get'] as $key => $value){
				$y[$key] = $data['unilevel_log_get'][$key];
			}
			return array($y);
			return $data;
	}
	public function unilevelget_old($id)
	{
			$data['unilevel_log_get'] = Tbl_unilevel_point_log::where('unilevel_distributed','!=', 0)
			->where('unilevel_type', 1)
			->where('unilevel_sponsor', $this->slotnow->slot_id)
			->where('unilevel_level', $id)->product()
			->get()->toArray();
			// dd($data) ;
			foreach($data['unilevel_log_get'] as $key => $value){
				$y[$key] = $data['unilevel_log_get'][$key];
			}
			return array($y);
			return $data;
	}
	public function pipeline()
	{
		$data["page"] = "Unilevel Report";
		$data["_pipeline_settings"] = null;
		$_pipeline_settings = Tbl_pipeline_settings::orderBy("pipeline_order", "asc")->get();
		if(isset($this->slotnow->slot_id)){
			foreach($_pipeline_settings as $key => $pipeline_settings)
			{
				$data["_pipeline_settings"][$key] = $pipeline_settings;

				$data["_pipeline_settings"][$key]->downline_count = Tbl_pipeline::where("pipeline_upline", $this->slotnow->slot_id)->where("pipeline_level", $pipeline_settings->pipeline_level)->count();

				if($data["_pipeline_settings"][$key]->downline_count >= $pipeline_settings->pipeline_requirement)
				{
					$data["_pipeline_settings"][$key]->qualified = 1;
				}
				else
				{
					$data["_pipeline_settings"][$key]->qualified = 0;
				}
			}
		}

		return view('distributor.report.pipeline', $data);
	}
	public function stairway()
	{
		$data = [];
		if(isset($this->slotnow->slot_id))
		{
			Compute::staistep_checkif_rank_up($this->slotnow->slot_id);
			$data['settings'] = Tbl_stairstep_settings::get()->toarray();
			$data['tbl_stairstep'] = Tbl_stairstep::settings()->where('tbl_stairstep.stairstep_slot_id', '=', $this->slotnow->slot_id)->first();
			$next_level = $data['tbl_stairstep']['stairstep_settings_id'] + 1;
            $data['requirements_settings'] = Tbl_stairstep_settings::where('stairstep_level', $next_level)->first();
		}
		// dd($data);
		
		return view('distributor.report.stairway', $data);
	}
	public function samplecompute($id){
		$method = 0;
		return Compute::binary_cutoff_compute($this->slotnow);
	}
	 public static function stairstep_add_points($slot_id, $points, $method){
        $count = Tbl_stairstep::settings()->where('stairstep_slot_id', $slot_id)->count();
        if($count == 0)
        {
            $insert['stairstep_slot_id'] = $slot_id;
            $insert['stairstep_gv'] =  0;
            $insert['stairstep_pv'] =  0;
            $insert['stairstep_settings_id'] = 1;
            Tbl_stairstep::insert($insert);
        }
        $stair = Tbl_stairstep::settings()->where('stairstep_slot_id', $slot_id)->first();
        if($method == 'pv')
        {
           $update['stairstep_pv'] = $stair->stairstep_pv + $points;
        }
        else if($method == 'gv')
        {
           $update['stairstep_gv'] = $stair->stairstep_gv + $points;
        }
        Tbl_stairstep::where('stairstep_slot_id', $slot_id)->update($update);
        Compute::staistep_checkif_rank_up($slot_id);
        return false;
    }
	public function rebates(){
		// DB::table('tbl_ladder_rebates')
		//account_date_created
		$data['member'] = Tbl_slot::where('slot_id', $this->slotnow->slot_id)->account()->first();
		$count = Tbl_ladder_rebates::where('rebates_slot_id', $this->slotnow->slot_id)->count();
		if($count >= 1){
			$data['rebates'] = Tbl_ladder_rebates::where('rebates_slot_id', $this->slotnow->slot_id)->get();
			foreach($data['rebates'] as $key => $value){
				// echo date("Y-m-d");
				if($value->rebates_date_claimable ==date("Y-m-d")){
					$update['rebates_isclaimable'] = 1;
					Tbl_ladder_rebates::where('rebates_id', $value->rebates_id)->update($update);
				}
			}
		}
		else{
			$date1 = $data['member']['account_date_created'];
			$now = strtotime($date1); // or your date as well
			// $your_date = strtotime("2010-01-01");
			$your_date = strtotime("+9 months", strtotime($date1));
			$datediff = $your_date - $now;
			 // returns timestamp
			// echo date('Y-m-d',$effectiveDate);
			// echo $your_date . "<br/>";
			// echo $now . "<br/>";
			$range =  floor($datediff / (60 * 60 * 24));
			// echo $range . "<br/>";
			for($i = 0; $i <= $range; $i++){
				echo $i . "<br/>";
				$i += 44;
				$datereturn = strtotime("+".$i." days", strtotime($date1));
				
				// echo date("m/d/Y", $datereturn) . '<br/>';
				$rebates_date_claimable= date("Y-m-d", $datereturn);
				$rebates_isclaimable = 0;
				$rebates_slot_id = $this->slotnow->slot_id;
				$rebates_bonus_gp = 1;
				$rebates_date_claimed = 0;
				$insert['rebates_date_claimable'] = $rebates_date_claimable;
				$insert['rebates_isclaimable'] = $rebates_isclaimable;
				$insert['rebates_slot_id'] = $rebates_slot_id;
				$insert['rebates_bonus_gp'] = $rebates_bonus_gp;
				$insert['rebates_date_claimed'] = $rebates_date_claimed;
				Tbl_ladder_rebates::insert($insert);
			}
		}
		$data['rebates'] = Tbl_ladder_rebates::where('rebates_slot_id', $this->slotnow->slot_id)->get();


		// dd($data);
		// $data = [];
		return view('distributor.rebates.rebates', $data);
	}
	public static function rebatesclaim($id){
		$rebates = Tbl_ladder_rebates::where('rebates_id', $id)->first();
		if($rebates['rebates_isclaimable'] == 1 ){
			$update['rebates_isclaimable'] = 2;
			$updata['rebates_date_claimed'] = date("Y-m-d");
			Tbl_ladder_rebates::where('rebates_id', $id)->update($update);
			$member = Tbl_slot::where('slot_id', $rebates['rebates_slot_id'])->account()->first();
			$membergp = $member['slot_gp'];
			$update2['slot_gp'] = $rebates['rebates_bonus_gp'] + $membergp;
			Tbl_slot::where('slot_id', $rebates['rebates_slot_id'])->update($update2);
		}
		
	}
	public function convertgptocash()
	{
		
	}
}
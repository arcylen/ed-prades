<?php namespace App\Http\Controllers;
use App\Classes\Compute;
use App\Tbl_slot;
use App\Tbl_account;
use App\Tbl_pay_cheque;
use App\Tbl_pay_cheque_batch;
use App\Tbl_binary_report;
use App\Tbl_binary_breakdown;
use App\Tbl_direct_report;
use App\Tbl_indirect_report;
use App\Tbl_pipeline_report;
use App\Tbl_pay_cheque_schedule;


use App\Tbl_royalty_bonus;
use App\Tbl_gsb_report;
use App\Tbl_indirect_sb;
use App\Tbl_direct_sb;
use App\Tbl_total_gsb;
use Redirect;
use PDF;
use Datatables;
use Schema;
use Request;
use App\Globals\AdminNav;
use DB;
use App\Tbl_unilevel_point_log;
class AdminPaychequeController extends AdminController
{
    public function schedule()
    {
        $this->schedule_create_table_if_not_exist();
        $data["_schedule"] = $this->schedule_get_schedules();
        if(Request::isMethod("post"))
        {
            foreach($data["_schedule"] as $schedule)
            {
                $update["pay_schedule_mode"] = Request::input("mode")[$schedule->pay_schedule_id];
                $update["pay_schedule_montly"] = Request::input("monthly")[$schedule->pay_schedule_id];
                $update["pay_schedule_weekly"] =  Request::input("weekly")[$schedule->pay_schedule_id];
                $update["pay_schedule_time"] =  Request::input("time")[$schedule->pay_schedule_id] . ":00";
                Tbl_pay_cheque_schedule::where("pay_schedule_id", $schedule->pay_schedule_id)->update($update);
            }

            return Redirect::back();
        }
        else
        {
            $data["page"] = "Pay Schedule";
            $data["_days"] = $this->schedule_get_days();
            $data["_time"] = $this->schedule_get_time();
            return view("admin.utilities.paysched", $data);
        }
    }
    public function schedule_execute()
    {
        $schedule = Tbl_pay_cheque_schedule::where("pay_schedule_id", Request::input("sked"))->first();
        $paychequefunction = $schedule->pay_schedule_key . "_paycheque";
        Compute::$paychequefunction();
        return Redirect::back();
    }
    public function schedule_get_days()
    {
        return ["monday","tuesday","wednesday","friday","saturday","sunday"];
    }
    public function schedule_get_time()
    {
        $data["_time"][1] = "1:00 AM";
        $data["_time"][2] = "2:00 AM";
        $data["_time"][3] = "3:00 AM";
        $data["_time"][4] = "4:00 AM";
        $data["_time"][5] = "5:00 AM";
        $data["_time"][6] = "6:00 AM";
        $data["_time"][7] = "7:00 AM";
        $data["_time"][8] = "8:00 AM";
        $data["_time"][9] = "9:00 AM";
        $data["_time"][10] = "10:00 AM";
        $data["_time"][11] = "11:00 AM";
        $data["_time"][12] = "12:00 AM";
        $data["_time"][13] = "1:00 PM";
        $data["_time"][14] = "2:00 PM";
        $data["_time"][15] = "3:00 PM";
        $data["_time"][16] = "4:00 PM";
        $data["_time"][17] = "5:00 PM";
        $data["_time"][18] = "6:00 PM";
        $data["_time"][19] = "7:00 PM";
        $data["_time"][20] = "8:00 PM";
        $data["_time"][21] = "9:00 PM";
        $data["_time"][22] = "10:00 PM";
        $data["_time"][23] = "11:00 PM";
        $data["_time"][24] = "12:00 PM"; 
        return $data["_time"];
    }
    public function schedule_create_table_if_not_exist()
    {
        if (!Schema::hasTable('tbl_pay_cheque_schedule'))
        {
            Schema::create('tbl_pay_cheque_schedule', function($table)
            {
                $table->increments('pay_schedule_id');
                $table->string('pay_schedule_mode')->default('weekly');
                $table->string('pay_schedule_key');
                $table->integer('pay_schedule_montly');
                $table->string('pay_schedule_weekly');
                $table->time('pay_schedule_time');
                $table->dateTime('pay_schedule_last');
            });
        }
    }
    public function schedule_get_schedules()
    {
        /* INSERT DATA IF NOT EXIST */
        // $insert[0]["pay_schedule_mode"] = "weekly";
        // $insert[0]["pay_schedule_key"] = "binary";
        // $insert[0]["pay_schedule_montly"] = "";
        // $insert[0]["pay_schedule_weekly"] = "sunday";
        // $insert[0]["pay_schedule_time"] = "1:00:00";

        $insert[0]["pay_schedule_mode"] = "weekly";
        $insert[0]["pay_schedule_key"] = "rb";
        $insert[0]["pay_schedule_montly"] = "";
        $insert[0]["pay_schedule_weekly"] = "sunday";
        $insert[0]["pay_schedule_time"] = "1:00:00";

        $insert[1]["pay_schedule_mode"] = "weekly";
        $insert[1]["pay_schedule_key"] = "dsb";
        $insert[1]["pay_schedule_montly"] = "";
        $insert[1]["pay_schedule_weekly"] = "sunday";
        $insert[1]["pay_schedule_time"] = "2:00:00";

        $insert[2]["pay_schedule_mode"] = "weekly";
        $insert[2]["pay_schedule_key"] = "isb";
        $insert[2]["pay_schedule_montly"] = "";
        $insert[2]["pay_schedule_weekly"] = "sunday";
        $insert[2]["pay_schedule_time"] = "3:00:00";


        $insert[3]["pay_schedule_mode"] = "monthly";
        $insert[3]["pay_schedule_key"] = "gsb";
        $insert[3]["pay_schedule_montly"] = "28";
        $insert[3]["pay_schedule_weekly"] = "";
        $insert[3]["pay_schedule_time"] = "1:00:00";

        // $insert[3]["pay_schedule_mode"] = "weekly";
        // $insert[3]["pay_schedule_key"] = "pipeline";
        // $insert[3]["pay_schedule_montly"] = "";
        // $insert[3]["pay_schedule_weekly"] = "sunday";
        // $insert[3]["pay_schedule_time"] = "4:00:00";

        $insert[4]["pay_schedule_mode"] = "monthly";
        $insert[4]["pay_schedule_key"] = "royalty_bonus";
        $insert[4]["pay_schedule_montly"] = "5";
        $insert[4]["pay_schedule_weekly"] = "";
        $insert[4]["pay_schedule_time"] = "1:00:00";

        $data["_schedule"] = Tbl_pay_cheque_schedule::get();

        /* INSERT PAY SCHEDULE IF RECORD IS EMPTY */
        if($data["_schedule"]->isEmpty())
        {
            Tbl_pay_cheque_schedule::insert($insert); 
            $data["_schedule"] = Tbl_pay_cheque_schedule::get();  
        }

        return $data["_schedule"];
    }
    public function index()
    {   
        
        // 1 = not requested
        // 2 = requested
        // 3 = denied
        $data["page"] = "Pay cheque";
        $data["_account"] = Tbl_account::where("account_paycheque_total", "!=", 0)->get();
        foreach ($data["_account"] as $key => $value) {
            # code...
            $data['_proccess_id'][$key] = Tbl_slot::where("slot_owner", $value->account_id)->pluck('slot_id');
        }
       
// ->orderby('tbl_pay_cheque.pay_cheque_batch_id', 'desc')
        $data["_pay_cheque"] = Tbl_pay_cheque::batch()->where('tbl_pay_cheque.pay_cheque_requested', 2)->where('tbl_pay_cheque.pay_cheque_processed', 0)->orderby('tbl_pay_cheque.pay_cheque_batch_id', 'desc')->where('tbl_pay_cheque.pay_cheque_amount', '!=', 0)->slot()->paginate(20);
            foreach ($data["_pay_cheque"] as $key => $value) {
                $data["_pay_cheque_batch"][$key] =  Tbl_pay_cheque_batch::where('pay_cheque_batch_id', $value->pay_cheque_batch_id)->get();
            }
        $data["_pay_cheque_notrequested"] = Tbl_pay_cheque::batch()->where('tbl_pay_cheque.pay_cheque_requested', 1)->where('tbl_pay_cheque.pay_cheque_processed', 0)->orderby('tbl_pay_cheque.pay_cheque_batch_id', 'desc')->where('tbl_pay_cheque.pay_cheque_amount', '!=', 0)->slot()->get();
            foreach ($data["_pay_cheque_notrequested"] as $key => $value) {
                $data["_pay_cheque_batch_notrequested"][$key] =  Tbl_pay_cheque_batch::where('pay_cheque_batch_id', $value->pay_cheque_batch_id)->get();
            }
        $data["_pay_cheque_processed"] = Tbl_pay_cheque::batch()->where('tbl_pay_cheque.pay_cheque_processed', 1)->orderby('tbl_pay_cheque.pay_cheque_batch_id', 'desc')->where('tbl_pay_cheque.pay_cheque_amount', '!=', 0)->slot()->get();
        foreach ($data["_pay_cheque_processed"] as $key => $value) {
            $data["_pay_cheque_batch_processed"][$key] =  Tbl_pay_cheque_batch::where('pay_cheque_batch_id', $value->pay_cheque_batch_id)->get();
        }
          // dd($data);
        $data["processing_fee"] = DB::table('tbl_settings')->where('key', 'processing_fee')->pluck('value');
        if($data["processing_fee"] == null){
            $insert['key'] = "processing_fee";
            $insert['value'] = 0; 
            DB::table('tbl_settings')->insert($insert);
            $data["processing_fee"] = DB::table('tbl_settings')->where('key', 'processing_fee')->pluck('value');
        }
        $data["withholding_tax"] = DB::table('tbl_settings')->where('key', 'withholding_tax')->pluck('value');
        if($data["withholding_tax"] == null){
            $insert2['key'] = "withholding_tax";
            $insert2['value'] = 0; 
            DB::table('tbl_settings')->insert($insert2);
            $data["withholding_tax"] = DB::table('tbl_settings')->where('key', 'withholding_tax')->pluck('value');
        }
        // dd($data);
        $code = "Process Pay Cheque";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {
            // dd($data);
            // return Datatables::eloquent( Tbl_pay_cheque::batch()->where('tbl_pay_cheque.pay_cheque_requested', 1)->where('tbl_pay_cheque.pay_cheque_processed', 0))->make(true);
            return view("admin.transaction.paycheque", $data);
        }
        else
        {
            return Redirect::back();
        }

        
    }
    public function indexsubmit(){
        $update1['value']= Request::input('processing_fee');
        $update2['value'] = Request::input('withholding_tax');
        DB::table('tbl_settings')->where('key', 'processing_fee')->update($update1);
        DB::table('tbl_settings')->where('key', 'withholding_tax')->update($update2);
        return Redirect::back();
    }
    public function process($id, $slotid)
    {
        $data["pay_cheque"] = Tbl_pay_cheque::where("pay_cheque_id", $id)->batch()->first();
		// return $data["pay_cheque"];
		$data["_slot"] = null;
        $_slot = Tbl_slot::where("slot_id", $slotid)->limit(1)->get()->toArray();
        if(!empty($_slot)){
            // return $_slot;
        }
        else{
            $_slot = Tbl_slot::where("slot_sponsor", $slotid)->limit(1)->get()->toArray();
        }
        $data["owner_name"] = Tbl_slot::join("tbl_account","tbl_account.account_id","=","tbl_slot.slot_owner")->where("slot_id",$slotid)->pluck("account_name");
        // return $_slot;
        foreach($_slot as $key => $slot)
        {
            $data["_slot"][$key] = $slot;
            $_pay_cheque = Tbl_pay_cheque::where("pay_cheque_slot", $slotid)->where('pay_cheque_id', $id)->batch()->get()->toArray();
            // return $_pay_cheque;
            $data["_pay_cheque"] = null;

            /* GET DATA FOR PAYCHEQUE */
            foreach($_pay_cheque as $key2 => $pay_cheque)
            {
                $data["_pay_cheque"][$key2] = $pay_cheque;      

                // if($pay_cheque["pay_cheque_batch_reference"] == "binary")
                // {
                //     $data["_binary_report"] = null;
                //     /* GET BINARY REPORT */
                //     $_binary_report = Tbl_binary_report::where("binary_payout_paycheck", $pay_cheque["pay_cheque_id"])->get()->toArray();
                    
                //     foreach($_binary_report as $key3 => $binary_report)
                //     {
                //         /* BINARY BREAKDOWN */
                //         $_binary_breakdown = Tbl_binary_breakdown::where("binary_report_id", $binary_report["binary_id"])->slot()->account()->get()->toArray();
                //         $data["_binary_report"][$key3] = $binary_report;
                //         $data["_binary_report"][$key3]["breakdown"] = $_binary_breakdown;
                //     }

                //     $data["_pay_cheque"][$key2]["_binary_report"] = $data["_binary_report"];
                // }
                // elseif($pay_cheque["pay_cheque_batch_reference"] == "direct")
                // {
                //     $data["_direct_report"] = null;
                //     $data["_direct_report"] = Tbl_direct_report::where("direct_pay_cheque", $pay_cheque["pay_cheque_id"])->slotchild()->account()->get()->toArray();
                //     $data["_pay_cheque"][$key2]["_direct_report"] = $data["_direct_report"];
                // }
                // elseif($pay_cheque["pay_cheque_batch_reference"] == "indirect")
                // {
                //     $data["_indirect_report"] = null;
                //     $data["_indirect_report"] = Tbl_indirect_report::where("indirect_pay_cheque", $pay_cheque["pay_cheque_id"])->slotregistree()->account()->get()->toArray();
                //     $data["_pay_cheque"][$key2]["_indirect_report"] = $data["_indirect_report"];
                // }
                // elseif($pay_cheque["pay_cheque_batch_reference"] == "pipeline")
                // {
                //     $data["_pipeline_report"] = null;
                //     $data["_pipeline_report"] = Tbl_pipeline_report::where("pipeline_pay_cheque", $pay_cheque["pay_cheque_id"])->settings()->get()->toArray();
                //     $data["_pay_cheque"][$key2]["_pipeline_report"] = $data["_pipeline_report"];
                // }
                // elseif($pay_cheque["pay_cheque_batch_reference"] == "unilevel")
                // {
                //     $data["_indirect_report"] = null;
                //     $data["_indirect_report"] = Tbl_unilevel_point_log::where("unilevel_distributed", $pay_cheque["pay_cheque_id"])->where('unilevel_type', 1)->slotregistree()->account()->membership()->get()->toArray();
                //     $data["_pay_cheque"][$key2]["_unilevel_report"] = $data["_indirect_report"];
                // }

                if($pay_cheque["pay_cheque_batch_reference"] == "Referral Bonus")
                {
                    $data["_direct_report"] = null;
                    $data["_direct_report"] = Tbl_direct_report::where("direct_pay_cheque", $pay_cheque["pay_cheque_id"])->slotchild()->account()->get()->toArray();
                    $data["_pay_cheque"][$key2]["_direct_report"] = $data["_direct_report"];
                }
                elseif($pay_cheque["pay_cheque_batch_reference"] == "Direct Sales Bonus")
                {
                    $data["_dsb_report"] = null;
                    $data["_dsb_report"] = Tbl_direct_sb::where("dsb_paycheque", $pay_cheque["pay_cheque_id"])->slotchild()->account()->get()->toArray();
                    $data["_pay_cheque"][$key2]["_dsb_report"] = $data["_dsb_report"];
                }
                elseif($pay_cheque["pay_cheque_batch_reference"] == "Indirect Sales Bonus")
                {
                    $data["_isb_report"] = null;
                    $data["_isb_report"] = Tbl_indirect_sb::where("isb_paycheque", $pay_cheque["pay_cheque_id"])->slotchild()->account()->get()->toArray();
                    $data["_pay_cheque"][$key2]["_isb_report"] = $data["_isb_report"];
                }   
                elseif($pay_cheque["pay_cheque_batch_reference"] == "Royalty Bonus")
                {
                    $data["_rb_report"] = null;
                    $data["_rb_report"] = Tbl_royalty_bonus::where("rb_paycheque", $pay_cheque["pay_cheque_id"])->slotchild()->account()->get()->toArray();
                    $data["_pay_cheque"][$key2]["_rb_report"] = $data["_rb_report"];
                }               
                elseif($pay_cheque["pay_cheque_batch_reference"] == "Group Sales Bonus")
                {
                    $data["_gsb_report"] = null;
                    $data["_gsb_report"] = Tbl_total_gsb::where("gsb_paycheque", $pay_cheque["pay_cheque_id"])->slot()->groupsalesbonus()->account()->orderby("total_gsb_level","ASC")->get()->toArray();

                    $data["_gsbreport"] = Tbl_total_gsb::where("gsb_paycheque", $pay_cheque["pay_cheque_id"])->slot()->groupsalesbonus()->account()->orderby("total_gsb_level","ASC")->first();
                    $data["position"] = $data["_gsbreport"]->sequence_name;

                    $data["_gsb_history"] = Tbl_gsb_report::where("gsb_distributed", $pay_cheque["pay_cheque_id"])->slot()->account()->orderby("level","ASC")->get()->toArray();
                    $data["_pay_cheque"][$key2]["_gsb_report"] = $data["_gsb_report"];
                    $data["_pay_cheque"][$key2]["_gsb_history"] = $data["_gsb_history"];
                }
            }

            $data["_slot"][$key]["_pay_cheque"] = $data["_pay_cheque"];
            // dd($data);//
        }
        $data['payment_option'] = DB::table('tbl_pay_cheque_payment_options')->where('pay_cheque_id', $id)->first();
        // dd($data);
        // return view("admin.transaction.paycheque_voucher", $data);
        $pdf = PDF::loadView("admin.transaction.paycheque_voucher", $data);
                    return $pdf->stream('Paycheque.pdf');
                    return Redirect::to('/admin/transaction/paycheque');

    }
    public function processsingle($id)
    {
        
        $data["_pay_cheque_processed"] = Tbl_pay_cheque::batch()->where('tbl_pay_cheque.pay_cheque_processed', 1)->where('tbl_pay_cheque.pay_cheque_id', $id)->limit(1)->get()->toArray();
        if($data["_pay_cheque_processed"][0]['pay_cheque_batch_id']){
            $data["_pay_cheque_batch_processed"] = Tbl_pay_cheque_batch::where('pay_cheque_batch_id',$data["_pay_cheque_processed"][0]['pay_cheque_batch_id'])->get()->toArray();
        } 
        // dd( $data);
        $pdf = PDF::loadView("admin.transaction.paycheque_voucher_single", $data);
                    return $pdf->stream('Paycheque.pdf');
                    return Redirect::to('/admin/transaction/paycheque');

    }
    public function binary()
    {
        Compute::binary_paycheque();
        Compute::direct_paycheque();
        Compute::indirect_paycheque();
        Compute::pipeline_paycheque();
        return Redirect::back()->send();
    }
    
    public function unilevel()
    {
        return Redirect::back()->send();
    }
    public function paychequeprocess($id)
    {
        $update['pay_cheque_processed'] = 1;
        $update['pay_cheque_requested'] = 2;
        $data["processing_fee"] = DB::table('tbl_settings')->where('key', 'processing_fee')->pluck('value');
        $data["withholding_tax"] = DB::table('tbl_settings')->where('key', 'withholding_tax')->pluck('value');
        $sum =  Tbl_pay_cheque::where('pay_cheque_id', $id)->pluck('pay_cheque_amount');
        $with = $sum * $data["withholding_tax"] /100;
        $sum = $sum - $with;
        $sum2 = $sum - $data["processing_fee"];
        $update['pay_cheque_amount'] = $sum2;
        // return $sum2;
        Tbl_pay_cheque::where('pay_cheque_id', $id)->update($update);
        $update2['processing_fee'] =    $data["processing_fee"];
        $update2['withholding_tax'] =   $with;
        DB::table('tbl_pay_cheque_payment_options')->where('pay_cheque_id', $id)->update($update2);
        return Redirect::back();
        

        $_pay_cheque = Tbl_pay_cheque::where("pay_cheque_slot",$id)->batch()->get()->toArray();
        // dd($_pay_cheque);
        $data["_pay_cheque"] = null;
        foreach($_pay_cheque as $key2 => $pay_cheque)
        {
            $data["_pay_cheque"][$key2] = $pay_cheque;
            $update['pay_cheque_processed'] = 1;
            Tbl_pay_cheque::where("pay_cheque_slot", $id)->where('pay_cheque_id', $pay_cheque['pay_cheque_id'])->update($update);
        }  
        return Redirect::back();  
    }
    public function deny($id){
        $update['pay_cheque_processed'] = 0;
        $update['pay_cheque_requested'] = 1;
        Tbl_pay_cheque::where('pay_cheque_id', $id)->update($update);
        return Redirect::back();
    }
}
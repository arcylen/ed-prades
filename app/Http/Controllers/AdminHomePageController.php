<?php namespace App\Http\Controllers;
use DB;
use Redirect;
use Request;
use App\Classes\Image;
use App\Tbl_about;
use \Input as Input;

use App\Globals\AdminNav;
class AdminHomePageController extends AdminController
{
	public function index()
	{
		$id = Request::input("id");
		$frheader = DB::table("tbl_about")->where("about_name", "First Row Header")->first();
		$frcontent = DB::table("tbl_about")->where("about_name", "First Row Content")->first();
		$srleft = DB::table("tbl_about")->where("about_name", "Second Row Left Header")->first();
		$srright = DB::table("tbl_about")->where("about_name", "Second Row Right Header")->first();
		$srleftcontent = DB::table("tbl_about")->where("about_name", "Second Row Left Content")->first();
		$srrightcontent = DB::table("tbl_about")->where("about_name", "Second Row Right Content")->first();
		// dd($data);
		if($frheader == null)
		{
			$a["about_name"] = "First Row Header";
			$a["about_description"] = "";
			DB::table("tbl_about")->insert($a);
			$data["frheader"] = DB::table("tbl_about")->where("about_name", "First Row Header")->first();
		}
		else
		{
			$data["frheader"] = $frheader;
		}
		if($frcontent == null)
		{
			$a["about_name"] = "First Row Content";
			$a["about_description"] = "";
			DB::table("tbl_about")->insert($a);
			$data["frcontent"] = DB::table("tbl_about")->where("about_name", "First Row Content")->first();
		}
		else
		{
			$data["frcontent"] = $frcontent;
		}
		if($srleft == null)
		{
			$a["about_name"] = "Second Row Left Header";
			$a["about_description"] = "";
			DB::table("tbl_about")->insert($a);
			$data["srleft"] = DB::table("tbl_about")->where("about_name", "Second Row Left")->first();
		}
		else
		{
			$data["srleft"] = $srleft;
		}
		if($srright == null)
		{
			$a["about_name"] = "Second Row Right Header";
			$a["about_description"] = "";
			DB::table("tbl_about")->insert($a);
			$data["srright"] = DB::table("tbl_about")->where("about_name", "Second Row Right")->first();
		}
		else
		{
			$data["srright"] = $srright;
		}
		if($srleftcontent == null)
		{
			$a["about_name"] = "Second Row Left Content";
			$a["about_description"] = "";
			DB::table("tbl_about")->insert($a);
			$data["srleftcontent"] = DB::table("tbl_about")->where("about_name", "Second Row Left Content")->first();
		}
		else
		{
			$data["srleftcontent"] = $srleftcontent;
		}
		if($srrightcontent == null)
		{
			$a["about_name"] = "Second Row Right Content";
			$a["about_description"] = "";
			DB::table("tbl_about")->insert($a);
			$data["srrightcontent"] = DB::table("tbl_about")->where("about_name", "Second Row Right Content")->first();
		}
		else
		{
			$data["srrightcontent"] = $srrightcontent;
		}
		
		
		
		$image1 = DB::table("tbl_front_slider")->where("slider_number", "1")->first();
		$image2 = DB::table("tbl_front_slider")->where("slider_number", "2")->first();
		$image3 = DB::table("tbl_front_slider")->where("slider_number", "3")->first();
		$image4 = DB::table("tbl_front_slider")->where("slider_number", "4")->first();
		$image5 = DB::table("tbl_front_slider")->where("slider_number", "5")->first();
		$image6 = DB::table("tbl_front_slider")->where("slider_number", "6")->first();
		$image7 = DB::table("tbl_front_slider")->where("slider_number", "7")->first();
		$image8 = DB::table("tbl_front_slider")->where("slider_number", "8")->first();
		
		if($image1 == null)
		{
			$a["slider_number"] = "1";
			$a["slider_image"] = "0";
			DB::table("tbl_front_slider")->insert($a);
			$data["one"] = DB::table("tbl_front_slider")->where("slider_number", "1")->first();
		}
		else
		{
			$data["one"] = $image1;
		}
		if($image2 == null)
		{
			$a["slider_number"] = "2";
			$a["slider_image"] = "0";
			DB::table("tbl_front_slider")->insert($a);
			$data["two"] = DB::table("tbl_front_slider")->where("slider_number", "2")->first();
		}
		else
		{
			$data["two"] = $image2;
		}
		if($image3 == null)
		{
			$a["slider_number"] = "3";
			$a["slider_image"] = "0";
			DB::table("tbl_front_slider")->insert($a);
			$data["three"] = DB::table("tbl_front_slider")->where("slider_number", "3")->first();
		}
		else
		{
			$data["three"] = $image3;
		}
		if($image4 == null)
		{
			$a["slider_number"] = "4";
			$a["slider_image"] = "0";
			DB::table("tbl_front_slider")->insert($a);
			$data["four"] = DB::table("tbl_front_slider")->where("slider_number", "4")->first();
		}
		else
		{
			$data["four"] = $image4;
		}
		if($image5 == null)
		{
			$a["slider_number"] = "5";
			$a["slider_image"] = "0";
			DB::table("tbl_front_slider")->insert($a);
			$data["five"] = DB::table("tbl_front_slider")->where("slider_number", "5")->first();
		}
		else
		{
			$data["five"] = $image5;
		}
		if($image6 == null)
		{
			$a["slider_number"] = "6";
			$a["slider_image"] = "0";
			DB::table("tbl_front_slider")->insert($a);
			$data["six"] = DB::table("tbl_front_slider")->where("slider_number", "6")->first();
		}
		else
		{
			$data["six"] = $image6;
		}
		if($image7 == null)
		{
			$a["slider_number"] = "7";
			$a["slider_image"] = "0";
			DB::table("tbl_front_slider")->insert($a);
			$data["seven"] = DB::table("tbl_front_slider")->where("slider_number", "7")->first();
		}
		else
		{
			$data["seven"] = $image7;
		}
		if($image8 == null)
		{
			$a["slider_number"] = "8";
			$a["slider_image"] = "0";
			DB::table("tbl_front_slider")->insert($a);
			$data["eight"] = DB::table("tbl_front_slider")->where("slider_number", "8")->first();
		}
		else
		{
			$data["eight"] = $image8;
		}
		
		// dd($data);
		$code = "About Us";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {       
        return view('admin.content.homepage', $data);
        }
        else
        {
            return Redirect::back();
        } 
	}
	public function submit()
	{
		$frheader = Request::input("frheaderid");
		$frcontent = Request::input("frcontentid");
		$srleft = Request::input("srleftid");
		$srright = Request::input("srrightid");
		$srleftcontent = Request::input("srleftcontentid");
		$srrightcontent = Request::input("srrightcontentid");
		
		$frheaders = Request::input("frheader");
		$frcontents = Request::input("frcontent");
		$srlefts = Request::input("srleft");
		$srrights = Request::input("srright");
		$srleftcontents = Request::input("srleftcontent");
		$srrightcontents = Request::input("srrightcontent");
		
		
		$date = date('Y-m-d H:i:s');
		
		DB::table("tbl_about")->where("about_id", $frheader)->update(['about_description' => $frheaders, 'created_at' => $date, 'updated_at' => $date]);
		DB::table("tbl_about")->where("about_id", $frcontent)->update(['about_description' => $frcontents, 'created_at' => $date, 'updated_at' => $date]);
		DB::table("tbl_about")->where("about_id", $srleft)->update(['about_description' => $srlefts, 'created_at' => $date, 'updated_at' => $date]);
		DB::table("tbl_about")->where("about_id", $srright)->update(['about_description' => $srrights, 'created_at' => $date, 'updated_at' => $date]);
		DB::table("tbl_about")->where("about_id", $srleftcontent)->update(['about_description' => $srleftcontents, 'created_at' => $date, 'updated_at' => $date]);
		DB::table("tbl_about")->where("about_id", $srrightcontent)->update(['about_description' => $srrightcontents, 'created_at' => $date, 'updated_at' => $date]);
		
		// IMAGE SLIDER //
		$file1 = Input::file('addimage');
		$file2 = Input::file('addimage2');
		$file3 = Input::file('addimage3');
		$file4 = Input::file('addimage4');
		$file5 = Input::file('addimage5');
		$file6 = Input::file('addimage6');
		$file7 = Input::file('addimage7');
		$file8 = Input::file('addimage8');
		// dd($file1);
		if($file1 != null)
		{
			$image1 = 'public/images/'.$file1->getClientOriginalName() . '';
		}
		else
		{
			$image1 = Request::input("1");
		}
		if($file2 != null)
		{
			$image2 = 'public/images/'.$file2->getClientOriginalName() . '';
		}
		else
		{
			$image2 = Request::input("2");
		}
		if($file3 != null)
		{
			$image3 = 'public/images/'.$file3->getClientOriginalName() . '';
		}
		else
		{
			$image3 = Request::input("3");
		}
		if($file4 != null)
		{
			$image4 = 'public/images/'.$file4->getClientOriginalName() . '';
		}
		else
		{
			$image4 = Request::input("4");
		}
		if($file5 != null)
		{
			$image5 = 'public/images/'.$file5->getClientOriginalName() . '';
		}
		else
		{
			$image5 = Request::input("5");
		}
		if($file6 != null)
		{
			$image6 = 'public/images/'.$file6->getClientOriginalName() . '';
		}
		else
		{
			$image6 = Request::input("6");
		}
		if($file7 != null)
		{
			$image7 = 'public/images/'.$file7->getClientOriginalName() . '';
		}
		else
		{
			$image7 = Request::input("7");
		}
		if($file8 != null)
		{
			$image8 = 'public/images/'.$file8->getClientOriginalName() . '';
		}
		else
		{
			$image8 = Request::input("8");
		}
		// dd($image1);
		$i['slider_image'] = $image1;
		DB::table("tbl_front_slider")->where("slider_number", 1)->update($i);
		
		$j['slider_image'] = $image2;
		DB::table("tbl_front_slider")->where("slider_number", 2)->update($j);
		
		$a['slider_image'] = $image3;
		DB::table("tbl_front_slider")->where("slider_number", 3)->update($a);
		
		$s['slider_image'] = $image4;
		DB::table("tbl_front_slider")->where("slider_number", 4)->update($s);
		
		$d['slider_image'] = $image5;
		DB::table("tbl_front_slider")->where("slider_number", 5)->update($d);
		
		$f['slider_image'] = $image6;
		DB::table("tbl_front_slider")->where("slider_number", 6)->update($f);
		
		$g['slider_image'] = $image7;
		DB::table("tbl_front_slider")->where("slider_number", 7)->update($g);
		
		$h['slider_image'] = $image8;
		DB::table("tbl_front_slider")->where("slider_number", 8)->update($h);
		
        return Redirect::to("/admin/content/homepage");
	}


	public function super_test()
	{
		return "super_test";
	}
	public function termsandagreement(){
		$data['terms'] = DB::table("tbl_about")->where("about_name", "terms")->first();
		// dd($data);
		if($data['terms'] == null)
		{
			$a["about_name"] = "terms";
			$a["about_description"] = '';
			DB::table("tbl_about")->insert($a);
			$data["terms"] = DB::table("tbl_about")->where("about_name", "terms")->first();
		}
		// dd($data);
		return view('admin.content.terms', $data);
	}
	public function sumbitterms(){
		
		$frheader = 'terms';
		$terms = Request::input("terms");
		// return $terms;
		$date = date('Y-m-d H:i:s');
		DB::table("tbl_about")->where("about_name", 'terms')->update(['about_description' => $terms, 'created_at' => $date, 'updated_at' => $date]);
		return Redirect::to("/admin/content/terms");
	}
	
}
<?php namespace App\Http\Controllers;
use Session;
use Request;
use App\Tbl_product;
use App\Tbl_product_discount;
use App\Classes\Product;
use App\Classes\Customer;
use DB;
class DistributorProductController extends DistributorController
{
	public function index()
	{	
		$data['_product'] = "";
		if(isset($this->slotnow->slot_membership))
		{
			$data["membership"] = DB::table("tbl_membership")->where("membership_id", $this->slotnow->slot_membership)->first();
			$_product = Tbl_product::where('archived',0)->get();

			if($_product)
			{
				foreach ($_product as $key => $value)
				{
					$discount = Tbl_product_discount::where("product_id", $value->product_id)->where("membership_id", $data["membership"]->membership_id)->first();
					$data['_product'][$key] = $value;

					if($discount)
					{
						$data['_product'][$key]->discount = $discount->discount;
						$data['_product'][$key]->discounted_price = $value->price - ($value->price * ($discount->discount / 100));
					}
					else
					{
						$data['_product'][$key]->discount = 0;
						$data['_product'][$key]->discounted_price = $value->price;
					}

					
				}
			}
			$data['slotid'] = $this->slotnow->slot_id;
		}
		
		
		
		// dd($data);
        return view('distributor.product.product', $data);
	}
}
<?php namespace App\Http\Controllers;
use App\Tbl_pay_cheque_schedule;
use App\Classes\Compute;
use Carbon\Carbon;
use Mail;

class ScheduleController extends Controller
{
	public $url;
	public function paycheque()
	{
		
        Compute::check_if_active();
        Compute::check_ifrank_up();
        Compute::royalty_rank_up();
        
		$notify = false;
		ob_start();
		$this->url = strtoupper($_SERVER['HTTP_HOST']);
		$url = 'http://' . $_SERVER['HTTP_HOST'];
		echo "< h3>PAY CHEQUE PROCESSING SCHEDULE<br> " . Carbon::now()->format('F d Y @ h:i A') . "</h3>";
		echo "Source is <a target='_blank' href=" . $url .  ">" . $url . "</a><br><br>";
		$data["_schedule"] = Tbl_pay_cheque_schedule::get();
		foreach($data["_schedule"] as $schedule)
		{
			echo "Checking Schedule for <b>" . strtoupper($schedule->pay_schedule_key) . "</b><br>";

			if($schedule->pay_schedule_mode == "weekly")
			{
				echo "- Confirmed Schedule is weekly every " . ucfirst($schedule->pay_schedule_weekly) ." around " .  date("h:i A", strtotime($schedule->pay_schedule_time)) . "<br>";
				echo "- Checking Day Requirement (Today is " . Carbon::now()->format('l') . ")<br>";
				if(strtolower(Carbon::now()->format('l')) == $schedule->pay_schedule_weekly)
				{
					echo "- Requirement is met since today is " . Carbon::now()->format('l') . "<br>";
					echo "- Checking if time requirement has been met. Time requirement is around " .  date("H", strtotime($schedule->pay_schedule_time)) . " and current time is " . Carbon::now()->format('H') . " <br>";
					if(Carbon::now()->format('H') == date("H", strtotime($schedule->pay_schedule_time)))
					{
						echo "- Time requirement has been meet!" . "<br>";
						echo "- Checking if code was already executed today. Last execution date is ". date("F, d Y", strtotime($schedule->pay_schedule_last)) . " and today is " . Carbon::now()->format('F, d Y') . "<br>";
						if(date("F, d Y", strtotime($schedule->pay_schedule_last)) == Carbon::now()->format('F, d Y'))	
						{
							echo "<span style='color: red;'>- Script can only be run once a day.</span><br>";
						}
						else
						{
					        $paychequefunction = $schedule->pay_schedule_key . "_paycheque";
					        Compute::$paychequefunction();
							echo "<span style='color: green;'>- <b>Success!</b> Script has been successfully executed for pay cheque of " . $schedule->pay_schedule_key . ".</span><br>";
						
							$notify = true;
						}
					}
					else
					{
						echo "<span style='color: red;'>- Time requirement is not met.</span><br>";
					}
				}
				else
				{
					echo "<span style='color: red;'>- Requirement is not met since today is " . Carbon::now()->format('l') . "</span><br>";
				}
			}
			elseif($schedule->pay_schedule_mode == "monthly")
			{
				echo "- Confirmed Schedule is monthly every " . ordinal($schedule->pay_schedule_montly) ." of every month around " .  date("h:i A", strtotime($schedule->pay_schedule_time)) . "<br>";
				echo "- Today is the " . ordinal(Carbon::now()->format('d')) . " of the month and the requirement is the " . ordinal($schedule->pay_schedule_montly) . " of the month.<br>";

				if(ordinal(Carbon::now()->format('d')) == ordinal($schedule->pay_schedule_montly))
				{
					echo "- Checking if time requirement has been met. Time requirement is around " .  date("H", strtotime($schedule->pay_schedule_time)) . " and current time is " . Carbon::now()->format('H') . " <br>";
					if(Carbon::now()->format('H') == date("H", strtotime($schedule->pay_schedule_time)))
					{
						echo "- Time requirement has been meet!" . "<br>";
						echo "- Checking if code was already executed today. Last execution date is ". date("F, d Y", strtotime($schedule->pay_schedule_last)) . " and today is " . Carbon::now()->format('F, d Y') . "<br>";
						if(date("F, d Y", strtotime($schedule->pay_schedule_last)) == Carbon::now()->format('F, d Y'))	
						{
							echo "<span style='color: red;'>- Script can only be run once a day.</span><br>";
						}
						else
						{
					        $paychequefunction = $schedule->pay_schedule_key . "_paycheque";
					        Compute::$paychequefunction();
							echo "<span style='color: green;'>- <b>Success!</b> Script has been successfully executed for pay cheque of " . $schedule->pay_schedule_key . ".</span><br>";
						
							$notify = true;
						}
					}
					else
					{
						echo "<span style='color: red;'>- Time requirement is not met.</span><br>";
					}
				}
				else
				{
					echo "<span style='color: red;'>- Requirement is not met since today is " . ordinal(Carbon::now()->format('d')) . " of the month.</span><br>";
				}
			}
			else
			{
				echo "- Confirmed Schedule is  everyday around " .  date("h:i A", strtotime($schedule->pay_schedule_time)) . "<br>";
				echo "- Checking if time requirement has been met. Time requirement is around " .  date("H", strtotime($schedule->pay_schedule_time)) . " and current time is " . Carbon::now()->format('H') . " <br>";
				if(Carbon::now()->format('H') == date("H", strtotime($schedule->pay_schedule_time)))
				{
					echo "- Time requirement has been meet!" . "<br>";
					echo "- Checking if code was already executed today. Last execution date is ". date("F, d Y", strtotime($schedule->pay_schedule_last)) . " and today is " . Carbon::now()->format('F, d Y') . "<br>";
					if(date("F, d Y", strtotime($schedule->pay_schedule_last)) == Carbon::now()->format('F, d Y'))	
					{
						echo "<span style='color: red;'>- Script can only be run once a day.</span><br>";
					}
					else
					{
				        $paychequefunction = $schedule->pay_schedule_key . "_paycheque";
				        Compute::$paychequefunction();
						echo "<span style='color: green;'>- <b>Success!</b> Script has been successfully executed for pay cheque of " . $schedule->pay_schedule_key . ".</span><br>";
						$notify = true;	
					}
				}
				else
				{
					echo "<span style='color: red;'>- Time requirement is not met.</span><br>";
				}
			}

			echo "<br>";
		}
		
		$notify = $this->binary_daily($notify);

		$html = ob_get_contents();
		ob_end_clean();

		$config = array('indent' => TRUE, 'output-xhtml' => TRUE); 
		$prettyhtml = tidy_parse_string($html, $config, 'UTF8');
		$data["mail_content"] = $prettyhtml;
		
		if($notify == true)
		{
			Mail::send("admin.schedule", $data, function ($message)
			{
			    $message->from('noreply@esharetrading.net', $this->url);
			    $message->subject("Paycheque Notification");
			    $message->to('digimanotifications@gmail.com');
			});
		}
		else
		{
			echo $data["mail_content"];	
		}
	}	
	public function binary_daily($notify)
	{
		$time_requirement = 0;
		echo "Checking Schedule for <b> DAILY BINARY PROCESSING.</b><br>";
		echo "Schedule is 12:00 AM everyday.<br>";
		echo "- Checking if time requirement has been met. Time requirement is around " . $time_requirement . " and current time is " . Carbon::now()->format('H') . " <br>";
		if(Carbon::now()->format('H') == $time_requirement)
		{
			echo "- Time requirement has been meet!" . "<br>";
	        Compute::binary_cutoff();
			echo "<span style='color: green;'>- <b>Success!</b> Script has been successfully executed for DAILY BINARY CUTOFF.</span><br>";
			$notify = true;	
		}
		else
		{
			echo "<span style='color: red;'>- Time requirement is not met.</span><br>";
		}
		
		return $notify;
	}
	public function royalty_bonus()
	{
		Compute::royalty_bonus();
	}
}
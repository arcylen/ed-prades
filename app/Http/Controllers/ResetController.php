<?php namespace App\Http\Controllers;
use Request;
use DB;
use Crypt;
use App\Classes\Compute;
use Carbon\Carbon;   

class ResetController extends Controller
{
    public function index()
    {
        // $value =Crypt::decrypt("eyJpdiI6IitZbFVMMnpPelRvUDR4VWNtNWRqUVE9PSIsInZhbHVlIjoidkVWbWZBRmhwdkhaRFNmMHU0UlgxUT09IiwibWFjIjoiZDZlN2QxMmNjMTUzYjYwYmY2MjBiZDJlYmQ2MzgzZTgxOTAwZTQzNWQ3NzU0ZmIwNzk4ZmNkYTg4MGQzYmYyMSJ9");
        // return $value;
        // Compute::check_ifrank_up();
        // Compute::royalty_rank_up();
        Compute::royalty_bonus();


        //SHORTCUT
        // $up["active_member"] = 1;
        // $up["consultant_unilevel"] = 2;
        // DB::table("tbl_slot")->where("sequence_level_id",5)->update($up);        
        return view('reset.reset');
    }

    public function execute()
    {
        if(Request::input("password") == "0SlO051O")
        {
            /* tbl_account  */
            DB::table("tbl_voucher")->delete();
            DB::table("tbl_voucher")->delete();
            DB::table("tbl_account")->delete();
            DB::table("tbl_unilevel_point_log")->delete();
            //  DB::table("tbl_indirect_report")->delete();
            
            $tbl_account["account_id"] = 1;
            $tbl_account["account_name"] = settings("company_name");
            $tbl_account["account_email"] = "gtplus.net@gmail.com";
            $tbl_account["account_username"] = "developer";
            $tbl_account["account_password"] = Crypt::encrypt("water123");
            $tbl_account["account_date_created"] = Carbon::now();;
            DB::table("tbl_account")->insert($tbl_account);

            /* tbl_membership  */
            DB::table("tbl_membership")->delete();
            $tbl_membership["membership_id"] = 1;
            $tbl_membership["membership_name"] = "Entry";
            $tbl_membership["membership_price"] = 1500;
            $tbl_membership["membership_binary_points"] = 1;
            $tbl_membership["membership_required_pv_sales"] = 1500;
            $tbl_membership["membership_required_pv"] = 1500;
            $tbl_membership["max_pairs_per_day"] = 8;
            $tbl_membership["membership_required_upgrade"] = 1500;
            $tbl_membership["membership_repurchase_level"] = 2;
            $tbl_membership["membership_direct_sponsorship_bonus"] = 500;
            DB::table("tbl_membership")->insert($tbl_membership);

            /* tbl_admin_position  */
            DB::table("tbl_admin_position")->delete();
            $tbl_admin_position["admin_position_id"] = 1;
            $tbl_admin_position["admin_position_name"] = "developer";
            $tbl_admin_position["admin_position_rank"] = 0;
            $tbl_admin_position["admin_position_module"] = 0;
            DB::table("tbl_admin_position")->insert($tbl_admin_position);

            /* tbl_admin  */
            DB::table("tbl_admin")->delete();
            $tbl_admin["admin_id"] = 1;
            $tbl_admin["account_id"] = 1;
            $tbl_admin["admin_position_id"] = 1;
            DB::table("tbl_admin")->insert($tbl_admin);

            /* tbl_rank  */
            DB::table("tbl_rank")->delete();
            $tbl_rank["rank_id"] = 1;
            $tbl_rank["rank_name"] = "Distributor";
            $tbl_rank["rank_level"] = 1;
            DB::table("tbl_rank")->insert($tbl_rank);

            /* tbl_slot  */
            DB::table("tbl_slot")->delete();
            $tbl_slot["slot_id"] = 1;
            $tbl_slot["slot_owner"] = 1;
            $tbl_slot["slot_membership"] = 1;
            $tbl_slot["slot_rank"] = 1;
            $tbl_slot["slot_sponsor"] = 2147483647;
            $tbl_slot["slot_placement"] = 2147483647;
            DB::table("tbl_slot")->insert($tbl_slot);
            DB::statement("ALTER TABLE tbl_slot AUTO_INCREMENT = 2");
 
            /* tbl_code_type  */
            DB::table("tbl_code_type")->delete();
            DB::statement("INSERT INTO `tbl_code_type` (`code_type_id`, `code_type_name`) VALUES(1,	'Paid Slot');");
            //  DB::statement("INSERT INTO `tbl_code_type` (`code_type_id`, `code_type_name`) VALUES(1,	'Paid Slot'), (2,	'Free Slot'), (3,	'Comission Deductable');");
            /* tbl_code_type  */
            DB::table("tbl_inventory_update_type")->delete();
            DB::statement("INSERT INTO `tbl_inventory_update_type` (`inventory_update_type_id`, `inventory_update_type_name`) VALUES (1,	'Claimable Voucher'), (2,	'Deduct Right Away'), (3,	'No Inventory Update');");
 
            /* tbl_shipping_type  */
            DB::table("tbl_shipping_type")->delete();
            $tbl_shipping_type["id"] = 1;
            $tbl_shipping_type["shipping_type_name"] = "Pick Up";
            DB::table("tbl_shipping_type")->insert($tbl_shipping_type);


            /* tbl_tree_placement */
            DB::table("tbl_tree_placement")->delete();

            /* tbl_tree_sponsor */
            DB::table("tbl_tree_sponsor")->delete();

            /* tbl_royalty_bonus */
            DB::table("tbl_royalty_bonus")->delete();

            /* tbl_sequence */
            // DB::table("tbl_sequence_setting")->delete();
            DB::table("tbl_indirect_unilevel_setting")->delete();
            DB::table("tbl_direct_unilevel_setting")->delete();
            
            DB::table("tbl_log_rank_up_history")->delete();
            
            DB::table("tbl_gsb_report")->delete();
            DB::table("tbl_total_gsb")->delete();
            
            // DB::table("tbl_sequence")->delete();
            // DB::statement("INSERT INTO `tbl_sequence` (`sequence_id`, `sequence_name`, `required_spv`, `required_personal_spv`, `level_rep`, `updated_at`, `type`, `active_consultant`, `active_manager`, `active_assoc_director`, `active_director`, `personal_bonus`) VALUES
            // (1,	'Consultant',	10000,	0,	5,	'2016-10-05 03:18:39',	3,	0,	0,	0,	0,	100),
            // (2,	'Manager',	15000,	750,	5,	'2016-10-05 03:19:15',	3,	3,	0,	0,	0,	15),
            // (3,	'Associate Director',	45000,	2500,	5,	'2016-10-05 03:20:15',	3,	2,	3,	0,	0,	20),
            // (4,	'Director',	140000,	7000,	5,	'2016-10-05 03:21:12',	3,	0,	3,	2,	0,	25);");

            // DB::statement("INSERT INTO `tbl_sequence_setting` (`set_id`, `seq_level`, `seq_value`, `sequence_id`) VALUES
            // (1,	1,	20,	1);");

            DB::table("tbl_direct_unilevel")->delete();
            DB::statement("INSERT INTO `tbl_direct_unilevel` (`sequence_id`, `sequence_name`, `required_spv`, `required_personal_spv`, `level_rep`, `updated_at`,`type`) VALUES
            (1,	'Direct Sales Bonus',	1500,	0,	1,	'2016-09-28 03:49:05','1');");

             DB::statement("INSERT INTO `tbl_direct_unilevel_setting` (`set_id`, `seq_level`, `seq_value`, `sequence_id`) VALUES
            (1,	1,	20,	1);");

            DB::table("tbl_indirect_unilevel")->delete();
            DB::statement("INSERT INTO `tbl_indirect_unilevel` (`sequence_id`, `sequence_name`, `required_spv`, `required_personal_spv`, `level_rep`, `updated_at`,`type`) VALUES
            (1,	'Indirect Sales  Bonus',	5000,	0,	2,	'2016-09-28 03:49:38','2');");

             DB::statement("INSERT INTO `tbl_indirect_unilevel_setting` (`set_id`, `seq_level`, `seq_value`, `sequence_id`) VALUES
            (1,	1,	0,	1),
            (2,	2,	10,	1);");

            DB::table("tbl_unilevel_setting")->delete();
            DB::statement("INSERT INTO `tbl_unilevel_setting` (`indirect_seting_id`, `level`, `value`, `membership_id`) VALUES
            (1,	1,	100,	1),
            (2,	2,	100,	1);");

            /* tbl_binary_breakdown */
            DB::table("tbl_binary_breakdown")->delete();

            
            /* tbl_dsb */
            DB::table("tbl_direct_sb")->delete();
            
            /* tbl_isb */
            DB::table("tbl_indirect_sb")->delete();

            /* tbl_binary_report */
            DB::table("tbl_binary_report")->delete();

            /* tbl_binary_report */
            DB::table("tbl_binary_admin_report")->delete();

            /* tbl_binary_report */
            DB::table("tbl_pay_cheque")->delete();

            /* tbl_binary_report */
            DB::table("tbl_pay_cheque_batch")->delete(); 

            /* tbl_direct_report */
            DB::table("tbl_direct_report")->delete(); 

            /* tbl_binary_report */
            DB::table("tbl_direct_admin_report")->delete(); 

            /* tbl_pipeline */
            DB::table("tbl_pipeline")->delete(); 
            $tbl_pipeline["pipeline_upline"] = 2147483647;
            $tbl_pipeline["pipeline_downline"] = 1;
            $tbl_pipeline["pipeline_level"] = 1;
            DB::table("tbl_pipeline")->insert($tbl_pipeline);
            
            /* tbl_pipeline_report */
            DB::table("tbl_pipeline_report")->delete(); 
            /* tbl_pipeline_admin_report */
            DB::table("tbl_pipeline_admin_report")->delete(); 

            /* tbl_pipeline_report */
            DB::table("tbl_indirect_report")->delete(); 
            /* tbl_pipeline_admin_report */
            DB::table("tbl_indirect_admin_report")->delete(); 

            /* tbl_code_type  */
            // DB::table("tbl_pipeline_settings")->delete();
            // DB::statement("INSERT INTO `tbl_pipeline_settings` (`pipeline_id`, `pipeline_level`, `pipeline_label`, `pipeline_graduate_amount`, `pipeline_requirement`, `pipeline_binary_points`, `pipeline_graduate_trigger_next`, `pipeline_order`) VALUES (3, 1,  '1-C',  5000,   10, 0,  1,  3), (4, 2,  '2',    20000,  10, 1000,   1,  4), (5, 3,  '3',    50000,  10, 0,  1,  5), (6, 4,  '4',    150000, 10, 5000,   1,  6), (7, 5,  '5',    300000, 10, 15000,  1,  7), (8, 6,  '6',    500000, 10, 30000,  1,  8), (1, 1,  '1-A',  2000,   2,  0,  0,  1), (2, 1,  '1-B',  3000,   5,  0,  0,  2);");
            die("Data Reset Successful!");

            
        }
        else
        {
            die("Sorry, you need to enter the right passkey to reset all data");
        }
    }
}
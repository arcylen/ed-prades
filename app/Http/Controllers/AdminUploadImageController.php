<?php namespace App\Http\Controllers;
use Request;
use DB;
use Crypt;
use Validator;
use App\Classes\Customer;
use Redirect;
use Session;
use App\Tbl_account;
use App\Tbl_lead;
use Carbon\Carbon;
use App\Classes\Log;
use App\Tbl_beneficiary_rel;
use App\Tbl_beneficiary;

class AdminUploadImageController extends Controller
{
    public function uploadPicture()
    {
        $file = Request::file('file');
        $cover_image_filename = $file->getClientOriginalName();
        
        $file->move(public_path('images'), $cover_image_filename);
        $path = public_path('images');
        return '/images/'.$cover_image_filename;
    }
}
<?php namespace App\Http\Controllers;
use Request;
use App\Tbl_country;
use DB;
use Redirect;
use Crypt;
use Carbon\Carbon;
use App\Classes\Customer;
use App\Classes\Compute;
use Session;
use DateTime;
use App\Tbl_slot;
use App\Tbl_account;
use App\Tbl_membership_code;
class DistributorRegisterController extends DistributorController
{
	public function index()
	{
		$data["page"] = "Registration";
		$data["pin"] = Request::input("pin");
		$data["activation"] = Request::input("activation");
		$data["activation_arr"] = explode("-", Request::input("activation"));
		$data["by"] = Request::input("by");
		$data["ses"] = Request::input("ses");
		$data["_country"] = Tbl_country::where("archived", 0)->get();
		return view('distributor.register', $data);
	}
	public function register()
	{
		// dd($_POST);
		$registertype = Request::input("registertype");
		$pin = Request::input("pin");
		$activation = camel_case(Request::input("activation"));
		$by = Request::input("by");
		$ses = Request::input("sessioncheck");
		$binaryplacement = Request::input("binary-placement");
		$binaryposition = Request::input("binary-position");
		$username = Request::input("username");
		$password = Request::input("np");

		$check = Tbl_membership_code::where('code_pin', $pin)
											 ->where('code_activation', $activation)
											 ->where('used', 0)
											 ->first();
		$checkbinarypostion = 	Tbl_slot::where('slot_id', $binaryplacement)->count();
		if($checkbinarypostion >= 1){
			if($check != null)
			{
				$positioncheck = Tbl_slot::where('slot_position', $binaryposition)
													  ->where('slot_placement', $binaryplacement)
													  ->where('slot_sponsor', $by)
													  ->first();
				if($positioncheck == null)
				{
					if($registertype == 1)
					{
						$register_validator['account_name'] = Request::input("fullname");
						$register_validator['account_email'] = Request::input("email");
						$register_validator['account_contact_number'] = Request::input("contactnumber");
						$register_validator['gender'] = Request::input("gender");
						$register_validator['birthday'] = Request::input("datepicker");
						$register_validator['account_country_id'] = Request::input("country");
						$register_validator['account_date_created'] = Carbon::now();
						$register_validator['account_username'] = $username;
						$register_validator['account_password'] = Crypt::encrypt($password);
						$confirmpass = Request::input("cp");
						$fullname = Request::input("fullname");
						$emailcheck = Tbl_account::where('account_email', Request::input('email'))->count();
						$usernamecheck = Tbl_account::where('account_username', $username)->count();
						if($emailcheck != 1)
						{
							if($usernamecheck != 1)
							{
								if($confirmpass == $password)
								{
									
									$membershipquery = Tbl_membership_code::where('code_pin', $pin)
																					   ->where('code_activation', $activation)->first();
									$membership_id = $membershipquery->membership_id;
									$slot_owner = Tbl_account::insertGetId($register_validator);
									$insert["slot_membership"] =  $membership_id;
									$insert["slot_type"] =  "PS";
									$insert["slot_rank"] =  1;
									$insert["slot_sponsor"] =  $by;
									$insert["slot_placement"] =  $binaryplacement;
									$insert["slot_position"] =  $binaryposition;
									$insert["slot_binary_left"] =  0;
									$insert["slot_binary_right"] =  0;
									$insert["slot_personal_points"] =  0;
									$insert["slot_group_points"] =  0;
									$insert["distributed"] =  0;
									$insert["slot_upgrade_points"] = 0;
									$insert["slot_total_withrawal"] =  0;
									$insert["slot_code_pin"] =  $pin;
									$insert["slot_total_earning"] =  0;
									$insert["created_at"] =  Carbon::now();
									$insert["slot_owner"] =  $slot_owner;
									$insert["membership_entry_id"] =  $membership_id;
									$membership_code['used'] = 1;
										
									$s_id = Tbl_slot::insertGetId($insert);
									Tbl_membership_code::where('code_pin', $pin)->where('code_activation', $activation)->update($membership_code);
									// $s_id = DB::table('tbl_slot')->insertGetId($insert);
									Compute::tree($s_id);
									Compute::entry($s_id);
									
									DB::table("tbl_membership_code")->where('code_pin', $pin)->where('code_activation', $activation)->update($membership_code);
	
									$bank['bank_name'] = "-";
									$bank['bank_branch'] = "-";
									$bank['bank_account_name'] = "-";
									$bank['bank_account_number'] = "-";
									$bank['bank_user_id'] = $slot_owner;
									DB::table('tbl_bank_deposit')->insert($bank);
	
									$cheque['cheque_name'] = "-";
									$cheque['cheque_user_id'] = $slot_owner;
									DB::table('tbl_cheque')->insert($cheque);
	
									if($ses == "1")
									{
										$noti["title"] = "Success!";
										$noti["message"] = "Slot Number ".$s_id." has been successfully created for account of ".$fullname."";
										return Redirect::to("/distributor/codevault")->with("notification", $noti);
									}
									else
									{
										$member = Customer::authenticate($username, $password);
	
							            if($member)
							            {
							            	if($member->account_approved == 1)
							            	{
							            		if ($member->blocked == 0) 
							            		{
							            			$pass =	Crypt::encrypt($password);
									                Customer::login($member->account_id,$pass);
													$noti["title"] = "Success!";
													$noti["message"] = "Slot Number ".$s_id." has been successfully created for account of ".$fullname."";
													return Redirect::to("/distributor/codevault")->with("notification", $noti);
							            		}
									            else
									            {
									            	return Redirect::to("/member/login")->with("errored","Invalid Account.");
									            }
							            	}
							            	else if($member->account_expired == 1)
							            	{
												return Redirect::to("/member/login")->with("errored","This account is already expired.");
							            	}
							            	else
							            	{
					            				$expired_day = $this->expiration();	
						    					$fdate = new DateTime($member->account_date_created);
												$ldate = new DateTime(Carbon::now());
										        $difference = date_diff($fdate, $ldate);
										        $count = Tbl_slot::where('slot_owner',$member->account_id)->count();
	
										        if($difference->days >= $expired_day)
										        {
										        	if($count == 0)
										        	{
										        		Tbl_account::where('account_id',$member->account_id)->update(['account_expired'=>1]);
										        		return Redirect::to("member/login")->with("errored","This account is already expired.");
										        	}
										        	else
										        	{
									        		 Tbl_account::where('account_id',$member->account_id)->update(['account_approved'=>1]);
													 $member = Customer::authenticate($username, $password);
								          			 $pass   =	Crypt::encrypt($password);
										         	 Customer::login($member->account_id,$pass);
										         	 return Redirect::to("distributor");
										        	}
										        }
										        else
										        {
									        		if($count == 0)
										        	{
														 $member = Customer::authenticate($username, $password);
														 $pass   =	Crypt::encrypt($password);
											             Customer::login($member->account_id,$pass);
											             return Redirect::to("distributor");
										        	}
										        	else
										        	{
									        		 	 Tbl_account::where('account_id',$member->account_id)->update(['account_approved'=>1]);
														 $member = Customer::authenticate($username, $password);
														 $pass   =	Crypt::encrypt($passwprd);
											             Customer::login($member->account_id,$pass);
											             return Redirect::to("distributor");
										        	}
										        }
					            			}  			   
					            		}
									}
								}
								else
								{
									$noti["title"] = "Error!";
									$noti["message"] = "Password didn't match.";
									return Redirect::to("/distributor/register")->with("notification", $noti)->withInput();
								}
							}
							else
							{
								$noti["title"] = "Error!";
								$noti["message"] = "Username is already in use.";
								return Redirect::to("/distributor/register")->with("notification", $noti)->withInput();
							}
						}
						else
						{
							$noti["title"] = "Error!";
							$noti["message"] = "Email is already in use.";
							return Redirect::to("/distributor/register")->with("notification", $noti)->withInput();
						}
					}
					else
					{
						$userquery = Tbl_account::where('account_username', $username)->first();
						if($userquery != null)
						{
							$userpass = Crypt::decrypt($userquery->account_password);
							if($userpass == $password)
							{	
								
								$userid = $userquery->account_id;
								$membershipquery = Tbl_membership_code::where('code_pin', $pin)->where('code_activation', $activation)->first();
								
								$membership_id = $membershipquery->membership_id;
	
								$insert["slot_membership"] =  $membership_id;
								$insert["slot_code_pin"] =  $pin;
								$insert["slot_type"] =  "PS";
								$insert["slot_rank"] =  1;
								$insert["slot_sponsor"] =  $by;
								$insert["slot_placement"] =  $binaryplacement;
								$insert["slot_position"] =  $binaryposition;
								$insert["slot_binary_left"] =  0;
								$insert["slot_binary_right"] =  0;
								$insert["slot_personal_points"] =  0;
								$insert["slot_group_points"] =  0;
								$insert["distributed"] =  0;
								$insert["slot_upgrade_points"] = 0;
								$insert["slot_total_withrawal"] =  0;
								$insert["slot_total_earning"] =  0;
								$insert["created_at"] =  Carbon::now();
								$insert["slot_owner"] =  Customer::id();
								$insert["membership_entry_id"] =  $membership_id;
								$membership_code['used'] = 1;
	
								$s_id = Tbl_slot::insertGetId($insert);
								Tbl_membership_code::where('code_pin', $pin)->where('code_activation', $activation)->update($membership_code);
									
								// $s_id = DB::table('tbl_slot')->insertGetId($insert);
								
								Compute::tree($s_id);
								Compute::entry($s_id);
								
								DB::table("tbl_membership_code")->where('code_pin', $pin)->where('code_activation', $activation)->update($membership_code);
								
								$noti["title"] = "Success!";
								$noti["message"] = "Slot Number ".$s_id." has been successfully created.";
								return Redirect::to("/distributor/codevault")->with("notification", $noti);
								if($ses == 1)
								{
									$noti["title"] = "Success!";
									$noti["message"] = "Slot Number ".$s_id." has been successfully created.";
									return Redirect::to("/distributor/codevault")->with("notification", $noti);
								}
								else
								{
									$member = Customer::authenticate($username, $password);
	
						            if($member)
						            {
						            	if($member->account_approved == 1)
						            	{
						            		if ($member->blocked == 0) 
						            		{
						            			$pass =	Crypt::encrypt($password);
								                Customer::login($member->account_id,$pass);
												$noti["title"] = "Success!";
												$noti["message"] = "Slot Number ".$s_id." has been successfully created.";
												return Redirect::to("/distributor/codevault")->with("notification", $noti);
						            		}
								            else
								            {
								            	return Redirect::to("member/login")->with("errored","Invalid Account.");
								            }
						            	}
						            	else if($member->account_expired == 1)
						            	{
											return Redirect::to("member/login")->with("errored","This account is already expired.");
						            	}
						            	else
						            	{
				            				$expired_day = $this->expiration();	
					    					$fdate = new DateTime($member->account_date_created);
											$ldate = new DateTime(Carbon::now());
									        $difference = date_diff($fdate, $ldate);
									        $count = Tbl_slot::where('slot_owner',$member->account_id)->count();
	
									        if($difference->days >= $expired_day)
									        {
									        	if($count == 0)
									        	{
									        		Tbl_account::where('account_id',$member->account_id)->update(['account_expired'=>1]);
									        		return Redirect::to("member/login")->with("errored","This account is already expired.");
									        	}
									        	else
									        	{
								        		 Tbl_account::where('account_id',$member->account_id)->update(['account_approved'=>1]);
												 $member = Customer::authenticate($username, $password);
							          			 $pass   =	Crypt::encrypt($password);
									         	 Customer::login($member->account_id,$pass);
									         	 return Redirect::to("distributor");
									        	}
									        }
									        else
									        {
								        		if($count == 0)
									        	{
													 $member = Customer::authenticate($username, $password);
													 $pass   =	Crypt::encrypt($password);
										             Customer::login($member->account_id,$pass);
										             return Redirect::to("distributor");
									        	}
									        	else
									        	{
								        		 	 Tbl_account::where('account_id',$member->account_id)->update(['account_approved'=>1]);
													 $member = Customer::authenticate($username, $password);
													 $pass   =	Crypt::encrypt($passwprd);
										             Customer::login($member->account_id,$pass);
										             return Redirect::to("distributor");
									        	}
									        }
				            			}	       			   
				            		}
								}
							}
							else
							{
								$noti["title"] = "Error!";
								$noti["message"] = "Username and Password didn't match.";
								return Redirect::to("/distributor/register/")->with("notification", $noti)->withInput();
							}
						}
						else
						{
							$noti["title"] = "Error!";
							$noti["message"] = "Account doesn't exist.";
							return Redirect::to("/distributor/register/")->with("notification", $noti)->withInput();
						}
					}
				}
				else
				{
					$noti["title"] = "Error!";
					$noti["message"] = "Position is already occupied.";
					return Redirect::to("/distributor/register/")->with("notification", $noti)->withInput();
				}
			}
			else
			{
				$noti["title"] = "Error!";
				$noti["message"] = "Wrong Pin/Activation Code.";
				return Redirect::to("/distributor/register/")->with("notification", $noti)->withInput();
			}	
		}
		else{
			$noti["title"] = "Error!";
			$noti["message"] = "Binary placement does not exist, check the genealogy for available placement.";
			return Redirect::to("/distributor/register")->with("notification", $noti)->withInput();	
		}
		
	}
	public function tree($id){
		return Compute::tree($id);
	}
}
<?php namespace App\Http\Controllers;
use Request;
use DB;
use App\Classes\Stockist;
use Redirect;
use Session;
use gapi;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Tbl_voucher;
use App\Tbl_membership_code;
use App\Tbl_stockist_inventory;
use App\Tbl_stockist_package_inventory;

class StockistDashboardController extends StockistController
{
    public function index()
    {
        $id = Stockist::info()->stockist_id;
        $data['voucher'] = Tbl_voucher::leftJoin('tbl_account','tbl_account.account_id','=', 'tbl_voucher.account_id')
                                ->where('origin',Stockist::info()->stockist_id)->where('status', 'processed')
                                ->sum('total_amount');
        $data['order_processed'] = DB::table("tbl_voucher")->where('status','=','processed')
											->where('origin', '=', Stockist::info()->stockist_id)
											->sum('total_amount'); 
		$data['order_unclaimed'] = DB::table("tbl_voucher")->where('status','=','unclaimed')
											->where('origin', '=', Stockist::info()->stockist_id)
											->sum('total_amount');   									
                                
        $data['membership'] = Tbl_membership_code::byStockist(Stockist::info()->stockist_id)->getMembership()
        ->getCodeType()->getPackage()->getInventoryType()->getUsedBy()->where('tbl_membership_code.used',0)->count();
        $data['membership_code_used'] = Tbl_membership_code::byStockist(Stockist::info()->stockist_id)->getMembership()
        ->getCodeType()->getPackage()->getInventoryType()->getUsedBy()->where('tbl_membership_code.used',1)->count();
         $data['_product'] = Tbl_stockist_inventory::where('stockist_id',$id)->join('tbl_product','tbl_product.product_id','=','tbl_stockist_inventory.product_id')
         ->where('tbl_product.archived',0)->sum('stockist_quantity');
        $data['_package'] = Tbl_stockist_package_inventory::where('stockist_id',$id)->join('tbl_product_package','tbl_product_package.product_package_id','=','tbl_stockist_package_inventory.product_package_id')
        ->where('tbl_product_package.archived',0)->sum('package_quantity');
        // dd($data);
        
        return view('stockist.dashboard', $data);
    }
}

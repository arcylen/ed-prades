<?php namespace App\Http\Controllers;
use DB;
use App\Classes\Image;
use App\Classes\Customer;
use Request;
use Input;
use Mail;
use App\Classes\Globals;
use Redirect;
use Crypt;
use Validator;
use Newsletter;

class FrontController extends Controller
{
	
	public function __construct()
	{
	    $content_data = collect(DB::table("tbl_about")->get())->keyBy("about_name");
	    $settings_data = collect(DB::table("tbl_settings")->get())->keyBy("key");
	    
		view()->share("content_data", $content_data);
		view()->share("settings_data", $settings_data);
	}

	public function index()
	{
		$data["_product"] = DB::table("tbl_product")->where("image_file", "!=", "default.jpg")->where("image_file", "!=", "")->where("archived", 0)->get();
		foreach ($data["_product"] as $key => $value) 
		{
			$data["_product"][$key]->image = Image::view_v2($value->image_file, "410x300", "product");
			$data["_product"][$key]->category = DB::table("tbl_product_category")->where("product_category_id", $value->product_category_id)->first();
		}

        return view('front.home', $data);
		// return Redirect('/member/login');
	}
	public function about()
	{
        return view('front.about');
	}
	public function stories()
	{
		$data["_stories"] = DB::table("tbl_stories")->where("archived", 0)->get();

        return view('front.stories', $data);
	}
	public function opportunity()
	{
        return view('front.opportunity');
	}
	public function service()
	{	
		$data["_service"] = DB::table("tbl_service")->where("archived", 0)->get();
		foreach ($data["_service"] as $key => $value) 
		{
			$get = $value->service_image;
			$imagee = Image::view($get, "723x530");
			$data["_service"][$key]->image = $imagee;
		}

        return view('front.service', $data);
	}
	public function updates()
	{
		// PROMOTIONS
		$data["_promotions"] = DB::table("promotions")->get();
		foreach($data["_promotions"] as $key => $value)
		{
			$data["_promotions"][$key]->image_file = Image::view_v2($value->image, "400x320", "promotions");
		}
		// NEWS
		$data["_news"] = DB::table("tbl_news")->where("archived", 0)->get();
		foreach ($data["_news"] as $key => $value) 
		{
			$data["_news"][$key]->image_file = Image::view_v2($value->news_image, "500x300", "product");
		}

		return view('front.updates', $data);
	}
	public function product()
	{
		$id = Request::input("category");
		if ($id) 
		{
			$data["product_category"] = DB::table("tbl_product_category")->where("product_category_id", $id)->first();
			$data["product_category"]->image_file = Image::view($data["product_category"]->product_category_icon, "100x100", "product_icon");
			$data["_product"] = DB::table("tbl_product")->where("product_category_id", $id)->where("image_file", "!=", "default.jpg")->where("image_file", "!=", "")->where("archived", 0)->get();
			foreach ($data["_product"] as $key => $value) 
			{
				$data["_product"][$key]->image = Image::view_v2($value->image_file, "410x300", "product");
			}

			$data["_category"] = DB::table("tbl_product_category")->where("archived", 0)->get();
		
        	return view('front.product', $data);
		}
		else
		{
			return Redirect::to("/product/category");
		}
	}
	public function product_category()
	{
		$data["_category"] = DB::table("tbl_product_category")->where("archived", 0)->get();
		foreach ($data["_category"] as $key => $value) 
		{
			$data["_category"][$key]->image_file = Image::view_v2($value->product_category_icon, "60x60", "product_icon");
		}

		return view('front.product_category', $data);
	}
	public function product_view($id)
	{
		$data["product"] = DB::table("tbl_product")->where("product_id", $id)->first();
		$data["product"]->image = Image::view_v2($data["product"]->image_file, "500x500", "product");
		$data["product_category"] = DB::table("tbl_product_category")->where("product_category_id", $data["product"]->product_category_id)->first();
		$data["product_category"]->image_file = Image::view($data["product_category"]->product_category_icon, "100x100", "product_icon");

		$data["_product"] = DB::table("tbl_product")->where("product_id",'!=',$id)->where("image_file", "!=", "default.jpg")->where("image_file", "!=", "")->where("archived", 0)->get();
		foreach ($data["_product"] as $key => $value) 
		{
			$data["_product"][$key]->image = Image::view_v2($value->image_file, "410x300", "product");
			$data["_product"][$key]->category = DB::table("tbl_product_category")->where("product_category_id", $value->product_category_id)->first();
		}

		return view('front.product_content', $data);
	}
	public function product_checkout()
	{
		$data = [];
		return view('front.checkout', $data);
	}
	public function news()
	{
		$data["_news"] = DB::table("tbl_news")->where("archived", 0)->get();
		$data["_newss"] = DB::table("tbl_news")->where("archived", 0)->orderBy('news_id', 'desc')->take(4)->get();
		$data["_product"] =  DB::table("tbl_product")->where("archived", 0)->where("image_file", "!=", "default.jpg")->where("image_file", "!=", "")->orderBy('product_id', 'desc')->take(6)->get();
		foreach ($data["_news"] as $key => $value) 
		{
			$get = $value->news_image;
			$imagee = Image::view($get, "700x301");
			$data["_news"][$key]->image = $imagee;
		}
		foreach ($data["_product"] as $keys => $values) 
		{
			$gets = $values->image_file;
			$imagees = Image::view($gets, "75x75");
			$data["_product"][$keys]->image = $imagees;
		}

        return view('front.news', $data);
	}
	public function news_content()
	{
		$id = Request::input("id");
		$data["news"] = DB::table("tbl_news")->where("news_id", $id)->first();

		$datee = $data["news"]->news_date;
		$time=strtotime($datee);
		$month=date("F",$time);
		$day=date("d",$time);
		$year=date("Y",$time);

		$data["news"]->month = $month;
		$data["news"]->day = $day;
		$data["news"]->year = $year;
		
		$data["news"]->image_file = Image::view_v2($data["news"]->news_image, "");

		// $multidate = Globals::multiexplode(array("-","-"," "), $datee);
		// $date["month"] = $multidate["1"];
		// $date["day"] = $multidate["2"];

        return view('front.news_content', $data);
	}
	public function contact()
	{
		$data["_downloads"] = DB::table("downloads")->get();
		$data["_news"] = DB::table("tbl_news")->where("archived", 0)->get();

        return view('front.contact', $data);
	}
	public function contact_submit()
	{
		$fromEmail = Input::get('email');
	    $fromName = Input::get('name');
	    $subject = Input::get('subject');
	    $data["data"] = Input::get('message');

	    $toEmail = 'admin@ultraproactive.net';
	    $toName = 'Customer';

	    Mail::send('emails.contact', $data, function($message) use ($toEmail, $toName, $fromEmail, $fromName, $subject)
	    {
	        $message->to($toEmail, $toName);

	        $message->from($fromEmail, $fromName);

	        $message->subject($subject);
	    });

	    return Redirect::to("/contact");
	}
	public function newsletter_submit()
	{
		$validator = Validator::make(Request::input(), [
	        'full_name' => 'required',
	        'email_address' => 'required|email',
	        'phone_number' => 'required',
	    ]);

		Newsletter::subscribe(Request::input('email_address'), ['fullName'=>Request::input("full_name"), 'phoneNumber'=>Request::input("phone_number")]);
	
		return Redirect::back()->with("success", "Successfully submit.");
	}
	public function register()
	{
        return view('front.register');
	}
	public function mindsync()
	{
		$data["_image"] = DB::table("tbl_mindsync")->where("mindsync_image", "!=", "")->where("archived", 0)->get();
		foreach ($data["_image"] as $key => $value) 
		{
			$image = $value->mindsync_image;
			$image_view = "http://image.primiaworks.com/uploads/ultraproactive.net/image/$image/$image";
			$data["_image"][$key]->image = $image_view;
		}
		$data["_testimony"] = DB::table("tbl_mindsync")->where("mindsync_title", "!=", "")->where("mindsync_description", "!=", "")->where("archived", 0)->get();
		$data["_video"] = DB::table("tbl_mindsync")->where("mindsync_video", "!=", "")->where("archived", 0)->get();
		// foreach ($data["_mindsync"] as $key => $value) 
		// {
		// 	$explode = $value;
		// 	$get = explode(",", $explode->mindsync_image);
		// 	foreach ($get as $susi => $halaga) 
		// 	{
		// 		$imagee[$susi] = Image::view($halaga, "300x300");
		// 	}
		// 	// $imagee = "http://image.primiaworks.com/uploads/ultraproactive.net/image/$get/$get";
		// 	$data["_mindsync"][$key]->image = $imagee;
		// }
		
		return view('front.mindsync', $data);
	}
	public function faq()
	{
		$data["type"] = Request::input("type");
		if (isset($data["type"])) 
		{
			$data["_product"] = DB::table("tbl_faq")->where("archived", 0)->where("faq_type", "product")->get();
			$data["_mindsync"] = DB::table("tbl_faq")->where("archived", 0)->where("faq_type", "mindsync")->get();
			$data["_opportunity"] = DB::table("tbl_faq")->where("archived", 0)->where("faq_type", "opportunity")->get();
			$data["_glossary"] = DB::table("tbl_faq")->where("archived", 0)->where("faq_type", "glossary")->get();
			return view('front.faq', $data);
		}
        else
       	{
       		return Redirect::to("/");
       	}
	}
	public function foodcart()
	{
		$data["_foodcart"] = DB::table("tbl_foodcart")->where("archived", 0)->get();
		foreach ($data["_foodcart"] as $key => $value) 
		{
			$image = $value->foodcart_image;
			$image_view = Image::view_main($image);
			$data["_foodcart"][$key]->image = $image_view;
		}

        return view('front.foodcart', $data);
	}
	public function frontlogin()
	{
		$uname = Request::input("username");
		$pass = Request::input("password");

		$login = DB::table('tbl_account')->where('account_username', $uname)->first();
		if($login != null)
		{
			$account_password = Crypt::decrypt($login->account_password);
			if($account_password == $pass)
			{
				$pass =	Crypt::encrypt($pass);
				Customer::login($login->account_id,$pass);
				return "0";
			}
			else
			{
				return "1";
			}
		}
		else
		{
			return "2";
		}
	}
}
<?php namespace App\Http\Controllers;
use DB;
use Redirect;
use Request;
use App\Classes\Image;
use App\Tbl_about;

use App\Globals\AdminNav;
class AdminContactController extends AdminController
{
	public function index()
	{
		$id = Request::input("id");
		$address = DB::table("tbl_about")->where("about_name", "Address")->first();
		$contactnum = DB::table("tbl_about")->where("about_name", "Contact Number")->first();
		$email = DB::table("tbl_about")->where("about_name", "E-Mail")->first();
		$business = DB::table("tbl_about")->where("about_name", "Business Hours")->first();
		// dd($data);
		if($address == null)
		{
			$a["about_name"] = "Address";
			$a["about_description"] = "";
			DB::table("tbl_about")->insert($a);
			$data["address"] = DB::table("tbl_about")->where("about_name", "Address")->first();
		}
		else
		{
			$data["address"] = $address;
		}
		if($contactnum == null)
		{
			$c["about_name"] = "Contact Number";
			$c["about_description"] = "";
			DB::table("tbl_about")->insert($c);
			$data["contactnum"] = DB::table("tbl_about")->where("about_name", "Contact Number")->first();
		}
		else
		{
			$data["contactnum"] = $contactnum;
		}
		if($email == null)
		{
			$m["about_name"] = "E-Mail";
			$m["about_description"] = "";
			DB::table("tbl_about")->insert($m);
			$data["email"] = DB::table("tbl_about")->where("about_name", "E-Mail")->first();
		}
		else
		{
			$data["email"] = $email;
		}
		if($business == null)
		{
			$v["about_name"] = "Business Hours";
			$v["about_description"] = "";
			DB::table("tbl_about")->insert($v);
			$data["businesshours"] = DB::table("tbl_about")->where("about_name", "Business Hours")->first();
		}
		else
		{
			$data["businesshours"] = $business;
		}
		
		$code = "Contact Us";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        
        if($access == "1")
        {       
        return view('admin.content.contact', $data);
        }
        else
        {
            return Redirect::back();
        } 
	}
	public function submit()
	{
		$address = Request::input("addressid");
		$contactnum = Request::input("contactnumid");
		$email = Request::input("emailid");
		$businesshours = Request::input("businesshoursid");
		
		$addresss = Request::input("address");
		$contactnums = Request::input("contactnum");
		$emails = Request::input("email");
		$businesshourss = Request::input("businesshours");
        // dd($address,$contactnum,$email,$businesshours,$addresss,$contactnums,$emails,$businesshourss);
		$date = date('Y-m-d H:i:s');
		
		DB::table("tbl_about")->where("about_id", $address)->update(['about_description' => $addresss, 'created_at' => $date, 'updated_at' => $date]);
		DB::table("tbl_about")->where("about_id", $contactnum)->update(['about_description' => $contactnums, 'created_at' => $date, 'updated_at' => $date]);
		DB::table("tbl_about")->where("about_id", $email)->update(['about_description' => $emails, 'created_at' => $date, 'updated_at' => $date]);
		DB::table("tbl_about")->where("about_id", $businesshours)->update(['about_description' => $businesshourss, 'created_at' => $date, 'updated_at' => $date]);

        return Redirect::to("/admin/content/contact");
	}

	public function super_test()
	{
		return "super_test";
	}	
}
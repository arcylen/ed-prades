<?php namespace App\Http\Controllers;
use DB;
use Redirect;
use Request;
use App\Classes\Image;
use App\Classes\Log;
use App\Classes\Admin;
use Input;
use App\Globals\AdminNav;
class AdminNewsController extends AdminController
{
	public function index()
	{
		
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits News Maintenance");
		$data["_news"] = DB::table("tbl_news")->where("archived", 0)->get();

		$code = "News";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {       
        return view('admin.content.news', $data);
        }
        else
        {
            return Redirect::back();
        } 
	}
	public function add()
	{
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits Add News");
        return view('admin.content.news_add');
	}
	public function add_submit()
	{
		$title = Request::input("title");
		$description = Request::input("description");
		$date = date('Y-m-d H:i:s');
		$image = Request::file("image_file");
		if(Input::hasFile('image_file'))
				{
					$string = str_random(15);
		            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		            // generate a pin based on 2 * 7 digits + a random character
		            $pin = mt_rand(1000000, 9999999)
		                . mt_rand(1000000, 9999999)
		                . $characters[rand(0, strlen($characters) - 1)];
		            // shuffle the result
		            $string1 = str_shuffle($pin);
		            $get = $string1;
		            $file = Input::file('image_file');
	  				//image name to save on DB//
		            $image = '/resources/assets/img/product/' . $get.'-'.$file->getClientOriginalName() . '';
		            $image_file = $image;
		            $file -> move('resources/assets/img/product' , $get.'-'.$file->getClientOriginalName('image_file'));
		        }
		        else
		        {
		        	$image_file = "/resources/assets/img/1428733091.jpg";
		        }
		 $image = $image_file;        
		$id = DB::table("tbl_news")->insertGetId(['news_title' => $title, 'news_description' => $description, 'news_date' => $date, 'news_image' => $image]);
		$new = DB::table("tbl_news")->where('news_id',$id)->first();
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." Add News #".$id,null,serialize($new));
        return Redirect::to("/admin/content/news");
	}	
	public function edit()
	{
		$id = Request::input("id");
		$data["news"] = DB::table("tbl_news")->where("news_id", $id)->first();
		$data["news"]->image = Image::view_v2($data["news"]->news_image, "500x500", "product");

		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits Edit News Id #".$id);

        return view('admin.content.news_edit', $data);
	}
	public function edit_submit()
	{
		$id = Request::input("id");
		$title = Request::input("title");
		$description = Request::input("description");
		$date = date('Y-m-d H:i:s');
		$image = Request::file("image_file");
		if(Input::hasFile('image_file'))
				{
					$string = str_random(15);
		            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		            // generate a pin based on 2 * 7 digits + a random character
		            $pin = mt_rand(1000000, 9999999)
		                . mt_rand(1000000, 9999999)
		                . $characters[rand(0, strlen($characters) - 1)];
		            // shuffle the result
		            $string1 = str_shuffle($pin);
		            $get = $string1;
		            $file = Input::file('image_file');
	  				//image name to save on DB//
		            $image = '/resources/assets/img/product/' . $get.'-'.$file->getClientOriginalName() . '';
		            $image_file = $image;
		            $file -> move('resources/assets/img/product' , $get.'-'.$file->getClientOriginalName('image_file'));
		        }
		        else
		        {
		        	$image_file = "/resources/assets/img/1428733091.jpg";
		        }
		        $image = $image_file; 
		$old = DB::table("tbl_news")->where('news_id',$id)->first();

		DB::table("tbl_news")->where("news_id", $id)->update(['news_title' => $title, 'news_description' => $description, 'news_date' => $date, 'news_image' => $image]);
		
		$new = DB::table("tbl_news")->where('news_id',$id)->first();
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." Add News",serialize($old),serialize($new));

        return Redirect::to("/admin/content/news");
	}	
	public function delete()
	{
		$id = Request::input("id");
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits archive News Id #".$id);
		DB::table("tbl_news")->where("news_id", $id)->update(['archived' => 1]);

        return Redirect::to("/admin/content/news");
	}	
}
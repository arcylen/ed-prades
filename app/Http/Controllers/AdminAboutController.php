<?php namespace App\Http\Controllers;
use DB;
use Redirect;
use Request;
use App\Classes\Image;
use App\Tbl_about;

use App\Globals\AdminNav;
class AdminAboutController extends AdminController
{
	public function index()
	{
		// ID
		$id = Request::input("id");
		// SEED
		$seed['Introduction First Line (Front)'] = "Ready to start";
		$seed['Introduction Second Line (Front)'] = "living your dreams?";
		$seed['Sub-Introduction (Front)'] = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit!";
		$seed['Introduction Button Text (Front)'] = "LEARN HOW";
		$seed['Mission'] = "To help each individual uplift standard of living and attain financial freedom by providing bottomless income opportunities & quality consumer products that are deemed essential for good health and longevity.";
		$seed['Vision'] = "We envision to be a TOTAL direct sales company through dynamic, systematic distribution of products and proactive corporate management and staffs, and that helps alleviate the standards of living of our distributors by offering a realistic income opportunities.";
		$seed["Commitment"] = "We are committed to provide our members a business based on honesty and integrity with the highest ethical business standards with a marketing plan that effectively works. 
We are committed to provide you with high quality and fairly priced products. We aims for total customer satisfaction. 

We are committed to provide you with professional training programs to help you achieve your dreams. It is composed of a wide array of professional training modules, literatures, brochures, etc. 

We are committed to market opportunities so that you will have a business with no boundaries. 

We are committed to become the best direct selling multi -level marketing company.";
		$seed['Opportunity'] = 'An individual who is at least 18 (eighteen) years or older (may vary depending on the laws of every country) can join BOSS. However, BOSS Corp. reserves the right not to accept an application or reject/return an application after acceptance. After personally executing the Distributor Application Form and purchase of any product of BOSS, a prospect becomes an independent distributor of the Company. The distributor will be given a computer number, which the distributor will use worldwide. The distributor can now sponsor anybody into their organization and this organization will remain the same in every country where BOSS does business. Distributorship is “One Member-One Account Policy”. 

Signing up for distributorship of another person without the latter’s knowledge is strictly prohibited and therefore will be considered an invalid application. Perpetrators will be subject for termination from distributorship. Moreover, perpetrators may be violating civil or criminal acts punishable by law in most countries. A prospect/new distributor cannot put friends, family members or anyone between him/her and the sponsor who showed him/her the business when he/she signs up as this will lead to cutting off bonuses and stealing from the sponsor. In the event this happens, the situation will be rectified and will be corrected and all bonuses lost must be paid back to their uplines. Failing to do so, the distributorship will be terminated. No sponsor is allowed to give permission to any prospect/new distributor to put distributors between him/her and the new prospect/distributor who was shown the business as this will also affect the upline(s)’ bonuses. The distributorship of the said sponsor will be suspended for one (1) year for this wrong leadership. We encourage the new distributor to start using products to see how good they are and if he/she does not like them, to return them and refund the money back from the Company. Unless the distributor liked our products, this company is not for him/ her. 

We do not want people to join this company just because of the compensation program. Proper MLM is selling and wholesaling good products where the distributors are proud of to their customers and distributors. We are not in the business of selling a marketing plan. Product knowledge is mandatory for all distributors. You should know what you are selling.';
		$seed['Product Introduction (Home)'] = "Say something about your product introduction. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.";
		$seed['Ending Footnote Text'] = "Say something to motivate them and start your business. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.";
		$seed['Ending Footnote Button Text'] = "START NOW!";
		$seed['Banner Title (Opportunity)'] = "Opportunity For A Better Life!";
		$seed['Banner Sub-title (Opportunity)'] = "A short slogan about having opportunity joining your company.";
		$seed['Banner Title (Product Category)'] = "Opportunity For A Better Life!";
		$seed['Banner Sub-title (Product Category)'] = "Say something about your product introduction. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.";
		$seed['Banner Title (Company)'] = "Welcome Boss!";
		$seed['Banner Sub-title (Company)'] = "Let us get things start rolling. Be your OWN BOSS and EARN like a BOSS!";
		$seed['Welcome Text (Company)'] = "On your willingness and desire to improve yourself and your lifestyle through our industry called Multi-Level Marketing (MLM). We are hopeful that this will be instrumental in helping you discover and achieve the full potentials of your dreams. 

The BOSS aims for the success of any member regardless of which line or sponsor the member is under. It promotes success through UNITY and COOPERATION which further eliminates competition among the leaders. This has been the guiding principle of the BOSS Culture which promotes balanced lifestyle and success. It is expounded into 3 parts:  

First: BOSS advocates the obedience of the principles of network marketing and keeping up with the pledges between company and customers.  

Second: BOSS supports multi-cooperative companies to maintain premium quality of products at affordable prices.  

Third: BOSS emphasizes a lifestyle of sharing to employees and members with the hopes of creating a balanced society of opportunities. 

These philosophy translates an honest, transparent, and pure company and it is our hope that you will reciprocate the price with hard work, commitment and determination to build your business as a BOSS and be an ambassador of the Company. ";
		$seed['Banner Title (Updates)'] = "What's up Boss?";
		$seed['Banner Sub-title (Updates)'] = "Say something about this page content.";
		$seed['News Sub-title (Updates)'] = "Say something about general news content. Lorem ipsum dolor sit amet, consectetuer adipiscing elit? Aenean commodo ligula eget dolor.";
		$seed['Promotions Sub-title (Updates)'] = "Say something about general promotions content. Lorem ipsum dolor sit amet, consectetuer adipiscing elit.";
		$seed['Business Hours (Contact)'] = "Monday - Friday at 9:00am - 6:00pm";

		foreach ($seed as $key => $value) 
		{
			$exist = DB::table("tbl_about")->where("about_name", $key)->first();
			if (!$exist) 
			{
				$insert['about_name'] = $key;
				$insert['about_description'] = $value;

				DB::table("tbl_about")->insert($insert);
			}
		}
		// CONTENT
		$data["_content"] = DB::table("tbl_about")->where("archived", 0)->get();
		// ACCESS
		$code = "Content Text";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);

        if($access == "1")
        {       
        	return view('admin.content.about', $data);
        }
        else
        {
            return Redirect::back();
        } 
	}
	public function submit()
	{
		$content = Request::input("content");
		foreach ($content as $key => $value) 
		{
			DB::table("tbl_about")->where("about_name", $value["name"])->update(["about_description" => $value["description"]]);
		}

        return Redirect::to("/admin/content/about");
	}


	public function super_test()
	{
		return "super_test";
	}	
}
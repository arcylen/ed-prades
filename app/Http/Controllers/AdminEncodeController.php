<?php namespace App\Http\Controllers;
use Request;
use Redirect;
use App\Globals\AdminNav;

class AdminEncodeController extends AdminController
{
	public function index()
	{
		$data["page"] = "Test Encoding";
		
		$code = "Test Encode";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {    	
		return view("admin.computation.testencode", $data);
        }
        else
        {
            return Redirect::back();
        } 
	}
	public function frame()
	{
		
	}
}
<?php namespace App\Http\Controllers;
use App\Tbl_pay_cheque;
use App\Tbl_pay_cheque_batch;
use App\Tbl_binary_report;
use App\Tbl_binary_breakdown;
use App\Tbl_slot;
use App\Tbl_direct_report;
use App\Tbl_pipeline_report;
use App\Tbl_indirect_report;
use App\Tbl_indirect_sb;
use App\Tbl_gsb_report;
use App\Tbl_direct_sb;
use App\Tbl_total_gsb;
use App\Tbl_royalty_bonus;
use App\Classes\Customer;
use DB;
use PDF;
use App\Tbl_unilevel_point_log;
class DistributorChequeController extends DistributorController
{
	public function index()
	{
		$data["page"] = "Pay Cheque";
		if(isset($this->slotnow->slot_id)){
			$data["_pay_cheque"] = Tbl_pay_cheque::where("pay_cheque_slot", $this->slotnow->slot_id)->batch()->orderby('pay_cheque_id', 'desc')->get();
            // dd($data["_pay_cheque"]);
			foreach ($data["_pay_cheque"] as $key => $value) {
				$data["_pay_cheque_batch"][$key] =  Tbl_pay_cheque::where('pay_cheque_id', $value->pay_cheque_id)->sum('pay_cheque_amount');
			}
		}
// 		dd($data);
        if($this->slotnow->slot_id){
          $data['_slot'] = Tbl_slot::where("slot_id", $this->slotnow->slot_id)->limit(1)->first();  
        }
        
		$data["processing_fee"] = DB::table('tbl_settings')->where('key', 'processing_fee')->pluck('value');
        $data["withholding_tax"] = DB::table('tbl_settings')->where('key', 'withholding_tax')->pluck('value');
		$data['member'] = Customer::info();
		$data['bank_info'] = DB::table('tbl_bank_deposit')->where('bank_user_id', Customer::id())->first();
		$data['cheque_info'] = DB::table('tbl_cheque')->where('cheque_user_id', Customer::id())->first();
		return view("distributor.cheque.cheque", $data);
	}
	public function voucher($id)
	{
	    
		$data["pay_cheque"] = Tbl_pay_cheque::where("pay_cheque_id", $id)->batch()->first();
		// return $data["pay_cheque"];
		$data["_slot"] = null;
         $_slot = Tbl_slot::where("slot_id", $this->slotnow->slot_id)->limit(1)->get()->toArray();
        if(!empty($_slot)){
            // return $_slot;
        }
        else{
            $_slot = Tbl_slot::where("slot_sponsor",$this->slotnow->slot_id)->limit(1)->get()->toArray();
        }
        $data["owner_name"] = Tbl_slot::join("tbl_account","tbl_account.account_id","=","tbl_slot.slot_owner")->where("slot_id",$this->slotnow->slot_id)->pluck("account_name");
        foreach($_slot as $key => $slot)
        {
            $data["_slot"][$key] = $slot;
            $_pay_cheque = Tbl_pay_cheque::where("pay_cheque_slot", $this->slotnow->slot_id)->where('pay_cheque_id', $id)->batch()->get()->toArray();

            $data["_pay_cheque"] = null;

            /* GET DATA FOR PAYCHEQUE */
            foreach($_pay_cheque as $key2 => $pay_cheque)
            {
                $data["_pay_cheque"][$key2] = $pay_cheque;      

                // if($pay_cheque["pay_cheque_batch_reference"] == "binary")
                // {
                //     $data["_binary_report"] = null;
                //     /* GET BINARY REPORT */
                //     $_binary_report = Tbl_binary_report::where("binary_payout_paycheck", $pay_cheque["pay_cheque_id"])->get()->toArray();
                    
                //     foreach($_binary_report as $key3 => $binary_report)
                //     {
                //         /* BINARY BREAKDOWN */
                //         $_binary_breakdown = Tbl_binary_breakdown::where("binary_report_id", $binary_report["binary_id"])->slot()->account()->get()->toArray();
                //         $data["_binary_report"][$key3] = $binary_report;
                //         $data["_binary_report"][$key3]["breakdown"] = $_binary_breakdown;
                //     }

                //     $data["_pay_cheque"][$key2]["_binary_report"] = $data["_binary_report"];
                // }
                // elseif($pay_cheque["pay_cheque_batch_reference"] == "direct")
                // {
                //     $data["_direct_report"] = null;
                //     $data["_direct_report"] = Tbl_direct_report::where("direct_pay_cheque", $pay_cheque["pay_cheque_id"])->slotchild()->account()->get()->toArray();
                //     $data["_pay_cheque"][$key2]["_direct_report"] = $data["_direct_report"];
                // }
                // elseif($pay_cheque["pay_cheque_batch_reference"] == "indirect")
                // {
                //     $data["_indirect_report"] = null;
                //     $data["_indirect_report"] = Tbl_indirect_report::where("indirect_pay_cheque", $pay_cheque["pay_cheque_id"])->slotregistree()->account()->get()->toArray();
                //     $data["_pay_cheque"][$key2]["_indirect_report"] = $data["_indirect_report"];
                // }
                // elseif($pay_cheque["pay_cheque_batch_reference"] == "pipeline")
                // {
                //     $data["_indirect_report"] = null;
                //     $data["_indirect_report"] = Tbl_pipeline_report::where("pipeline_pay_cheque", $pay_cheque["pay_cheque_id"])->settings()->get()->toArray();
                //     $data["_pay_cheque"][$key2]["_pipeline_report"] = $data["_indirect_report"];
                // }
                // elseif($pay_cheque["pay_cheque_batch_reference"] == "unilevel")
                // {
                //     $data["_indirect_report"] = null;
                //     $data["_indirect_report"] = Tbl_unilevel_point_log::where("unilevel_distributed", $pay_cheque["pay_cheque_id"])->where('unilevel_type', 1)->slotregistree()->account()->Membership()->get()->toArray();
                //     $data["_pay_cheque"][$key2]["_unilevel_report"] = $data["_indirect_report"];
                // }

                  if($pay_cheque["pay_cheque_batch_reference"] == "Referral Bonus")
                {
                    $data["_direct_report"] = null;
                    $data["_direct_report"] = Tbl_direct_report::where("direct_pay_cheque", $pay_cheque["pay_cheque_id"])->slotchild()->account()->get()->toArray();
                    $data["_pay_cheque"][$key2]["_direct_report"] = $data["_direct_report"];
                }
                elseif($pay_cheque["pay_cheque_batch_reference"] == "Direct Sales Bonus")
                {
                    $data["_dsb_report"] = null;
                    $data["_dsb_report"] = Tbl_direct_sb::where("dsb_paycheque", $pay_cheque["pay_cheque_id"])->slotchild()->account()->get()->toArray();
                    $data["_pay_cheque"][$key2]["_dsb_report"] = $data["_dsb_report"];
                }
                elseif($pay_cheque["pay_cheque_batch_reference"] == "Indirect Sales Bonus")
                {
                    $data["_isb_report"] = null;
                    $data["_isb_report"] = Tbl_indirect_sb::where("isb_paycheque", $pay_cheque["pay_cheque_id"])->slotchild()->account()->get()->toArray();
                    $data["_pay_cheque"][$key2]["_isb_report"] = $data["_isb_report"];
                } 
                elseif($pay_cheque["pay_cheque_batch_reference"] == "Royalty Bonus")
                {
                    $data["_rb_report"] = null;
                    $data["_rb_report"] = Tbl_royalty_bonus::where("rb_paycheque", $pay_cheque["pay_cheque_id"])->slotchild()->account()->get()->toArray();
                    $data["_pay_cheque"][$key2]["_rb_report"] = $data["_rb_report"];
                }                  
                elseif($pay_cheque["pay_cheque_batch_reference"] == "Group Sales Bonus")
                {
                    $data["_gsb_report"] = null;
                    $data["_gsb_report"] = Tbl_total_gsb::where("gsb_paycheque", $pay_cheque["pay_cheque_id"])->slot()->groupsalesbonus()->account()->orderby("total_gsb_level","ASC")->get()->toArray();

                    $data["_gsbreport"] = Tbl_total_gsb::where("gsb_paycheque", $pay_cheque["pay_cheque_id"])->slot()->groupsalesbonus()->account()->orderby("total_gsb_level","ASC")->first();
                    $data["position"] = $data["_gsbreport"]->sequence_name;
                    $data["_gsb_history"] = Tbl_gsb_report::where("gsb_distributed", $pay_cheque["pay_cheque_id"])->slot()->account()->orderby("level","ASC")->get()->toArray();
                    $data["_pay_cheque"][$key2]["_gsb_report"] = $data["_gsb_report"];
                    $data["_pay_cheque"][$key2]["_gsb_history"] = $data["_gsb_history"];
                }
            }

            $data["_slot"][$key]["_pay_cheque"] = $data["_pay_cheque"];
            // dd($data);
        }
        $data['payment_option'] = DB::table('tbl_pay_cheque_payment_options')->where('pay_cheque_id', $id)->first();
        // dd($data);
        $pdf = PDF::loadView("admin.transaction.paycheque_voucher", $data);
                    return $pdf->stream('Paycheque.pdf');
                    return Redirect::to('/admin/transaction/paycheque');
                    
                    
		if($data["pay_cheque"]->pay_cheque_batch_reference == "binary")
		{
			return $this->voucher_binary($data["pay_cheque"]);
		}
	}
	public function voucher_binary($pay_cheque)
	{
		$data["page"] = "Pay Cheque - Voucher";
		$data["pay_cheque"] = $pay_cheque;
		$data["_binary_report"] = null;
		$_binary_report = Tbl_binary_report::where("binary_payout_paycheck", $pay_cheque->pay_cheque_id)->get()->toArray();
		
		foreach($_binary_report as $key => $binary_report)
		{
			$data["_binary_report"][$key] = $binary_report;
			$data["_binary_report"][$key]["breakdown"] = Tbl_binary_breakdown::where("binary_report_id", $binary_report["binary_id"])->slot()->account()->get()->toArray();
		}

		return view("distributor.cheque.binary_voucher", $data);
	}
}
<?php namespace App\Http\Controllers;
use DB;
use Redirect;
use Request;
use App\Classes\Image;
use App\Classes\Admin;
use App\Classes\Log;
use App\Globals\AdminNav;
use Input;

class AdminProductCategoryController extends AdminController
{
	public function index()
	{
		$data["_product_category"] = DB::table("tbl_product_category")->where("archived", 0)->get();
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits Product Category");
        
        $code = "Product Categories";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {    
        	return view('admin.maintenance.product_category', $data);
        }
        else
        {
            return Redirect::back();
        }
	}
	public function add()
	{
        return view('admin.maintenance.product_category_add');
	}
	public function add_submit()
	{
		if(Input::hasFile('image_file'))
		{
			$string = str_random(15);
            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            // generate a pin based on 2 * 7 digits + a random character
            $pin = mt_rand(1000000, 9999999)
                . mt_rand(1000000, 9999999)
                . $characters[rand(0, strlen($characters) - 1)];
            // shuffle the result
            $string1 = str_shuffle($pin);
            $get = $string1;
            $file = Input::file('image_file');
            $module = "product_icon";
				//image name to save on DB//
            $image = '/resources/assets/img/'.$module.'/' . $get.'-'.$file->getClientOriginalName() . '';
            $insert["product_category_icon"] = $image;
            $file -> move('resources/assets/img/'.$module.'' , $get.'-'.$file->getClientOriginalName('image_file'));
        }
        else
        {
        	$insert["product_category_icon"] = "/resources/assets/img/1428733091.jpg";
        }

        if(Input::hasFile('image_banner'))
        {
            $string = str_random(15);
            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            // generate a pin based on 2 * 7 digits + a random character
            $pin = mt_rand(1000000, 9999999)
                . mt_rand(1000000, 9999999)
                . $characters[rand(0, strlen($characters) - 1)];
            // shuffle the result
            $string1 = str_shuffle($pin);
            $get = $string1;
            $file = Input::file('image_banner');
            $module = "product_banner";
                //image name to save on DB//
            $image = '/resources/assets/img/'.$module.'/' . $get.'-'.$file->getClientOriginalName() . '';
            $insert["product_category_banner"] = $image;
            $file -> move('resources/assets/img/'.$module.'' , $get.'-'.$file->getClientOriginalName('image_banner'));
        }
        else
        {
            $insert["product_category_banner"] = "/resources/assets/img/1428733091.jpg";
        }

		$insert["product_category_name"] = Request::input("title");
        $insert["product_category_description"] = Request::input("description");
		$insert["product_category_color"] = Request::input("color");
		$insert["created_at"] = date('Y-m-d H:i:s');

		$id = DB::table("tbl_product_category")->insertGetId($insert);
		$new = DB::table("tbl_product_category")->where('product_category_id',$id)->first();
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." add Product Category id #".$id,null,serialize($new));
        return Redirect::to("/admin/maintenance/product_category");
	}	
	public function edit()
	{
		$id = Request::input("id");
		$data["product_category"] = DB::table("tbl_product_category")->where("product_category_id", $id)->first();
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." edit Product Category id #".$id);
        return view('admin.maintenance.product_category_edit', $data);
	}
	public function edit_submit()
	{
		$id = Request::input("id");
		if(Input::hasFile('image_file'))
		{
			$string = str_random(15);
            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            // generate a pin based on 2 * 7 digits + a random character
            $pin = mt_rand(1000000, 9999999)
                . mt_rand(1000000, 9999999)
                . $characters[rand(0, strlen($characters) - 1)];
            // shuffle the result
            $string1 = str_shuffle($pin);
            $get = $string1;
            $file = Input::file('image_file');
            $module = "product_icon";
				//image name to save on DB//
            $image = '/resources/assets/img/'.$module.'/' . $get.'-'.$file->getClientOriginalName() . '';
            $insert["product_category_icon"] = $image;
            $file -> move('resources/assets/img/'.$module.'' , $get.'-'.$file->getClientOriginalName('image_file'));
            // dd($image_file1);
        }
        else
        {
        	$img1 = DB::table("tbl_product_category")->where('product_category_id', $id)->first();
        	$insert["product_category_icon"] = $img1->product_category_icon;
        	// dd($image_file1);
        }

        if(Input::hasFile('image_banner'))
        {
            $string = str_random(15);
            $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
            // generate a pin based on 2 * 7 digits + a random character
            $pin = mt_rand(1000000, 9999999)
                . mt_rand(1000000, 9999999)
                . $characters[rand(0, strlen($characters) - 1)];
            // shuffle the result
            $string1 = str_shuffle($pin);
            $get = $string1;
            $file = Input::file('image_banner');
            $module = "product_banner";
                //image name to save on DB//
            $image = '/resources/assets/img/'.$module.'/' . $get.'-'.$file->getClientOriginalName() . '';
            $insert["product_category_banner"] = $image;
            $file -> move('resources/assets/img/'.$module.'' , $get.'-'.$file->getClientOriginalName('image_banner'));
            // dd($image_file1);
        }
        else
        {
            $img1 = DB::table("tbl_product_category")->where('product_category_id', $id)->first();
            $insert["product_category_banner"] = $img1->product_category_banner;
            // dd($image_file1);
        }

		$insert["product_category_name"] = Request::input("title");
        $insert["product_category_description"] = Request::input("description");
		$insert["product_category_color"] = Request::input("color");
		$insert["updated_at"] = date('Y-m-d H:i:s');
		$old = DB::table("tbl_product_category")->where("product_category_id", $id)->first();
		DB::table("tbl_product_category")->where("product_category_id", $id)->update($insert);
		$new = DB::table("tbl_product_category")->where("product_category_id", $id)->first();
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." edit Product Category id #".$id,serialize($old),serialize($new));
        return Redirect::to("/admin/maintenance/product_category");
	}	
	public function delete()
	{
		$id = Request::input("id");

		DB::table("tbl_product_category")->where("product_category_id", $id)->update(['archived' => 1]);
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." delete Product Category id #".$id);
        return Redirect::to("/admin/maintenance/product_category");
	}	
}
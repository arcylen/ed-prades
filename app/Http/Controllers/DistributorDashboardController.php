<?php namespace App\Http\Controllers;
use App\Tbl_account_log;
use App\Tbl_slot;
use App\Tbl_slot_log;
use App\Tbl_lead;
use App\Tbl_membership;
use App\Tbl_pay_cheque;
use App\Tbl_pay_cheque_batch;
use App\Tbl_news;
use App\Classes\Customer;
use DB;
use Crypt;
use Request;
use Redirect;
use App\Tbl_product_code;
use App\Tbl_tree_placement;
use App\Tbl_wallet_logs;
use App\Tbl_direct_report;
use App\Tbl_total_gsb;
use App\Tbl_indirect_report;
use App\Tbl_royalty_bonus;
use App\Tbl_pipeline_report;
use App\Tbl_binary_report;
use Session;
use App\Classes\Compute;
use Carbon\Carbon;
use App\Classes\Log;
use App\Tbl_membership_code;
use App\Tbl_account;
use App\Rel_membership_code;
use App\Rel_membership_product;
use App\Tbl_binary_pairing;
class DistributorDashboardController extends DistributorController
{
	public function index()
	{
		$id = Customer::id();
		// $data['error_message'] = null;
		if(isset($_POST['sbmtclaim']))
		{
			$error_message = $this->claim_code(Request::input(),$id);
			// dd($data['error_message']);
			return Redirect::back()->withErrors([$error_message, $error_message]);
		}
		if(isset($_POST['slot_position']))
		{	
				$info = $this->addslot(Request::input());
				if(isset($info['success']))
				{
					$message = $info['success'];
					return Redirect::back()->withErrors([$message, $message]);
				}
		}


        $data = $this->getslotbyid($id);
        // dd($data);
        return view('distributor.dashboard.dashboard', $data);
	}
	public function binary_projection($data)
	{
		$slot_info = $this->slotnow;
        $max_pair = $slot_info->max_pairs_per_day;
        $number_of_pairs = 0;
        $earnings = 0;
        $flushout = 0;

        $_pairing = Tbl_binary_pairing::orderBy("pairing_point_l", "desc")->where('membership_id',$slot_info->slot_membership)->get();
        $initial["left"] = $binary["left"] = $slot_info->slot_binary_left;
        $initial["right"] = $binary["right"] = $slot_info->slot_binary_right; 


        foreach($_pairing as $pairing)
        {   
            if($pairing->membership_id == $slot_info->slot_membership)
            {
                while($binary["left"] >= $pairing->pairing_point_l && $binary["right"] >= $pairing->pairing_point_r)
                {
                    $number_of_pairs++;

                    if($number_of_pairs > $max_pair) //FLUSHOUT
                    {
                        Compute::$total_binary_flushout += $pairing->pairing_income;
                        Compute::$total_binary_pairing++;
                        $flushout+= $pairing->pairing_income;

                    }
                    else //PAIRING
                    {
                        $earnings += $pairing->pairing_income;
                        Compute::$total_binary_payout += $pairing->pairing_income;
                        Compute::$total_binary_pairing++;
                    }

                    $binary["left"] = $binary["left"] - $pairing->pairing_point_l;
                    $binary["right"] = $binary["right"] - $pairing->pairing_point_r;
                }
            }
        }

        $data["today_pairing"] = $earnings;
        $data["today_flushout"] = $flushout;
        $data["remain_left"] = $binary["left"];
        $data["remain_right"] = $binary["right"];

        return $data;
	}
	public function addslot($data)
	{
		// ignore_user_abort(true);
		// set_time_limit(0);


		// $info2 = $this->addslotnoinsert(Request::input());

		// if(isset($info2['success']))
		// {
		// 	$message2 = $info2['success'];
		// 	$strURL = "/member/code_vault?message=$message2";
		// 	header("Location: $strURL", true);
		// }

		// header("Connection: close", true);
		// header("Content-Encoding: none\r\n");
		// header("Content-Length: 0", true);


		// flush();
		// ob_flush();

		// session_write_close();

		$return["message"] = "";
		$data["message"] = "";

		if(strtolower(Request::input("slot_position")) == 'left' || strtolower(Request::input("slot_position")) == 'right')
		{
				$getslot = Tbl_membership_code::where('code_pin',$data['code_number'])->getmembership()->first();
				$check_placement = Tbl_slot::checkposition(Request::input("placement"), strtolower(Request::input("slot_position")))->first();
				$check_id = Tbl_slot::id(Request::input("slot_number"))->first();
				$checkifowned = Tbl_account::where('tbl_account.account_id',Customer::id())->belongstothis()->get();
				$ifused = Tbl_membership_code::where('code_pin',$data['code_number'])->where('used',1)->first();
				$checkslot = Tbl_slot::where('slot_id',$data['sponsor'])->get();
				$checking = false;
				$checking2 = false;
				foreach($checkifowned as $c)
				{
					if($c->code_pin == $data['code_number'])
					{
						$checking = true;
					}
				}

				foreach($checkslot as $c)
				{
					if($c->slot_id == $data['sponsor'])
					{
						$checking2 = true;
					}
				}
				
			 	$limit = DB::table('tbl_settings')->where('key','slot_limit')->first();
				$count = Tbl_slot::where('slot_owner',Customer::id())->count();
				if($limit->value <=  $count)
				{
					$data["message"] = "This account is already reach the max slot per account. Max slot per account is ".$limit->value.".";
				}
				// elseif($check_placement)
				// {
				// 	$return["message"] = "The position you're trying to use is already occupied";
				// }
				// if(Request::input("placement") == 0)
				// {
				// 	$return["message"] = "The position you're trying to use is already occupied";
				// }
				if($ifused)
				{
					$return["message"] = "This code is already used";
				}
				elseif($data["message"] != "")
				{
					$return["message"] = $data["message"];
				}
				else
				{
					if($getslot->code_type_id == 3)
					{
						if($checking == true && $checking2 == true)
						{
							$ifused = Tbl_membership_code::where('code_pin',$data['code_number'])->where('used',1)->first();
							
							if($ifused)
							{
								$return["message"] = "This code is already used";
							}
							else
							{
								Tbl_membership_code::where('code_pin',$data['code_number'])->update(['used'=>1]);
								$insert["slot_membership"] =  $getslot->membership_id;
								$insert["slot_type"] =  "CD";
								$insert["slot_rank"] =  1;
								// $insert["slot_wallet"] =  0 - $getslot->membership_price;
								// $insert["cd_done"] = 1;
								$insert["slot_sponsor"] =  $data['sponsor'];
								$insert["slot_placement"] =  $data['placement'];
								$insert["slot_position"] =  strtolower($data['slot_position']);
								$insert["slot_binary_left"] =  0;
								$insert["slot_binary_right"] =  0;
								$insert["slot_personal_points"] =  0;
								$insert["slot_group_points"] =  0;
								$insert["distributed"] =  0;
								$insert["slot_upgrade_points"] = 0;
								$insert["slot_total_withrawal"] =  0;
								$insert["slot_total_earning"] =  0 - $getslot->membership_price;
								$insert["created_at"] =  Carbon::now();
								$insert["slot_owner"] =  Customer::id();
								$insert["membership_entry_id"] =  $getslot->membership_id;
								$slot_id = Tbl_slot::insertGetId($insert);

								$logs = "Successfully create slot #".$slot_id." using membership code #".$getslot->code_pin.".";
								$amount = 0 - $getslot->membership_price;
								Log::slot($slot_id, $logs, $amount, "New slot",$slot_id);

								Compute::tree($slot_id);
								Compute::entry($slot_id);
								$return["placement"] = Request::input("placement");
								$message['success'] = "Slot Created.";
								$get = Rel_membership_code::where('code_pin',$data['code_number'])->first();
								if(isset($get->product_package_id))
								{
									$insert2['slot_id'] = $slot_id;
									$insert2['product_package_id'] = $get->product_package_id;
									Rel_membership_product::insert($insert2);								
								}
								$code = Tbl_membership_code_sale_has_code::where('code_pin',$data['code_number'])->first();
								if($code)
								{
									$code_sale = Tbl_membership_code_sale::where('membershipcode_or_num',$code->membershipcode_or_num)->first();
									if($code_sale)
									{
										Tbl_voucher::where('voucher_id',$code_sale->voucher_id)->update(["slot_id"=>$slot_id]);
									}								
								}
								Tbl_slot::where('slot_id',$slot_id)->update(['distributed'=>1]);
								return $message;
							}
						}
					}
					else if($getslot->code_type_id == 1)
					{
						if($checking == true && $checking2 == true)
						{
							$ifused = Tbl_membership_code::where('code_pin',$data['code_number'])->where('used',1)->first();
							if($ifused)
							{
								$return["message"] = "This code is already used";
							}
							else
							{
								Tbl_membership_code::where('code_pin',$data['code_number'])->update(['used'=>1]);
								$insert["slot_membership"] =  $getslot->membership_id;
								$insert["slot_type"] =  "PS";
								$insert["slot_rank"] =  1;
								// $insert["slot_wallet"] =  0;
								$insert["slot_sponsor"] =  $data['sponsor'];
								$insert["slot_placement"] =  $data['placement'];
								$insert["slot_position"] =  strtolower($data['slot_position']);
								$insert["slot_binary_left"] =  0;
								$insert["slot_binary_right"] =  0;
								$insert["slot_personal_points"] =  0;
								$insert["slot_group_points"] =  0;
								$insert["slot_upgrade_points"] = 0;
								$insert["distributed"] =  0;
								$insert["slot_total_withrawal"] =  0;
								$insert["slot_total_earning"] =  0;
								$insert["created_at"] =  Carbon::now();
								$insert["slot_owner"] =  Customer::id();
								$insert["membership_entry_id"] =  $getslot->membership_id;
								$slot_id = Tbl_slot::insertGetId($insert);

								$logs = "Successfully create slot #".$slot_id." using membership code #".$getslot->code_pin.".";
								$amount = 0;
								Log::slot($slot_id, $logs, $amount, "New slot",$slot_id);

								Compute::tree($slot_id);
								Compute::entry($slot_id);
								$return["placement"] = Request::input("placement");
								$message['success'] = "Slot Created.";


								$get = Rel_membership_code::where('code_pin',$data['code_number'])->first();
								if(isset($get->product_package_id))
								{
									$insert2['slot_id'] = $slot_id;
									$insert2['product_package_id'] = $get->product_package_id;
									Rel_membership_product::insert($insert2);								
								}

								Tbl_slot::where('slot_id',$slot_id)->update(['distributed'=>1]);
								return $message;

							}
							
						}
					}
					else
					{
						if($checking == true && $checking2 == true)
						{
							$ifused = Tbl_membership_code::where('code_pin',$data['code_number'])->where('used',1)->first();
							if($ifused)
							{
								$return["message"] = "This code is already used";
							}
							else
							{
								Tbl_membership_code::where('code_pin',$data['code_number'])->update(['used'=>1]);
								$insert["slot_membership"] =  $getslot->membership_id;
								$insert["slot_type"] =  "FS";
								$insert["slot_rank"] =  1;
								// $insert["slot_wallet"] =  0;
								$insert["slot_sponsor"] =  $data['sponsor'];
								$insert["slot_placement"] =  $data['placement'];
								$insert["slot_position"] =  strtolower($data['slot_position']);
								$insert["slot_binary_left"] =  0;
								$insert["slot_binary_right"] =  0;
								$insert["slot_personal_points"] =  0;
								$insert["slot_group_points"] =  0;
								$insert["slot_upgrade_points"] = 0;
								$insert["slot_total_withrawal"] =  0;
								$insert["slot_total_earning"] =  0;
								$insert["distributed"] =  0;
								$insert["created_at"] =  Carbon::now();
								$insert["slot_owner"] =  Customer::id();
								$insert["membership_entry_id"] =  $getslot->membership_id;
								$slot_id = Tbl_slot::insertGetId($insert);

								$logs = "Successfully create slot #".$slot_id." using membership code #".$getslot->code_pin.".";
								$amount = 0;
								Log::slot($slot_id, $logs, $amount, "New slot",$slot_id);

								Compute::tree($slot_id);
								Compute::entry($slot_id);
								$return["placement"] = Request::input("placement");
								$message['success'] = "Slot Created.";
								$get = Rel_membership_code::where('code_pin',$data['code_number'])->first();
								if(isset($get->product_package_id))
								{
									$insert2['slot_id'] = $slot_id;
									$insert2['product_package_id'] = $get->product_package_id;
									Rel_membership_product::insert($insert2);								
								}


								Tbl_slot::where('slot_id',$slot_id)->update(['distributed'=>1]);
								return $message;
							}
						}						
					}
				}			
		}
		else
		{
			$message = "";
			return $message;
		}
		// return redirect::to("/distributor?message=$message");
		// sleep(1);
		// exit;


	}
	public function encash(){

	}
	public function notification()
	{

		$data["_notification"] = $this->get_notifications(true);
        return view('member.notification', $data);
	}
	public function get_notifications($all)
	{
		$_notification = Tbl_wallet_logs::orderBy('wallet_logs_id', 'desc');

		if(!$all)
		{
			$_notification->take(6);
		}
		$_notification = $_notification->where('slot_id', Session::get('currentslot'));
		$_notification = $_notification->get();



		$data["_notification"] = null;

		foreach($_notification as $key => $notification)
		{
			$data["_notification"][$key] = $notification;
			$data["_notification"][$key]->date = date("F d, Y", strtotime($notification->created_at)) .  " - " . date("h:i A", strtotime($notification->created_at));
		}

		return $data["_notification"];
	}

	public function changepassword()
	{
		$query = DB::table('tbl_account')->where('account_id', Customer::id())->first();
		if(Request::input('npw') == Request::input('cpw'))
		{
			$opw = Request::input('opw');
			$opwd = Crypt::decrypt($query->account_password);
			// dd($opw, $opwd);
			if($opw == $opwd)
			{
				$change_password['account_password'] = Crypt::encrypt(Request::input('npw'));
				DB::table('tbl_account')->where('account_id', Customer::id())->update($change_password);

				$noti["title"] = "Success!";
				$noti["message"] = "Your password is successfully updated.";
				return Redirect::to("/distributor")->with("notification", $noti);
			}
			else
			{
				$noti["title"] = "Error!";
				$noti["message"] = "Old password is incorrect.";
				return Redirect::to("/distributor")->with("notification", $noti);
			}
		}
		else
		{
			$noti["title"] = "Error!";
			$noti["message"] = "Password didn't match.";
			return Redirect::to("/distributor")->with("notification", $noti);
		}
	}
	public function updateprofile()
	{
		$encashment_type = Request::input('encashmentselect');

		$update_validator['gender'] = Request::input('account-gender');
		$update_validator['birthday'] = Request::input('datepicker');
		$update_validator['account_country_id'] = Request::input('account-location');
		$update_validator['address'] = Request::input('address');
		$update_validator['account_username'] = Request::input('username');
		$update_validator['account_email'] = Request::input('email');
		$update_validator['account_contact_number'] = Request::input('contact-number');
		$update_validator['account_encashment_type'] = $encashment_type;
		if($encashment_type == 1)
		{
			$bank = DB::table('tbl_bank_deposit')->where('bank_user_id', Customer::id())->count();
			$bank_update_validator['bank_name'] = Request::input('bankselect');
			$bank_update_validator['bank_branch'] = Request::input('bank-branch');
			$bank_update_validator['bank_account_name'] = Request::input('bank-account-name');
			$bank_update_validator['bank_account_number'] = Request::input('bank-account-id');
			$bank_update_validator['bank_user_id'] = Customer::id();
			if($bank !=1)
			{
				DB::table('tbl_bank_deposit')->insert($bank_update_validator);
			}
			else
			{
				DB::table('tbl_bank_deposit')->where('bank_user_id', Customer::id())->update($bank_update_validator);
			}
		}

		if($encashment_type == 2)
		{
			$check_update_validator['cheque_name'] = Request::input('cheque-name');
			$check_update_validator['cheque_user_id'] = Customer::id();
			$cheque = DB::table('tbl_cheque')->where('cheque_user_id', Customer::id())->count();
			if($cheque !=1)
			{
				DB::table('tbl_cheque')->insert($check_update_validator);
			}
			else
			{
				DB::table('tbl_cheque')->where('cheque_user_id', Customer::id())->update($check_update_validator);
			}
		}
		DB::table('tbl_account')->where('account_id', Customer::id())->update($update_validator);
		$noti["title"] = "Success!";
		$noti["message"] = "Your account is successfully updated.";
		return redirect()->back()->with("notification", $noti);
		// dd($update_validator);
	}
	public function updatabank(){
		$encashment_type = Request::input('encashmentselect');
		if($encashment_type == 1)
		{
			$bank = DB::table('tbl_bank_deposit')->where('bank_user_id', Customer::id())->count();
			$bank_update_validator['bank_name'] = Request::input('bankselect');
			$bank_update_validator['bank_branch'] = Request::input('bank-branch');
			$bank_update_validator['bank_account_name'] = Request::input('bank-account-name');
			$bank_update_validator['bank_account_number'] = Request::input('bank-account-id');
			$bank_update_validator['bank_user_id'] = Customer::id();
			if($bank !=1)
			{
				DB::table('tbl_bank_deposit')->insert($bank_update_validator);
			}
			else
			{
				DB::table('tbl_bank_deposit')->where('bank_user_id', Customer::id())->update($bank_update_validator);
			}
		}

		if($encashment_type == 2)
		{
			$check_update_validator['cheque_name'] = Request::input('cheque-name');
			$check_update_validator['cheque_user_id'] = Customer::id();
			$cheque = DB::table('tbl_cheque')->where('cheque_user_id', Customer::id())->count();
			if($cheque !=1)
			{
				DB::table('tbl_cheque')->insert($check_update_validator);
			}
			else
			{
				DB::table('tbl_cheque')->where('cheque_user_id', Customer::id())->update($check_update_validator);
			}
		}
		$noti["title"] = "Success!";
		$noti["message"] = "Your account is successfully updated.";
		return redirect()->back()->with("notification", $noti);
	}
	public function encashsments(){
		// return $_POST;
		$method = Request::input('method');
		$paymentype = Request::input('payment_type');
		
		$update['pay_cheque_requested'] = 2;
		$paycheckid = Tbl_pay_cheque::where('tbl_pay_cheque.pay_cheque_id', $method)->update($update);
		if($paymentype == 1){
			// bankselect
			// bank-branch
			// bank-account-name
			// bank-account-id
			$insert["bank_type"] = $paymentype;
			$insert["bank_name"] = Request::input('bankselect');
			$insert["bank_branch"] = Request::input('bank-branch');
			$insert["bank_account_name"] = Request::input('bank-account-name');
			$insert["bank_account_number"] = Request::input('bank-account-id');
			$insert["pay_cheque_id"] = $method;
			DB::table('tbl_pay_cheque_payment_options')->insert($insert);
		}
		else{
			$insert["bank_type"] = $paymentype;
			$insert["bank_name"] = 0;
			$insert["bank_branch"] = 0;
			$insert["bank_account_name"] = Request::input('cheque-name');
			$insert["bank_account_number"] = 0;
			$insert["pay_cheque_id"] = $method;
			DB::table('tbl_pay_cheque_payment_options')->insert($insert);
		}
		Session::flash('message', 'The wallet '.formatslot( $method). '  has been requested for encashsment.');
		return Redirect::back();
	}
	public function claim_code($data,$id)
	{
		$info = null;
		$pin = DB::table('tbl_membership_code')->where('lock',0)->where('code_pin','=',$data['pin'])->first();
		if($pin)
		{	
			$used = DB::table('tbl_membership_code')->where('used',1)->where('code_pin','=',$data['pin'])->first();
			if($used)
			{
				$info = "This code is already used";
			}
			else if($pin->account_id == $id) 
			{
				$info = "You already have this code";
			}

			else if($pin->code_activation == $data['activation'])
			{

				$getId = DB::table('tbl_membership_code')->where('code_pin','=',$data['pin'])->first();
				DB::table('tbl_membership_code')->where('code_pin','=',$data['pin'])->update(['account_id'=>$id]);
				$getName = DB::table('tbl_account')->where('account_id',$id)->first();
				$insert['code_pin'] = $data['pin'];
				$insert['by_account_id'] = $getId->account_id;
				$insert['to_account_id'] = $id;
				$insert['updated_at'] = Carbon::now();
				$insert['description'] = "Claimed by ".$getName->account_name;
				$pint = $data['pin'];
				$fromname = DB::table('tbl_account')->where('account_id',$getId->account_id)->first();
				DB::table("tbl_member_code_history")->insert($insert);
				Log::account(Customer::id(),"You claimed a membership code from $fromname->account_name  (Pin #$pint))");
				Log::account($fromname->account_id,"$getName->account_name claimed your membership code (Pin #$pint))");
			}
			else
			{
				$info = "Pin code's activation key mismatch.";
			}
		}
		else
		{
			$iflock = DB::table('tbl_membership_code')->where('lock',1)->where('code_pin','=',$data['pin'])->first();
			if($iflock)
			{
				$info = "This code is lock and cannot be claimed.";
			}
			else
			{
				$info = "Pin code doesn't exist.";	
			}
		}
			return $info;
	}
	public function getslotbyid($id)
	{
		$account_id = Customer::id();
		$joined_date = Customer::info()->account_date_created;
		$data['joined_date'] = date("M d,Y", strtotime($joined_date));
		$data['bank_info'] = DB::table('tbl_bank_deposit')->where('bank_user_id', Customer::id())->first();
		$data['cheque_info'] = DB::table('tbl_cheque')->where('cheque_user_id', Customer::id())->first();
		 $data['newsactive'] = Tbl_news::where('archived', 0)->take(1)->get();
        $data['news'] = Tbl_news::where('archived', 0)->skip(1)->take(2)->get();
		if(isset($this->slotnow->slot_id)){
			$data['registered'] = 1;
			$data["_notification"] = $this->get_notifications(false);

			$data["total_direct"] = Tbl_direct_report::where("direct_sponsor", $this->slotnow->slot_id)->where("direct_pay_cheque",0)->sum('direct_amount');
			$data["total_indirect"] = Tbl_indirect_report::where("indirect_slot", $this->slotnow->slot_id)->sum('indirect_amount');
			$data["total_pipeline"] = Tbl_pipeline_report::where("pipeline_slot_id", $this->slotnow->slot_id)->sum('pipeline_earned_amount');
			$data = $this->binary_projection($data);
			
			$data["total_dsb"] = DB::table("tbl_direct_sb")->where("dsb_sponsor",$this->slotnow->slot_id)->where("dsb_paycheque",0)->sum("dsb_amount");
			$data["total_isb"] = DB::table("tbl_indirect_sb")->where("isb_sponsor",$this->slotnow->slot_id)->where("isb_paycheque",0)->sum("isb_amount");
			$data["_gsb"] = DB::table("tbl_total_gsb")->where("gsb_slot_id",$this->slotnow->slot_id)
								->where("personal_true",0)->where("total_gsb_distributed",0)->sum("total_earned");
			$data["_personal_gsb"] = DB::table("tbl_total_gsb")->where("gsb_slot_id",$this->slotnow->slot_id)->where("personal_true",1)->sum("total_earned");
			
			$gspv = DB::table("tbl_slot")->where("slot_id",$this->slotnow->slot_id)->sum("total_group_spv");
			$pspv = DB::table("tbl_slot")->where("slot_id",$this->slotnow->slot_id)->sum("total_personal_spv");
			$data["spv_permonth"] = $gspv + $pspv;
			// $data["total_gsb"] = $data["_personal_gsb"] + $data["_gsb"];
			$data["royalty_bonus"] = Tbl_royalty_bonus::where("rb_slot_id",$this->slotnow->slot_id)->where("rb_paycheque",0)->sum("rb_bonus");   
			$data["personal_gsb"] = DB::table("tbl_total_gsb")
										->join("tbl_sequence","tbl_sequence.sequence_id","=","tbl_total_gsb.sequence_level_id")
										->where("tbl_total_gsb.gsb_slot_id",$this->slotnow->slot_id)
										->where("tbl_total_gsb.personal_true",1)
										->where("tbl_total_gsb.total_gsb_distributed",0)
										->first();
									
			$data["gsb_report"] = DB::table("tbl_total_gsb")->join("tbl_sequence","tbl_sequence.sequence_id","=","tbl_total_gsb.sequence_level_id")
								->join("tbl_sequence_setting","tbl_sequence_setting.sequence_id","=","tbl_total_gsb.sequence_level_id")
								->where("gsb_slot_id",$this->slotnow->slot_id)
								->orderby("total_gsb_level","ASC")
								->where("tbl_total_gsb.personal_true",0)
								->groupby("total_gsb_level")
								->where("total_gsb_distributed",0)->get();

			$data["get_total"] = Tbl_total_gsb::where("gsb_slot_id",$this->slotnow->slot_id)
															->where("total_gsb_distributed",0)
															->get();
			// dd($data["gsb_report"]);
			$total = 0;
			if($data["get_total"])
			{
				$total_all = null;
				foreach ($data["get_total"] as $key => $value) {
					$total_all[$key] = $value;
					$total += (($total_all[$key]->gsb_percentage/100) * $total_all[$key]->total_spv);
				}
			}

 			$data["total_gsb"] = $total;

			// dd(	$data["gsb_report"]);
			$data["total_binary"] = Tbl_binary_report::where("binary_slot_id", $this->slotnow->slot_id)->sum('binary_earned') + $data["today_pairing"];
			$data["total_income"] = $data["total_direct"] + $data["royalty_bonus"] + $data["total_gsb"]  + $data["total_isb"] + $data["total_dsb"];
			$data["total_ready"] = Tbl_pay_cheque::where('pay_cheque_slot', $this->slotnow->slot_id)->where('pay_cheque_requested', 1)->sum('pay_cheque_amount');
			$data["total_request"]  = Tbl_pay_cheque::where('pay_cheque_slot', $this->slotnow->slot_id)->where('pay_cheque_requested', 2)->where('pay_cheque_processed', 0)->sum('pay_cheque_amount');
			$data["total_released"] = Tbl_pay_cheque::where('pay_cheque_slot', $this->slotnow->slot_id)->where('pay_cheque_processed', 1)->sum('pay_cheque_amount');
			$data["total_wallet"] = $data["total_income"] - ($data["total_request"] + $data["total_released"]);
			$data["total_count"] = Tbl_slot::where("slot_owner", Customer::id())->count();
			$data['leadc'] = Tbl_lead::where('lead_account_id',Customer::id())->count();
			$data['code'] = DB::table('tbl_membership_code')  ->where('tbl_membership_code.archived',0)
															  ->where('tbl_membership_code.blocked',0)
															  ->where('tbl_membership_code.used',0)
															  ->where('tbl_membership_code.account_id','=',Customer::id())
															  ->count();

			$data['left_side'] = Tbl_tree_placement::where('placement_tree_parent_id',Customer::slot_id())->where('placement_tree_position','left')->count(); 												  
			$data['right_side'] = Tbl_tree_placement::where('placement_tree_parent_id',Customer::slot_id())->where('placement_tree_position','right')->count();
			$data['prod'] = Tbl_product_code::where("account_id", Customer::id())->where('tbl_product_code.used',0)->voucher()->product()->orderBy("product_pin", "desc")->unused()->count();													  

			
			$slot_info = Tbl_slot::membership()->id(Customer::slot_id())->first();
			if($slot_info)
			{
				$travel = Compute::compute_travel($slot_info);
				$data['points'] = $travel['points'];
				$data['reward'] = $travel['reward'];
				$data['oldwallet'] = Tbl_wallet_logs::id(Session::get('currentslot'))->where('keycode','Old System Wallet')->wallet()->sum('wallet_amount');	
				$data["next_membership"] = Tbl_membership::where("membership_required_upgrade", ">",  $slot_info->membership_required_upgrade)->orderBy("membership_required_upgrade", "asc")->first();			
			}

			// $data['earnings']['total_income'] = Tbl_pay_cheque::where('pay_cheque_slot', $this->slotnow->slot_id)->where('pay_cheque_processed', 0)->sum('pay_cheque_amount');
			// $data['earnings']['total_withdrawal'] = Tbl_pay_cheque::where('pay_cheque_slot', $this->slotnow->slot_id)->where('pay_cheque_processed', 1)->sum('pay_cheque_amount');
		}
		else
		{
			$data['registered'] = 0;
		}
		$data['code'] = DB::table('tbl_membership_code')  ->where('tbl_membership_code.archived',0)
														  ->where('tbl_membership_code.blocked',0)
														  ->where('tbl_membership_code.used',0)
														  ->join('tbl_account','tbl_account.account_id','=','tbl_membership_code.account_id')
														  ->join('tbl_code_type','tbl_code_type.code_type_id','=','tbl_membership_code.code_type_id')
														  ->join('tbl_membership','tbl_membership.membership_id','=','tbl_membership_code.membership_id')
														  ->leftjoin('tbl_product_package','tbl_product_package.product_package_id','=','tbl_membership_code.product_package_id')
														  ->where('tbl_membership_code.account_id','=',$id)
														  ->orderBy('tbl_membership_code.code_pin','ASC')
														  ->get();

		$data['used_code'] = DB::table('tbl_membership_code') ->where('tbl_membership_code.archived',0)
															  ->where('tbl_membership_code.blocked',0)
															  ->where('tbl_membership_code.used',1)
															  ->join('tbl_account','tbl_account.account_id','=','tbl_membership_code.account_id')
															  ->join('tbl_code_type','tbl_code_type.code_type_id','=','tbl_membership_code.code_type_id')
															  ->join('tbl_membership','tbl_membership.membership_id','=','tbl_membership_code.membership_id')
															  ->leftjoin('tbl_product_package','tbl_product_package.product_package_id','=','tbl_membership_code.product_package_id')
															  ->where('tbl_membership_code.account_id','=',$id)
															  ->orderBy('tbl_membership_code.code_pin','ASC')
															  ->get();

		$data['getallslot'] = Tbl_slot::where('slot_owner',Customer::id())->get();
        foreach($data['code'] as $key => $d)
        {
    	$get =	 DB::table('tbl_member_code_history')->where('code_pin',$d->code_pin)
    												 ->join('tbl_account','tbl_account.account_id','=','tbl_member_code_history.by_account_id')
    											     ->orderBy('updated_at','DESC')
    											     ->first();
   											     
	    $data['code'][$key]->encrypt    = Crypt::encrypt($d->code_pin);	
          if($get)
          {
         	$data['code'][$key]->transferer = $get->account_name;	  			         	
          }
          else
          {
				$get = DB::table('tbl_membership_code')->where('code_pin',$d->code_pin)->join('tbl_stockist','tbl_stockist.stockist_id','=','tbl_membership_code.origin')->first();          	
          		if($get)
          		{
          			$data['code'][$key]->transferer = $get->stockist_full_name;
          		}	
          		else
          		{
          			$data['code'][$key]->transferer = "Admin";
          		}
          }						     
        }

        foreach($data['used_code'] as $key => $d)
        {
        	$get =	 DB::table('tbl_member_code_history')->where('code_pin',$d->code_pin)
        												 ->join('tbl_account','tbl_account.account_id','=','tbl_member_code_history.by_account_id')
        											     ->orderBy('updated_at','DESC')
        											     ->first();
       											     
		    $data['used_code'][$key]->encrypt    = Crypt::encrypt($d->code_pin);	
          if($get)
          {
         	$data['used_code'][$key]->transferer = $get->account_name;	  			         	
          }
          else
          {
				$get = DB::table('tbl_membership_code')->where('code_pin',$d->code_pin)->join('tbl_stockist','tbl_stockist.stockist_id','=','tbl_membership_code.origin')->first();          	
          		if($get)
          		{
          			$data['used_code'][$key]->transferer = $get->stockist_full_name;
          		}	
          		else
          		{
          			$data['used_code'][$key]->transferer = "Admin";
          		}
          }				     
        }

		$data['prodcode'] = Tbl_product_code::where("account_id", Customer::id())->where('tbl_product_code.used',0)->where('tbl_product_code.archived',0)->voucher()->product()->orderBy("product_pin", "desc")->unused()->get();										 
		$data['count']= DB::table('tbl_membership_code')->where('archived',0)->where('tbl_membership_code.used',0)->where('account_id','=',$id)->where('tbl_membership_code.blocked',0)->count();		
		$data['used_count']= DB::table('tbl_membership_code')->where('archived',0)->where('tbl_membership_code.used',1)->where('account_id','=',$id)->where('tbl_membership_code.blocked',0)->count();	
		$data['count2'] = Tbl_product_code::where("account_id", Customer::id())->where('tbl_product_code.used',0)->voucher()->product()->orderBy("product_pin", "desc")->unused()->count();										 
		
		if($data['count2'] == 0)
		{
			$data['prodcode'] = null;
		}
		
		$data['exist_lead'] = null;
	 	$check_date = Tbl_lead::where('account_id',$id)->where('tbl_lead.used',0)->first();
		
	 	if(isset($check_date))
	 	{	
		    	 	if(strtotime($check_date->join_date)  > strtotime(Carbon::now()->subDays(30)) && $check_date->used == 0)
				 	{
		    			$data['exist_lead'] = Tbl_lead::where('tbl_lead.account_id',$id)->getslot()->getaccount()->get();
				 	}
	 	}
		return $data;
	}
}
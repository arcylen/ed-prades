<?php namespace App\Http\Controllers;
use App\Tbl_direct_admin_report;
use App\Tbl_direct_report;
use Redirect;
use App\Classes\Compute;
use Request;
use DB;
use App\Globals\AdminNav;
class AdminDirectController extends AdminController
{
	public function index()
	{
		$data["page"] = "Direct Processing";
		$data["_direct_report"] = Tbl_direct_admin_report::orderBy('admin_report_id', 'desc')->get();
		
		$code = "Direct Breakdown";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {
			return view("admin.transaction.direct", $data);
		}
        else
        {
            return Redirect::back();
        }
	}
	public function daily()
	{
		Compute::direct_cutoff();

		return Redirect::to("/admin/transaction/direct")->send();
	}
	public function directreport()
	{
		$id = Request::input('id');
		// dd($id);
		$data['_detail'] = Tbl_direct_report::where('direct_admin_report', $id)
														   ->join('tbl_slot', 'tbl_slot.slot_id', '=', 'tbl_direct_report.direct_sponsor')
														   ->join('tbl_account', 'tbl_account.account_id', '=', 'tbl_slot.slot_owner')
														   ->get();
		// dd($data);
		return view('admin.transaction.directreport', $data);
	}
}
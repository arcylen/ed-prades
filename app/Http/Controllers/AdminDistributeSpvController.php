<?php namespace App\Http\Controllers;
use DB;
use Redirect;
use Request;
use App\Classes\Image;
use App\Classes\Compute;
use App\Tbl_about;
use App\Tbl_voucher;
use App\Tbl_voucher_has_product;
use App\Tbl_product_code;
use App\Globals\AdminNav;
class AdminDistributeSpvController extends AdminController
{
	public function index()
	{
		// dd($data["_voucher"]);
		return view("admin.transaction.distributespv");
	}
	public function distribute($id)
	{
		$data["_spv"] = Tbl_voucher::join("tbl_slot","tbl_slot.slot_id","=","tbl_voucher.slot_id")
										->join("tbl_account","tbl_account.account_id","=","tbl_slot.slot_owner")
										->join("tbl_voucher_has_product","tbl_voucher_has_product.voucher_id","=","tbl_voucher.voucher_id")
										->join("tbl_product_code", "tbl_voucher_has_product.voucher_item_id", "=", "tbl_product_code.voucher_item_id")
										->where("tbl_voucher.voucher_id",$id)
										->get();

		foreach ($data["_spv"] as $key => $value) {
			$data["_spv"][$key] = $value;

			$code_info = Tbl_product_code::where("product_pin", $data["_spv"][$key]->product_pin)->voucher()->product()->where("used",0)->first();
			if($code_info)
			{
					$update["used"] = 1;
					Tbl_product_code::where("product_pin", $data["_spv"][$key]->product_pin)->update($update);
					$unilevel_pts = $code_info->unilevel_pts; 
					$upgrade_pts = $code_info->upgrade_pts; 
					$binary_pts = $code_info->binary_pts;
					Compute::repurchase($data["_spv"][$key]->slot_id, $binary_pts, $unilevel_pts, $upgrade_pts, $data["_spv"][$key]->product_pin);
			}
		}
		$update_voucher["distributed"] = 1;
		Tbl_voucher::where("voucher_id",$id)->update($update_voucher);

		return Redirect::back();
	}
	public function data()
	{
		$filter = Request::input("filter");
		$data["filter"] = $filter;
		if($filter)
		{

		$data["_voucher"] = Tbl_voucher::join("tbl_slot","tbl_slot.slot_id","=","tbl_voucher.slot_id")
										->join("tbl_account","tbl_account.account_id","=","tbl_slot.slot_owner")
										->join("tbl_voucher_has_product","tbl_voucher_has_product.voucher_id","=","tbl_voucher.voucher_id")
										->join("tbl_product_code", "tbl_voucher_has_product.voucher_item_id", "=", "tbl_product_code.voucher_item_id")
										->where("tbl_voucher.distributed",$filter)
										->groupBy("tbl_voucher.voucher_id")
										->orderBy("tbl_voucher.created_at","DESC")
										->paginate(20);
		}
		else
		{

		$data["_voucher"] = Tbl_voucher::join("tbl_slot","tbl_slot.slot_id","=","tbl_voucher.slot_id")
										->join("tbl_account","tbl_account.account_id","=","tbl_slot.slot_owner")
										->join("tbl_voucher_has_product","tbl_voucher_has_product.voucher_id","=","tbl_voucher.voucher_id")
										->join("tbl_product_code", "tbl_voucher_has_product.voucher_item_id", "=", "tbl_product_code.voucher_item_id")
										->where("tbl_voucher.distributed",0)
										->groupBy("tbl_voucher.voucher_id")
										->orderBy("tbl_voucher.created_at","DESC")
										->paginate(20);
		}
		return view("admin.transaction.distributespv_table",$data);
	}
}
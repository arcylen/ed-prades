<?php namespace App\Http\Controllers;
use Request;
use Redirect;
use App\Tbl_membership;
use App\Tbl_binary_pairing;
use App\Tbl_product;
use App\Tbl_royalty_promotion_setting;
use App\Tbl_royalty_setting;
use App\Tbl_indirect_unilevel;
use App\Tbl_unilevel_setting;
use App\Tbl_direct_unilevel;
use App\Tbl_sequence;
use App\Tbl_indirect_unilevel_setting;
use App\Tbl_direct_unilevel_setting;
use App\Tbl_sequence_setting;
use App\Tbl_pipeline_settings;
use App\Tbl_slot;
use App\Classes\Compute;
use DB;
use App\Tbl_matching_bonus;
use App\Tbl_unilevel_check_match;
use App\Classes\Log;
use App\Tbl_tree_sponsor;
use App\Tbl_travel_reward;
use Validator;
use App\Tbl_travel_qualification;
use App\Classes\Admin;
use App\Tbl_stairstep_settings;
use Session;
use App\Globals\AdminNav;
class AdminComplanController extends AdminController
{
	public function index()
	{
        return view('admin.utilities.complan');
	}
	public function binary()
	{
		$data["_membership"] = Tbl_membership::active()->entry()->get();
		$data["_membership_pairs"] = Tbl_membership::active()->get();

		foreach($data['_membership_pairs'] as $key => $d)
		{
			$data['_membership_pairs'][$key]->count = DB::table('tbl_binary_pairing')->where('membership_id',$d->membership_id)->count();
		}

		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits Binary Computation");
		
		$data["_pairing"] = Tbl_binary_pairing::get();
		$data["_product"] = Tbl_product::active()->get();
		
		$code = "Binary Computation";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {    
			return view('admin.computation.binary', $data);
        }
        else
        {
            return Redirect::back();
        } 
	}
	public function binary_add()
	{
		
		if(Request::isMethod("post"))
		{

			$insert["pairing_point_l"] = Request::input("pairing_points_l");
			$insert["pairing_point_r"] = Request::input("pairing_points_r");
			$insert["pairing_income"] = Request::input("pairing_income");
			$insert["membership_id"] = Request::input("membership");
			$id = Tbl_binary_pairing::insertGetId($insert);

			$new = DB::table('tbl_binary_pairing')->where('pairing_id',$id)->first();

			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." Add a Binary Computation Id #".$id,null,serialize($new));

			return Redirect::to('/admin/utilities/binary/membership/binary/edit?id='.Request::input('membership'));
		}
		else
		{
			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits Add Binary Computation Membership Id #".Request::input('membership'));
			return view('admin.computation.binary_add');	
		}
	}
	public function binary_edit()
	{
		if(Request::isMethod("post"))
		{
			$old = DB::table('tbl_binary_pairing')->where('pairing_id',Request::input("id"))->first();
			$update["pairing_point_l"] = Request::input("pairing_points_l");
			$update["pairing_point_r"] = Request::input("pairing_points_r");
			$update["pairing_income"] = Request::input("pairing_income");
			Tbl_binary_pairing::where("pairing_id", Request::input("id"))->update($update);
			$new = DB::table('tbl_binary_pairing')->where('pairing_id',Request::input("id"))->first();

			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." edit a Binary Computation Id #".Request::input('id'),serialize($old),serialize($new));

			return Redirect::to('/admin/utilities/binary/membership/binary/edit?id='.Request::input('membership'));
		}
		else
		{
			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits Edit Binary Computation Id #".Request::input("id"));
			$data["data"] = Tbl_binary_pairing::where("pairing_id", Request::input("id"))->first();
			return view('admin.computation.binary_edit', $data);	
		}
	}
	public function binary_delete()
	{
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." delete a Binary Computation Id #".Request::input('id'));
		Tbl_binary_pairing::where("pairing_id", Request::input("id"))->delete();
		return Redirect::to('/admin/utilities/binary/membership/binary/edit?id='.Request::input('membership'));
	}
	public function binary_membership_edit()
	{
		if(Request::isMethod("post"))
		{
			$old = DB::table('tbl_membership')->where('membership_id',Request::input('id'))->first();
			$update["membership_binary_points"] = Request::input("membership_binary_points");
			Tbl_membership::where("membership_id", Request::input("id"))->update($update);
			$new = DB::table('tbl_membership')->where('membership_id',Request::input('id'))->first();
			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." edit a Binary Points Membership Id #".Request::input('id'),serialize($old),serialize($new));
			return Redirect::to('/admin/utilities/binary');
		}
		else
		{
			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits Binary Points Membership Id #".Request::input('id'));
			$data["data"] = Tbl_membership::where("membership_id", Request::input("id"))->first();
			return view('admin.computation.binary_membership_edit', $data);	
		}
	}
	public function binary_product_edit()
	{
		if(Request::isMethod("post"))
		{
			$old = DB::table('tbl_product')->where('product_id',Request::input('id'))->first();
			$update["binary_pts"] = Request::input("binary_pts");
			Tbl_product::where("product_id", Request::input("id"))->update($update);
			$new = DB::table('tbl_product')->where('product_id',Request::input('id'))->first();
			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." edit a Product Binary Points Product Id #".Request::input('id'),serialize($old),serialize($new));
			return Redirect::to('/admin/utilities/binary');
		}
		else
		{
			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits Product Binary Points Product Id #".Request::input('id'));
			$data["data"] = Tbl_product::where("product_id", Request::input("id"))->first();
			return view('admin.computation.binary_product_edit', $data);
		}	
	}

	public function pipeline()
	{
		$data["_pipeline"] = Tbl_pipeline_settings::get();
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits PIPELINE BONUS PERCENTAGE PER MEMBERSHIP ");
		
		$code = "Pipeline Computation";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {    
			return view('admin.computation.pipeline', $data);
        }
        else
        {
            return Redirect::back();
        } 
	}
	public function pipeline_edit_get()
	{
		$id = Request::input('id');
		$data['pipeline'] = Tbl_pipeline_settings::where('pipeline_id', $id)->first();
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits Edit Pipeline Computation Id #".Request::input("id"));
		// dd($data);
		return view('admin.computation.pipeline_edit', $data);
	}
	public function pipeline_edit_post()
	{
		$id = Request::input('p_id');
		$old = DB::table('tbl_pipeline_settings')->where('pipeline_id', $id)->get();
		// dd($id);
		$update['pipeline_graduate_amount'] = request::input('pipeline_graduate_amount');
		$update['pipeline_requirement'] = request::input('pipeline_requirement');
		$update['pipeline_binary_points'] = request::input('pipeline_binary_points');
		DB::table('tbl_pipeline_settings')->where('pipeline_id', $id)->update($update);
		$new = DB::table('tbl_pipeline_settings')->where('pipeline_id',Request::input('p_id'))->get();
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." edit PIPELINE BONUS/ UPDATE / MEMBERSHIP ID #".Request::input('p_id'),serialize($old),serialize($new));
		return Redirect::to('/admin/utilities/pipeline');
	}

	public function direct()
	{
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits DIRECT SPONSOR BONUS PER MEMBERSHIP");
		$data["_membership"] = Tbl_membership::active()->get();

		$code = "Direct Sponsorship Bonus";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {    	
			return view('admin.computation.direct', $data);
        }
        else
        {
            return Redirect::back();
        } 
	}
	public function direct_edit()
	{
		if(Request::isMethod("post"))
		{
			$old = DB::table('tbl_membership')->where('membership_id',Request::input('id'))->first();

			$number = preg_replace("/[^A-Za-z0-9 ]/", '', Request::input("membership_direct_sponsorship_bonus"));
			$update["membership_direct_sponsorship_bonus"] = $number;
			$string = preg_replace('/\s+/', '', Request::input("membership_direct_sponsorship_bonus"));
			$percentage = substr($string, -1);
			if($percentage == "%")
			{
				$update["if_matching_percentage"] = 1;
			}
			else
			{
				$update["if_matching_percentage"] = 0;
			}
			Tbl_membership::where("membership_id", Request::input("id"))->update($update);

			$new = DB::table('tbl_membership')->where('membership_id',Request::input('id'))->first();

			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." edit a DIRECT SPONSORSHIP BONUS / UPDATE MEMBERSHIP ID #".Request::input('id'),serialize($old),serialize($new));
			
			return Redirect::to('/admin/utilities/direct');
		}
		else
		{
			$data["data"] = Tbl_membership::where("membership_id", Request::input("id"))->first();
			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits EDIT DIRECT SPONSOR BONUS PER MEMBERSHIP ID #".Request::input('id'));
			return view('admin.computation.direct_edit', $data);	
		}
	}
	public function indirect()
	{
		$data["_membership"] = Tbl_membership::active()->get();
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits INDIRECT LEVEL BONUS PER MEMBERSHIP");
	
		$code = "Indirect Sponsorship Bonus";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {    	
		return view('admin.computation.indirect', $data);
        }
        else
        {
            return Redirect::back();
        } 
	}
	public function indirect_edit()
	{
		if(Request::isMethod("post"))
		{
			$update["membership_indirect_level"] = Request::input("membership_indirect_level");
			Tbl_membership::where("membership_id", Request::input("id"))->update($update);
			$old = DB::table('tbl_indirect_setting')->where('membership_id',Request::input('id'))->get();
			$ctr = 0;
			Tbl_indirect_setting::where("membership_id", Request::input("id"))->delete();
			foreach(Request::input("level") as $level => $value)
			{
				$insert[$ctr]["level"] = $level;
				$insert[$ctr]["value"] = $value;
				$insert[$ctr]["membership_id"] = Request::input("id");
				$ctr++;
			}
			Tbl_indirect_setting::insert($insert);

			$new = DB::table('tbl_indirect_setting')->where('membership_id',Request::input('id'))->get();

			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." edit INDIRECT LEVEL BONUS / UPDATE / MEMBERSHIP ID #".Request::input('id'),serialize($old),serialize($new));
			return Redirect::to('/admin/utilities/indirect');
		}
		else
		{

			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits EDIT INDIRECT LEVEL BONUS / UPDATE / MEMBERSHIP ID #".Request::input('id'));
			$data["data"] = Tbl_membership::where("membership_id", Request::input("id"))->first();
			$data["_level"] = Tbl_indirect_setting::where("membership_id", Request::input("id"))->get();
			return view('admin.computation.indirect_edit', $data);	
		}
	}

	public function travel_reward()
	{
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits TRAVEL REWARD");
		if(Request::input('status') == "archived")
		{
			$data["_reward"] = Tbl_travel_reward::where('archived',1)->get();
		}
		else
		{
			$data["_reward"] = Tbl_travel_reward::where('archived',0)->get();
		}
		$code = "Travel And Car Bonus Reward";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {    	
		return view('admin.computation.travel_reward', $data);
        }
        else
        {
            return Redirect::back();
        } 
	}

	public function travel_reward_edit()
	{
		$data['_error'] = null;
		$data['data'] = Tbl_travel_reward::where('travel_reward_id',Request::input('id'))->first();
		if(Request::isMethod("post"))
		{
				$rules['travel_reward_name'] = 'required|unique:tbl_membership,membership_name|regex:/^[A-Za-z0-9\s-_]+$/';
				$rules['required_points'] = 'numeric|min:1';
				$validator = Validator::make(Request::input(),$rules);
				if (!$validator->fails())
				{
					$old = DB::table('tbl_travel_reward')->where('travel_reward_id',Request::input('id'))->first();

					$update['travel_reward_name'] = Request::input('travel_reward_name');
					$update['required_points'] = Request::input('required_points');
					Tbl_travel_reward::where('travel_reward_id',Request::input('id'))->update($update);	

					$new = DB::table('tbl_travel_reward')->where('travel_reward_id',Request::input('id'))->first();

					Log::Admin(Admin::info()->account_id,Admin::info()->account_username." edit EDIT REWARD ID #".Request::input('id'),serialize($old),serialize($new));	
					return Redirect::to('/admin/utilities/travel_reward');
				}
				else
				{	
					$errors =  $validator->errors();
					$data['_error']['travel_reward_name'] = $errors->get('travel_reward_name');
					$data['_error']['required_points'] = $errors->get('required_points');
				}			
		}
		else
		{
			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits EDIT REWARD ID #".Request::input('id'));
		}

		return view('admin.computation.travel_reward_edit',$data);	
	}

	public function travel_reward_add()
	{	
		$data['_error'] = null;
		if(Request::isMethod("post"))
		{
				$rules['travel_reward_name'] = 'required|unique:tbl_membership,membership_name|regex:/^[A-Za-z0-9\s-_]+$/';
				$rules['required_points'] = 'numeric|min:1';
				$validator = Validator::make(Request::input(),$rules);
				if (!$validator->fails())
				{

					$insert['travel_reward_name'] = Request::input('travel_reward_name');
					$insert['required_points'] = Request::input('required_points');
					$id = Tbl_travel_reward::insertGetId($insert);

					$new = DB::table('tbl_travel_reward')->where('travel_reward_id',$id)->first();

					Log::Admin(Admin::info()->account_id,Admin::info()->account_username." ADD REWARD ID #".$id,null,serialize($new));	
					return Redirect::to('/admin/utilities/travel_reward');
				}
				else
				{	
					$errors =  $validator->errors();
					$data['_error']['travel_reward_name'] = $errors->get('travel_reward_name');
					$data['_error']['required_points'] = $errors->get('required_points');
				}			
		}
		else
		{
			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits ADD REWARD");
		}

		return view('admin.computation.travel_reward_add',$data);	
	}

	public function travel_reward_delete()
	{
		Tbl_travel_reward::where('travel_reward_id',Request::input('id'))->update(['archived'=>1]);	
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." archive REWARD ID #".Request::input('id'));
		return Redirect::to('/admin/utilities/travel_reward');
	}

	public function travel_reward_restore()
	{
		Tbl_travel_reward::where('travel_reward_id',Request::input('id'))->update(['archived'=>0]);	
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." restore REWARD ID #".Request::input('id'));
		return Redirect::to('/admin/utilities/travel_reward?status=archived');
	}

	public function travel_qualification()
	{
		if(Request::input('status') == "archived")
		{
			$data["_qualification"] = Tbl_travel_qualification::where('archived',1)->get();
		}
		else
		{
			$data["_qualification"] = Tbl_travel_qualification::where('archived',0)->get();
		}
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits TRAVEL QUALIFICATION");

		$code = "Travel And Car Bonus Qualification";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {    	
		return view('admin.computation.travel_qualification', $data);
        }
        else
        {
            return Redirect::back();
        } 
	}

	public function travel_qualification_edit()
	{
		$data['_error'] = null;
		$data['data'] = Tbl_travel_qualification::where('travel_qualification_id',Request::input('id'))->first();
		if(Request::isMethod("post"))
		{
				$rules['travel_qualification_name'] = 'required|unique:tbl_membership,membership_name|regex:/^[A-Za-z0-9\s-_]+$/';
				$rules['item'] = 'numeric|min:1';
				$rules['points'] = 'numeric|min:1';
				$validator = Validator::make(Request::input(),$rules);
				if (!$validator->fails())
				{
					$old = DB::table('tbl_travel_qualification')->where('travel_qualification_id',Request::input('id'))->first();

					$update['travel_qualification_name'] = Request::input('travel_qualification_name');
					$update['item'] = Request::input('item');
					$update['points'] = Request::input('points');
					Tbl_travel_qualification::where('travel_qualification_id',Request::input('id'))->update($update);

					$new = DB::table('tbl_travel_qualification')->where('travel_qualification_id',Request::input('id'))->first();

					Log::Admin(Admin::info()->account_id,Admin::info()->account_username." edit EDIT QUALIFICATION ID #".Request::input('id'),serialize($old),serialize($new));	
				
					return Redirect::to('/admin/utilities/travel_qualification');
				}
				else
				{	
					$errors =  $validator->errors();
					$data['_error']['travel_qualification_name'] = $errors->get('travel_qualification_name');
					$data['_error']['item'] = $errors->get('item');
					$data['_error']['points'] = $errors->get('points');
				}			
		}
		else
		{
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits EDIT QUALIFICATION ID #".Request::input('id'));
		}

		return view('admin.computation.travel_qualification_edit',$data);	
	}

	public function travel_qualification_add()
	{	
		$data['_error'] = null;
		if(Request::isMethod("post"))
		{
				$rules['travel_qualification_name'] = 'required|unique:tbl_membership,membership_name|regex:/^[A-Za-z0-9\s-_]+$/';
				$rules['item'] = 'numeric|min:1';
				$rules['points'] = 'numeric|min:1';
				$validator = Validator::make(Request::input(),$rules);
				if (!$validator->fails())
				{
					$insert['travel_qualification_name'] = Request::input('travel_qualification_name');
					$insert['item'] = Request::input('item');
					$insert['points'] = Request::input('points');
					$id = Tbl_travel_qualification::insertGetId($insert);	
				
					$new = DB::table('tbl_travel_qualification')->where('travel_qualification_id',$id)->first();

					Log::Admin(Admin::info()->account_id,Admin::info()->account_username." ADD QUALIFICATION ID #".$id,null,serialize($new));	

					return Redirect::to('/admin/utilities/travel_qualification');
				}
				else
				{	
					$errors =  $validator->errors();
					$data['_error']['travel_qualification_name'] = $errors->get('travel_qualification_name');
					$data['_error']['item'] = $errors->get('item');
					$data['_error']['points'] = $errors->get('points');
				}			
		}
		else
		{
				Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits ADD QUALIFICATION");
		}

		return view('admin.computation.travel_qualification_add',$data);	
	}

	public function travel_qualification_delete()
	{
		Tbl_travel_qualification::where('travel_qualification_id',Request::input('id'))->update(['archived'=>1]);	
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." archive QUALIFICATION ID #".Request::input('id'));
		return Redirect::to('/admin/utilities/travel_qualification');
	}

	public function travel_qualification_restore()
	{
		Tbl_travel_qualification::where('travel_qualification_id',Request::input('id'))->update(['archived'=>0]);	
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." restore QUALIFICATION ID #".Request::input('id'));
		return Redirect::to('/admin/utilities/travel_qualification?status=archived');
	}

	public function matching()
	{
		$data["_membership"] = Tbl_membership::active()->get();
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits MENTOR BONUS PERCENTAGE PER MEMBERSHIP ");

		$code = "Mentor Bonus Computation";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {    	
		return view('admin.computation.matching', $data);
        }
        else
        {
            return Redirect::back();
        } 
	}
	public function matching_edit()
	{
		if(Request::isMethod("post"))
		{
			$old = DB::table('tbl_matching_bonus')->where('membership_id',Request::input('id'))->get();

			Tbl_matching_bonus::where("membership_id", Request::input("id"))->delete();
			foreach(Request::input("level")["level"] as $level => $value)
			{
				$insert["membership_id"] = Request::input('id');
				$insert["matching_percentage"] = Request::input("level")["level"][$level];
				$insert["matching_requirement_count"] = Request::input("level")["count"][$level];
				// $insert["matching_requirement_membership_id"] = Request::input("level")["member"][$level];
				$insert["level"] = $level;
				Tbl_matching_bonus::where("membership_id", Request::input("id"))->insert($insert);
			}
			Tbl_membership::where("membership_id", Request::input("id"))->update(["membership_mentor_level"=>$level]);


			$new = DB::table('tbl_matching_bonus')->where('membership_id',Request::input('id'))->get();

			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." edit MENTOR BONUS/ UPDATE / MEMBERSHIP ID #".Request::input('id'),serialize($old),serialize($new));

			

			return Redirect::to('/admin/utilities/matching');
		}
		else
		{
			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits MENTOR BONUS/ UPDATE / MEMBERSHIP ID #".Request::input('id'));

			$data["data"] = Tbl_membership::where("membership_id", Request::input("id"))->first();
			$data["member"] = Tbl_membership::where("archived",0)->select('membership_id','membership_name')->get();
			$data["_member"] = Tbl_membership::where("archived",0)->select('membership_id','membership_name')->get();
			$data["member"] = json_encode($data['member']);
			$data["_level"] = Tbl_matching_bonus::where("membership_id", Request::input("id"))->get();
			return view('admin.computation.matching_edit', $data);	
		}
	}

	public function unilevel_check_match()
	{
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits UNILEVEL CHECK MATCH");
		$data["_membership"] = Tbl_membership::active()->get();

		$code = "Unilevel Check Match";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {    	
		return view('admin.computation.unilevel_check_match', $data);
        }
        else
        {
            return Redirect::back();
        } 
	}
	public function unilevel_check_match_edit()
	{
		if(Request::isMethod("post"))
		{
			$old = DB::table('tbl_unilevel_check_match')->where('membership_id',Request::input('id'))->get();

			$update["check_match_level"] = Request::input("check_match_level");
			// $update["membership_required_pv"] = Request::input("membership_required_pv");
			// $update["membership_required_gpv"] = Request::input("membership_required_gpv");
			// $update["multiplier"] = Request::input("multiplier");
			Tbl_membership::where("membership_id", Request::input("id"))->update($update);

			$ctr = 0;
			Tbl_unilevel_check_match::where("membership_id", Request::input("id"))->delete();

			foreach(Request::input("level") as $level => $value)
			{
				$insert[$ctr]["level"] = $level;
				$insert[$ctr]["value"] = $value;
				$insert[$ctr]["membership_id"] = Request::input("id");
				$ctr++;
			}

			Tbl_unilevel_check_match::insert($insert);

			$new = DB::table('tbl_unilevel_check_match')->where('membership_id',Request::input('id'))->get();

			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." edit UNILEVEL CHECK MATCH / UPDATE ID #".Request::input('id'),serialize($old),serialize($new));
			return Redirect::to('/admin/utilities/unilevel_check_match');
		}
		else
		{
			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits UNILEVEL CHECK MATCH / UPDATE ID #".Request::input('id'));
			$data["data"] = Tbl_membership::where("membership_id", Request::input("id"))->first();
			$data["_level"] = Tbl_unilevel_check_match::where("membership_id", Request::input("id"))->get();
			return view('admin.computation.unilevel_check_match_edit', $data);	
		}
	}

	public function unilevel()
	{
		$data["_membership"] = Tbl_membership::active()->get();
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits UNILEVEL BONUS PERCENTAGE PER MEMBERSHIP");
		
		$code = "Unilevel Computation";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {    	
		return view('admin.computation.unilevel', $data);
        }
        else
        {
            return Redirect::back();
        } 
	}
	public function sequence()
	{
		$data["_direct"] = Tbl_direct_unilevel::where("type",1)->get();
		$data["_indirect"] = Tbl_indirect_unilevel::where("type",2)->get();
		$data["_class_sequence"] = Tbl_sequence::where("type",3)->get();
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits ClASS SEQUENCE COMPUTATION (DSB,ISB,GSB)");
		
		$code = "Unilevel Computation";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {    	
		return view('admin.computation.sequence', $data);
        }
        else
        {
            return Redirect::back();
        } 
	}
	public function royalty_bonus()
	{
		$data["_rb"] = Tbl_royalty_setting::join("tbl_royalty_promotion_setting","tbl_royalty_promotion_setting.rb_id","=","tbl_royalty_setting.rb_id")->get();
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits ROYALTY BONUS SETTING");
		
		$data["rb_profit"] = Tbl_royalty_setting::first();
		// dd($data);
		
		$data["rank"] = Session::get("admin_info");
		$code = "Royalty Bonus";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {    	
		return view('admin.computation.royalty_bonus', $data);
        }
        else
        {
            return Redirect::back();
        } 
	}
	public function save_profit()
	{
		$new_value = Request::input("value");
		$old = Request::input("old");

		$update["rb_profitsharing"] = $new_value;
		$data = Tbl_royalty_setting::get();
		// dd($data);
		$dt = null;
		foreach ($data as $key => $value) 
		{
			$dt[$key] = $value; 
			Tbl_royalty_setting::where("rb_id",$dt[$key]->rb_id)->update($update);
		}

		return "success";
	}
	public function rb_edit()
	{
		if(Request::isMethod("post"))
		{
			$old = Tbl_royalty_setting::join("tbl_royalty_promotion_setting","tbl_royalty_promotion_setting.rb_id","=","tbl_royalty_setting.rb_id")
												->where("tbl_royalty_setting.rb_id",Request::input("id"))
												->first();

			$rb_value = Request::input('rb_value');
			$rb_required_pv = Request::input('rb_required_pv');
			$rb_a_assoc_director = Request::input('rb_a_assoc_director');
			$rb_a_director = Request::input('rb_a_director');
			$rb_a_avp = Request::input('rb_a_avp');
			$rb_a_vp = Request::input('rb_a_vp');
			$rb_a_svp = Request::input('rb_a_svp');
			$rb_a_evp = Request::input('rb_a_evp');
			$rb_a_president = Request::input('rb_a_president');


			$rbp_required_pv = Request::input('rbp_required_pv');
			$rbp_required_personal_pv = Request::input('rbp_required_personal_pv');
			$rbp_a_assoc_director = Request::input('rbp_a_assoc_director');
			$rbp_a_director = Request::input('rbp_a_director');
			$rbp_a_avp = Request::input('rbp_a_avp');
			$rbp_a_vp = Request::input('rbp_a_vp');
			$rbp_a_svp = Request::input('rbp_a_svp');
			$rbp_a_evp = Request::input('rbp_a_evp');
			$rbp_a_president = Request::input('rbp_a_president');
			
			$up_rb["rb_value"] = $rb_value;		
			$up_rb["rb_required_spv"] = $rb_required_pv;
			$up_rb["rb_active_assoc_director"] = $rb_a_assoc_director;		
			$up_rb["rb_active_director"] = $rb_a_director;
			$up_rb["rb_active_avp"] = $rb_a_avp;		
			$up_rb["rb_active_vp"] = $rb_a_vp;
			$up_rb["rb_active_svp"] = $rb_a_svp;
			$up_rb["rb_active_evp"] = $rb_a_evp;
			$up_rb["rb_active_president"] = $rb_a_president;

			Tbl_royalty_setting::where("rb_id",Request::input("id"))->update($up_rb);
			
			$up_rbp["rbp_required_spv"] = $rbp_required_pv;
			$up_rbp["rbp_required_personal_spv"] = $rbp_required_personal_pv;
			$up_rbp["rbp_active_assoc_director"] = $rbp_a_assoc_director;		
			$up_rbp["rbp_active_director"] = $rbp_a_director;
			$up_rbp["rbp_active_avp"] = $rbp_a_avp;		
			$up_rbp["rbp_active_vp"] = $rbp_a_vp;
			$up_rbp["rbp_active_svp"] = $rbp_a_svp;
			$up_rbp["rbp_active_evp"] = $rbp_a_evp;
			$up_rbp["rbp_active_president"] = $rbp_a_president;
			$up_rbp["rb_id"] = Request::input("id");
			
			
			Tbl_royalty_promotion_setting::where("rb_id",Request::input("id"))->update($up_rbp);

			$new = Tbl_royalty_setting::join("tbl_royalty_promotion_setting","tbl_royalty_promotion_setting.rb_id","=","tbl_royalty_setting.rb_id")
												->where("tbl_royalty_setting.rb_id",Request::input("id"))
												->first();

			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." edit ROYALTY BONUS / UPDATE ID #".Request::input('id'),serialize($old),serialize($new));
			return Redirect::to('/admin/utilities/royalty_bonus');
		}
		else
		{
			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits ROYALTY BONUS / UPDATE ID #".Request::input('id'));
			$data["data"] = Tbl_royalty_setting::join("tbl_royalty_promotion_setting","tbl_royalty_promotion_setting.rb_id","=","tbl_royalty_setting.rb_id")
												->where("tbl_royalty_setting.rb_id",Request::input("id"))
												->first();
			// dd($data);
			return view('admin.computation.royalty_bonus_edit', $data);	
		}
	}
	public function add_rb()
	{

		$position = Request::input('position_name');
		$rb_value = Request::input('rb_value');
		$rb_required_pv = Request::input('rb_required_pv');
		$rb_a_assoc_director = Request::input('rb_a_assoc_director');
		$rb_a_director = Request::input('rb_a_director');
		$rb_a_avp = Request::input('rb_a_avp');
		$rb_a_vp = Request::input('rb_a_vp');
		$rb_a_svp = Request::input('rb_a_svp');
		$rb_a_evp = Request::input('rb_a_evp');
		$rb_a_president = Request::input('rb_a_president');


		$rbp_required_pv = Request::input('rbp_required_pv');
		$rbp_required_personal_pv = Request::input('rbp_required_personal_pv');
		$rbp_a_assoc_director = Request::input('rbp_a_assoc_director');
		$rbp_a_director = Request::input('rbp_a_director');
		$rbp_a_avp = Request::input('rbp_a_avp');
		$rbp_a_vp = Request::input('rbp_a_vp');
		$rbp_a_svp = Request::input('rbp_a_svp');
		$rbp_a_evp = Request::input('rbp_a_evp');
		$rbp_a_president = Request::input('rbp_a_president');
		
		$insert_rb["rb_position"] = $position;
		$insert_rb["rb_value"] = $rb_value;		
		$insert_rb["rb_required_spv"] = $rb_required_pv;
		$insert_rb["rb_active_assoc_director"] = $rb_a_assoc_director;		
		$insert_rb["rb_active_director"] = $rb_a_director;
		$insert_rb["rb_active_avp"] = $rb_a_avp;		
		$insert_rb["rb_active_vp"] = $rb_a_vp;
		$insert_rb["rb_active_svp"] = $rb_a_svp;
		$insert_rb["rb_active_evp"] = $rb_a_evp;
		$insert_rb["rb_active_president"] = $rb_a_president;

		$rb_id = Tbl_royalty_setting::insertGetId($insert_rb);
		
		$insert_rbp["rbp_required_spv"] = $rbp_required_pv;
		$insert_rbp["rbp_required_personal_spv"] = $rbp_required_personal_pv;
		$insert_rbp["rbp_active_assoc_director"] = $rbp_a_assoc_director;		
		$insert_rbp["rbp_active_director"] = $rbp_a_director;
		$insert_rbp["rbp_active_avp"] = $rbp_a_avp;		
		$insert_rbp["rbp_active_vp"] = $rbp_a_vp;
		$insert_rbp["rbp_active_svp"] = $rbp_a_svp;
		$insert_rbp["rbp_active_evp"] = $rbp_a_evp;
		$insert_rbp["rbp_active_president"] = $rbp_a_president;
		$insert_rbp["rb_id"] = $rb_id;

		Tbl_royalty_promotion_setting::insert($insert_rbp);

		return "1";

	}
	public function unilevel_edit()
	{
		if(Request::isMethod("post"))
		{
			$old = DB::table('tbl_unilevel_setting')->where('membership_id',Request::input('id'))->get();
			$update["membership_repurchase_level"] = Request::input("membership_repurchase_level");
			$update["membership_required_pv"] = Request::input("membership_required_pv");
			// $update["membership_required_gpv"] = Request::input("membership_required_gpv");
			$update["multiplier"] = Request::input("multiplier");
			Tbl_membership::where("membership_id", Request::input("id"))->update($update);

			$ctr = 0;
			Tbl_unilevel_setting::where("membership_id", Request::input("id"))->delete();

			foreach(Request::input("level") as $level => $value)
			{
				$insert[$ctr]["level"] = $level;
				$insert[$ctr]["value"] = $value;
				$insert[$ctr]["membership_id"] = Request::input("id");
				$ctr++;
			}

			Tbl_unilevel_setting::insert($insert);
			$new = DB::table('tbl_unilevel_setting')->where('membership_id',Request::input('id'))->get();

			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." edit UNILEVEL BONUS PERCENTAGE / UPDATE ID #".Request::input('id'),serialize($old),serialize($new));
			return Redirect::to('/admin/utilities/unilevel');
		}
		else
		{
			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits UNILEVEL BONUS PERCENTAGE / UPDATE ID #".Request::input('id'));
			$data["data"] = Tbl_membership::where("membership_id", Request::input("id"))->first();
			$data["_level"] = Tbl_unilevel_setting::where("membership_id", Request::input("id"))->get();
			return view('admin.computation.unilevel_edit', $data);	
		}
	}
	public function sequence_edit()
	{
		if(Request::isMethod("post"))
		{
			if(Request::input("type") == 1 )
			{
				$old = Tbl_direct_unilevel::where('sequence_id',Request::input('id'))->get();
				$update["level_rep"] = Request::input("level_rep");
				$update["required_spv"] = Request::input("required_pv");
				$update["required_personal_spv"] = Request::input("required_personal_pv");
				// $update["membership_required_gpv"] = Request::input("membership_required_gpv");
				// $update["multiplier"] = Request::input("multiplier");
				Tbl_direct_unilevel::where("sequence_id", Request::input("id"))->update($update);
	
				$ctr = 0;
				Tbl_direct_unilevel_setting::where("sequence_id", Request::input("id"))->delete();
				
				if(Request::input("level") != null)
				{
					foreach(Request::input("level") as $level => $value)
					{
						$insert[$ctr]["seq_level"] = $level;
						$insert[$ctr]["seq_value"] = $value;
						$insert[$ctr]["sequence_id"] = Request::input("id");
						$ctr++;
					}
					
				Tbl_direct_unilevel_setting::insert($insert);
				}
	
				$new = Tbl_direct_unilevel_setting::where('sequence_id',Request::input('id'))->get();
				
			}
			else if(Request::input("type") == 2 )
			{
				$old = Tbl_indirect_unilevel::where('sequence_id',Request::input('id'))->get();
				$update["level_rep"] = Request::input("level_rep");
				$update["required_spv"] = Request::input("required_pv");
				$update["required_personal_spv"] = Request::input("required_personal_pv");
				// $update["membership_required_gpv"] = Request::input("membership_required_gpv");
				// $update["multiplier"] = Request::input("multiplier");
				Tbl_indirect_unilevel::where("sequence_id", Request::input("id"))->update($update);
	
				$ctr = 0;
				Tbl_indirect_unilevel_setting::where("sequence_id", Request::input("id"))->delete();
				
				if(Request::input("level") != null)
				{
					foreach(Request::input("level") as $level => $value)
					{
						$insert[$ctr]["seq_level"] = $level;
						$insert[$ctr]["seq_value"] = $value;
						$insert[$ctr]["sequence_id"] = Request::input("id");
						$ctr++;
					}
					
				Tbl_indirect_unilevel_setting::insert($insert);
				}
	
				$new = Tbl_indirect_unilevel_setting::where('sequence_id',Request::input('id'))->get();

			}
			else if(Request::input("type") == 3 )
			{
				$old = Tbl_sequence::where('sequence_id',Request::input('id'))->get();
				$update["level_rep"] = Request::input("level_rep");
				$update["required_spv"] = Request::input("required_pv");
				$update["personal_bonus"] = Request::input("personal_bonus");
				$update["required_personal_spv"] = Request::input("required_personal_pv");
				$update["active_consultant"] = Request::input("a_consultant");
				$update["active_manager"] = Request::input("a_manager");
				$update["active_assoc_director"] = Request::input("a_assoc_director");
				$update["active_director"] = Request::input("a_director");
				// $update["membership_required_gpv"] = Request::input("membership_required_gpv");
				// $update["multiplier"] = Request::input("multiplier");
				Tbl_sequence::where("sequence_id", Request::input("id"))->update($update);
	
				$ctr = 0;
				Tbl_sequence_setting::where("sequence_id", Request::input("id"))->delete();
				
				if(Request::input("level") != null)
				{
					foreach(Request::input("level") as $level => $value)
					{
						$insert[$ctr]["seq_level"] = $level;
						$insert[$ctr]["seq_value"] = $value;
						$insert[$ctr]["sequence_id"] = Request::input("id");
						$ctr++;
					}
					
				Tbl_sequence_setting::insert($insert);
				}
	
				$new = Tbl_sequence_setting::where('sequence_id',Request::input('id'))->get();

			}
			
			
		
			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." edit UNILEVEL BONUS PERCENTAGE / UPDATE ID #".Request::input('id'),serialize($old),serialize($new));
			return Redirect::to('/admin/utilities/class_sequence');
		}
		else
		{
			// dd(Request::input("type"));
			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits UNILEVEL BONUS PERCENTAGE / UPDATE ID #".Request::input('id'));
			if(Request::input("type") == 1 )
			{
				$data["data"] = Tbl_direct_unilevel::where("sequence_id", Request::input("id"))->first();
				$data["_level"] = Tbl_direct_unilevel_setting::where("sequence_id", Request::input("id"))->get();
			}
			else if(Request::input("type") == 2 )
			{
				$data["data"] = Tbl_indirect_unilevel::where("sequence_id", Request::input("id"))->first();
				$data["_level"] = Tbl_indirect_unilevel_setting::where("sequence_id", Request::input("id"))->get();
			}
			else if(Request::input("type") == 3 )
			{
				$data["data"] = Tbl_sequence::where("sequence_id", Request::input("id"))->first();
				$data["_level"] = Tbl_sequence_setting::where("sequence_id", Request::input("id"))->get();
			}
			
			return view('admin.computation.sequence_edit', $data);	
		}
	}
	public function add_position()
	{
		$position = Request::input('sequence_name');
		$required_spv = Request::input('required_spv');
		$required_personal_spv = Request::input('required_personal_spv');
		
		$insert["sequence_name"] = $position;
		$insert["required_spv"] = $required_spv;
		$insert["type"] = 3;
		$insert["required_personal_spv"] = $required_personal_spv;
		
		Tbl_sequence::insert($insert);
		
		return "1";
		
	}
	public function leadership_bonus()
	{
		$data["_membership"] = Tbl_membership::active()->get();
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits LEADERSHIP BONUS PERCENTAGE PER MEMBERSHIP");

		$code = "Leadership Bonus";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {    	
		return view('admin.computation.leadership_bonus', $data);
        }
        else
        {
            return Redirect::back();
        }
	}
	public function leadership_bonus_edit()
	{
		if(Request::isMethod("post"))
		{
			$old = DB::table('tbl_membership')->where('membership_id',Request::input('id'))->first();
			$update["leadership_bonus"] = Request::input("leadership_bonus");
			// $update["membership_required_pv"] = Request::input("membership_required_pv");
			// $update["membership_required_gpv"] = Request::input("membership_required_gpv");
			// $update["multiplier"] = Request::input("multiplier");
			Tbl_membership::where("membership_id", Request::input("id"))->update($update);

			
			$new = DB::table('tbl_membership')->where('membership_id',Request::input('id'))->first();
			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." edit LEADERSHIP BONUS PERCENTAGE / UPDATE ID #".Request::input('id'),serialize($old),serialize($new));

			return Redirect::to('/admin/utilities/leadership_bonus');
		}
		else
		{

			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits LEADERSHIP BONUS PERCENTAGE / UPDATE #".Request::input('id'));

			$data["data"] = Tbl_membership::where("membership_id", Request::input("id"))->first();
			$data["_level"] = Tbl_unilevel_check_match::where("membership_id", Request::input("id"))->get();
			return view('admin.computation.leadership_bonus_edit', $data);	
		}
	}


	/* BREAKAWAY BONUS */
	public function breakaway_bonus()
	{
		$data["_membership"] = Tbl_membership::active()->get();
		Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits BREAKAWAY BONUS PERCENTAGE PER MEMBERSHIP");

		$code = "Breakaway Bonus";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {    	
		return view('admin.utilities.breakaway_bonus', $data);
        }
        else
        {
            return Redirect::back();
        }
	}


	public function breakaway_bonus_edit()
	{
		if(Request::isMethod("post"))
		{
			$update["breakaway_bonus_level"] = Request::input("breakaway_bonus_level");
			// $update["membership_required_pv"] = Request::input("membership_required_pv");
			// $update["membership_required_gpv"] = Request::input("membership_required_gpv");
			// $update["multiplier"] = Request::input("multiplier");
			Tbl_membership::where("membership_id", Request::input("id"))->update($update);

			$update["membership_repurchase_level"] = Request::input("membership_repurchase_level");
			$update["membership_required_pv"] = Request::input("membership_required_pv");
			// $update["membership_required_gpv"] = Request::input("membership_required_gpv");
			$update["multiplier"] = Request::input("multiplier");
			Tbl_membership::where("membership_id", Request::input("id"))->update($update);

			$ctr = 0;
			$old = DB::table('tbl_breakaway_bonus_setting')->where('membership_id',Request::input('id'))->get();
			DB::table('tbl_breakaway_bonus_setting')->where("membership_id", Request::input("id"))->delete();

			foreach(Request::input("level") as $level => $value)
			{
				$insert[$ctr]["level"] = $level;
				$insert[$ctr]["value"] = $value;
				$insert[$ctr]["membership_id"] = Request::input("id");
				$ctr++;
			}

			DB::table('tbl_breakaway_bonus_setting')->insert($insert);

			
			$new = DB::table('tbl_breakaway_bonus_setting')->where('membership_id',Request::input('id'))->get();

			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." edit BREAKAWAY BONUS PERCENTAGE / UPDATE ID #".Request::input('id'),serialize($old),serialize($new));
			return Redirect::to('/admin/utilities/breakaway_bonus');
		}
		else
		{
			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." visits BREAKAWAY BONUS PERCENTAGE / UPDATE ID #".Request::input('id'));
			$data["data"] = Tbl_membership::where("membership_id", Request::input("id"))->first();
			$data["_level"] = DB::table('tbl_breakaway_bonus_setting')->where("membership_id", Request::input("id"))->get();
			return view('admin.utilities.breakaway_bonus_edit', $data);	
		}
	}

	public function rank()
	{
		$data["_membership"] = Tbl_membership::active()->get();
		foreach($data["_membership"] as $key => $d)
		{
			$data["_membership"][$key]->required_leg = Tbl_membership::active()->where('membership_id',$d->membership_unilevel_leg_id)->first();
			if($data["_membership"][$key]->required_leg)
			{
				$data["_membership"][$key]->required_leg = $data["_membership"][$key]->required_leg->membership_name;

			}
		}
		
		$code = "Promotion Setting";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {    	
		return view('admin.computation.rank', $data);
        }
        else
        {
            return Redirect::back();
        } 
	}

	public function rank_edit()
	{
		if(Request::isMethod("post"))
		{
			if(Request::input("unilevel"))
			{
				$update["membership_required_unilevel_leg"] = 1;
				$update["membership_unilevel_leg_id"] = Request::input("member");
			}
			else
			{
				$update["membership_required_unilevel_leg"] = 0;
				$update["membership_unilevel_leg_id"] = null;
			}

			$update["membership_required_direct"] = Request::input("membership_required_direct");
			$update["membership_required_pv_sales"] = Request::input("membership_required_pv_sales");
			$update["membership_required_month_count"] = Request::input("membership_required_month_count");
			Tbl_membership::where("membership_id", Request::input("id"))->update($update);
			return Redirect::to('/admin/utilities/rank');
		}
		else
		{
			$data["member"] = Tbl_membership::where("archived",0)->select('membership_id','membership_name')->get();
			$data["member"] = json_encode($data['member']);
			$data["data"] = Tbl_membership::where("membership_id", Request::input("id"))->first();

			return view('admin.computation.rank_edit', $data);	
		}
	}

	public function recompute()
	{

		echo base64_decode("/wEdAAZrrO8aGTCCus/TZdechI3DVas/k2InBMdmWIvDoWXbxc2tOHisUnaEBsHG08XGrcn2ey+ClwVUuCLirNvBH1XQhrLGxMrn4FQpOnM3xdjwVvU8JWd5KFnnFaHYbyhbG4W4+gXph+J+x6Zg4UgKHR3RRuxouoDxWuvWuu+k84Jgxg==");


		if(Request::input("action") == "")
		{
			$data["_account"] = Tbl_slot::orderBy("slot_id", "asc")	->rank()
																	->membership()
																	->account()
																	->where("membership_id", 5)
																	->get();
			return view("admin.computation.recomputation", $data);
		}
		elseif(Request::input("action") == "initialize")
		{
			DB::table('tbl_tree_placement')->delete();
			DB::table('tbl_tree_sponsor')->delete();
			echo json_encode("success");
		}
		elseif(Request::input("action") == "compute")
		{
			Compute::tree(Request::input("slot_id"));
			echo json_encode("success");
		}
	}

	public function binary_entry()
	{
		$data["_pairing"] = Tbl_binary_pairing::where('membership_id',Request::input("id"))->get();
		$data["data"] = Tbl_membership::where("membership_id", Request::input("id"))->first();

		if(Request::isMethod("post"))
		{
			$old = DB::table('tbl_membership')->where('membership_id',Request::input('id'))->first();
			$update["max_pairs_per_day"] = Request::input("max");
			$update["every_gc_pair"] = Request::input("every_gc_pair");
			Tbl_membership::where("membership_id", Request::input("id"))->update($update);
			$new = DB::table('tbl_membership')->where('membership_id',Request::input('id'))->first();
			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." Edit Pairing Combination Membership Id #".Request::input('id'),serialize($old),serialize($new));


			return Redirect::to('/admin/utilities/binary');
		}
		else
		{
			Log::Admin(Admin::info()->account_id,Admin::info()->account_username." View Pairing Combination Membership Id #".Request::input('id'));
		}
		
		return view('admin.computation.binary_entry', $data);	
	}
		public function stairstep(){
		$data = [];
		$count = Tbl_stairstep_settings::count();
		if($count == 0)
		{
			//stairstep_bonus
			$insert1['stairstep_level'] = 1;
			$insert1['stairstep_name'] = 'Distributor';
			$insert1['stairstep_required_gv'] = 0;
			$insert1['stairstep_required_pv'] = 0;
			$insert1['stairstep_bonus'] = 0;
			
			$insert2['stairstep_level'] = 2;
			$insert2['stairstep_name'] = 'Manager';
			$insert2['stairstep_required_gv'] = 2000;
			$insert2['stairstep_required_pv'] = 200;
			$insert2['stairstep_bonus'] = 5000;
			
			$insert3['stairstep_level'] = 3;
			$insert3['stairstep_name'] =  'Sapphire';
			$insert3['stairstep_required_gv'] = 4000;
			$insert3['stairstep_required_pv'] = 400;
			$insert3['stairstep_bonus'] = 10000;
			
			$insert4['stairstep_level'] = 4;
			$insert4['stairstep_name'] = 'Diamond';
			$insert4['stairstep_required_gv'] = 6000;
			$insert4['stairstep_required_pv'] = 600 ;
			$insert4['stairstep_bonus'] = 20000;
			
			$insert5['stairstep_level'] = 5;
			$insert5['stairstep_name'] = 'Crown';
			$insert5['stairstep_required_gv'] = 8000;
			$insert5['stairstep_required_pv'] = 800;
			$insert5['stairstep_bonus'] = 40000;
			
			$insert6['stairstep_level'] = 6;
			$insert6['stairstep_name'] = 'Royal Crown';
			$insert6['stairstep_required_gv'] = 40000;
			$insert6['stairstep_required_pv'] = 1200;
			$insert6['stairstep_bonus'] = 80000;
			
			Tbl_stairstep_settings::insert($insert1);
			Tbl_stairstep_settings::insert($insert2);
			Tbl_stairstep_settings::insert($insert3);
			Tbl_stairstep_settings::insert($insert4);
			Tbl_stairstep_settings::insert($insert5);
			Tbl_stairstep_settings::insert($insert6);
			
		}
		else {
		
		}
		$data['stairstep'] = Tbl_stairstep_settings::get()->toarray();
		return view('admin.stairway.stairstep', $data);
		
	}
	public function ladder_rebates(){
		$data = [];
		return view('admin.rebates.rebates', $data);
	}
}
<?php namespace App\Http\Controllers;
use App\Classes\Customer;
use DB;
use Request;
use Session;
use Redirect;
use Crypt;
use Carbon\Carbon;
use App\Tbl_account;
use App\Tbl_membership_code;
use App\Tbl_slot;
use App\Tbl_product_package_has;
use App\Tbl_product_package;
use App\Tbl_membership;
use App\Tbl_code_type;
use App\Tbl_inventory_update_type;
use Datatables;
use Validator;
use App\Tbl_tree_placement;
use App\Classes\Compute;
use App\Classes\Log;
use App\Tbl_voucher_has_product;
use App\Tbl_product_code;
use App\Rel_membership_product;
use App\Rel_membership_code;
use App\Tbl_voucher;
use App\Classes\Globals;
use App\Tbl_product;
use App\Tbl_membership_code_sale;
use App\Tbl_lead;
use App\Tbl_membership_code_sale_has_code;
use App\Tbl_wallet_logs;


class DistributorCodevaultController extends DistributorController
{
	public function index()
	{
		$id = Customer::id();
		$data = $this->getslotbyid($id);
		$data['_error'] = Session::get('message');
		$data['success']  = Session::get('success');
		$data['availprod'] = Tbl_product_package::where('tbl_product_package.archived',0)->join('tbl_membership','tbl_membership.membership_id','=','tbl_product_package.membership_id')->where('tbl_membership.if_product_include',1)->get();
		$data['membership2'] = 	    					 Tbl_membership::where('archived',0)
	    												 ->orderBy('membership_price','ASC')
	    												 ->where('membership_entry',1)
	    												 ->get();
					
  		$data['exist_lead'] = null;
	 	$check_date = Tbl_lead::where('account_id',$id)->where('tbl_lead.used',0)->first();

	 	if(isset($check_date))
	 	{	
		    	 	if(strtotime($check_date->join_date)  > strtotime(Carbon::now()->subDays(30)) && $check_date->used == 0)
				 	{
		    			$data['exist_lead'] = Tbl_lead::where('tbl_lead.account_id',$id)->getslot()->getaccount()->get();
				 	}
	 	}
											 
	    $data['getlead'] = Tbl_lead::where('lead_account_id',Customer::id())->getaccount()->get();
		if($data['availprod'])
		{
			foreach($data['availprod'] as $key => $d)
			{
				$s = Tbl_product_package_has::where('product_package_id',$d->product_package_id)->select('tbl_product.product_id','tbl_product.product_name','tbl_product.price','tbl_product_package_has.quantity')->product()->get();
				$data['availprod'][$key]->productlist  = json_encode($s);
			}	
		}

		$s = Tbl_account::where('tbl_account.account_id',$id)->belongstothis()->get();

		if(isset($_POST['sbmtclaim']))
		{
			$data['error'] = $this->claim_code(Request::input(),$id);
			return Redirect::to('member/code_vault')->with('message',$data['error']);
		}
		if(isset($_POST['unlockpass']))
		{
			$data['error'] = $this->unlock(Request::input('yuan'),Request::input('pass'),$id);
			return Redirect::to('member/code_vault')->with('message',$data['error']);
		}
		if(isset($_POST['unlockpass2']))
		{	
			$data['error'] = $this->unlock2(Request::input('yuan2'),Request::input('pass'),$id);
			return Redirect::to('member/code_vault')->with('message',$data['error']);
		}
		if(isset($_POST['codesbmt']))
		{
			$data['error'] = $this->transfer(Request::input('code'),Request::input('account'),Request::input('pass'),$id,$s);
			return Redirect::to('member/code_vault')->with('message',$data['error']);
		}
		if(isset($_POST['prodsbmt']))
		{
			$data['error'] = $this->transfer_code(Request::input('code'),Request::input('account'),Request::input('pass'),$id,$j,Request::input('voucher'));
			return Redirect::to('member/code_vault')->with('message',$data['error']);
		}

		if(Request::input('message'))
		{
			sleep(3);
			return Redirect::to('member/code_vault')->with('success',Request::input('message'));	
		}

		if(Request::input('wait'))
		{
			sleep(3);
			return Redirect::to('member/code_vault');
		}
		if(isset($_POST['slot_position']))
		{	
				$info = $this->addslot(Request::input());
				if(isset($info['success']))
				{
					$message = $info['success'];
					return Redirect::to('member/code_vault')->with('success',$message);	
				}
		}


		if(isset($_POST['sbmitbuy']))
		{
			$check = $this->check_additional_package(Request::input('package'));
			if($check == "Yes")
			{

			}
			else
			{
				return Redirect::to('member/code_vault')->with('message',"Included package doesn't have enough stocks.");
			}
			
			if( Request::input('memid'))
			{
				$info = $this->add_code(Request::input());	

				if(isset($info['success']))
				{
					$message = $info['success'];
					return Redirect::to('member/code_vault')->with('success',$message);	
				}
				else
				{
					$message = $info['_error'];
					return Redirect::to('member/code_vault')->with('message',$message);
				}
		
			}
		}
		// dd($data);
        return view('distributor.codevault.codevault',$data);
	}
	public function used()
	{
		$id = Customer::id();
		$data = $this->getslotbyid($id);
		$data['_error'] = Session::get('message');
		$data['success']  = Session::get('success');
		$data['availprod'] = Tbl_product_package::where('tbl_product_package.archived',0)->join('tbl_membership','tbl_membership.membership_id','=','tbl_product_package.membership_id')->where('tbl_membership.if_product_include',1)->get();
		$data['membership2'] = 	    					 Tbl_membership::where('archived',0)
	    												 ->orderBy('membership_price','ASC')
	    												 ->where('membership_entry',1)
	    												 ->get();
					
  		$data['exist_lead'] = null;
	 	$check_date = Tbl_lead::where('account_id',$id)->where('tbl_lead.used',0)->first();

	 	if(isset($check_date))
	 	{	
		    	 	if(strtotime($check_date->join_date)  > strtotime(Carbon::now()->subDays(30)) && $check_date->used == 0)
				 	{
		    			$data['exist_lead'] = Tbl_lead::where('tbl_lead.account_id',$id)->getslot()->getaccount()->get();
				 	}
	 	}
											 
	    $data['getlead'] = Tbl_lead::where('lead_account_id',Customer::id())->getaccount()->get();
		if($data['availprod'])
		{
			foreach($data['availprod'] as $key => $d)
			{
				$s = Tbl_product_package_has::where('product_package_id',$d->product_package_id)->select('tbl_product.product_id','tbl_product.product_name','tbl_product.price','tbl_product_package_has.quantity')->product()->get();
				$data['availprod'][$key]->productlist  = json_encode($s);
			}	
		}

		$s = Tbl_account::where('tbl_account.account_id',$id)->belongstothis()->get();

		if(isset($_POST['sbmtclaim']))
		{
			$data['error'] = $this->claim_code(Request::input(),$id);
			return Redirect::to('member/code_vault')->with('message',$data['error']);
		}
		if(isset($_POST['unlockpass']))
		{
			$data['error'] = $this->unlock(Request::input('yuan'),Request::input('pass'),$id);
			return Redirect::to('member/code_vault')->with('message',$data['error']);
		}
		if(isset($_POST['unlockpass2']))
		{	
			$data['error'] = $this->unlock2(Request::input('yuan2'),Request::input('pass'),$id);
			return Redirect::to('member/code_vault')->with('message',$data['error']);
		}
		if(isset($_POST['codesbmt']))
		{
			$data['error'] = $this->transfer(Request::input('code'),Request::input('account'),Request::input('pass'),$id,$s);
			return Redirect::to('member/code_vault')->with('message',$data['error']);
		}
		if(isset($_POST['prodsbmt']))
		{
			$data['error'] = $this->transfer_code(Request::input('code'),Request::input('account'),Request::input('pass'),$id,$j,Request::input('voucher'));
			return Redirect::to('member/code_vault')->with('message',$data['error']);
		}

		if(Request::input('message'))
		{
			sleep(3);
			return Redirect::to('member/code_vault')->with('success',Request::input('message'));	
		}

		if(Request::input('wait'))
		{
			sleep(3);
			return Redirect::to('member/code_vault');
		}
		if(isset($_POST['slot_position']))
		{	
				$info = $this->addslot(Request::input());
				if(isset($info['success']))
				{
					$message = $info['success'];
					return Redirect::to('member/code_vault')->with('success',$message);	
				}
		}


		if(isset($_POST['sbmitbuy']))
		{
			$check = $this->check_additional_package(Request::input('package'));
			if($check == "Yes")
			{

			}
			else
			{
				return Redirect::to('member/code_vault')->with('message',"Included package doesn't have enough stocks.");
			}
			
			if( Request::input('memid'))
			{
				$info = $this->add_code(Request::input());	

				if(isset($info['success']))
				{
					$message = $info['success'];
					return Redirect::to('member/code_vault')->with('success',$message);	
				}
				else
				{
					$message = $info['_error'];
					return Redirect::to('member/code_vault')->with('message',$message);
				}
		
			}
		}
		// dd($data);
        return view('distributor.codevault.codevault_used',$data);
	}
	public function product_available()
	{
		$data['getallslot'] = Tbl_slot::where('slot_owner',Customer::id())->get();
		$data['prodcode'] = Tbl_product_code::where("account_id", Customer::id())->where('tbl_product_code.used',0)->where('tbl_product_code.archived',0)->voucher()->product()->orderBy("product_pin", "desc")->unused()->get();
		$data['prodcount'] = Tbl_product_code::where("account_id", Customer::id())->where('tbl_product_code.used',0)->where('tbl_product_code.archived',0)->voucher()->product()->orderBy("product_pin", "desc")->unused()->count();
		// dd($data);
		return view('distributor.codevault.codevault_product',$data);
	}
	public function product_used()
	{
		
		$data['prodcode'] = Tbl_product_code::where("account_id", Customer::id())->where('tbl_product_code.used',1)->where('tbl_product_code.archived',0)->voucher()->product()->orderBy("product_pin", "desc")->get();
		$data['prodcount'] = Tbl_product_code::where("account_id", Customer::id())->where('tbl_product_code.used',1)->where('tbl_product_code.archived',0)->voucher()->product()->orderBy("product_pin", "desc")->count();
		// dd($data);
		return view('distributor.codevault.codevault_product_used',$data);
	}
	public function getslotbyid($id)
	{
		// dd($id);
		$data['code'] = 					Tbl_membership_code::where('tbl_membership_code.archived',0)
														  ->where('tbl_membership_code.blocked',0)
														  ->where('tbl_membership_code.used',0)
														  ->join('tbl_account','tbl_account.account_id','=','tbl_membership_code.account_id')
														  ->join('tbl_code_type','tbl_code_type.code_type_id','=','tbl_membership_code.code_type_id')
														  ->join('tbl_membership','tbl_membership.membership_id','=','tbl_membership_code.membership_id')
														  ->leftjoin('tbl_product_package','tbl_product_package.product_package_id','=','tbl_membership_code.product_package_id')
														  ->where('tbl_membership_code.account_id','=',$id)
														  //luke
														  ->groupby('tbl_membership_code.code_pin')
														  //endluke
														  ->orderBy('tbl_membership_code.code_pin','ASC')
														  ->get();

		$data['used_code'] = 				Tbl_membership_code::where('tbl_membership_code.archived',0)
															  ->where('tbl_membership_code.blocked',0)
															  ->where('tbl_membership_code.used',1)
															  ->join('tbl_account','tbl_account.account_id','=','tbl_membership_code.account_id')
															  ->join('tbl_code_type','tbl_code_type.code_type_id','=','tbl_membership_code.code_type_id')
															  ->join('tbl_membership','tbl_membership.membership_id','=','tbl_membership_code.membership_id')
															  //->join('tbl_slot','tbl_slot.slot_code_pin','=','tbl_membership_code.code_pin')
															  ->leftjoin('tbl_product_package','tbl_product_package.product_package_id','=','tbl_membership_code.product_package_id')
															  ->where('tbl_membership_code.account_id', $id)
															  ////luke
															  ->groupby('tbl_membership_code.code_pin')
															  ////endluke
															  ->orderBy('tbl_membership_code.code_pin','ASC')
															  ->get();
										// dd($data);					  
		$data['getallslot'] = Tbl_slot::where('slot_owner',Customer::id())->get();
		
        foreach($data['code'] as $key => $d)
        {
    	$get =	 DB::table('tbl_member_code_history')->where('code_pin',$d->code_pin)
    												 ->join('tbl_account','tbl_account.account_id','=','tbl_member_code_history.by_account_id')
    											     ->orderBy('updated_at','DESC')
    											     ->first();
   											     
	    $data['code'][$key]->encrypt    = Crypt::encrypt($d->code_pin);	
          if($get)
          {
         	$data['code'][$key]->transferer = $get->account_name;	  			         	
          }
          else
          {
				$get = DB::table('tbl_membership_code')->where('code_pin',$d->code_pin)->join('tbl_stockist','tbl_stockist.stockist_id','=','tbl_membership_code.origin')->first();          	
          		if($get)
          		{
          			$data['code'][$key]->transferer = $get->stockist_full_name;
          		}	
          		else
          		{
          			$data['code'][$key]->transferer = "Admin";
          		}
          }						     
        }

        foreach($data['used_code'] as $key => $d)
        {
        	$get =	 DB::table('tbl_member_code_history')->where('code_pin',$d->code_pin)
        												 ->join('tbl_account','tbl_account.account_id','=','tbl_member_code_history.by_account_id')
        											     ->orderBy('updated_at','DESC')
        											     ->first();
       											     
		    $data['used_code'][$key]->encrypt    = Crypt::encrypt($d->code_pin);	
          if($get)
          {
         	$data['used_code'][$key]->transferer = $get->account_name;	  			         	
          }
          else
          {
				$get = DB::table('tbl_membership_code')->where('code_pin',$d->code_pin)->join('tbl_stockist','tbl_stockist.stockist_id','=','tbl_membership_code.origin')->first();          	
          		if($get)
          		{
          			$data['used_code'][$key]->transferer = $get->stockist_full_name;
          		}	
          		else
          		{
          			$data['used_code'][$key]->transferer = "Admin";
          		}
          }				     
        }

		$data['prodcode'] = Tbl_product_code::where("account_id", Customer::id())->where('tbl_product_code.used',0)->where('tbl_product_code.archived',0)->voucher()->product()->orderBy("product_pin", "desc")->unused()->get();										 
		$data['count']= DB::table('tbl_membership_code')->where('archived',0)->where('tbl_membership_code.used',0)->where('account_id','=',$id)->where('tbl_membership_code.blocked',0)->count();		
		$data['used_count']= DB::table('tbl_membership_code')->where('archived',0)->where('tbl_membership_code.used',1)->where('account_id','=',$id)->where('tbl_membership_code.blocked',0)->count();	
		$data['count2'] = Tbl_product_code::where("account_id", Customer::id())->where('tbl_product_code.used',0)->voucher()->product()->orderBy("product_pin", "desc")->unused()->count();										 
		
		if($data['count2'] == 0)
		{
			$data['prodcode'] = null;
		}

		return $data;
	}
	public function use_product_code()
	{
		// return "asd";


		// ignore_user_abort(true);
		// set_time_limit(0);
		// $strURL = "/distributor/codevault/product?wait=5";
		// header("Location: $strURL", true);
		// header("Connection: close", true);
		// header("Content-Encoding: none\r\n");
		// header("Content-Length: 0", true);
		// flush();
		// ob_flush();
		// session_write_close();


		$customer_id = Customer::id();

		$product_pin = Request::input("product_pin");
		$slot_id = Request::input("slot");

		/* CHECK IF SLOT AND CODE PIN BELONGS TO THE ACCOUNT */
		$code_info = Tbl_product_code::where("product_pin", $product_pin)->voucher()->product()->first();
		$check_if_archived = Tbl_product_code::where("product_pin", $product_pin)->first();
		$slot_info = Tbl_slot::id($slot_id)->first();

		if($customer_id != $code_info->account_id)
		{
			die("This code doesn't belong to this account.");
		}

		if($customer_id != $slot_info->slot_owner)
		{
			die("The slot you're trying to use doesn't belong to this account.");
		}

		if($code_info)
		{
			if($code_info->used == 1)
			{
				die("You're trying to code that was already used.");
			}
			elseif($check_if_archived->archived == 1)
			{
				die("You're trying to code that was already blocked by admin.");
			}
			else
			{
				$code_info = Tbl_product_code::where("product_pin", $product_pin)->voucher()->product()->first();
				if($code_info->used == 1)
				{
					die("You're trying to code that was already used.");
				}
				else
				{
					$update["used"] = 1;
					Tbl_product_code::where("product_pin", $product_pin)->update($update);
					$unilevel_pts = $code_info->unilevel_pts; 
					$upgrade_pts = $code_info->upgrade_pts; 
					$binary_pts = $code_info->binary_pts;
					Compute::repurchase($slot_id, $binary_pts, $unilevel_pts, $upgrade_pts, $product_pin);
					return redirect()->back();
				}
			}

		}
		return redirect()->back();
		// sleep(5);
		// exit;
	}
}
<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\AdminController;

use App\Download;
use Illuminate\Http\Request;
use Session;

class DownloadsController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $downloads = Download::paginate(25);

        return view('crud.downloads.index', compact('downloads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('crud.downloads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        

if ($request->hasFile('file')) {
    $uploadPath = public_path('/uploads/');

    $extension = $request->file('file')->getClientOriginalExtension();
    $fileName = rand(11111, 99999) . '.' . $extension;

    $request->file('file')->move($uploadPath, $fileName);
    $requestData['file'] = $fileName;
}

        Download::create($requestData);

        Session::flash('flash_message', 'Download added!');

        return redirect('admin/content/downloads');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $download = Download::findOrFail($id);

        return view('crud.downloads.show', compact('download'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $download = Download::findOrFail($id);

        return view('crud.downloads.edit', compact('download'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        

if ($request->hasFile('file')) {
    $uploadPath = public_path('/uploads/');

    $extension = $request->file('file')->getClientOriginalExtension();
    $fileName = rand(11111, 99999) . '.' . $extension;

    $request->file('file')->move($uploadPath, $fileName);
    $requestData['file'] = $fileName;
}

        $download = Download::findOrFail($id);
        $download->update($requestData);

        Session::flash('flash_message', 'Download updated!');

        return redirect('admin/content/downloads');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Download::destroy($id);

        Session::flash('flash_message', 'Download deleted!');

        return redirect('admin/content/downloads');
    }
}

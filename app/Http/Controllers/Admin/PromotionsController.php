<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\AdminController;

use App\Promotion;
use Illuminate\Http\Request;
use Session;

class PromotionsController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $promotions = Promotion::paginate(25);

        return view('crud.promotions.index', compact('promotions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('crud.promotions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        

if ($request->hasFile('image')) {
    $uploadPath = public_path('/resources/assets/img/promotions');

    $extension = $request->file('image')->getClientOriginalExtension();
    $fileName = rand(11111, 99999) . '.' . $extension;

    $request->file('image')->move($uploadPath, $fileName);
    $requestData['image'] = "/resources/assets/img/promotions/".$fileName;
}

        Promotion::create($requestData);

        Session::flash('flash_message', 'Promotion added!');

        return redirect('admin/content/promotions');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $promotion = Promotion::findOrFail($id);

        return view('crud.promotions.show', compact('promotion'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $promotion = Promotion::findOrFail($id);

        return view('crud.promotions.edit', compact('promotion'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        

if ($request->hasFile('image')) {
    $uploadPath = public_path('/resources/assets/img/promotions');

    $extension = $request->file('image')->getClientOriginalExtension();
    $fileName = rand(11111, 99999) . '.' . $extension;

    $request->file('image')->move($uploadPath, $fileName);
    $requestData['image'] = "/resources/assets/img/promotions/".$fileName;
}

        $promotion = Promotion::findOrFail($id);
        $promotion->update($requestData);

        Session::flash('flash_message', 'Promotion updated!');

        return redirect('admin/content/promotions');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Promotion::destroy($id);

        Session::flash('flash_message', 'Promotion deleted!');

        return redirect('admin/content/promotions');
    }
}

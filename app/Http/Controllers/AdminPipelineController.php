<?php namespace App\Http\Controllers;
use App\Tbl_pipeline_admin_report;
use App\Tbl_pipeline_report;
use Redirect;
use App\Classes\Compute;
use Request;
use DB;
use App\Globals\AdminNav;

class AdminPipelineController extends AdminController
{
	public function index()
	{
		$data["page"] = "Pipeline Processing";
		$data["_pipeline_admin_report"] = Tbl_pipeline_admin_report::orderBy("pipeline_admin_report_id", "desc")->get();
		
		$code = "Pipeline Breakdown";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {
			return view("admin.transaction.pipeline", $data);
		}
        else
        {
            return Redirect::back();
        }
	}
	public function pipelinereport()
	{
		$id = Request::input('id');
		$data['_detail'] = Tbl_pipeline_report::where('pipeline_admin_report', $id)
														   ->join('tbl_slot', 'tbl_slot.slot_id', '=', 'tbl_pipeline_report.pipeline_slot_id')
														   ->join('tbl_account', 'tbl_account.account_id', '=', 'tbl_slot.slot_owner')
														   ->get();
		// dd($data);
		return view('admin.transaction.pipelinebreakdown', $data);
	}
}
<?php namespace App\Http\Controllers;
use App\Tbl_indirect_admin_report;
use App\Tbl_indirect_report;
use Redirect;
use App\Classes\Compute;
use Request;
use DB;
use App\Globals\AdminNav;
class AdminIndirectController extends AdminController
{
	public function index()
	{
		$data["page"] = "Direct Processing";
		$data["_indirect_report"] = Tbl_indirect_admin_report::orderBy('admin_report_id', 'desc')->get();

		$code = "Indirect Breakdown";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {
			return view("admin.transaction.indirect", $data);
		}
        else
        {
            return Redirect::back();
        }
	}
	public function daily()
	{
		return Redirect::to("/admin/transaction/indirect")->send();
	}
	public function indirectreport()
	{
		$id = Request::input('id');
		// dd($id);
		$data['_detail'] = Tbl_indirect_report::where('indirect_admin_report', $id)
														   ->join('tbl_slot', 'tbl_slot.slot_id', '=', 'tbl_indirect_report.indirect_slot')
														   ->join('tbl_account', 'tbl_account.account_id', '=', 'tbl_slot.slot_owner')
														   ->get();
		// dd($data);
		return view('admin.transaction.indirectreport', $data);
	}
}
<?php namespace App\Http\Controllers;
use App\Tbl_binary_admin_report;
use App\Tbl_binary_report;
use Redirect;
use App\Classes\Compute;
use Request;
use DB;

use App\Globals\AdminNav;
class AdminBinaryController extends AdminController
{
	public function index()
	{
		$data["page"] = "Binary Processing";
		$data["_binary_admin_report"] = Tbl_binary_admin_report::orderBy('binary_admin_id', 'desc')->get();
		
		$code = "Binary Breakdown";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {
		    return view("admin.transaction.binary", $data);
		}
        else
        {
            return Redirect::back();
        }
	}

	public function breakdownreport()
	{
		$id = Request::input('id');
		$data['_detail'] = Tbl_binary_report::where('binary_admin_id', $id)
														 ->join('tbl_slot', 'tbl_slot.slot_id', '=', 'tbl_binary_report.binary_slot_id')
														 ->join('tbl_account', 'tbl_account.account_id', '=', 'tbl_slot.slot_owner')
														 ->get();

		return view('admin.transaction.breakdownreport', $data);
	}

	public function daily()
	{
		Compute::binary_cutoff();
		return Redirect::to("/admin/transaction/binary")->send();
	}
}



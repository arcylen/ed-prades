<?php namespace App\Http\Controllers;
use DB;
use Redirect;
use Request;
use App\Classes\Image;
use App\Tbl_about;

use App\Globals\AdminNav;
class AdminFrontFooterController extends AdminController
{
	public function index()
	{
		$id = Request::input("id");
		$contactnumber = DB::table("tbl_about")->where("about_name", "Contact Number")->first();
		$email = DB::table("tbl_about")->where("about_name", "E-Mail")->first();
		$newsletter = DB::table("tbl_about")->where("about_name", "News Letter")->first();
		$rights = DB::table("tbl_about")->where("about_name", "Rights")->first();
		// dd($data);
		if($contactnumber == null)
		{
			$a["about_name"] = "Contact Number";
			$a["about_description"] = "";
			DB::table("tbl_about")->insert($a);
			$data["contactnumber"] = DB::table("tbl_about")->where("about_name", "About")->first();
		}
		else
		{
			$data["contactnumber"] = $contactnumber;
		}
		if($email == null)
		{
			$m["about_name"] = "E-Mail";
			$m["about_description"] = "";
			DB::table("tbl_about")->insert($m);
			$data["email"] = DB::table("tbl_about")->where("about_name", "Mission")->first();
		}
		else
		{
			$data["email"] = $email;
		}
		
		if($newsletter == null)
		{
			$m["about_name"] = "News Letter";
			$m["about_description"] = "";
			DB::table("tbl_about")->insert($m);
			$data["newsletter"] = DB::table("tbl_about")->where("about_name", "News Letter")->first();
		}
		else
		{
			$data["newsletter"] = $newsletter;
		}
		if($rights == null)
		{
			$m["about_name"] = "Rights";
			$m["about_description"] = "";
			DB::table("tbl_about")->insert($m);
			$data["rights"] = DB::table("tbl_about")->where("about_name", "Rights")->first();
		}
		else
		{
			$data["rights"] = $rights;
		}
		$code = "Front Footer";
        $action = "access";
        $access = AdminNav::checkaccess($code,$action);
        if($access == "1")
        {       
        return view('admin.content.frontfooter', $data);
        }
        else
        {
            return Redirect::back();
        } 
	}
	public function submit()
	{
		$contactnumber = Request::input("contactnumberid");
		$email = Request::input("emailid");
		$newsletter = Request::input("newsletterid");
		$rights = Request::input("rightsid");
		
		$contactnumbers = Request::input("contactnumber");
		$emails = Request::input("email");
		$newsletters = Request::input("newsletter");
		$rightss = Request::input("rights");

		$date = date('Y-m-d H:i:s');
		
		DB::table("tbl_about")->where("about_id", $contactnumber)->update(['about_description' => $contactnumbers, 'created_at' => $date, 'updated_at' => $date]);
		DB::table("tbl_about")->where("about_id", $email)->update(['about_description' => $emails, 'created_at' => $date, 'updated_at' => $date]);
		DB::table("tbl_about")->where("about_id", $newsletter)->update(['about_description' => $newsletters, 'created_at' => $date, 'updated_at' => $date]);
		DB::table("tbl_about")->where("about_id", $rights)->update(['about_description' => $rightss, 'created_at' => $date, 'updated_at' => $date]);

        return Redirect::to("/admin/content/frontfooter");
	}


	public function super_test()
	{
		return "super_test";
	}	
}
<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_ladder_rebates extends Model
{
	protected $table = 'tbl_ladder_rebates';
	public $timestamps = false;
}
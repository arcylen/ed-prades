<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_stairstep_report extends Model
{
    protected $table = 'tbl_stairstep_report';
    protected $primaryKey = 'stairstep_report_id';
    public $timestamps = false;
}
<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_direct_unilevel extends Model
{
	protected $table = 'tbl_direct_unilevel';
	protected $primaryKey = 'sequence_id';
}
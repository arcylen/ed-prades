<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_royalty_setting extends Model
{
	protected $table = 'tbl_royalty_setting';
	protected $primaryKey = 'rb_id';
	public $timestamps = false;

}  


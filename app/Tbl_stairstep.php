<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_stairstep extends Model
{
    protected $table = 'tbl_stairstep';
    protected $primaryKey = 'id';
    public $timestamps = false;
    public function scopeSettings($query)
    {
        return $query->leftJoin('tbl_stairstep_settings', 'tbl_stairstep_settings.stairstep_level', '=', 'tbl_stairstep.stairstep_settings_id');
    }
    public function scopeNextlevel($query)
    {
        return $query->leftJoin('tbl_stairstep_settings', 'tbl_stairstep_settings.stairstep_level', '=', 'tbl_stairstep.stairstep_settings_id');
    }
}
<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_royalty_promotion_setting extends Model
{
	protected $table = 'tbl_royalty_promotion_setting';
	protected $primaryKey = 'rbp_id';
	public $timestamps = false;

}


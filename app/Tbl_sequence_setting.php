<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_sequence_setting extends Model
{
	protected $table = 'tbl_sequence_setting';
	protected $primaryKey = 'set_id';
}
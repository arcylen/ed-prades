<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_direct_admin_report extends Model
{
    protected $table = 'tbl_direct_admin_report';
    protected $primaryKey = 'admin_report_id';
    public $timestamps = false;
}
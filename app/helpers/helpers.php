<?php
function currency($amount)
{
    return "PHP " . number_format($amount, 2);
}
function settings($key)
{
    $settings["company_name"] = "Business Online Sale System";
    $settings["developer_username"] = "developer";
    $settings["developer_password"] = "water123";
    return $settings[$key];
}
function formatslot($int)
{
    return str_pad($int, 6, '0', STR_PAD_LEFT);
}
function formatcol($str)
{
    return ucwords(str_replace("_", " ", $str));
}
function remove_spaces($string)
{
    return preg_replace('/\s+/', '', $string);
}

function create_dir_if_not_exist($directoryName)
{
    //Check if the directory already exists.
    if(!is_dir($directoryName)){
        //Directory does not exist, so lets create it.
        $oldmask = umask(0);
        mkdir($directoryName, 0777, false);
        umask($oldmask);
    }
}
function generate_file_with_content($path, $content)
{
    $fp = fopen($path,"wb");
    fwrite($fp,$content);
    fclose($fp);
}

function check_if_word_exist_on_file($path, $word)
{
    $handle = fopen($path, 'r');
    $valid = false; // init as false
    while (($buffer = fgets($handle)) !== false)
    {
        if (strpos($buffer, $word) !== false) {
            $valid = TRUE;
            break; // Once you find the string, you should break out the loop.
        }      
    }
    
    fclose($handle);
    
    return $valid;
}
function ordinal($number) {
    $ends = array('th','st','nd','rd','th','th','th','th','th','th');
    if ((($number % 100) >= 11) && (($number%100) <= 13))
        return $number. 'th';
    else
        return $number. $ends[$number % 10];
}
function hyphenate($str) {
    return implode("-", str_split($str, 3));
}
function parse_number($number, $dec_point=null) {
    if (empty($dec_point)) {
        $locale = localeconv();
        $dec_point = $locale['decimal_point'];
    }
    return floatval(str_replace($dec_point, '.', preg_replace('/[^\d'.preg_quote($dec_point).']/', '', $number)));
}

<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_binary_admin_report extends Model
{
	protected $table = 'tbl_binary_admin_report';
	protected $primaryKey = "binary_admin_id";
	public $timestamps = false;
}
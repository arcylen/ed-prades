<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_pipeline_settings extends Model
{
    protected $table = 'tbl_pipeline_settings';
    protected $primaryKey = 'pipeline_id';
    public $timestamps = false;
}
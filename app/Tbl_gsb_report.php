<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_gsb_report extends Model
{
	protected $table = 'tbl_gsb_report';
	protected $primaryKey = "gsb_id";
    public $timestamps = false;

    public function scopeSlot($query)
    {
        return $query->join('tbl_slot', 'tbl_slot.slot_id', '=', 'tbl_gsb_report.gsb_sponsor');
    }
    public function scopeGroupsalesbonus($query)
    {
        return $query->join('tbl_sequence', 'tbl_sequence.sequence_id', '=', 'tbl_total_gsb.sequence_level_id');
    }
    public function scopeAccount($query)
    {
        return $query->leftJoin('tbl_account', 'tbl_account.account_id', '=', 'tbl_slot.slot_owner');
    }
}

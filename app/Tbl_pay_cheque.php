<?php
namespace App;

use Illuminate\Database\Eloquent\Model;


class Tbl_pay_cheque extends Model
{
	protected $table = 'tbl_pay_cheque';
	protected $primaryKey = "pay_cheque_id";
	public $timestamps = false;

	public function scopeBatch($query)
	{
	    return $query->leftJoin('tbl_pay_cheque_batch', 'tbl_pay_cheque_batch.pay_cheque_batch_id', '=', 'tbl_pay_cheque.pay_cheque_batch_id');
	}
    public function scopeSlot($query)
    {
        return $query->leftJoin('tbl_slot', 'tbl_slot.slot_id', '=', 'tbl_pay_cheque.pay_cheque_slot')
        ->leftJoin("tbl_account", "tbl_account.account_id", "=", "tbl_slot.slot_owner");
    }

}
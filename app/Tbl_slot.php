<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Tbl_slot extends Model
{
	protected $table = 'tbl_slot';
	protected $primaryKey = 'slot_id';
    public $timestamps = false;
    
    public function scopeRank($query)
    {
        return $query->leftJoin("tbl_rank", "tbl_rank.rank_id", "=", "tbl_slot.slot_rank");
    }
    public function scopeMembership($query)
    {
        return $query->leftJoin("tbl_membership", "tbl_membership.membership_id", "=", "tbl_slot.slot_membership");
    }
    public function scopeDirectunilevel($query)
    {
        return $query->leftJoin("tbl_direct_unilevel", "tbl_direct_unilevel.sequence_id", "=", "tbl_slot.direct_unilevel_id");
    }
    public function scopeRoyalty($query)
    {
        return $query->leftJoin("tbl_royalty_setting", "tbl_royalty_setting.rb_id", "=", "tbl_slot.royalty_level")
                    ->leftJoin("tbl_royalty_promotion_setting", "tbl_royalty_setting.rb_id", "=", "tbl_royalty_promotion_setting.rb_id");
    }
    public function scopeGroupsalesbonus($query)
    {
        return $query->leftJoin("tbl_sequence", "tbl_sequence.sequence_id", "=", "tbl_slot.sequence_level_id");
    }
    public function scopeConsultantspv($query)
    {
        return $query->leftJoin("tbl_sequence", "tbl_sequence.sequence_id", "=", "tbl_slot.consultant_unilevel");
    }
    public function scopeIndirectunilevel($query)
    {
        return $query->leftJoin("tbl_indirect_unilevel", "tbl_indirect_unilevel.sequence_id", "=", "tbl_slot.indirect_unilevel_id");
    }
    public function scopeAccount($query)
    {
        return $query->leftJoin("tbl_account", "tbl_account.account_id", "=", "tbl_slot.slot_owner");
    }
    public function scopeId($query, $slot_id)
    {
        return $query->where("tbl_slot.slot_id", $slot_id);
    }
    public function scopePlacement($query, $slot_id)
    {
        return $query->where("placement", $slot_id);
    }
    public function scopeCheckposition($query, $placement, $position)
    {
        return $query->where("slot_position", $position)->where("slot_placement", $placement);
    }
    public function scopeChosenProduct($query)
    {
        return $query->leftjoin('rel_membership_product','rel_membership_product.slot_id','=','tbl_slot.slot_id');
    }
    public function scopeGetChosen($query)
    {
        return $query->leftjoin('tbl_product_package_has','tbl_product_package_has.product_package_id','=','rel_membership_product.product_package_id');
    }
    public function scopeSum($query)
    {
        return $query->leftjoin('tbl_wallet_logs','tbl_wallet_logs.slot_id','=','tbl_slot.slot_id');
    }

}

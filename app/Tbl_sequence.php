<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_sequence extends Model
{
	protected $table = 'tbl_sequence';
	protected $primaryKey = 'sequence_id';
}
<?php namespace App\Globals;
use Session;
use DB;

class AdminNav
{
      public static function checkaccess($code, $action){
      $admin_info = Session::get('admin_info');
      $rank = Session::get('admin_position_rank');
      
            if($admin_info)
            {
                  if($admin_info->admin_position_id==1){
                        return "1";
                  }
                  else
                  {
                  $checkifaccess = DB::table('tbl_admin_permission')
                                 ->where('admin_position_id', '=', $admin_info->admin_position_id)
                                 ->where('sub_page_name', '=', $code)
                                 ->where('action', '=', $action)
                                 ->where('permission', '=', 1)
                                 ->count();
                        if($checkifaccess >=1){
                              return "1";
                        }
                        elseif($rank == 0)
                        {
                              return "1";     
                        }
                        else{
                              return "0";
                        }   
                  }
            }
            else
            {
                  return Redirect('/admin/login');
            } 
               
      }
    public static function index()
    {

      $path = '/admin/';
      $count = 0;  
      $nav[$count]['name'] = 'Transaction';
      $nav[$count]['segment'] = 'transaction';

      $nav[$count]['submenu'][0]['label'] = 'Sell Products';
      $nav[$count]['submenu'][0]['code'] = 'sellproduct';
      $nav[$count]['submenu'][0]['url'] = $path . $nav[$count]['segment'] . '/sales';
      $nav[$count]['submenu'][0]['action'] = ['access'];
      $nav[$count]['submenu'][0]['seperator'] = 'hidden';

      $nav[$count]['submenu'][1]['label'] = 'Sell Codes';
      $nav[$count]['submenu'][1]['code'] = 'sellcodes';
      $nav[$count]['submenu'][1]['url'] = $path . $nav[$count]['segment'] . '/view_voucher_codes';
      $nav[$count]['submenu'][1]['action'] = ['access'];
      $nav[$count]['submenu'][1]['seperator'] = 'divider';


      $nav[$count]['submenu'][2]['label'] = 'Pay Cheque';
      $nav[$count]['submenu'][2]['code'] = 'paycheque';
      $nav[$count]['submenu'][2]['url'] = $path . $nav[$count]['segment'] . '/paycheque';
      $nav[$count]['submenu'][2]['action'] = ['access'];
      $nav[$count]['submenu'][2]['seperator'] = 'hidden';
      
      $nav[$count]['submenu'][3]['label'] = 'Wallet Logs';
      $nav[$count]['submenu'][3]['code'] = 'Wallet Logs';
      $nav[$count]['submenu'][3]['url'] = $path . $nav[$count]['segment'] . '/wallet';
      $nav[$count]['submenu'][3]['action'] = ['access'];
      $nav[$count]['submenu'][3]['seperator'] = 'hidden';

      $nav[$count]['submenu'][4]['label'] = 'Process Claims';
      $nav[$count]['submenu'][4]['code'] = 'processclaims';
      $nav[$count]['submenu'][4]['url'] = $path . $nav[$count]['segment'] . '/claims';
      $nav[$count]['submenu'][4]['action'] = ['access'];
      $nav[$count]['submenu'][4]['seperator'] = 'divider';

      $nav[$count]['submenu'][5]['label'] = 'Distribute SPV';
      $nav[$count]['submenu'][5]['code'] = 'distributespv';
      $nav[$count]['submenu'][5]['url'] = $path . $nav[$count]['segment'] . '/distributespv';
      $nav[$count]['submenu'][5]['action'] = ['access'];
      $nav[$count]['submenu'][5]['seperator'] = 'divider';

      // $nav[$count]['submenu'][5]['label'] = 'Binary Breakdown';
      // $nav[$count]['submenu'][5]['code'] = 'binarybreakdown';
      // $nav[$count]['submenu'][5]['url'] = $path . $nav[$count]['segment'] . '/binary';
      // $nav[$count]['submenu'][5]['action'] = ['access'];
      // $nav[$count]['submenu'][5]['seperator'] = 'divider';

      


      // $nav[$count]['submenu'][6]['label'] = 'Indirect Breakdown';
      // $nav[$count]['submenu'][6]['code'] = 'indirect';
      // $nav[$count]['submenu'][6]['url'] = $path . $nav[$count]['segment'] . '/indirect';
      // $nav[$count]['submenu'][6]['action'] = ['access'];
      // $nav[$count]['submenu'][6]['seperator'] = 'hidden';


      // $nav[$count]['submenu'][7]['label'] = 'Pipeline Breakdown';
      // $nav[$count]['submenu'][7]['code'] = 'pipeline';
      // $nav[$count]['submenu'][7]['url'] = $path . $nav[$count]['segment'] . '/pipeline';
      // $nav[$count]['submenu'][7]['action'] = ['access'];
      // $nav[$count]['submenu'][7]['seperator'] = 'hidden';


      // $nav[$count]['submenu'][8]['label'] = 'Dynamic Compression';
      // $nav[$count]['submenu'][8]['code'] = 'dynamiccom';
      // $nav[$count]['submenu'][8]['url'] = $path . $nav[$count]['segment'] . '/unilevel-distribution/dynamic';
      // $nav[$count]['submenu'][8]['action'] = ['access'];
      // $nav[$count]['submenu'][8]['seperator'] = 'hidden';


      // $nav[$count]['submenu'][9]['label'] = 'Global Pool Sharing';
      // $nav[$count]['submenu'][9]['code'] = 'gpoolsharing';
      // $nav[$count]['submenu'][9]['url'] = $path . $nav[$count]['segment'] . '/global_pool_sharing';
      // $nav[$count]['submenu'][9]['action'] = ['access'];
      // $nav[$count]['submenu'][9]['seperator'] = 'hidden';


      $nav[$count]['submenu'][10]['label'] = 'Transfer Request Slot';
      $nav[$count]['submenu'][10]['code'] = 'transrequestslot';
      $nav[$count]['submenu'][10]['url'] = $path . $nav[$count]['segment'] . '/sales/transfer_slot_request';
      $nav[$count]['submenu'][10]['action'] = ['access'];
      $nav[$count]['submenu'][10]['seperator'] = 'hidden';
      
      // $nav[$count]['submenu'][11]['label'] = 'Unilevel Distribution';
      // $nav[$count]['submenu'][11]['code'] = 'transrequestslot';
      // $nav[$count]['submenu'][11]['url'] = $path . $nav[$count]['segment'] . '/sales/unileveldistribute';
      // $nav[$count]['submenu'][11]['action'] = ['access'];
      // $nav[$count]['submenu'][11]['seperator'] = 'divider';
      
      $nav[$count]['submenu'][12]['label'] = 'Payment Options';
      $nav[$count]['submenu'][12]['code'] = 'transrequestslot';
      $nav[$count]['submenu'][12]['url'] = $path . $nav[$count]['segment'] . '/paymentoptions';
      $nav[$count]['submenu'][12]['action'] = ['access'];
      $nav[$count]['submenu'][12]['seperator'] = 'hidden';



      $count++;
      $nav[$count]['name'] = 'Maintenance';
      $nav[$count]['segment'] = 'maintenance';

      $nav[$count]['submenu'][0]['label'] = 'Account';
      $nav[$count]['submenu'][0]['code'] = 'accounts';
      $nav[$count]['submenu'][0]['url'] = $path . $nav[$count]['segment'] . '/accounts';
      $nav[$count]['submenu'][0]['action'] = ['access'];
      $nav[$count]['submenu'][0]['seperator'] = 'hidden';


      $nav[$count]['submenu'][1]['label'] = 'Account Slots';
      $nav[$count]['submenu'][1]['code'] = 'slotsaccount';
      $nav[$count]['submenu'][1]['url'] = $path . $nav[$count]['segment'] . '/slots';
      $nav[$count]['submenu'][1]['action'] = ['access'];
      $nav[$count]['submenu'][1]['seperator'] = 'hidden';


      $nav[$count]['submenu'][2]['label'] = 'Account Block';
      $nav[$count]['submenu'][2]['code'] = 'acountblock';
      $nav[$count]['submenu'][2]['url'] = $path . $nav[$count]['segment'] . '/account_block';
      $nav[$count]['submenu'][2]['action'] = ['access'];
      $nav[$count]['submenu'][2]['seperator'] = 'divider';


      $nav[$count]['submenu'][3]['label'] = 'Product';
      $nav[$count]['submenu'][3]['code'] = 'productmaintenance';
      $nav[$count]['submenu'][3]['url'] = $path . $nav[$count]['segment'] . '/product';
      $nav[$count]['submenu'][3]['action'] = ['access'];
      $nav[$count]['submenu'][3]['seperator'] =  'hidden';

      $nav[$count]['submenu'][4]['label'] = 'Product Package';
      $nav[$count]['submenu'][4]['code'] = 'productpackage';
      $nav[$count]['submenu'][4]['url'] = $path . $nav[$count]['segment'] . '/product_package';
      $nav[$count]['submenu'][4]['action'] = ['access'];
      $nav[$count]['submenu'][4]['seperator'] = 'hidden';

      $nav[$count]['submenu'][5]['label'] = 'Product Categories';
      $nav[$count]['submenu'][5]['code'] = 'productcategories';
      $nav[$count]['submenu'][5]['url'] = $path . $nav[$count]['segment'] . '/product_category';
      $nav[$count]['submenu'][5]['action'] = ['access'];
      $nav[$count]['submenu'][5]['seperator'] = 'hidden';

      $nav[$count]['submenu'][6]['label'] = 'Product Inventory';
      $nav[$count]['submenu'][6]['code'] = 'productinventory';
      $nav[$count]['submenu'][6]['url'] = $path . $nav[$count]['segment'] . '/inventory';
      $nav[$count]['submenu'][6]['action'] = ['access'];
      $nav[$count]['submenu'][6]['seperator'] = 'divider';

      $nav[$count]['submenu'][7]['label'] = 'Membership';
      $nav[$count]['submenu'][7]['code'] = 'membership';
      $nav[$count]['submenu'][7]['url'] = $path . $nav[$count]['segment'] . '/membership';
      $nav[$count]['submenu'][7]['action'] = ['access'];
      $nav[$count]['submenu'][7]['seperator'] = 'hidden';

      $nav[$count]['submenu'][8]['label'] = 'Membership Codes';
      $nav[$count]['submenu'][8]['code'] = 'memcodes';
      $nav[$count]['submenu'][8]['url'] = $path . $nav[$count]['segment'] . '/codes';
      $nav[$count]['submenu'][8]['action'] = ['access'];
      $nav[$count]['submenu'][8]['seperator'] = 'divider';

      $nav[$count]['submenu'][9]['label'] = 'Country';
      $nav[$count]['submenu'][9]['code'] = 'country';
      $nav[$count]['submenu'][9]['url'] = $path . $nav[$count]['segment'] . '/country';
      $nav[$count]['submenu'][9]['action'] = ['access'];
      $nav[$count]['submenu'][9]['seperator'] = 'hidden';

      $nav[$count]['submenu'][10]['label'] = 'Deductions';
      $nav[$count]['submenu'][10]['code'] = 'deduction';
      $nav[$count]['submenu'][10]['url'] = $path . $nav[$count]['segment'] . '/deduction';
      $nav[$count]['submenu'][10]['action'] = ['access'];
      $nav[$count]['submenu'][10]['seperator'] = 'hidden';

      $nav[$count]['submenu'][11]['label'] = 'Ranking';
      $nav[$count]['submenu'][11]['code'] = 'ranking';
      $nav[$count]['submenu'][11]['url'] = $path . $nav[$count]['segment'] . '/ranking';
      $nav[$count]['submenu'][11]['action'] = ['access'];
      $nav[$count]['submenu'][11]['seperator'] = 'hidden';

      // $nav[$count]['submenu'][12]['label'] = 'Stockist';
      // $nav[$count]['submenu'][12]['code'] = 'stockist';
      // $nav[$count]['submenu'][12]['url'] = $path . 'admin_stockist';
      // $nav[$count]['submenu'][12]['action'] = ['access'];
      // $nav[$count]['submenu'][12]['seperator'] = 'hidden';

      // $nav[$count]['submenu'][13]['label'] = 'Stockist User';
      // $nav[$count]['submenu'][13]['code'] = 'stockistuser';
      // $nav[$count]['submenu'][13]['url'] = $path . 'admin_stockist_user';
      // $nav[$count]['submenu'][13]['action'] = ['access'];
      // $nav[$count]['submenu'][13]['seperator'] = 'hidden';

      // $nav[$count]['submenu'][14]['label'] = 'Stockist Type';
      // $nav[$count]['submenu'][14]['code'] = 'stockisttype';
      // $nav[$count]['submenu'][14]['url'] = $path . 'stockist_type';
      // $nav[$count]['submenu'][14]['action'] = ['access'];
      // $nav[$count]['submenu'][14]['seperator'] = 'hidden';

      // $nav[$count]['submenu'][15]['label'] = 'Stockist Inventory';
      // $nav[$count]['submenu'][15]['code'] = 'stockistinventory';
      // $nav[$count]['submenu'][15]['url'] = $path . 'stockist_inventory';
      // $nav[$count]['submenu'][15]['action'] = ['access'];
      // $nav[$count]['submenu'][15]['seperator'] = 'hidden';

      // $nav[$count]['submenu'][16]['label'] = 'Stockist Refill Wallet';
      // $nav[$count]['submenu'][16]['code'] = 'stockistwallet';
      // $nav[$count]['submenu'][16]['url'] = $path . 'stockist_wallet';
      // $nav[$count]['submenu'][16]['action'] = ['access'];
      // $nav[$count]['submenu'][16]['seperator'] = 'hidden';

      // $nav[$count]['submenu'][17]['label'] = 'Stockist Order Request';
      // $nav[$count]['submenu'][17]['code'] = 'stockistorderwallet';
      // $nav[$count]['submenu'][17]['url'] = $path . 'stockist_request';
      // $nav[$count]['submenu'][17]['action'] = ['access'];
      // $nav[$count]['submenu'][17]['seperator'] = 'hidden';


      $count++;
      $nav[$count]['name'] = 'Utilities';
      $nav[$count]['segment'] = 'utilities';

      $nav[$count]['submenu'][0]['label'] = 'Admin';
      $nav[$count]['submenu'][0]['code'] = 'admin';
      $nav[$count]['submenu'][0]['url'] = $path . $nav[$count]['segment'] . '/admin_maintenance';
      $nav[$count]['submenu'][0]['action'] = ['access'];
      $nav[$count]['submenu'][0]['seperator'] = 'hidden';

      $nav[$count]['submenu'][1]['label'] = 'Admin Positions';
      $nav[$count]['submenu'][1]['code'] = 'adminposition';
      $nav[$count]['submenu'][1]['url'] = $path . $nav[$count]['segment'] . '/position';
      $nav[$count]['submenu'][1]['action'] = ['access'];
      $nav[$count]['submenu'][1]['seperator'] = 'hidden';

      $nav[$count]['submenu'][2]['label'] = 'Company Setting';
      $nav[$count]['submenu'][2]['code'] = 'companysetting';
      $nav[$count]['submenu'][2]['url'] = $path . $nav[$count]['segment'] . '/setting';
      $nav[$count]['submenu'][2]['action'] = ['access'];
      $nav[$count]['submenu'][2]['seperator'] = 'divider';

      // $nav[$count]['submenu'][3]['label'] = 'Binary Computation';
      // $nav[$count]['submenu'][3]['code'] = 'binarycomputation';
      // $nav[$count]['submenu'][3]['url'] = $path . $nav[$count]['segment'] . '/binary';
      // $nav[$count]['submenu'][3]['action'] = ['access'];
      // $nav[$count]['submenu'][3]['seperator'] = 'hidden';

      // $nav[$count]['submenu'][4]['label'] = 'Pipeline Computation';
      // $nav[$count]['submenu'][4]['code'] = 'pipelinecomputation';
      // $nav[$count]['submenu'][4]['url'] = $path . $nav[$count]['segment'] . '/pipeline';
      // $nav[$count]['submenu'][4]['action'] = ['access'];
      // $nav[$count]['submenu'][4]['seperator'] = 'hidden';

      $nav[$count]['submenu'][5]['label'] = 'Referral Bonus';
      $nav[$count]['submenu'][5]['code'] = 'dirrectsponsorship';
      $nav[$count]['submenu'][5]['url'] = $path . $nav[$count]['segment'] . '/direct';
      $nav[$count]['submenu'][5]['action'] = ['access'];
      $nav[$count]['submenu'][5]['seperator'] = 'hidden';

      $nav[$count]['submenu'][6]['label'] = 'Sales Bonus';
      $nav[$count]['submenu'][6]['code'] = 'classseqcom';
      $nav[$count]['submenu'][6]['url'] = $path . $nav[$count]['segment'] . '/class_sequence';
      $nav[$count]['submenu'][6]['action'] = ['access'];
      $nav[$count]['submenu'][6]['seperator'] = 'hidden';
     
      // $nav[$count]['submenu'][6]['label'] = 'Indirect Sponsorship Bonus';
      // $nav[$count]['submenu'][6]['code'] = 'indirectsponsorship';
      // $nav[$count]['submenu'][6]['url'] = $path . $nav[$count]['segment'] . '/indirect';
      // $nav[$count]['submenu'][6]['action'] = ['access'];
      // $nav[$count]['submenu'][6]['seperator'] = 'hidden';

      // $nav[$count]['submenu'][7]['label'] = 'Stair Step Computation';
      // $nav[$count]['submenu'][7]['code'] = 'stairstepcomputation';
      // $nav[$count]['submenu'][7]['url'] = $path . $nav[$count]['segment'] . '/stairstep';
      // $nav[$count]['submenu'][7]['action'] = ['access'];
      // $nav[$count]['submenu'][7]['seperator'] = 'hidden';
      
      // $nav[$count]['submenu'][7]['label'] = 'Ladder Rebates';
      // $nav[$count]['submenu'][7]['code'] = 'stairstepcomputation';
      // $nav[$count]['submenu'][7]['url'] = $path . $nav[$count]['segment'] . '/rebates';
      // $nav[$count]['submenu'][7]['action'] = ['access'];
      // $nav[$count]['submenu'][7]['seperator'] = 'hidden';
      
      // $nav[$count]['submenu'][7]['label'] = 'Mentor Bonus Computation';
      // $nav[$count]['submenu'][7]['code'] = 'mentorbonus';
      // $nav[$count]['submenu'][7]['url'] = $path . $nav[$count]['segment'] . '/matching';
      // $nav[$count]['submenu'][7]['action'] = ['access'];
      // $nav[$count]['submenu'][7]['seperator'] = 'hidden';

      $nav[$count]['submenu'][8]['label'] = 'Royalty Bonus';
      $nav[$count]['submenu'][8]['code'] = 'rb_setting';
      $nav[$count]['submenu'][8]['url'] = $path . $nav[$count]['segment'] . '/royalty_bonus';
      $nav[$count]['submenu'][8]['action'] = ['access'];
      $nav[$count]['submenu'][8]['seperator'] = 'divider';

      // $nav[$count]['submenu'][8]['label'] = 'Unilevel Check Match';
      // $nav[$count]['submenu'][8]['code'] = 'checkmatch';
      // $nav[$count]['submenu'][8]['url'] = $path . $nav[$count]['segment'] . '/unilevel_check_match';
      // $nav[$count]['submenu'][8]['action'] = ['access'];
      // $nav[$count]['submenu'][8]['seperator'] = 'hidden';

      // $nav[$count]['submenu'][9]['label'] = 'Unilevel Computation';
      // $nav[$count]['submenu'][9]['code'] = 'unilvlcomputation';
      // $nav[$count]['submenu'][9]['url'] = $path . $nav[$count]['segment'] . '/unilevel';
      // $nav[$count]['submenu'][9]['action'] = ['access'];
      // $nav[$count]['submenu'][9]['seperator'] = 'divider';

      $nav[$count]['submenu'][10]['label'] = 'Pay Cheque Schedules';
      $nav[$count]['submenu'][10]['code'] = 'paychequesked';
      $nav[$count]['submenu'][10]['url'] = $path . $nav[$count]['segment'] . '/paysched';
      $nav[$count]['submenu'][10]['action'] = ['access'];
      $nav[$count]['submenu'][10]['seperator'] = 'hidden';

      // $nav[$count]['submenu'][10]['label'] = 'Breakaway Bonus';
      // $nav[$count]['submenu'][10]['code'] = 'breakawaybonus';
      // $nav[$count]['submenu'][10]['url'] = $path . $nav[$count]['segment'] . '/breakaway_bonus';
      // $nav[$count]['submenu'][10]['action'] = ['access'];
      // $nav[$count]['submenu'][10]['seperator'] = 'hidden';

      // $nav[$count]['submenu'][11]['label'] = 'Leadership Bonus';
      // $nav[$count]['submenu'][11]['code'] = 'leadershipbonus';
      // $nav[$count]['submenu'][11]['url'] = $path . $nav[$count]['segment'] . '/leadership_bonus';
      // $nav[$count]['submenu'][11]['action'] = ['access'];
      // $nav[$count]['submenu'][11]['seperator'] = 'divider';

      // $nav[$count]['submenu'][12]['label'] = 'Travel And Car Bonus Qualification';
      // $nav[$count]['submenu'][12]['code'] = 'travelandcar';
      // $nav[$count]['submenu'][12]['url'] = $path . $nav[$count]['segment'] . '/travel_qualification';
      // $nav[$count]['submenu'][12]['action'] = ['access'];
      // $nav[$count]['submenu'][12]['seperator'] = 'hidden';

      // $nav[$count]['submenu'][13]['label'] = 'Travel And Car Bonus Reward';
      // $nav[$count]['submenu'][13]['code'] = 'travelandcarreward';
      // $nav[$count]['submenu'][13]['url'] = $path . $nav[$count]['segment'] . '/travel_reward';
      // $nav[$count]['submenu'][13]['action'] = ['access'];
      // $nav[$count]['submenu'][13]['seperator'] = 'divider';

      // $nav[$count]['submenu'][14]['label'] = 'Test Encode';
      // $nav[$count]['submenu'][14]['code'] = 'testencode';
      // $nav[$count]['submenu'][14]['url'] = $path . $nav[$count]['segment'] . '/test_encode';
      // $nav[$count]['submenu'][14]['action'] = ['access'];
      // $nav[$count]['submenu'][14]['seperator'] = 'divider';

      // $nav[$count]['submenu'][15]['label'] = 'Promotion Setting';
      // $nav[$count]['submenu'][15]['code'] = 'promotionsetting';
      // $nav[$count]['submenu'][15]['url'] = $path . $nav[$count]['segment'] . '/rank';
      // $nav[$count]['submenu'][15]['action'] = ['access'];
      // $nav[$count]['submenu'][15]['seperator'] = 'hidden';


      $count++;
      $nav[$count]['name'] = 'Reports';
      $nav[$count]['segment'] = 'reports';

      $nav[$count]['submenu'][0]['label'] = 'Product Sales Report';
      $nav[$count]['submenu'][0]['code'] = 'salesreport';
      $nav[$count]['submenu'][0]['url'] = $path . $nav[$count]['segment'] . '/product_sales';
      $nav[$count]['submenu'][0]['action'] = ['access'];
      $nav[$count]['submenu'][0]['seperator'] = 'hidden';

      $nav[$count]['submenu'][1]['label'] = 'Membership Sales Report';
      $nav[$count]['submenu'][1]['code'] = 'membershipreport';
      $nav[$count]['submenu'][1]['url'] = $path . $nav[$count]['segment'] . '/membership_sales';
      $nav[$count]['submenu'][1]['action'] = ['access'];
      $nav[$count]['submenu'][1]['seperator'] = 'hidden';

      $nav[$count]['submenu'][2]['label'] = 'Audit Trail';
      $nav[$count]['submenu'][2]['code'] = 'audittrail';
      $nav[$count]['submenu'][2]['url'] = $path . $nav[$count]['segment'] . '/audit_trail';
      $nav[$count]['submenu'][2]['action'] = ['access'];
      $nav[$count]['submenu'][2]['seperator'] = 'hidden';

      $nav[$count]['submenu'][3]['label'] = 'Product Inventory Report';
      $nav[$count]['submenu'][3]['code'] = 'inventoryreport';
      $nav[$count]['submenu'][3]['url'] = $path . $nav[$count]['segment'] . '/prod_inventory';
      $nav[$count]['submenu'][3]['action'] = ['access'];
      $nav[$count]['submenu'][3]['seperator'] = 'hidden';

      $nav[$count]['submenu'][4]['label'] = 'Bonus Summary';
      $nav[$count]['submenu'][4]['code'] = 'bonussummary';
      $nav[$count]['submenu'][4]['url'] = $path . $nav[$count]['segment'] . '/bonus_summary';
      $nav[$count]['submenu'][4]['action'] = ['access'];
      $nav[$count]['submenu'][4]['seperator'] = 'hidden';

      // $nav[$count]['submenu'][5]['label'] = 'GC Summary';
      // $nav[$count]['submenu'][5]['code'] = 'gcsummary';
      // $nav[$count]['submenu'][5]['url'] = $path . $nav[$count]['segment'] . '/gc_summary';
      // $nav[$count]['submenu'][5]['action'] = ['access'];
      // $nav[$count]['submenu'][5]['seperator'] = 'hidden';

      $nav[$count]['submenu'][6]['label'] = 'Top Earner';
      $nav[$count]['submenu'][6]['code'] = 'topearner';
      $nav[$count]['submenu'][6]['url'] = $path . $nav[$count]['segment'] . '/top_earner';
      $nav[$count]['submenu'][6]['action'] = ['access'];
      $nav[$count]['submenu'][6]['seperator'] = 'hidden';

      $nav[$count]['submenu'][7]['label'] = 'Top Recruiter';
      $nav[$count]['submenu'][7]['code'] = 'top_recruiter';
      $nav[$count]['submenu'][7]['url'] = $path . $nav[$count]['segment'] . '/top_recruiter';
      $nav[$count]['submenu'][7]['action'] = ['access'];
      $nav[$count]['submenu'][7]['seperator'] = 'hidden';

      $nav[$count]['submenu'][8]['label'] = 'Other Reports';
      $nav[$count]['submenu'][8]['code'] = 'otherreport';
      $nav[$count]['submenu'][8]['url'] = $path . $nav[$count]['segment'] . '/other_reports';
      $nav[$count]['submenu'][8]['action'] = ['access'];
      $nav[$count]['submenu'][8]['seperator'] = 'hidden';

      $nav[$count]['submenu'][9]['label'] = 'MATCHING BONUS COUNT';
      $nav[$count]['submenu'][9]['code'] = 'bonuscount';
      $nav[$count]['submenu'][9]['url'] = $path . $nav[$count]['segment'] . '/check_gc';
      $nav[$count]['submenu'][9]['action'] = ['access'];
      $nav[$count]['submenu'][9]['seperator'] = 'hidden';

      $count++;
      $nav[$count]['name'] = 'Content';
      $nav[$count]['segment'] = 'content';

      $nav[$count]['submenu'][0]['label'] = 'News';
      $nav[$count]['submenu'][0]['code'] = 'news';
      $nav[$count]['submenu'][0]['url'] = $path . $nav[$count]['segment'] . '/news';
      $nav[$count]['submenu'][0]['action'] = ['access'];
      $nav[$count]['submenu'][0]['seperator'] = 'hidden';

      // $nav[$count]['submenu'][1]['label'] = 'Stories';
      // $nav[$count]['submenu'][1]['code'] = 'stories';
      // $nav[$count]['submenu'][1]['url'] = $path . $nav[$count]['segment'] . '/stories';
      // $nav[$count]['submenu'][1]['action'] = ['access'];
      // $nav[$count]['submenu'][1]['seperator'] = 'hidden';

      // $nav[$count]['submenu'][2]['label'] = 'Slide';
      // $nav[$count]['submenu'][2]['code'] = 'slide';
      // $nav[$count]['submenu'][2]['url'] = $path . $nav[$count]['segment'] . '/slide';
      // $nav[$count]['submenu'][2]['action'] = ['access'];
      // $nav[$count]['submenu'][2]['seperator'] = 'hidden';

      // $nav[$count]['submenu'][3]['label'] = 'Team';
      // $nav[$count]['submenu'][3]['code'] = 'team';
      // $nav[$count]['submenu'][3]['url'] = $path . $nav[$count]['segment'] . '/team';
      // $nav[$count]['submenu'][3]['action'] = ['access'];
      // $nav[$count]['submenu'][3]['seperator'] = 'hidden';

      // $nav[$count]['submenu'][4]['label'] = 'F.A.Q.';
      // $nav[$count]['submenu'][4]['code'] = 'faq';
      // $nav[$count]['submenu'][4]['url'] = $path . $nav[$count]['segment'] . '/faq';
      // $nav[$count]['submenu'][4]['action'] = ['access'];
      // $nav[$count]['submenu'][4]['seperator'] = 'hidden';

      // $nav[$count]['submenu'][5]['label'] = 'Mind Sync';
      // $nav[$count]['submenu'][5]['code'] = 'mindsync';
      // $nav[$count]['submenu'][5]['url'] = $path . $nav[$count]['segment'] . '/mindsync';
      // $nav[$count]['submenu'][5]['action'] = ['access'];
      // $nav[$count]['submenu'][5]['seperator'] = 'hidden';

      // $nav[$count]['submenu'][6]['label'] = 'Food Cart';
      // $nav[$count]['submenu'][6]['code'] = 'foodcart';
      // $nav[$count]['submenu'][6]['url'] = $path . $nav[$count]['segment'] . '/foodcart';
      // $nav[$count]['submenu'][6]['action'] = ['access'];
      // $nav[$count]['submenu'][6]['seperator'] = 'hidden';
      
      // $nav[$count]['submenu'][7]['label'] = 'Home Page';
      // $nav[$count]['submenu'][7]['code'] = 'homepage';
      // $nav[$count]['submenu'][7]['url'] = $path . $nav[$count]['segment'] . '/homepage';
      // $nav[$count]['submenu'][7]['action'] = ['access'];
      // $nav[$count]['submenu'][7]['seperator'] = 'hidden';

      $nav[$count]['submenu'][8]['label'] = 'Content Text';
      $nav[$count]['submenu'][8]['code'] = 'aboutus';
      $nav[$count]['submenu'][8]['url'] = $path . $nav[$count]['segment'] . '/about';
      $nav[$count]['submenu'][8]['action'] = ['access'];
      $nav[$count]['submenu'][8]['seperator'] = 'hidden';
      
      // $nav[$count]['submenu'][9]['label'] = 'Contact Us';
      // $nav[$count]['submenu'][9]['code'] = 'contactus';
      // $nav[$count]['submenu'][9]['url'] = $path . $nav[$count]['segment'] . '/contact';
      // $nav[$count]['submenu'][9]['action'] = ['access'];
      // $nav[$count]['submenu'][9]['seperator'] = 'hidden';
      
      // $nav[$count]['submenu'][10]['label'] = 'Terms and Agreement';
      // $nav[$count]['submenu'][10]['code'] = 'contactus';
      // $nav[$count]['submenu'][10]['url'] = $path . $nav[$count]['segment'] . '/terms';
      // $nav[$count]['submenu'][10]['action'] = ['access'];
      // $nav[$count]['submenu'][10]['seperator'] = 'hidden';
      
      // $nav[$count]['submenu'][11]['label'] = 'Front Footer';
      // $nav[$count]['submenu'][11]['code'] = 'frontfooter';
      // $nav[$count]['submenu'][11]['url'] = $path . $nav[$count]['segment'] . '/frontfooter';
      // $nav[$count]['submenu'][11]['action'] = ['access'];
      // $nav[$count]['submenu'][11]['seperator'] = 'hidden';

      $nav[$count]['submenu'][12]['label'] = 'Downloads';
      $nav[$count]['submenu'][12]['code'] = 'downloads';
      $nav[$count]['submenu'][12]['url'] = $path . $nav[$count]['segment'] . '/downloads';
      $nav[$count]['submenu'][12]['action'] = ['access'];
      $nav[$count]['submenu'][12]['seperator'] = 'hidden';

      $nav[$count]['submenu'][13]['label'] = 'Promotions';
      $nav[$count]['submenu'][13]['code'] = 'promotions';
      $nav[$count]['submenu'][13]['url'] = $path . $nav[$count]['segment'] . '/promotions';
      $nav[$count]['submenu'][13]['action'] = ['access'];
      $nav[$count]['submenu'][13]['seperator'] = 'hidden';

      // $count++;
      // $nav[$count]['name'] = 'Developer';
      // $nav[$count]['segment'] = 'developer';

      // $nav[$count]['submenu'][0]['label'] = 'Migration';
      // $nav[$count]['submenu'][0]['code'] = 'migration';
      // $nav[$count]['submenu'][0]['url'] = $path . 'migration';
      // $nav[$count]['submenu'][0]['action'] = ['access'];
      // $nav[$count]['submenu'][0]['seperator'] = 'hidden';

      // $nav[$count]['submenu'][1]['label'] = 'Disable Area';
      // $nav[$count]['submenu'][1]['code'] = 'disable';
      // $nav[$count]['submenu'][1]['url'] = $path . 'migration' . '/disable';
      // $nav[$count]['submenu'][1]['action'] = ['access'];
      // $nav[$count]['submenu'][1]['seperator'] = 'hidden';

      // $nav[$count]['submenu'][2]['label'] = 'Re Entry (CD to PS)';
      // $nav[$count]['submenu'][2]['code'] = 'reentry';
      // $nav[$count]['submenu'][2]['url'] = $path . $nav[$count]['segment'] . '/re_entry';
      // $nav[$count]['submenu'][2]['action'] = ['access'];
      // $nav[$count]['submenu'][2]['seperator'] = 'hidden';

      return $nav;
    }
   


}

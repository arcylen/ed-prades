<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_indirect_unilevel_setting extends Model
{
	protected $table = 'tbl_indirect_unilevel_setting';
	protected $primaryKey = 'set_id';
}
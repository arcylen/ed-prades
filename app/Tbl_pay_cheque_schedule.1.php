<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_pay_cheque_schedule extends Model
{
	protected $table = 'tbl_pay_cheque_schedule';
	protected $primaryKey = "pay_cheque_schedule_id";
    public $timestamps = false;
}

<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_direct_report extends Model
{
    protected $table = 'tbl_direct_report';
    protected $primaryKey = 'direct_id';
    public $timestamps = false;

    public function scopeSlot($query)
    {
        return $query->join('tbl_slot', 'tbl_slot.slot_id', '=', 'tbl_direct_report.direct_sponsor');
    }
    public function scopeSlotchild($query)
    {
        return $query->join('tbl_slot', 'tbl_slot.slot_id', '=', 'tbl_direct_report.direct_child');
    }
    public function scopeAccount($query)
    {
        return $query->leftJoin('tbl_account', 'tbl_account.account_id', '=', 'tbl_slot.slot_owner');
    }
}
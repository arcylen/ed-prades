<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_stairstep_settings extends Model
{
    protected $table = 'tbl_stairstep_settings';
    protected $primaryKey = 'stairstep_level';
    public $timestamps = false;
}
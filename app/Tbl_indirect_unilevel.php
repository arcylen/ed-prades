<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_indirect_unilevel extends Model
{
	protected $table = 'tbl_indirect_unilevel';
	protected $primaryKey = 'sequence_id';
}
<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_about extends Model
{
	protected $table = 'tbl_about';
	protected $primaryKey = "about_id";
    public $timestamps = false;
}

<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_direct_sb extends Model
{
    protected $table = 'tbl_direct_sb';
    protected $primaryKey = 'dsb_id';
    public $timestamps = false;

    public function scopeSlot($query)
    {
        return $query->join('tbl_slot', 'tbl_slot.slot_id', '=', 'tbl_direct_sb.dsb_sponsor');
    }
    public function scopeSlotchild($query)
    {
        return $query->join('tbl_slot', 'tbl_slot.slot_id', '=', 'tbl_direct_sb.dsb_child');
    }
    public function scopeAccount($query)
    {
        return $query->leftJoin('tbl_account', 'tbl_account.account_id', '=', 'tbl_slot.slot_owner');
    }
}
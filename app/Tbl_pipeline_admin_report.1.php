<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_pipeline_admin_report extends Model
{
	protected $table = 'tbl_pipeline_admin_report';
	protected $primaryKey = "pipeline_admin_report_id";
	public $timestamps = false;
}
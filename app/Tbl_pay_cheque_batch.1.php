<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_pay_cheque_batch extends Model
{
	protected $table = 'tbl_pay_cheque_batch';
	protected $primaryKey = "pay_cheque_batch_id";
	public $timestamps = false;
}
<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_indirect_report extends Model
{
    protected $table = 'tbl_indirect_report';
    protected $primaryKey = 'indirect_id';
    public $timestamps = false;

    public function scopeSlot($query)
    {
        return $query->join('tbl_slot', 'tbl_slot.slot_id', '=', 'tbl_indirect_report.indirect_slot');
    }
    public function scopeSlotregistree($query)
    {
        return $query->join('tbl_slot', 'tbl_slot.slot_id', '=', 'tbl_indirect_report.indirect_registree');
    }
    public function scopeAccount($query)
    {
        return $query->leftJoin('tbl_account', 'tbl_account.account_id', '=', 'tbl_slot.slot_owner');
    }
}
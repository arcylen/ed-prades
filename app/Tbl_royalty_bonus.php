<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_royalty_bonus extends Model
{
	protected $table = 'tbl_royalty_bonus';
	protected $primaryKey = "royalty_bonus_id";
    public $timestamps = false;

     public function scopeSlot($query)
    {
        return $query->join('tbl_slot', 'tbl_slot.slot_id', '=', 'tbl_royalty_bonus.rb_slot_id');
    }
    public function scopeAccount($query)
    {
        return $query->leftJoin('tbl_account', 'tbl_account.account_id', '=', 'tbl_slot.slot_owner');
    }
    public function scopeSlotchild($query)
    {
        return $query->join('tbl_slot', 'tbl_slot.slot_id', '=', 'tbl_royalty_bonus.rb_sponsor_id')
        			->join('tbl_royalty_setting',"tbl_royalty_setting.rb_id","=","tbl_slot.royalty_level");
    }
}

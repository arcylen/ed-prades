<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_indirect_sb extends Model
{
    protected $table = 'tbl_indirect_sb';
    protected $primaryKey = 'isb_id';
    public $timestamps = false;

    public function scopeSlot($query)
    {
        return $query->join('tbl_slot', 'tbl_slot.slot_id', '=', 'tbl_indirect_sb.isb_sponsor');
    }
    public function scopeSlotchild($query)
    {
        return $query->join('tbl_slot', 'tbl_slot.slot_id', '=', 'tbl_indirect_sb.isb_child');
    }
    public function scopeAccount($query)
    {
        return $query->leftJoin('tbl_account', 'tbl_account.account_id', '=', 'tbl_slot.slot_owner');
    }
}

<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_binary_breakdown extends Model
{
	protected $table = 'tbl_binary_breakdown';
	protected $primaryKey = "binary_breakdown_id";
	public $timestamps = false;

    public function scopeSlot($query)
    {
        return $query->leftJoin('tbl_slot', 'tbl_slot.slot_id', '=', 'tbl_binary_breakdown.binary_slot_id_cause');
    }
    public function scopeAccount($query)
    {
        return $query->leftJoin('tbl_account', 'tbl_account.account_id', '=', 'tbl_slot.slot_owner');
    }
}
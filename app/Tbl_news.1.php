<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Tbl_news extends Model
{
	protected $table = 'tbl_news';
	protected $fillable = ['tbl_news'];
}
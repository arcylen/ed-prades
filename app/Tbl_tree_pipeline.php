<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Tbl_tree_pipeline extends Model
{
	protected $table = 'tbl_tree_pipeline';
	protected $primaryKey = 'tree_id';
}

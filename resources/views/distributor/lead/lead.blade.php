@extends('distributor.layout')
@section('content')
<div class="row">
    <div class="col-md-6 col-lg-8">
        <div style="padding: 10px;" class="tab-content col-md-12 panel panel-default panel-block">
           <h3>Leads</h3>
           <div class="input-group panel-body">
              <span class="input-group-addon" id="basic-addon1">Lead Link</span>
              <input type="text" class="form-control" placeholder="" aria-describedby="" name="lead_register_link" value="http://{{$_SERVER['SERVER_NAME']}}/register_member/?lead={{$member->account_username}}">
           </div>

           <table class="table table-bordered table-hovered">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Username</th>
                    </tr>
                </thead>
                    <tbody>
 
                        @if($lead)
                            @foreach($lead as $lead_account)
                                <tr>
                                    <td>{{$lead_account->lead_account_id}}</td>
                                    <td>{{$lead_account->account_full_name}}</td>
                                    <td>{{$lead_account->account_username}}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="3">No leads</td>
                            </tr> 
                        @endif 
                    </tbody>
           </table>

          
        </div> 

    </div>

    <div class="col-md-6 col-lg-4">
        <div class="panel panel-default panel-block">
            <div class="list-group">
                <div class="list-group-item">
                    <h4 class="section-title">NEWS & ANNOUNCEMENTS</h4>
                    <div class="form-group">
                        <div id="hero-bar" class="graph" style="height: 200px; text-align: center;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
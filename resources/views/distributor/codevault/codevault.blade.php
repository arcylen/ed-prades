@extends('distributor.layout')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default panel-block panel-title-block" id="top">
            <div class="panel-heading">
                <div>
                    <i class="icon-qrcode"></i>
                    <h1>
                        <span class="page-title">Membership Codes List</span>
                        <small>
                            This are list of codes you've claimed. You can use them or you can send them to a friend's account.
                        </small>
                    </h1>
                </div>
            </div>
        </div>
        <div class="panel panel-default panel-block">
            <div class="list-group">
                <div class="list-group-item" id="responsive-bordered-table">
                    <!-- UNUSED CODE -->
                    <div class="form-group">
                        <h4 class="section-title">Available Membership Codes</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead class="">
                                    <tr>
                                        <th data-hide="phone">PIN</th>
                                        <th data-hide="phone">ACTIVATION</th>
                                        <th data-hide="phone">TYPE</th>
                                        <th data-hide="phone,phonie">MEMBERSHIP</th>
                                        <th data-hide="phone,phonie">PRODUCT/S</th> <!-- LET'S LIST THE PRODUCTS ON THIS SET -->
                                        <th data-hide="phone,phonie,tablet"></th>
                                    </tr>
                                </thead>
                                <tbody> 
                                
                                    @if($code) 
                                    @foreach($code as $c)
                                        <tr class="tibolru" loading="{{$c->code_pin}}">
                                            <td>{{$c->code_pin}}</td>
                                            <td>{{ hyphenate($c->code_activation) }}</td>
                                            <td>{{$c->code_type_name}}</td>
                                            <td>{{$c->membership_name}}</td>
                                            <td>{{$c->product_package_name}}</td>
                                            <td class="text-center">
                                                @if(isset($slotnow->slot_id))
                                                <a target="_blank" href="/distributor/register?pin={{$c->code_pin}}&activation={{ hyphenate($c->code_activation) }}&by={{ $slotnow->slot_id }}&ses=1" style="cursor: pointer;" class="createslot" value="{{$c->code_pin}}">CREATE ACCOUNT</a>
                                                @else
                                                You have no access. Please create a slot in the dashboard area.
                                                @endif
                                            </td>
                                        </tr>
                                        @endforeach  
                                    @else
                                        <tr>
                                            <td colspan="6"><center>You don't have any unused codes.</center></td>
                                        </tr> 
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>   
        </div>
    </div>
</div>

@endsection
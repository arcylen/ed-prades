@extends('distributor.layout')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default panel-block panel-title-block" id="top">
            <div class="panel-heading">
                <div>
                    <i class="icon-qrcode"></i>
                    <h1>
                        <span class="page-title">Product Codes List</span>
                        <small>
                            This are list of codes you've claimed. You can use them or you can send them to a friend's account.
                        </small>
                    </h1>
                </div>
            </div>
        </div>
        <div class="panel panel-default panel-block">
            <div class="list-group">
                <div class="list-group-item" id="responsive-bordered-table">
                    <!-- UNUSED CODE -->
                    <div class="form-group">
                        <h4 class="section-title">Available Product Codes</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Pin</th>
                                        <th data-hide="phone">Code</th>
                                        <th data-hide="phone">Product</th>
                                        <th data-hide="phone,phonie">Sales Value Points</th>
                                        <!--<th data-hide="phone,phonie">Binary Points</th>-->
                                        <th data-hide="phone,phonie,tablet"></th>
                                        <!--<th data-hide="phone,phonie,tablet"></th>-->
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($prodcount != null)
                                    @foreach($prodcode as $prod)
                                    <tr class="tibolru" loading="{{$prod->product_pin}}">
                                        <td>{{$prod->product_pin}}</td>
                                        <td>{{$prod->code_activation}}</td>
                                        <td>{{$prod->product_name}}</td>
                                        <td>{{number_format($prod->unilevel_pts, 2)}}</td>
                                        <!--<td>{{number_format($prod->binary_pts, 2)}}</td>-->
                                        <td><a style="cursor: pointer;" class="use-p" binary_pts="{{ $prod->binary_pts }}" unilevel_pts="{{ $prod->unilevel_pts }}" code_id="{{ $prod->product_pin }}">Use Code</a></td>
                                        <!--<td><a style="cursor: pointer;" class="transferer-p" vals="{{$prod->voucher_id}}" value="{{$prod->product_pin}} @ {{$prod->code_activation}}" val="{{$prod->product_pin}}">Transfer Code</a></td>-->
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="5">No available product code.</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>   
        </div>
    </div>
</div>



<!-- POPUP FOR PRODUCT CODE / USE CODE -->
<div class="remodal create-slot" data-remodal-id="use_code">
    <button data-remodal-action="close" class="remodal-close"></button>
    <div class="header">
        Use Product Code
    </div>
    <img src="/resources/assets/frontend/img/sobranglupet.png" style="max-width: 100%; margin: 20px auto">
    <div class="col-md-10 col-md-offset-1 para">
        <form class="form-horizontal" action="distributor/code_vault/use_product_code">
            <div class="form-group para">
                <label for="111" class="col-sm-3 control-label">Points Recipient</label>
                <input name="product_pin" class="product-code-id-reference" type="hidden" id="product_code_id" value="">
                <div class="col-sm-9">
                    <select class="form-control" id="111" name="slot">
                        @if($getallslot)
                            @foreach($getallslot as $get)
                                <option {{ $slotnow->slot_id == $get->slot_id ? 'selected' : '' }}  value="{{$get->slot_id}}"> Slot #{{$get->slot_id}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group para">
                <label for="222" class="col-sm-3 control-label">Sales Point Value</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control unilevel_pts_container" disabled>
                </div>
            </div>
            <div class="form-group para hide">
                <label for="333" class="col-sm-3 control-label">Binary Points</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control binary_pts_container" disabled>
                </div>
            </div>
        </div>
        <br> 
        <button class="button" type="button" data-remodal-action="cancel">Cancel</button>
        <button class="usingprodcode button" type="submit" name="usingcode">Use Code</button>
        <span class='loadingprodicon' style="margin-left: 50px;"><img class='loadingprodicon' src='/resources/assets/img/small-loading.GIF'></span>
    </form>
</div>



@endsection
@section('js')
<script type="text/javascript" src="/resources/assets/frontend/js/code_vault1.js"></script>
@endsection
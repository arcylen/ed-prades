@extends('distributor.layout')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default panel-block panel-title-block" id="top">
            <div class="panel-heading">
                <div>
                    <i class="icon-qrcode"></i>
                    <h1>
                        <span class="page-title">Product Codes List</span>
                        <small>
                            This are list of codes you've claimed. You can use them or you can send them to a friend's account.
                        </small>
                    </h1>
                </div>
            </div>
        </div>
        <div class="panel panel-default panel-block">
            <div class="list-group">
                <div class="list-group-item" id="responsive-bordered-table">
                    <!-- UNUSED CODE -->
                    <div class="form-group">
                        <h4 class="section-title">Used Product Codes</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Pin</th>
                                        <th data-hide="phone">Code</th>
                                        <th data-hide="phone">Product</th>
                                        <th data-hide="phone,phonie">Sales Value Points</th>
                                        <!--<th data-hide="phone,phonie">Binary Points</th>-->
                                        <th data-hide="phone,phonie,tablet"></th>
                                    </tr>
                                </thead>
                                <tbody> 
                                    @if($prodcount != null)
                                    @foreach($prodcode as $prod)
                                    <tr class="tibolru" loading="{{$prod->product_pin}}">
                                        <td>{{$prod->product_pin}}</td>
                                        <td>{{$prod->code_activation}}</td>
                                        <td>{{$prod->product_name}}</td>
                                        <td>{{number_format($prod->unilevel_pts, 2)}}</td>
                                        <!--<td>{{number_format($prod->binary_pts, 2)}}</td>-->
                                        
                                        <!--<td><a style="cursor: pointer;" class="transferer-p" vals="{{$prod->voucher_id}}" value="{{$prod->product_pin}} @ {{$prod->code_activation}}" val="{{$prod->product_pin}}">Transfer Code</a></td>-->
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td colspan="5">No product code is used.</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>   
        </div>
    </div>
</div>

@endsection

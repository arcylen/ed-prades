@extends('distributor.layout')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default panel-block panel-title-block" id="top">
            <div class="panel-heading">
                <div>
                    <i class="icon-qrcode"></i>
                    <h1>
                        <span class="page-title">Membership Codes List</span>
                        <small>
                            This are list of codes you've claimed. You can use them or you can send them to a friend's account.
                        </small>
                    </h1>
                </div>
            </div>
        </div>
        <div class="panel panel-default panel-block">
            <div class="list-group">
                <div class="list-group-item" id="responsive-bordered-table">
                    <!-- UNUSED CODE -->
                    <div class="form-group">
                        <h4 class="section-title">Used Codes</h4>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead class="">
                                    <tr>
                                        <th>Pin</th>
                                        <th data-hide="phone">Code</th>
                                        <th data-hide="phone">Type</th>
                                        <th data-hide="phone,phonie">Membership</th>
                                        <th data-hide="phone,phonie">Product Set</th>
                                        <th data-hide="phone,phonie">Used By</th>
                                    </tr>
                                </thead>
                                <tbody> 
                                    @if($used_code != null) 
                                    @foreach($used_code as $c)  
                                        <tr class="tibolru" loading="{{$c->code_pin}}">
                                            <td>{{$c->code_pin}}</td>
                                            <td>{{$c->code_activation}}</td>
                                            <td>{{$c->code_type_name}}</td>
                                            <td>{{$c->membership_name}}</td>
                                            <td>{{$c->product_package_name}}</td>
                                            <td>Slot #{{$c->slot_id}} ({{$c->account_name}})</td>
                                        </tr>
                                    @endforeach 
                                    @else
                                         <tr>
                                            <td colspan="6"><center>You don't have any used codes yet.</center></td>
                                        </tr>   
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>   
        </div>
    </div>
</div>

@endsection
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
@if($slot_tree)
<html>
    <head>
        <base href="<?php echo "http://" . $_SERVER["SERVER_NAME"] ?>">
        <script type="text/javascript" src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/resources/assets/members/css/member.css">
        <script type="text/javascript" src="resources/assets/genealogy/drag.js"></script>
        <link rel="stylesheet" type="text/css" href="/resources/assets/genealogy_tree/bootstrap/css/bootstrap.css">
        <script type="text/javascript" src="/resources/assets/genealogy_tree/bootstrap/js/bootstrap.js"></script>
        <link rel="stylesheet" type="text/css" href="/resources/assets/remodal/src/jquery.remodal.css">
        <link rel="stylesheet" type="text/css" href="/resources/assets/remodal/src/remodal-default-theme.css">
        <link rel="stylesheet" type="text/css" href="resources/assets/member/css/genealogy.css?version=3" />
        <title>Genealogy</title>
        <link rel="stylesheet" type="text/css" href="/resources/assets/members/css/member.css">
        <!-- Proton CSS: -->
        <link rel="stylesheet" href="/resources/assets/distributor/css/sidebar.css">
        <link rel="stylesheet" href="/resources/assets/distributor/styles/vendor/animate.css">
        <script src="/resources/assets/distributor/scripts/vendor/modernizr.js"></script>
        <link rel="stylesheet" href="/resources/assets/distributor/styles/6227bbe5.font-awesome.css" type="text/css" />
        <link rel="stylesheet" href="/resources/assets/distributor/styles/40ff7bd7.font-titillium.css" type="text/css" />
        <style type="text/css">
        .navbar-nav > li > a
        {
            color: #1767D2 !important;
            font-weight: 700;
        }
        .navbar
        {
            background-color: #F1CB40;
        }
        </style>
    </head>
    <body id="body" class="body" style="height: 100%">
    <div class="row">
        <div class="overscroll" style="width: 100%; height: inherit; overflow: auto;">
            <div class="tree-container" style="width:5000%; height: 100%;">
                <div class="tree">
                    <ul>
                        <li class="width-reference">
                            <span class="downline parent parent-reference PS" x="{{ $slot_tree->slot_id }}">   
                                <div id="info">
                                    <div id="photo">
                                        @if($member->image != "")
                                        <img src="{{$slot_tree->image}}">
                                        @else
                                        <img src="/resources/assets/img/default-image.jpg">
                                        @endif
                                    </div>
                                    <div id="cont">
                                        <div style="font-weight: 700;">{{ strtoupper($slot_tree->account_username) }}</div>
                                        <div>{{ strtoupper($slot_tree->account_name) }}</div>
                                        <b>{{ $slot_tree->membership_name }}</b>
                                    </div>
                                    <div>{{ $slot_tree->slot_type }}</div>
                                    <div>L:{{$l}} / R:{{$r}}</div>
                                    <div>Left Points:{{$slot_tree->slot_binary_left}} / Right Points:{{$slot_tree->slot_binary_right}}</div>
                                </div>
                                <div class="id">{{ $slot_tree->slot_id }}</div>
                            </span> 
                            <i class="downline-container">
                                {!! $downline !!}
                            </i>                  
                        </li>
                    </ul>       
                </div>
            </div>
        </div>
    </div>   
    </body>
    <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">


<!-- SUCCESS SLOT -->
<div class="remodal create-slot" data-remodal-id="slotcreated" data-remodal-options="hashTracking: false">
    <button data-remodal-action="close" class="remodal-close"></button>
    <div class="header">
    <img src="/resources/assets/frontend/img/icon-claim.png">
        Successfully Added
    </div>
</div>

<script type="text/javascript" src="/resources/assets/remodal/src/jquery.remodal.js"></script>
<script type="text/javascript">
    var mode = "{{ Request::input('mode') }}";
    var level = "{{ $level }}"
    var g_width = 0;
    var half;
    
    $(document).ready(function()
    {  

        $("li").show();   
        half = $(window).width() * 2500;
        g_width = $(".width-reference").width();
        $margin_left = ($(window).width() - $(".width-reference").width()) / 2;
        $(".tree-container").css("padding-left",half  + $margin_left);
        $(".overscroll").height($(document).height());
        
        
        $parent_position = $(".parent").position();
        $window_size = $(window).width();
        
        $(function(o){
            o = $(".overscroll").overscroll(
            {
                cancelOn: '.no-drag',
                scrollLeft: half,
                scrollTop: 0
            });
            $("#link").click(function()
            {
                if(!o.data("dragging"))
                {
                    console.log("clicked!");
                }
                else
                {
                    return false;
                }
            });
        }); 
        
        
    })
    
    var genealogy_loader = new genealogy_loader();
    function genealogy_loader()
    {
        init();
        function init()
        {
            $(document).ready(function()
            { 
                document_ready();
            });
        }
        function document_ready()
        {
            add_click_event_to_downlines();
        }
        function add_click_event_to_downlines()
        {      
            $(".downline").unbind("click");
            $(".downline").bind("click", function(e)
            {
                $currentScroll = $(".overscroll").scrollLeft();
                
                if($(e.currentTarget).siblings(".downline-container").find("ul").length == 0)
                {
                    $(e.currentTarget).append("<div class='loading'><img src='/resources/assets/img/485.gif'></div>");
                    $(".downline").unbind("click");
                    
                    $genealogy = $(e.currentTarget).attr("genealogy_function");
                    $networker_account_id = $(e.currentTarget).attr("x");
                    
                    $.ajax(
                    {
                        url:"/distributor/genealogy/pipeline/downline",
                        dataType:"json",
                        data:{x:$networker_account_id,complan_library:$genealogy,mode:mode,level:level},
                        type:"get",
                        success: function(data)
                        {    
                            $(".loading").remove();
                            $(e.currentTarget).siblings(".downline-container").append(data);
                            $(e.currentTarget).siblings(".downline-container").find("li").fadeIn();
                            adjust_tree();
                            add_click_event_to_downlines();
                            adjust_tree();
                            //$(e.currentTarget).parent("li").siblings("li").find("ul").remove();
                        }
                    });    
                }
                else
                {
                    $(e.currentTarget).siblings(".downline-container").find("ul").fadeOut(function()
                    {
                        this.remove();
                        adjust_tree();    
                    });
                    
                }
            });    
        }     
        function adjust_tree()
        {
                $curr_margin_left = parseFloat($(".tree-container").css("padding-left"));   
                $deduction = parseFloat(($(".width-reference").width() - g_width) / 2);
                g_width = $(".width-reference").width();
                $(".tree-container").css("padding-left", parseFloat($curr_margin_left  - $deduction));
        }
    }
</script>
</html>
@else
You have no slots available.
@endif





<script type="text/javascript">
var container = null;
var forthis = null

$('#body').on('click', '.positioning', function()
{
   forthis = $(this).closest('.downline-container').parent().children('span:first');
   console.log(forthis);
   container = $(this).closest('.downline-container').find("ul");
   var position = $(this).attr('position');
   var placement = $(this).attr('placement');
   var name = $(this).attr('y'); 
   $('#placement').val('Slot #'+placement +' ('+name+')');
   $('#placement2').val(placement);
   $('#position').val(position);
   $('#sponsor').val(placement);
   var inst = $('[data-remodal-id=newslot]').remodal();
   inst.open(); 
        $("#membership").val('Loading...');
        $("#type").val('Loading...');     
        $.ajax(
        {
            url:"member/genealogy/get",
            dataType:"json",
            data: {'code':$("#111").val(),'_token':$("#token").val()},
            type:"post",
            success: function(data)
            {
                if(data['code'] != "x")
                { 
                    $x = jQuery.parseJSON(data['code']);

                    $("#latest").val(data['latest']);
                    $("#membership").val($x.membership_name);
                    $("#type").val($x.code_type_name);     
                    $("#newslot").val(data['latest']); 
                }
            }
        }); 

    $(".sponsorname").val("Loading...");  
    $.ajax(
    {
        url:"member/code_vault/get",
        dataType:"json",
        data: {'slot':$("#sponsor").val(),'_token':$("#token").val()},
        type:"post",
        success: function(data)
        {
            if(data != "x")
            { 
              $x = jQuery.parseJSON(data);
              $(".sponsorname").val($x[1]);                            
            }
            else
            {
               $(".sponsorname").val("Sponsor's ID is not existing");  
            }
        } 
    });     
})

$("#111").bind('change',function()
{
    $("#membership").val('Loading...');
    $("#type").val('Loading...');     
    $.ajax(
    {
        url:"member/genealogy/get",
        dataType:"json",
        data: {'code':$("#111").val(),'_token':$("#token").val()},
        type:"post",
        success: function(data)
        {
            if(data['code'] != "x")
            { 
                $x = jQuery.parseJSON(data['code']);
                $("#latest").val(data['latest']);
                $("#membership").val($x.membership_name);
                $("#type").val($x.code_type_name); 
            }
        }
    }); 
});

$("#use").unbind("click");
$("#use").bind("click", function(e)
{
    $('#use').prop("disabled", true);
    $.ajax(
    {
        url:"member/genealogy/add_form_message",
        dataType:"json",
        data: $("#slotnew").serialize(),
        type:"post",
        success: function(data)
        {
            $('#use').prop("disabled", false);
            if(data.message == "")
            {
                $.ajax(
                {
                    url:"member/genealogy/add_form",
                    dataType:"json",
                    data: $("#slotnew").serialize(),
                    type:"post",
                    complete: function (asd) 
                    {
                        container.remove();
                        $("#111").find('option:selected').remove();;
                        var inst = $('[data-remodal-id=newslot]').remodal();
                        inst.close();                                      
                        var inst = $('[data-remodal-id=slotcreated]').remodal();
                        inst.open();     

                        if($('#111 option').length <= 0)
                        {
                            $(".removeifempty").remove();
                            $('.changeifempty').text('You have no available codes');
                            $(".loadingusecode").hide();
                        } 
                        else
                        {
                            $(".usingcode").show();  
                            $(".loadingusecode").hide();
                        } 
                    }
                });
            }
            else
            {
                alert(data.message);
                $(".usingcode").show();  
                $(".loadingusecode").hide();
            }
        }
    });

});
$(".loadingusecode").hide();
$(".usingcode").click(function(e)
{
    $(".usingcode").hide();  
    $(".loadingusecode").show();
});

$("#sponsor").keyup(function()
{           
    $(".sponsorname").val("Loading...");  
    $.ajax(
    {
        url:"member/code_vault/get",
        dataType:"json",
        data: {'slot':$("#sponsor").val(),'_token':$("#token").val()},
        type:"post",
        success: function(data)
        {
            if(data != "x")
            { 
              $x = jQuery.parseJSON(data);
              $(".sponsorname").val($x[1]);                            
            }
            else
            {
               $(".sponsorname").val("Sponsor's ID is not existing");  
            }
        } 
    });

}); 

$('.forslotchanging').click(function()
{
    $.ajax(
    {
        url:"member/slot/changeslot",
        dataType:"json",
        data: {'changeslot':$(this).attr('slotid'),'_token':$("#token").val()},
        type:"post",
        success: function(data)
        {
            if(mode != null)
            {
                window.location.href = window.location.pathname+"?mode="+mode;
            }
            else
            {
                window.location.href = window.location.pathname;    
            }   
        }
    });      
});


</script>

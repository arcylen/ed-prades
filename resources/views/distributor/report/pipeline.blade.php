@extends('distributor.layout')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default panel-block panel-title-block" id="top">
            <div class="panel-heading">
                <div>
                    <i class="icon-shopping-cart"></i>
                    <h1>
                        <span class="page-title">Report</span>
                        <small>
                            Binary Income Report
                        </small>
                    </h1>
                </div>
            </div>
        </div>
        <form method="POST">
        <div class="panel panel-default panel-block">
            <div class="list-group">
                <div class="list-group-item" id="responsive-bordered-table">
                    <div class="form-group">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-condensed text-center">
                                <thead class="">
                                    <tr>
                                        <th class="center">PIPELINE<br>LEVEL</th>
                                        <th class="center">REQUIREMENT TO<br>GRADUATE</th>
                                        <th class="center">CURRENT<br>DOWNLINE</th>
                                        <th class="center">AMOUNT EARNING<br>UPON GRADUATE</th>
                                        <th class="center">GRADUATE</th>
                                        <th class="center">PAY CHEQUE<br>PROCESSED</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($_pipeline_settings))
                                    @foreach($_pipeline_settings as $pipeline_settings)
                                    <tr>
                                        <td>Level {{ $pipeline_settings->pipeline_label }}</td>
                                        <td>{{ $pipeline_settings->pipeline_requirement }} Direct on Pipeline {{ $pipeline_settings->pipeline_level }}</td>
                                        <td>{{ number_format($pipeline_settings->downline_count) }}</td>
                                        <td style="color: {{ $pipeline_settings->qualified == 1 ? 'green' : '#aaa' }}" {{ $pipeline_settings->qualified == 1 ? 'checked=checked' : '' }}>{{ currency($pipeline_settings->pipeline_graduate_amount) }}</td>
                                        <td><input type="checkbox" {{ $pipeline_settings->qualified == 1 ? 'checked=checked' : '' }} disabled="disabled"></td>
                                        <td><input type="checkbox" disabled="disabled"></td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>                      
            </div>
        </div>  
        </form> 
        <div>

    </div>
    </div>
</div>
@endsection
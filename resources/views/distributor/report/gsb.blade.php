@extends('distributor.layout')
@section('content')
        <!-- <meta name="_token" id="_token" content="{{ csrf_token() }}" /> -->
<form>
    <div class="row">

        <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
        <div class="col-md-12">
            <div class="panel panel-default panel-block panel-title-block" id="top">
                <div class="panel-heading">
                    <div>
                        <i class="icon-shopping-cart"></i>
                        <h1>
                            <span class="page-title">Report</span>
                            <!-- <small>
                                Slot that are newly encoded today will not reflect on this computation.
                            </small> -->
                        </h1>
                    </div>
                </div>
            </div>
            <div class="panel panel-default panel-block">
                    <div class="list-group">
                        @if(isset($gsb_report))
                        <div class="list-group-item" id="responsive-bordered-table">
                        <div class="form-group">
                            <h4 class="section-title">
                            Group Sales Bonus <h2>{{$_position->sequence_name}}</h2>
                            </h4>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <thead class="">
                                        <tr>
                                            <th data-hide="phone">LEVEL</th>
                                            <th data-hide="phone">PERCENTAGE</th>
                                            <th data-hide="phone">NO OF SLOTS</th>
                                            <th data-hide="phone,phonie">TOTAL SPV</th>
                                            <th data-hide="phone,phonie">TOTAL EARNED</th> <!-- LET'S LIST THE PRODUCTS ON THIS SET -->
                                            <!-- <th data-hide="phone,phonie,tablet"></th> -->
                                        </tr>
                                    </thead>
                                     <tbody>
                                        @if(isset($personal_gsb))
                                        <tr>
                                            <td> {{$personal_gsb->total_gsb_level}} (Personal)</td>
                                            <td> {{$personal_gsb->personal_bonus}} %</td>
                                            <td> 1 SLOT/S</td>
                                            <td> {{number_format($personal_gsb->total_spv,2)}} SPV</td>
                                            <td> {{number_format((($personal_gsb->personal_bonus/100) * $personal_gsb->total_spv),2)}} Boss Coin</td>
                                        </tr>
                                        @endif
                                        @foreach($gsb_report as $gsb)
                                        <tr>
                                            <td>{{$gsb->total_gsb_level}}</td>
                                            <td>{{$gsb->seq_value}} %</td>
                                            <td> {{$gsb->total_no_of_slots}} SLOT/S</td> 
                                            <td><a style="cursor: pointer;" class="view-source" data-content="{{$gsb->total_gsb_id}}"> {{number_format($gsb->total_spv,2)}} SPV </a></td>
                                            <td>{{number_format((($gsb->seq_value/100) * $gsb->total_spv),2)}} Boss Coin</td>
                                        </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="4" align="right"><strong>TOTAL</strong> </td>
                                            <td >{{ number_format($total_gsb,2) }} Boss Coin</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>  
                    @endif                       
                </div>
            </div>  
        </div>
    </div>
<div class="modal fade gsb-modal" id="modal_gsb" role="dialog">
      <div class="modal-dialog text-center">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div class="clearfix text-center">
            <div class="row text-center">
                <div class="col-md-12">
                   <strong></strong>
                </div>
            </div>
            
            <div class="content">
                <div class="form-group">
                    <div class="panel panel-default panel-block">
                        <div class="list-group">
                            <div class="list-group-item" id="responsive-bordered-table">
                                <!-- UNUSED CODE -->
                                <div class="form-group">
                                    <h4 class="section-title">Group Sales Bonus History</h4>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <thead class="">
                                                <tr>
                                                    <th data-hide="phone">LEVEL</th>
                                                    <th data-hide="phone">PERCENTAGE</th>
                                                    <th data-hide="phone">EARNED FROM</th>
                                                    <th data-hide="phone,phonie">TOTAL SPV</th>
                                                    <th data-hide="phone,phonie">TOTAL EARNED</th> <!-- LET'S LIST THE PRODUCTS ON THIS SET -->
                                                    <!-- <th data-hide="phone,phonie,tablet"></th> -->
                                                </tr>
                                            </thead>
                                            <tbody class="per-lvl">
                                               
                                            </tbody>
                                            <tbody>
                                                 <tr>
                                                    <td colspan="4" align="right">TOTAL</td>
                                                    <td ><span class="total-earned"></span></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
          </div>
        </div>
    </div> 
</form>
<!--  -->

<script type="text/javascript" src="resources/assets/chosen_v1.4.2/chosen.jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
    $(".view-source").unbind("click");
    $(".view-source").bind("click", function()
        {
            var id = $(this).attr("data-content");
            
            var token = $("#_token").val();
            // console.log(token);
            $.ajax({
                url:"distributor/report/gsb/select_level",
                type: "get",
                data: {
                    "token":token,
                    "id":id
                },
               
                // dataType: 'json',
                success : function(data)
                {
                    console.log(data);
                    var json = JSON.parse(data);
                    var html;
                    var total = 0;
                    $(json).each(function (a, b){
                        var v = [];
                        var n = 0;
                        $.each(b, function (c, d){
                            v[n] = d;
                            n++;
                        })
                    
                        html += "<tr><td>"+v[1] +"</td><td>"+v[2] +" % </td><td> Slot #"+v[5] +"</td><td>"+ v[6] +" SPV </td><td>"+(v[2]/100)*v[6] +" Boss Coin</td></tr>";
                        total += (v[2]/100)*v[6];
                    });
                    // alert(html);
                    $(".per-lvl").html(html);
                    $(".total-earned").html(total + " Boss Coin");
                    $("#modal_gsb").modal();
                },
                error : function(err) {
                    refreshtoken();
               }
            });
        });
});
function refreshtoken(){
    $.get('refresh-csrf').done(function(data){
       $("#_token").val(data);
       // _token = data;
   });
}
</script>
@endsection
@extends('distributor.layout')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default panel-block panel-title-block" id="top">
            <div class="panel-heading">
                <div>
                    <i class="icon-shopping-cart"></i>
                    <h1>
                        <span class="page-title">Report</span>
                        <small>
                            Binary Income Report
                        </small>
                    </h1>
                </div>
            </div>
        </div>
        <form method="POST">
        <div class="panel panel-default panel-block">
            <div class="list-group">
                @if(isset($_binary))
                <div class="list-group-item" id="responsive-bordered-table">
                    <div class="form-group">
                        <h4 class="section-title">
                        Binary Income Report
                        </h4>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-condensed">
                                <thead class="">
                                    <tr>
                                        <th class="center">DATE</th>
                                        <th class="center">START<br>LEFT</th>
                                        <th class="center">START<br>RIGHT</th>
                                        <th class="center">TODAY<br>LEFT</th>
                                        <th class="center">TODAY<br>RIGHT</th>
                                        <th class="center">END<br>LEFT</th>
                                        <th class="center">END<br>RIGHT</th>
                                        <th class="center">FLUSHOUT</th>
                                        <th class="center">EARNED</th>
                                        <th class="center">PAY CHEQUE<br>PROCESSED</th>
                                        <th class="center">DETAILS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if($_binary->isEmpty())
                                        <tr>
                                            <td colspan="11"></td>
                                        </tr>
                                    @else
                                        @foreach($_binary as $binary)
                                        <tr>
                                            <td class="center">{{ date("F d, Y", strtotime($binary->binary_date)) }}</td>
                                            <td class="center">{{ $binary->binary_start_left }}</td>
                                            <td class="center">{{ $binary->binary_start_right }}</td>
                                            <td class="center">{{ $binary->binary_today_left }}</td>
                                            <td class="center">{{ $binary->binary_today_right }}</td>
                                            <td class="center">{{ $binary->binary_end_left }}</td>
                                            <td class="center">{{ $binary->binary_end_right }}</td>
                                            <td class="center">{{ currency($binary->binary_flushout) }}</td>
                                            <td class="center" style="color: {{ ($binary->binary_earned > 0 ? 'green' : 'black') }}">{{ currency($binary->binary_earned) }}</td>
                                            <td class="center"><input type="checkbox" {{ $binary->binary_payout_paycheck == 0 ? "" : "checked=checked" }} name="" disabled="disabled"></td>
                                            <td class="center"><a href="javascript:" onClick="details({{$binary->binary_id}})">DETAILS</a></td>
                                        </tr>
                                        @endforeach    
                                    @endif 
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>  
                @endif                       
            </div>
        </div>  
        </form> 
        <div>

    </div>
    </div>
</div>

<div id="details_modal" class="modal fade details-modal" role="dialog">
    <div class="modal-dialog" style="width: 60%; max-height:80%; overflow-y: auto;">
        <!-- Modal content-->
        <div class="modal-content">
        </div>
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
    function details(id)
    {
        $("#details_modal").modal();
        $.ajax(
        {
          url: "/distributor/report/binary/details",
          type: "get",
          data: {id:id},
          success: function(data)
          {
            $(".modal-content").html(data);
            $(".modal-content").show();
          }
        })
        // alert(id);
    }
</script>
@endsection
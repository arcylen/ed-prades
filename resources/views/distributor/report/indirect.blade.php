@extends('distributor.layout')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default panel-block panel-title-block" id="top">
            <div class="panel-heading">
                <div>
                    <i class="icon-shopping-cart"></i>
                    <h1>
                        <span class="page-title">Report</span>
                        <small>
                            Slot that are newly encoded today will not reflect on this computation.
                        </small>
                    </h1>
                </div>
            </div>
        </div>
        <div class="panel panel-default panel-block">
            <div class="list-group">
                @if(isset($_indirect))
                <div class="list-group-item" id="responsive-bordered-table">
                    <div class="form-group">
                        <h4 class="section-title">
                        Indirect Referral Income Report
                        </h4>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-condensed">
                                <thead class="">
                                    <tr>
                                        <th class="center"></th>
                                        @foreach($_membership as $membership)
                                        <th colspan="4" class="center">{{ strtoupper($membership["membership_name"]) }}</th>
                                        @endforeach  
                                    </tr>
                                    <tr>
                                        <th class="center">GENERATION</th>
                                        @foreach($_membership as $membership)
                                        <th class="center">AMOUNT</th>
                                        <th class="center">SLOT ON LEVEL</th>
                                        <th class="center">TOTAL INCOME</th>
                                        <th class="center">PROCESSED PAY CHEQUE</th>
                                        @endforeach  
                                    </tr>
                                </thead>
                                <tbody>
                                    @for($level = 2; $level <= $_level; $level++)
                                    <tr>
                                        <td class="center">GENERATION {{ $level }}</td>
                                        @foreach($_membership as $membership)
                                            @if($_indirect[$membership["membership_id"]][$level])
                                                <td class="center">{{ currency($_indirect[$membership["membership_id"]][$level]["income"]) }}</td>
                                                <td class="center"><a href="javascript:">{{ number_format($_indirect[$membership["membership_id"]][$level]["count"]) }} SLOT/S</a></td>
                                                <td class="center" style="color: green;">{{ currency($_indirect[$membership["membership_id"]][$level]["total"]) }}</td>
                                                <td class="center">{{ currency($_indirect[$membership["membership_id"]][$level]["total_processed"]) }}</td>
                                            @else
                                                <td class="center">-</td>
                                                <td class="center">-</td>
                                                <td class="center">-</td>
                                                <td class="center">-</td>
                                            @endif
                                        @endforeach 
                                    </tr>
                                    @endfor    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>  
                @endif                       
            </div>
        </div>  
    </div>
</div>
@endsection
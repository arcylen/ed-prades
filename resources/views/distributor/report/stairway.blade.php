@extends('distributor.layout')
@section('content')
@if('settings')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default panel-block panel-title-block" id="top">
            <div class="panel-heading">
                <div>
                    <i class="icon-shopping-cart"></i>
                    <h1>
                        <span class="page-title">Report</span>
                        <small>
                            Stairway Report
                        </small>
                    </h1>
                </div>
            </div>
        </div>
        <div class="panel panel-default panel-block panel-title-block" id="top">
            <div class="panel-heading">
                <div>
                    <h1>
                        <span class="page-title">{{$tbl_stairstep['stairstep_name']}}</span>
                        <small>
                            (Current Rank)
                        </small>
                    </h1>
                    <?php for($i = 1; $i <= $tbl_stairstep['stairstep_level']; $i++ ) { ?>
                    <i class="icon-star"></i> <?php } ?>
                </div>
            </div>
        </div>
        <form method="POST">
        <div class="panel panel-default panel-block">
            <div class="list-group">
                <div class="list-group-item" id="responsive-bordered-table">
                    <div class="form-group">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-condensed text-center">
                                <thead class="">
                                    <tr>
                                        <th class="center">STAIRSTEP <br> LEVEL</th>
                                        <th class="center">GROUP PV<br>REQUIREMENTS <br> PER LEG</th>
                                        <th class="center">PERSONAL PV<br>REQUIREMENTS</th>
                                        <th class="center">RANK UP <br>BONUS</th>
                                        <th class="center">COMPLETED</th>
                                        <th class="center">PAY CHEQUE<br>PROCESSED</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($settings))
                                    @foreach($settings as $set)
                                    <tr>
                                        <td>{{ $set['stairstep_name'] }}</td>
                                        <td>{{ $set['stairstep_special'] }} / {{ $set['stairstep_special_qty'] }}</td>
                                        <td>{{ $set['stairstep_required_pv'] }}</td>
                                        <td>{{ $set['stairstep_bonus'] }} </td>
                                        <td><input type="checkbox" @if($tbl_stairstep['stairstep_level'] >= $set['stairstep_level']) checked @endif disabled="disabled"></td>
                                        <td><input type="checkbox" disabled="disabled"></td>
                                    </tr>
                                    @endforeach
                                    @endif
                                </tbody>
                            </table>
                            
                        </div>
                    </div>
                </div>                      
            </div>
        </div>  
        </form> 
        <div>

    </div>
    </div>
</div>
@endif
@endsection
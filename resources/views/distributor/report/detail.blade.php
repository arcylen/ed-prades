<div class="modal-body">
  <div class="details-header clearfix">
    <div class="pull-left">
      <!-- <img class="cart-icon" src="/assets/front/img/cart-but.png"> -->
      <div class="text">Breakdown Details</div>
    </div>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
  </div>
  <div class="table-holder">
  @if($_detail != null)
    <table class="table">
      <thead>
        <tr>
          <th>EARNED LEFT</th>
          <th>EARNED RIGHT</th>
          <th>EARN REASON</th>
          <th>BREAKDOWN TIME</th>
        </tr>
      </thead>
      <tbody>
        @foreach($_detail as $detail)
        <tr class="stripe">
          <td>{{$detail->binary_earned_left}}</td>
          <td>{{$detail->binary_earned_right}}</td>
          <td>{{$detail->binary_earned_reason}}</td>
          <td>{{$detail->breakdown_time}}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
    @else
      <h3>No data Available.</h3>
    @endif
  </div>
</div>
@extends('distributor.layout')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default panel-block panel-title-block" id="top">
            <div class="panel-heading">
                <div>
                    <i class="icon-shopping-cart"></i>
                    <h1>
                        <span class="page-title">Unilevel Report</span>
                        <small>
                           <ul class="nav nav-tabs">
                          <li class="active"><a data-toggle="tab" href="#current">Current</a></li>
                          <li><a data-toggle="tab" href="#old">Past</a></li>
                        </ul>
                        </small>
                    </h1>
                </div>
            </div>
        </div>
        <div class="tab-content">
        <div id="current" class="tab-pane fade in active">
            <div class="panel panel-default panel-block">
                <h4 class="well">Summary</h4>
                <li class="list-group-item form-horizontal">  
                    <div class="form-group">
                        <label for="first-name" class="col-md-2 control-label">Personal PV</label>
                        <div class="col-md-10">
                            <input id="first-name" class="form-control" value="{{ number_format($slotnow->slot_personal_points, 2) }} PPV" disabled="disabled">
                        </div>
                    </div> 
                    <div class="form-group">
                        <label for="first-name" class="col-md-2 control-label">Group PV</label>
                        <div class="col-md-10">
                            <input id="first-name" class="form-control" value="{{ number_format($slotnow->slot_group_points, 2) }} GPV" disabled="disabled">
                        </div>
                    </div> 
                    <div class="form-group">
                        <label for="first-name" class="col-md-2 control-label">Required Personal PV</label>
                        <div class="col-md-10">
                            <input id="first-name" class="form-control" value="{{ number_format($slotnow->membership_required_pv, 2) }} PPV" disabled="disabled">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="first-name" class="col-md-2 control-label">Unilevel Status</label>
                        <div class="col-md-10">
                            <input id="first-name" class="form-control" value="{{ $slotnow->slot_personal_points >= $slotnow->membership_required_pv ? 'Qualified for Unilevel' : 'Not Yet Qualified' }}" disabled="disabled">
                        </div>
                    </div>
                </li>
                
            </div>  
             <div class="panel panel-default panel-block">
                <h4 class="well">Personal PV</h4>
                <table class="table">
                    <thead>
                        <tr>
                            <td>Points</td>
                            <td>Sponsor Slot#</td>
                            <td>Product Pin</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $count = 0; ?>
                        @foreach($unilevel_log_personal as $key => $value)
                        <tr>
                            <td>{{$value->unilevel_points}}</td>
                            <td>{{$value->unilevel_recipeint}}</td>
                            <td>{{$value->unilevel_product_pin}}</td>
                        </tr>
                        <?php $count++; ?>
                        @endforeach
                        @if($count == 0 )
                            <tr><td colspan="3"><center>No Available Personal PV.</center></td></tr>
                        @endif
                    </tbody>
                </table>
            </div>
            <div class="panel panel-default panel-block">
                <h4 class="well">Group PV</h4>
                <div class="tableuni">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <td>Level</td>
                                <td>Percentage</td>
                                <td>Points Earned</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($unilevelsettings as $key => $value)
                            <tr>
                                <td>{{$value->level}}</td>
                                <td>{{$value->value}} %</td>
                                <td>{{$unilevel_log[$key]}}</td>
                                <td><label class="btn btn-primary" id="view" onClick="view({{$value->level}})">View</label></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
         
        </div>
        <div id="old" class="tab-pane fade">
             <div class="panel panel-default panel-block">
                <h4 class="well">Personal PV</h4>
                <table class="table">
                    <thead>
                        <tr>
                            <td>Points</td>
                            <td>Sponsor Slot#</td>
                            <td>Product Pin</td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $count = 0; ?>
                        @foreach($unilevel_log_personal_old as $key => $value)
                        <tr>
                            <td>{{$value->unilevel_points}}</td>
                            <td>{{$value->unilevel_recipeint}}</td>
                            <td>{{$value->unilevel_product_pin}}</td>
                        </tr>
                        <?php $count++; ?>
                        @endforeach
                        @if($count == 0 )
                            <tr><td colspan="3"><center>No Available Personal PV.</center></td></tr>
                        @endif
                    </tbody>
                </table>
                <center>{!! $unilevel_log_personal_old->render() !!}</center>
            </div>
            <div class="panel panel-default panel-block">
                <h4 class="well">Group PV</h4>
                <div class="tableuni">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <td>Level</td>
                                <td>Percentage</td>
                                <td>Points Earned</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($unilevelsettings as $key => $value)
                            <tr>
                                <td>{{$value->level}}</td>
                                <td>{{$value->value}} %</td>
                                <td>{{$unilevel_log_old[$key]}}</td>
                                <td><label class="btn btn-primary" id="view" onClick="viewold({{$value->level}})">View</label></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>               
        </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="headerdata"></h4>
      </div>
      <div class="modal-body">
          <table class="table">
              <thead>
                  <tr>
                      <td>ID</td>
                      <td>Points Earned</td>
                      <td>Sponsor Slot #</td>
                      <td>Product Pin #</td>
                      <td>Product Name</td>
                  </tr>
                  
              </thead>
              <tbody id="datahtml">
                  
              </tbody>
          </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection
@section('js')
<script type="text/javascript">
    function view(id){
        $('#headerdata'). html = id;
        $('#datahtml').html('');
        $('#myModal').modal('toggle');
        var htmldata = null;
        $.getJSON('/distributor/report/unilevel/' + id ,    function (data) {

               $.each(data, function (f, name3) {
                   $.each(name3, function (g, name4) {
                       var v = [];
                       var n = 0;
                        $.each(name4, function (h, name5) {
                            v[n] = name5;
                            n++;
                        });
                        console.log(v[0]);
                        htmldata += "<tr><td>"+v[0] +"</td><td>"+v[2]+"</td><td>"+v[3]+"</td><td>"+v[8]+"</td><td>"+v[29]+"</td></tr>";
                        
                    });
               });
               $('#datahtml').html('');
               if(htmldata == ""){
                   htmldata = "<tr><td colspan='4'>No Record.</td></tr>"
               }
               $('#datahtml').html(htmldata);
        });
    }
    function viewold(id){
        $('#headerdata'). html = id;
        $('#datahtml').html('');
        $('#myModal').modal('toggle');
        var htmldata = null;
        $.getJSON('/distributor/report/unilevel_old/' + id ,    function (data) {

               $.each(data, function (f, name3) {
                   $.each(name3, function (g, name4) {
                       var v = [];
                       var n = 0;
                        $.each(name4, function (h, name5) {
                            v[n] = name5;
                            n++;
                        });
                        console.log(v[0]);
                        htmldata += "<tr><td>"+v[0] +"</td><td>"+v[2]+"</td><td>"+v[3]+"</td><td>"+v[8]+"</td><td>"+v[29]+"</td></tr>";
                        
                    });
               });
               $('#datahtml').html('');
               if(htmldata == ""){
                   htmldata = "<tr><td colspan='4'>No Record.</td></tr>"
               }
               $('#datahtml').html(htmldata);
        });
    }
</script>
@endsection
@extends('distributor.layout')
@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-default panel-block panel-title-block" id="top">
			<div class="panel-heading">
				<div>
					<i class="icon-shopping-cart"></i>
					<h1>
					<span class="page-title">Report &raquo; Binary Voucher</span>
					<small>
					Pay Cheque
					</small>
					</h1>
				</div>

				<button style="float: right;" class="btn btn-success" onclick="location.href='/distributor/cheque'" type="button">
					<i class="icon icon-double-angle-left"></i>
					 Back
				</button>
			</div>
		</div>
		<form method="POST">
			<div class="panel panel-default panel-block">
				<div class="list-group">
					<div class="list-group-item" id="responsive-bordered-table">
						<div class="form-group">
							<div class="table-responsive">
								<table class="table table-bordered table-striped table-condensed text-center">
									<tbody>
		                            @foreach($_binary_report as $binary_report)

		                                @foreach($binary_report["breakdown"] as $breakdown)
		                                    <tr >
		                                        <td>{{ date("h:i A", strtotime($breakdown["breakdown_time"])) }}</td>
		                                        <td colspan="2">{{ $breakdown["binary_earned_reason"] }}</td>
		                                        <td>{{ $breakdown["binary_earned_left"] }}</td>
		                                        <td>{{ $breakdown["binary_earned_right"] }}</td>
		                                        <td colspan="2">{{ $breakdown["account_name"] }}</td>
		                                        <td>SLOT NO. {{ $breakdown["binary_slot_id_cause"] }}</td>
		                                        <td style="text-align: center; color: green;"></td>
		                                    </tr>
		                                @endforeach

		                                <tr style="font-weight: bold;">
		                                    <td>{{ date("F d, Y", strtotime($binary_report["binary_date"])) }}</td>
		                                    <td>START LEFT <br>{{ $binary_report["binary_start_left"] }}</td>
		                                    <td>START RIGHT <br>{{ $binary_report["binary_start_right"] }}</td>
		                                    <td><b>- {{ $binary_report["binary_today_left"] }} -</b></td>
		                                    <td><b>- {{ $binary_report["binary_today_right"] }} -</b></td>
		                                    <td>END LEFT <br>{{ $binary_report["binary_end_left"] }}</td>
		                                    <td>END RIGHT <br>{{ $binary_report["binary_end_right"] }}</td>
		                                    <td>FLUSHOUT <br> <span style="color: red;">{{ currency($binary_report["binary_flushout"]) }}</span></td>
		                                    <td style="text-align: center; color: green;">{{ currency($binary_report["binary_earned"]) }}</td>
		                                </tr>
		                            @endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection
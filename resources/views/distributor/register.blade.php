@extends('distributor.layout')
@section('content')
        <div class="container">
            <div class="row marketing" style="margin-top: 40px;">
                <div class="col-ms-12">
                    <div class="alert alert-success message-holder">
                        <strong>Create Account!</strong> Please fill up the following fields carefully in order to create your accounts.
                    </div>
                </div>
                <form method="post">
                    <input type="hidden" value="{{ csrf_token() }}" name="_token">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <input type="hidden" name="sessioncheck" id="sessioncheck" value="{{ $ses }}">
                                <input type="hidden" name="" value="{{$rid=old('registertype')}}">
                                @if($rid == null)
                                <input type="hidden" name="registerid" id="registerid" value="1">
                                @else
                                <input type="hidden" name="registerid" id="registerid" value="{{old('registertype')}}">
                                @endif
                                <label>Do you have existing account?</label>
                                <select class="form-control" id="registertype" name="registertype">
                                    <option value="1" <?php if($rid==null){ echo 'selected';} ?>>I will create a new Account</option>
                                    <option value="2" <?php if($rid=='2'){ echo 'selected';} ?>>I already have an existing Account</option>
                                </select>
                                <small class="form-text text-muted">Choose method of account creation</small>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Pin Code Number</label>
                                <input name="pin" id="pin" type="text" class="form-control" value="{{ $pin }}{{ old('pin') }}">
                                <small class="form-text text-muted">Enter Pin Code Number</small>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Activation Code</label>
                                <input name="activation" type="text" class="form-control" value="{{ $activation }}{{ old('activation') }}">
                                <small class="form-text text-muted">Enter the Activation Code</small>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Sponsor ID (Slot)</label>
                                <input name="by" type="text" class="form-control" placeholder="" value="{{ $by }}{{ old('by') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Binary Placement</label>
                                <input type="text" class="form-control" placeholder="" name="binary-placement" value="{{ old('binary-placement') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Binary Position</label>
                                <select class="form-control" name="binary-position" value="{{ old('binary-position') }}">
                                    <option value="left">LEFT</option>
                                    <option value="right">RIGHT</option>
                                </select>
                            </div>
                        </div>
                        <div class="personal-info">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Full name</label>
                                <input type="text" class="form-control" placeholder="" name="fullname" required value="{{ old('fullname') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>E-Mail</label>
                                <input type="text" class="form-control" placeholder="" name="email" required value="{{ old('email') }}">
                            </div>
                        </div>  
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Contact Number</label>
                                <input type="text" class="form-control" placeholder="" name="contactnumber" value="{{ old('contactnumber') }}"> 
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Gender</label>
                                <select class="form-control" name="gender">
                                    <option value="1" selected>Male</option>
                                    <option value="2">Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Birthday</label>
                                <input type="text" class="form-control" placeholder="" id="datepicker" name="datepicker" value="{{ old('datepicker') }}">
                            </div>
                        </div>  
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Country</label>
                                <select class="form-control" name="country">
                                    @foreach($_country as $country)
                                    <option value="{{ $country->country_id }}" selected>{{ $country->country_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" class="form-control" placeholder="" name="username" id="username" required value="{{ old('username') }}">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Password</label>
                                <input type="password" class="form-control" placeholder="" name="np" required>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Confirm Password</label>
                                <input type="password" class="form-control" placeholder="" name="cp" required>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div class="col-md-12 text-right">
                        <button type="submit" class="btn btn-primary">Create Account</button>
                    </div>
                </form>
            </div>
        </div>
@endsection
@section('js')
        <script type="text/javascript" src="/resources/assets/external/jquery.min.js"></script>
        <script src="/resources/assets/distributor/scripts/e1d08589.bootstrap.min.js"></script>
        <script type="text/javascript" src="/resources/assets/distributor/register/register.js"></script>
<script type="text/javascript">
    $(document).ready(function()
    {
        var registertype = $("#registerid").val();
        var username = $("#username").val();

        if(registertype == 1)
        {
            var info = '<div class="col-md-4"> <div class="form-group"> <label>Full name</label> <input type="text" class="form-control" placeholder="" name="fullname"> </div> </div> <div class="col-md-4"> <div class="form-group"> <label>E-Mail</label> <input type="text" class="form-control" placeholder="" name="email"> </div> </div> <div class="col-md-4"> <div class="form-group"> <label>Contact Number</label> <input type="text" class="form-control" placeholder="" name="contactnumber"> </div> </div><div class="col-md-4"><div class="form-group"><label>Gender</label><select class="form-control" name="gender"><option value="1">Male</option><option value="2">Female</option></select></div></div> <div class="col-md-4"> <div class="form-group"> <label>Birthday</label> <input type="text" class="form-control" placeholder="" name="datepicker" id="datepicker"> </div> </div> <div class="col-md-4"> <div class="form-group"> <label>Country</label> <select class="form-control" name="country"> @foreach($_country as $country) <option value="{{ $country->country_id }}">{{ $country->country_name }}</option> @endforeach </select> </div> </div> <div class="col-md-4"> <div class="form-group"> <label>Username</label> <input type="text" class="form-control" placeholder="" name="username"> </div> </div> <div class="col-md-4"> <div class="form-group"> <label>Password</label> <input type="password" class="form-control" placeholder="" name="np"> </div> </div> <div class="col-md-4"> <div class="form-group"> <label>Confirm Password</label> <input type="password" class="form-control" placeholder="" name="cp"> </div> </div>'
        }
        if(registertype == 2)
        {
            var info = '<div class="col-md-4"> <div class="form-group"> <label>Username</label> <input type="text" class="form-control" placeholder="" name="username" value="'+username+'"> </div> </div> <div class="col-md-4"> <div class="form-group"> <label>Password</label> <input type="password" class="form-control" placeholder="" name="np"> </div> </div>'
        }
        $(".personal-info").html("");
        $(".personal-info").html(info);
    });
    $("#registertype").change(function()
    {
        var registertype = $("#registertype").val();

        if(registertype == 1)
        {
            var info = '<div class="col-md-4"> <div class="form-group"> <label>Full name</label> <input type="text" class="form-control" placeholder="" name="fullname"> </div> </div> <div class="col-md-4"> <div class="form-group"> <label>E-Mail</label> <input type="text" class="form-control" placeholder="" name="email"> </div> </div> <div class="col-md-4"> <div class="form-group"> <label>Contact Number</label> <input type="text" class="form-control" placeholder="" name="contactnumber"> </div> </div><div class="col-md-4"><div class="form-group"><label>Gender</label><select class="form-control" name="gender"><option value="1">Male</option><option value="2">Female</option></select></div></div> <div class="col-md-4"> <div class="form-group"> <label>Birthday</label> <input type="text" class="form-control" placeholder="" name="datepicker" id="datepicker"> </div> </div> <div class="col-md-4"> <div class="form-group"> <label>Country</label> <select class="form-control" name="country"> @foreach($_country as $country) <option value="{{ $country->country_id }}">{{ $country->country_name }}</option> @endforeach </select> </div> </div> <div class="col-md-4"> <div class="form-group"> <label>Username</label> <input type="text" class="form-control" placeholder="" name="username"> </div> </div> <div class="col-md-4"> <div class="form-group"> <label>Password</label> <input type="password" class="form-control" placeholder="" name="np"> </div> </div> <div class="col-md-4"> <div class="form-group"> <label>Confirm Password</label> <input type="password" class="form-control" placeholder="" name="cp"> </div> </div>'
        }
        if(registertype == 2)
        {
            var info = '<div class="col-md-4"> <div class="form-group"> <label>Username</label> <input type="text" class="form-control" placeholder="" name="username"> </div> </div> <div class="col-md-4"> <div class="form-group"> <label>Password</label> <input type="password" class="form-control" placeholder="" name="np"> </div> </div>'
        }
        $(".personal-info").html("");
        $(".personal-info").html(info);
    });
</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
<script>
$( function() {
$( "#datepicker" ).datepicker();
} );
</script>
<!-- ADDITIONALS -->
<?php echo (isset($script) ? $script : ""); ?>
<?php if(Session::get("notification")): ?>
<?php $notification = Session::get("notification"); ?>
<?php if($notification["title"] == "Error!"):?>
<script type="text/javascript">
    $(document).ready(function()
    {
        $(".message-holder").removeClass("alert-success");
        $(".message-holder").addClass("alert-danger");
        $(".message-holder").html("");
        $(".message-holder").html('<?php echo $notification["title"]; ?>&nbsp; <?php echo $notification["message"]; ?>');
    });
</script>
<?php elseif($notification["title"] == "Success!"):?>
<script type="text/javascript">
    $(document).ready(function()
    {
        $(".message-holder").html("");
        $(".message-holder").html('<?php echo $notification["title"]; ?>&nbsp; <?php echo $notification["message"]; ?>');
    });
</script>
<?php endif; ?>
<?php endif; ?>
<link media="screen" href="/resources/assets/distributor/styles/vendor/jquery.pnotify.default.css" rel="stylesheet">
@endsection
@extends('distributor.layout')
@section('content')

<input type="hidden" class="tokens" value="{{Session::get('currentslot')}}">

@if(isset($slotid))
<div class="row">

    <div class="col-md-12">
        <div class="panel panel-default panel-block panel-title-block" id="top">
            <div class="panel-heading">
                <div>
                    <i class="icon-shopping-cart"></i>
                    <h1>
                        <span class="page-title">Products</span>
                        <small>
                            Buy products for unilevel repurchase
                        </small>
                    </h1>
                </div>
            </div>
            <div class="cart well">
                <div class="cart-head btn btn-primary col-md-12" data-toggle="collapse" data-target="#demo"><img src="/resources/assets/frontend/img/icon-cart.png"> Show Cart</div>
                <div id="demo" class="collapse">
                <table class="table">
                    <thead>
                        <tr>
                            <th colspan="6"><center><img class="cart_preloader" src="/resources/assets/img/preloader_cart.gif" style="width: 60px; height: 64px;"></center></th>
                        </tr>
                        <tr>
                            <th>Id</th>
                            <th>Product</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Total</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="cart_container ">
                    </tbody>
                    <tr><td colspan="5"></td><td> <button type="button" id="checkout-cart" class="btn btn-success col-md-12">
                                Checkout Now!
                            </button></td></tr>
                </table>
                </div>
                <br/><br/>
            </div>

        </div>
        
        <form method="POST">
        <div class="panel panel-default panel-block">

            <div class="list-group">

                @if($_product)
                <div class="list-group-item" id="responsive-bordered-table">
                    <div class="form-group">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped">
                                <thead class="">
                                    <tr>
                                        <th>PRODUCT NAME</th>
                                        <th class="center">IMAGE</th>
                                        <th class="right">ORIGINAL PRICE</th>
                                        <th class="right">DISCOUNT ({{ strtoupper($membership->membership_name) }})</th>
                                        <th class="right">PRICE</th>
                                        <th class="right">UNILEVEL PTS</th>
                                        <th class="right">BINARY PTS</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($_product as $product)
                                    <tr>
                                        <td>{{ $product["product_name"] }}</td>
                                        <td class="center"><a target="_blank" href="product/view/{{ $product->product_id }}">[IMAGE]</a></td>
                                        <td class="right">{{currency($product->price)}}</td>
                                        <td class="right">{{number_format($product->discount, 2)}} %</td>
                                        <td class="right">{{currency($product->discounted_price)}}</td>
                                        <td class="right">{{$product->unilevel_pts}} PV</td>
                                        <td class="right">{{$product->binary_pts}} POINTS</td>
                                        <td class="btn btn-secondary"><a href="#" class="add-to-cart" product-id="{{$product->product_id}}"><i class="icon-shopping-cart"></i></a></td> 
                                    </tr> 
                                    @endforeach 
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>  
                @endif                       
            </div>
        </div>  
        </form> 
        <div>

    </div>
    </div>
    
</div>
@else
<div class="col-md-12 well"> <center><h1>Access denied.</h1><br />
    <h4>You can't access this without a slot.</center></h4>
</div>
@endif
<div class="remodal create-slot" data-remodal-id="check-out-modal" data-remodal-options="hashTracking: false, closeOnOutsideClick: false">
    <img class="checkout_preloader" src="/resources/assets/img/preloader_cart.gif" style="width: 60px; height: 64px;">
    <div id="checkout-form-container">
    </div>
</div>
@endsection
@section('js')
<script type="text/javascript">
$(function () {
         var $win = $(window);

         $win.scroll(function () {
             if ($win.scrollTop() == 0)
                 $(".cart").removeClass("asdasd");
             else if ($win.scrollTop() != 0)
                 $(".cart").addClass("asdasd");
         });
     });
$(document).ready(function()
{   

    
    add_to_cart_init();
    load_cart();
    remove_to_cart_init();
    show_checkout();

    // var options = {...};

    // var inst = $('[data-remodal-id=modal]').remodal();

    // inst.open();

    function show_checkout()
    {

        var $checkout_remodal = $('[data-remodal-id=check-out-modal]').remodal();
        $('#checkout-cart').on('click', function(event)
        {   
            // alert(123);
            event.preventDefault();
            // var options = {"closeOnConfirm":false,"closeOnEscape":false,"closeOnOutsideClick":false};
            // cart/checkout
            $checkout_remodal.open();
        });



        $(document).on('closing', $checkout_remodal, function ()
        {

            load_cart();
            $('#checkout-form-container').empty();
        });

        $(document).on('opened', $checkout_remodal, function ()
        {


            var $current_slot = $(".tokens").val();
            console.log('current_slot : ' + $current_slot);
            $('.checkout_preloader').fadeIn();
            $('#checkout-form-container').empty("");
            $.ajax({
                url: 'cart/checkout',
                type: 'GET',
                dataType: 'html',
                data: {slot_id: $current_slot},
            })


            .done(function($data) {
                setTimeout(function(){
                $('.checkout_preloader').fadeOut();
                $('#checkout-form-container').append($data);
                console.log("GET");
                }, 100);
            })
            .fail(function() {
                // console.log("error");
                alert("Error on showing checkout form.");
                $checkout_remodal.close();
            })
            .always(function() {
                console.log("complete");
            });
        });



            $('#checkout-form-container').on('click', '#submit-checkout', function(event)
            {
                event.preventDefault();

                var $current_slot = $(".tokens").val();

                console.log('current_slot : ' + $current_slot);
                $('.checkout_preloader').fadeIn();
                $('#checkout-form-container').empty("");
                var _token = '{{ csrf_token() }}';
                $.ajax({
                    url: 'cart/checkout',
                    type: 'POST',
                    dataType: 'html',
                    data: { _token: _token, slot_id: $current_slot},
                })


                .done(function($data) {
                    setTimeout(function(){
                    $('.checkout_preloader').fadeOut();
                    $('#checkout-form-container').append($data);
                    console.log("post");
                    // console.log($data);
                    }, 100);
                })
                .fail(function() {
                    // console.log("error");
                    alert("Error on showing checkout form.");
                    $checkout_remodal.close();
                })
                .always(function() {
                    // console.log("complete");
                });

            });

            $('#checkout-form-container').on('click', '#submit-checkout-gc', function(event)
            {
                event.preventDefault();

                var $current_slot = $(".tokens").val();

                console.log('current_slot : ' + $current_slot);
                $('.checkout_preloader').fadeIn();
                $('#checkout-form-container').empty("");
                var _token = '{{ csrf_token() }}';
                $.ajax({
                    url: 'cart/checkout',
                    type: 'POST',
                    dataType: 'html',
                    data: { _token: _token, slot_id: $current_slot,gc:"true"},
                })


                .done(function($data) {
                    setTimeout(function(){
                    $('.checkout_preloader').fadeOut();
                    $('#checkout-form-container').append($data);
                    console.log("post");
                    // console.log($data);
                    }, 100);
                })
                .fail(function() {
                    // console.log("error");
                    alert("Error on showing checkout form.");
                    $checkout_remodal.close();
                })
                .always(function() {
                    // console.log("complete");
                });

            });


            $('#checkout-form-container').on('click', '#cancel-checkout', function(event) {
                event.preventDefault();
                $checkout_remodal.close();
            });



            $('#checkout-form-container').on('click', '#back-to-product', function(event) {
                event.preventDefault();
                $checkout_remodal.close();
                
            }); 
    }



    function remove_to_cart_init()
    {
        $('table').delegate('.remove-to-cart', 'click', function(event)
        {   
            event.preventDefault();
            var $prod_id = $(this).attr('product-id');
            var _token = '{{ csrf_token() }}';

            $.ajax({
                url: 'cart/remove',
                type: 'post',
                dataType: 'json',
                data: {
                    _token: _token,
                    product_id: $prod_id,
                },
            })
            .done(function() {
                // console.log("success");
                load_cart();
            })
            .fail(function() {
                // console.log("error");
                alert("Error on removing item/s on cart");              

            })
            .always(function() {
                // console.log("complete");
            });
            
            /* Act on the event */
        });
    }


    function add_to_cart_init()
    {
        $('.add-to-cart').on('click',function(event)
        {
            // alert('asd');
            event.preventDefault();
            var $prod_id = $(this).attr('product-id');
            @if(isset($slotid))
                var $current_slot = {{$slotid}};
            @else 
                var $current_slot = 0;
            @endif
            
            var _token = '{{ csrf_token() }}';
            // alert($current_slot);

            $.ajax({
                url: 'cart/add',
                type: 'post',
                dataType: 'json',
                data: {
                    _token : _token,
                    product_id: $prod_id,
                    slot_id: $current_slot
                },
            })
            .done(function() {
                // console.log("success");
                load_cart();
            })
            .fail(function() {
                // console.log("error");
                alert("Error on adding to cart");               
            })
            .always(function() {
                // console.log("complete");
            });
            
            /* Act on the event */
        });
    }



    function load_cart()
    {   
        $('.cart_preloader').fadeIn();
        $( ".cart_container" ).addClass('cart_opacity')
        setTimeout(function(){
            $('.cart_preloader').fadeOut();
            $( ".cart_container" ).removeClass('cart_opacity');
            $( ".cart_container" ).load( "/cart" );
        }, 1000);

    }   
});</script>
@endsection
@extends('distributor.layout')
@section('content')
@if('settings')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default panel-block panel-title-block" id="top">
            <div class="panel-heading">
                <div>
                    <i class="icon-align-justify"></i>
                    <h1>
                        <span class="page-title">Report</span>
                        <small>
                            Ladder Rebates Report
                        </small>
                    </h1>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <table></table>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
    <table class="table table-hover table-responsive">
        <thead>
            <th>CLAIMABLE ON</th>
            <th>CLAIMED</th>
            <th>BONUS GP</th>
            <tH></tH>
        </thead>
        <tbody>
            @foreach($rebates as $value)
            <tr>
                <td>{{$value->rebates_date_claimable}}</td>
                <td class="@if($value->rebates_isclaimable == 2) success @elseif($value->rebates_isclaimable == 1) info @else warning @endif">@if($value->rebates_isclaimable == 2) Claimed @elseif($value->rebates_isclaimable == 1) Unclaimed @else Cannot Claim Yet @endif</td>
                <td>{{$value->rebates_bonus_gp}}</td>
                <td><button class="btn btn-primary" style="border: 1px solid black; border-radius: 0px;" onClick="getajaxtomodal({{$value->rebates_id}})" @if($value->rebates_isclaimable == 0) disabled="disabled" @elseif ($value->rebates_isclaimable == 2) disabled="disabled" @endif >Claim</button></td>
            </tr>
            @endforeach
        </tbody>
    </table>
    </div>
</div>
@endif
@endsection
@section('js')
<script type="text/javascript">
    function getajaxtomodal(id){
	$.ajax({
		url	 	: 	'/distributor/report/rebates/claim/' + id,
		type 	: 	'get',
		success : 	function(result){
			location.reload();
		},
		error	: 	function(err){
			alert('Something Went Wrong!')
		}
	});
}
</script>
@endsection
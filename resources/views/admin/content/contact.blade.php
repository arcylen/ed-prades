@extends('admin.layout')
@section('content')

    <div class="row header">
        <div class="title col-md-8">
            <h2><i class="fa fa-phone"></i> Contact Us</h2>
        </div>
        <div class="buttons col-md-4 text-right">
            <button onclick="$('#country-add-form').submit();" type="button" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
        </div>
    </div>
    <div class="col-md-12 form-group-container" style="overflow: hidden;">
        <form id="country-add-form" class="form-horizontal" action="admin/content/contact/submit" method="post">
            <div class="form-group col-md-8">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label for="Address" class="col-sm-2 control-label"><input type="text" readonly value="Address" style="border: 0; text-align: center; width: 100%;"></label>
                    <div class="col-sm-10">
                        <input type="hidden" name='addressid' id="addressid" value="{{$address->about_id}}">
                        <textarea id="address" class="form-control" name="address">{{$address->about_description}}</textarea>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="Contact Number" class="col-sm-2 control-label"><input type="text" readonly value="Contact Number" style="border: 0; text-align: center; width: 100%;"></label>
                    <div class="col-sm-10">
                        <input type="hidden" name='contactnumid' id="contactnumid" value="{{$contactnum->about_id}}">
                        <textarea id="contactnum" class="form-control" name="contactnum">{{$contactnum->about_description}}</textarea>
                    </div>
                </div> 
                
                <div class="form-group">
                    <label for="E-mail" class="col-sm-2 control-label"><input type="text" readonly value="E-mail" style="border: 0; text-align: center; width: 100%;"></label>
                    <div class="col-sm-10">
                        <input type="hidden" name='emailid' id="emailid" value="{{$email->about_id}}">
                        <textarea id="email" class="form-control" name="email">{{$email->about_description}}</textarea>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="Business Hours" class="col-sm-2 control-label"><input type="text" readonly value="Business Hours" style="border: 0; text-align: center; width: 100%;"></label>
                    <div class="col-sm-10">
                        <input type="hidden" name='businesshoursid' id="businesshoursid" value="{{$businesshours->about_id}}">
                        <textarea id="businesshours" class="form-control" name="businesshours">{{$businesshours->about_description}}</textarea>
                    </div>
                </div> 
            </div>
        </form>
    </div>
@endsection

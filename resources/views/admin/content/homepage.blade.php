@extends('admin.layout')
@section('content')

    <div class="row header">
        <div class="title col-md-8">
            <h2><i class="fa fa-rss"></i> Home Page</h2>
        </div>
        <div class="buttons col-md-4 text-right">
            <button onclick="$('#country-add-form').submit();" type="button" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
        </div>
    </div>
    <div class="col-md-12 form-group-container" style="overflow: hidden;">
        <form id="country-add-form" class="form-horizontal" action="admin/content/homepage/submit" method="post" enctype="multipart/form-data">
            <div class="form-group col-md-12">
                <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                
                <div class="col-md-6 form-group">
                    <label for="First Row Right (Header)" class="col-sm-4 control-label"><input type="text" readonly value="First Row Right (Header)" style="border: 0; text-align: center; width: 100%;"></label>
                    <div class="col-sm-8">
                        <input type="hidden" name='frheaderid' id="frheaderid" value="{{$frheader->about_id}}">
                        <textarea id="frheader" class="form-control" name="frheader">{{$frheader->about_description}}</textarea>
                    </div>
                </div>
                
                <div class="col-md-6 form-group">
                    <label for="First Row Right (Content)" class="col-sm-4 control-label"><input type="text" readonly value="First Row Right (Content)" style="border: 0; text-align: center; width: 100%;"></label>
                    <div class="col-sm-8">
                        <input type="hidden" name='frcontentid' id="frcontentid" value="{{$frcontent->about_id}}">
                        <textarea id="frcontent" class="form-control" name="frcontent">{{$frcontent->about_description}}</textarea>
                    </div>
                </div> 
                
                <div class="col-md-6 form-group">
                    <label for="Second Row Left (Header)" class="col-sm-4 control-label"><input type="text" readonly value="Second Row Left (Header)" style="border: 0; text-align: center; width: 100%;"></label>
                    <div class="col-sm-8">
                        <input type="hidden" name='srleftid' id="srleftid" value="{{$srleft->about_id}}">
                        <textarea id="srleft" class="form-control" name="srleft">{{$srleft->about_description}}</textarea>
                    </div>
                </div>
                
                <div class="col-md-6 form-group">
                    <label for="Second Row Left (Content)" class="col-sm-4 control-label"><input type="text" readonly value="Second Row Left (Content)" style="border: 0; text-align: center; width: 100%;"></label>
                    <div class="col-sm-8">
                        <input type="hidden" name='srleftcontentid' id="srleftcontentid" value="{{$srleftcontent->about_id}}">
                        <textarea id="srleftcontent" class="form-control" name="srleftcontent">{{$srleftcontent->about_description}}</textarea>
                    </div>
                </div>
                
                <div class="col-md-6 form-group">
                    <label for="Second Row Right (Header)" class="col-sm-4 control-label"><input type="text" readonly value="Second Row Right (Header)" style="border: 0; text-align: center; width: 100%;"></label>
                    <div class="col-sm-8">
                        <input type="hidden" name='srrightid' id="srrightid" value="{{$srright->about_id}}">
                        <textarea id="srright" class="form-control" name="srright">{{$srright->about_description}}</textarea>
                    </div>
                </div>
                
                <div class="col-md-6 form-group">
                    <label for="Second Row Right (Content)" class="col-sm-4 control-label"><input type="text" readonly value="Second Row Right (Content)" style="border: 0; text-align: center; width: 100%;"></label>
                    <div class="col-sm-8">
                        <input type="hidden" name='srrightcontentid' id="srrightcontentid" value="{{$srrightcontent->about_id}}">
                        <textarea id="srrightcontent" class="form-control" name="srrightcontent">{{$srrightcontent->about_description}}</textarea>
                    </div>
                </div>
                
                <div class="col-md-8 form-group">
                    <label for="Second Row Right (Content)" class="col-sm-4 control-label" style="text-align:left;font-size:20px;">Image for Slider</label>
                </div>
                <div class="col-md-12">
                    @if($one->slider_image == '0')<input type="hidden" value="{{$wan1 = '/resources/assets/frontend/img/add2.png'}}">@else<input type="hidden" value="{{$wan1 = $one->slider_image}}">@endif
                    <label class=" col-sm-3" style="border:1px solid #ddd;cursor:pointer">
                        <img id="1st" name="1st" src="{{$wan1}}"  style="width:100%;height:100%;max-width:250px;max-height:230px;">
                        <input type="file" id="addimage" name="addimage" style="display:none">
                    </label>
                    @if($two->slider_image == '0')<input type="hidden" value="{{$tu = '/resources/assets/frontend/img/add2.png'}}">@else<input type="hidden" value="{{$tu = $two->slider_image}}">@endif
                    <label class=" col-sm-3" style="border:1px solid #ddd;cursor:pointer">
                        <img id="2nd" name="2nd" src="{{$tu}}" style="width:100%;height:100%;max-width:250px;max-height:230px;">
                        <input type="file" id="addimage2" name="addimage2" style="display:none">
                    </label>
                    @if($three->slider_image == '0')<input type="hidden" value="{{$tri = '/resources/assets/frontend/img/add2.png'}}">@else<input type="hidden" value="{{$tri = $three->slider_image}}">@endif
                    <label class=" col-sm-3" style="border:1px solid #ddd;cursor:pointer">
                        <img id="3rd" name="3rd" src="{{$tri}}" style="width:100%;height:100%;max-width:250px;max-height:230px;">
                        <input type="file" id="addimage3" name="addimage3" style="display:none">
                    </label>
                    @if($four->slider_image == '0')<input type="hidden" value="{{$por = '/resources/assets/frontend/img/add2.png'}}">@else<input type="hidden" value="{{$por = $four->slider_image}}">@endif
                    <label class=" col-sm-3" style="border:1px solid #ddd;cursor:pointer">
                        <img id="4th" name="4th" src="{{$por}}" style="width:100%;height:100%;max-width:250px;max-height:230px;">
                        <input type="file" id="addimage4" name="addimage4" style="display:none">
                    </label>
                    @if($five->slider_image == '0')<input type="hidden" value="{{$payb = '/resources/assets/frontend/img/add2.png'}}">@else<input type="hidden" value="{{$payb = $five->slider_image}}">@endif
                    <label class=" col-sm-3" style="border:1px solid #ddd;cursor:pointer">
                        <img id="5th" name="5th" src="{{$payb}}" style="width:100%;height:100%;max-width:250px;max-height:230px;">
                        <input type="file" id="addimage5" name="addimage5" style="display:none">
                    </label>
                    @if($six->slider_image == '0')<input type="hidden" value="{{$siks = '/resources/assets/frontend/img/add2.png'}}">@else<input type="hidden" value="{{$siks = $six->slider_image}}">@endif
                    <label class=" col-sm-3" style="border:1px solid #ddd;cursor:pointer">
                        <img id="6th" name="6th" src="{{$siks}}" style="width:100%;height:100%;max-width:250px;max-height:230px;">
                        <input type="file" id="addimage6" name="addimage6" style="display:none">
                    </label>
                    @if($seven->slider_image == '0')<input type="hidden" value="{{$seben = '/resources/assets/frontend/img/add2.png'}}">@else<input type="hidden" value="{{$seben = $seven->slider_image}}">@endif
                    <label class=" col-sm-3" style="border:1px solid #ddd;cursor:pointer">
                        <img id="7th" name="7th" src="{{$seben}}" style="width:100%;height:100%;max-width:250px;max-height:230px;">
                        <input type="file" id="addimage7" name="addimage7" style="display:none">
                    </label>
                    @if($eight->slider_image == '0')<input type="hidden" value="{{$eyt = '/resources/assets/frontend/img/add2.png'}}">@else<input type="hidden" value="{{$eyt = $eight->slider_image}}">@endif
                    <label class=" col-sm-3" style="border:1px solid #ddd;cursor:pointer">
                        <img id="8th" name="8th" src="{{$eyt}}" style="width:100%;height:100%;max-width:250px;max-height:230px;">
                        <input type="file" id="addimage8" name="addimage8" style="display:none">
                    </label>
                    
                    <input type="hidden" name="1" value="{{$one->slider_image}}"><input type="hidden" name="2" value="{{$two->slider_image}}"><input type="hidden" name="3" value="{{$three->slider_image}}"><input type="hidden" name="4" value="{{$four->slider_image}}"><input type="hidden" name="5" value="{{$five->slider_image}}">
                    <input type="hidden" name="6" value="{{$six->slider_image}}"><input type="hidden" name="7" value="{{$seven->slider_image}}"><input type="hidden" name="8" value="{{$eight->slider_image}}">
                </div>
            </div>
        </form>
    </div>
@endsection
@section("script")
<script type="text/javascript">
    $("#addimage").change(function()
    {
        var file = document.getElementById('addimage').files[0];
    	filename = '/images/'+ file["name"];
    	var _token = document.getElementById('_token').value;
    	var formdata = new FormData();
    	var ajax = new XMLHttpRequest();
    	formdata.append('_token',_token);
    	formdata.append('file', file);
    	ajax.addEventListener("load", LoadData, false);
    	ajax.open("POST","/uploadPicture");
    	ajax.send(formdata);
    })

	function LoadData(Event){
	    var wan = Event.target.responseText;
		$("#1st").attr("src", wan);
		$(".divprogress").css("display","none");
		$("#progressBar").css("width", "0%" );
	}
	
	$("#addimage2").change(function()
    {
        var file = document.getElementById('addimage2').files[0];
    	filename = '/images/'+ file["name"];
    	var _token = document.getElementById('_token').value;
    	var formdata = new FormData();
    	var ajax = new XMLHttpRequest();
    	formdata.append('_token',_token);
    	formdata.append('file', file);
    	ajax.addEventListener("load", LoadData, false);
    	ajax.open("POST","/uploadPicture");
    	ajax.send(formdata);
    	readURL(this);
    })
    
    function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#2nd').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

	$("#addimage3").change(function()
    {
        var file = document.getElementById('addimage3').files[0];
    	filename = '/images/'+ file["name"];
    	var _token = document.getElementById('_token').value;
    	var formdata = new FormData();
    	var ajax = new XMLHttpRequest();
    	formdata.append('_token',_token);
    	formdata.append('file', file);
    	ajax.addEventListener("load", LoadData, false);
    	ajax.open("POST","/uploadPicture");
    	ajax.send(formdata);
    	readURL3(this);
    })
    
    function readURL3(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#3rd').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
	$("#addimage4").change(function()
    {
        var file = document.getElementById('addimage4').files[0];
    	filename = '/images/'+ file["name"];
    	var _token = document.getElementById('_token').value;
    	var formdata = new FormData();
    	var ajax = new XMLHttpRequest();
    	formdata.append('_token',_token);
    	formdata.append('file', file);
    	ajax.addEventListener("load", LoadData, false);
    	ajax.open("POST","/uploadPicture");
    	ajax.send(formdata);
    	readURL4(this);
    })
    
    function readURL4(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#4th').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
	$("#addimage5").change(function()
    {
        var file = document.getElementById('addimage5').files[0];
    	filename = '/images/'+ file["name"];
    	var _token = document.getElementById('_token').value;
    	var formdata = new FormData();
    	var ajax = new XMLHttpRequest();
    	formdata.append('_token',_token);
    	formdata.append('file', file);
    	ajax.addEventListener("load", LoadData, false);
    	ajax.open("POST","/uploadPicture");
    	ajax.send(formdata);
    	readURL5(this);
    })
    
    function readURL5(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#5th').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
	$("#addimage6").change(function()
    {
        var file = document.getElementById('addimage6').files[0];
    	filename = '/images/'+ file["name"];
    	var _token = document.getElementById('_token').value;
    	var formdata = new FormData();
    	var ajax = new XMLHttpRequest();
    	formdata.append('_token',_token);
    	formdata.append('file', file);
    	ajax.addEventListener("load", LoadData, false);
    	ajax.open("POST","/uploadPicture");
    	ajax.send(formdata);
    	readURL6(this);
    })
    
    function readURL6(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#6th').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
	$("#addimage7").change(function()
    {
        var file = document.getElementById('addimage7').files[0];
    	filename = '/images/'+ file["name"];
    	var _token = document.getElementById('_token').value;
    	var formdata = new FormData();
    	var ajax = new XMLHttpRequest();
    	formdata.append('_token',_token);
    	formdata.append('file', file);
    	ajax.addEventListener("load", LoadData, false);
    	ajax.open("POST","/uploadPicture");
    	ajax.send(formdata);
    	readURL7(this);
    })
    
    function readURL7(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#7th').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
	$("#addimage8").change(function()
    {
        var file = document.getElementById('addimage8').files[0];
    	filename = '/images/'+ file["name"];
    	var _token = document.getElementById('_token').value;
    	var formdata = new FormData();
    	var ajax = new XMLHttpRequest();
    	formdata.append('_token',_token);
    	formdata.append('file', file);
    	ajax.addEventListener("load", LoadData, false);
    	ajax.open("POST","/uploadPicture");
    	ajax.send(formdata);
    	readURL8(this);
    })
    
    function readURL8(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#8th').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
	
</script>
@endsection

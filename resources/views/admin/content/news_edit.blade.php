@extends('admin.layout')
@section('content')
    <div class="row header">
        <div class="title col-md-8">
            <h2><i class="fa fa-newspaper-o"></i> Add News</h2>
        </div>
        <div class="buttons col-md-4 text-right">
            <button onclick="location.href='admin/content/news'" type="button" class="btn btn-default"><i class="fa fa-angle-double-left"></i> Back</button>
            <button onclick="$('#country-add-form').submit();" type="button" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
        </div>
    </div>
    <div class="col-md-12 form-group-container" style="overflow: hidden;">
        <form id="country-add-form" class="form-horizontal" action="admin/content/news/edit_submit" method="post" enctype="multipart/form-data">
            <div class="col-md-8">
                <div class="form-group">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="hidden" name="id" value="{{ $news->news_id }}">
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">News Title</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="title" name="title" value="{{ $news->news_title }}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="description" class="col-sm-2 control-label">News Description</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="description" name="description">{{ $news->news_description }}</textarea>
                        </div>
                    </div> 
                </div>
                <div class="form-group text-center">
                    <label for="p-tags">News Image</label>
                    <input type="file" class="form-control" id="image_file" name="image_file" >
                </div>
            </div>
             <div class="col-md-4">
                <div class="text-center">
                    <div><img style="width: 100%;" src="{{ $news->image }}" id="imageholder"></div>
                    <label for="p-tags">Product Image</label>
                </div>
             </div>
        </form>
    </div>
@endsection
@section('script')
<script type="text/javascript">
$("#image_file").change(function() {
        readURL(this);
    });
    function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageholder').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
</script>
@endsection
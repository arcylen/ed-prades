@extends('admin.layout')
@section('content')

    <div class="row header">
        <div class="title col-md-8">
            <h2><i class="fa fa-rss"></i> Front Footer</h2>
        </div>
        <div class="buttons col-md-4 text-right">
            <button onclick="$('#country-add-form').submit();" type="button" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
        </div>
    </div>
    <div class="col-md-12 form-group-container" style="overflow: hidden;">
        <form id="country-add-form" class="form-horizontal" action="admin/content/frontfooter/submit" method="post">
            <div class="form-group col-md-12">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label for="About" class="col-sm-2 control-label"><input type="text" readonly value="{{$contactnumber->about_name}}" style="border: 0; text-align: center; width: 100%;"></label>
                    <div class="col-sm-10">
                        <input type="hidden" name='contactnumberid' id="contactnumberid" value="{{$contactnumber->about_id}}">
                        <textarea id="contactnumber" class="form-control" name="contactnumber">{{$contactnumber->about_description}}</textarea>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="Mission" class="col-sm-2 control-label"><input type="text" readonly value="{{$email->about_name}}" style="border: 0; text-align: center; width: 100%;"></label>
                    <div class="col-sm-10">
                        <input type="hidden" name='emailid' id="emailid" value="{{$email->about_id}}">
                        <textarea id="email" class="form-control" name="email">{{$email->about_description}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Mission" class="col-sm-2 control-label"><input type="text" readonly value="{{$newsletter->about_name}}" style="border: 0; text-align: center; width: 100%;"></label>
                    <div class="col-sm-10">
                        <input type="hidden" name='newsletterid' id="newsletterid" value="{{$newsletter->about_id}}">
                        <textarea id="newsletter" class="form-control" name="newsletter">{{$newsletter->about_description}}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Mission" class="col-sm-2 control-label"><input type="text" readonly value="{{$rights->about_name}}" style="border: 0; text-align: center; width: 100%;"></label>
                    <div class="col-sm-10">
                        <input type="hidden" name='rightsid' id="rightsid" value="{{$rights->about_id}}">
                        <textarea id="rights" class="form-control" name="rights">{{$rights->about_description}}</textarea>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

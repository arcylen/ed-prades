@extends('admin.layout')
@section('content')

    <div class="row header">
        <div class="title col-md-8">
            <h2><i class="fa fa-rss"></i> Terms and Agreement</h2>
        </div>
        <div class="buttons col-md-4 text-right">
            <button onclick="$('#submitform').submit();" type="button" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
        </div>
    </div>
   <table>
       <tr>
           <td></td>
           <td>
              
               
           </td>
       </tr>
   </table>
   <form id="submitform" class="form-horizontal" action="/admin/content/terms/submit" method="post">
   <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
   <div class="col-md-4">Terms</div>
   <div class="col-md-8"> <textarea class="form-control" rows="10" id="comment" name="terms" value="">{{$terms->about_description}}</textarea></div>
   </form>
@endsection
@section("script")

@endsection

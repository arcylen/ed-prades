@extends('admin.layout')
@section('content')

    <div class="row header">
        <div class="title col-md-8">
            <h2><i class="fa fa-rss"></i> Others</h2>
        </div>
        <div class="buttons col-md-4 text-right">
            <button onclick="$('#country-add-form').submit();" type="button" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
        </div>
    </div>
    <div class="col-md-12 form-group-container" style="overflow: hidden;">
        <form id="country-add-form" action="admin/content/about/submit" method="post">
            <div class="form-group">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="row clearfix">
                    @foreach($_content as $key => $content)
                    <div class="form-group col-sm-6">
                        <label for="About" class="col-sm-12 control-label"><input type="text" name="content[{{ $key }}][name]" readonly value="{{ $content->about_name }}" style="font-weight: 700; border: 0; text-align: center; width: 100%; text-align: left;"></label>
                        <div class="col-sm-12">
                            <textarea id="about" class="form-control" name="content[{{ $key }}][description]">{{$content->about_description}}</textarea>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </form>
    </div>
@endsection

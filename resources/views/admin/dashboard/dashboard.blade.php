@extends('admin.layout')
@section('content')

<div class="dashboard">
	<div class="panel-header row">
		<div class="col-md-4">
			<div class="panel-box">
				<i class="fa fa-users"></i>
				<span>{{ $members }}</span>
				<div class="panel-label">MEMBERS</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel-box">
				<i class="fa fa-user"></i>
				<span>{{ $slots }}</span>
				<div class="panel-label">SLOTS</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel-box">
				<i class="fa fa-cloud"></i>
				<span>{{ $codes }}</span>
				<div class="panel-label">CODE</div>
			</div>
		</div>
		<!-- <div class="col-md-3">
			<div class="panel-box">
				<i class="fa fa-money"></i>
				<span>$320,000</span>
				<div class="panel-label">TOTAL SALES</div>
			</div>
		</div> -->
	</div>
	<!--<input type="hidden" id="jsoncontainer" value="{{$json}}">-->
	<!--<div class="graph">-->
	<!--	<div id="container" style="width:100%; height:400px;"></div>-->
	<!--</div>-->

<div class="container" style="margin-top:10px">
	<ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#direct">Direct</a></li>
      <li><a data-toggle="tab" href="#indirect">Indirect</a></li>
      <li><a data-toggle="tab" href="#pipeline">Pipeline</a></li>
      <!--<li><a data-toggle="tab" href="#binary">Binary</a></li>-->
      <li><a data-toggle="tab" href="#allreport">Direct/Indirect/Pipeline</a></li>
      <li><a data-toggle="tab" href="#paycheque">Pay Cheque</a></li>
      <li><a data-toggle="tab" href="#claims">Claims</a></li>
    </ul>
	<div class="clearfix">
        <div class="tab-content form-cotrol col-md-12" class="">
          <div id="direct" class="tab-pane fade in active">
          	<div id="containerdirect" style="height: 400px;"></div>
          </div>
          <div id="indirect" class="tab-pane fade">
            <div id="containerindirects" style="height: 400px;" class="form-control"></div>
          </div>
          <div id="pipeline" class="tab-pane fade">
            <div id="containerpipeline" style="height: 400px;" class="form-control" ></div>
          </div>
          <div id="binary" class="tab-pane fade">
            <div id="containerbinary" style="height: 400px;" class="form-control"></div>
          </div>
          <div id="allreport" class="tab-pane fade">
            <div id="containerall" style="height: 400px;" class="form-control"></div>
          </div>
          <div id="paycheque" class="tab-pane fade">
            <div id="containerpaycheque" style="height: 400px;" class="form-control"></div>
          </div>
          <div id="claims" class="tab-pane fade">
            <div id="containerclaim" style="height: 400px;" class="form-control"></div>
          </div>
        </div>
    </div>
</div>
</div>



<div class="remodal" data-remodal-id="modal"
  data-remodal-options="hashTracking: false, closeOnOutsideClick: false">

  <button data-remodal-action="close" class="remodal-close"></button>
  <h1><i class="glyphicon glyphicon-warning-sign"> </i>WARNING</h1>
  @if(Session::get('not_allow'))
	  <p class="col-m-12 alert alert-warning not-allow">
	  	{{Session::get('not_allow')}}
	  </p>
  @endif
  <button data-remodal-action="cancel" class="remodal-cancel">Cancel</button>
  <button data-remodal-action="confirm" class="remodal-confirm">OK</button>
</div>
@endsection
@section('script')
<script type="text/javascript" src="">

</script>
<!--<script src="/assets/global/highcharts.js"></script>-->
<script src="https://code.highcharts.com/stock/highstock.js"></script>
<script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
<!--<script src="resources/assets/external/highchart.js"></script>-->
<script type="text/javascript">
$(document).ready(function() {
   $.getJSON('/admin/report/direct', function (data) {
        // Create the chart
        $('#containerdirect').highcharts('StockChart', {


            rangeSelector : {
                selected : 1
            },

            title : {
                text : 'Direct Report'
            },

            series : [{
                name : 'Direct Report',
                data : data,
                tooltip: {
                    valueDecimals: 2
                }
            }]
        });
    });
    $.getJSON('/admin/report/indirect', function (data) {
        // Create the chart
        $('#containerindirects').highcharts('StockChart', {


            rangeSelector : {
                selected : 1
            },

            title : {
                text : 'Indirect Report'
            },

            series : [{
                name : 'Indirect Report',
                data : data,
                tooltip: {
                    valueDecimals: 2
                }
            }]
        });
    });
    $.getJSON('/admin/report/pipeline', function (data) {
        // Create the chart
        $('#containerpipeline').highcharts('StockChart', {


            rangeSelector : {
                selected : 1
            },

            title : {
                text : 'Pipeline Report'
            },

            series : [{
                name : 'Pipeline Report',
                data : data,
                tooltip: {
                    valueDecimals: 2
                }
            }]
        });
    });
    $.getJSON('/admin/report/binary', function (data) {
        // Create the chart
        $('#containerbinary').highcharts('StockChart', {


            rangeSelector : {
                selected : 1
            },

            title : {
                text : 'Binary Report'
            },

            series : [{
                name : 'Binary Report',
                data : data,
                tooltip: {
                    valueDecimals: 2
                }
            }]
        });
    });
});
        var seriesOptions = [],
        seriesCounter = 0,
        names = ['direct', 'indirect', 'pipeline'];

    /**
     * Create the chart when all data is loaded
     * @returns {undefined}
     */
    function createChart() {

        $('#containerall').highcharts('StockChart', {
				
            rangeSelector: {
                selected: 4
            },
            title : {
                text : 'Direct/Indirect/Pipeline'
            },
            yAxis: {
                labels: {
                    formatter: function () {
                        return (this.value > 0 ? ' + ' : '') + this.value + '%';
                    }
                },
                plotLines: [{
                    value: 0,
                    width: 2,
                    color: 'silver'
                }]
            },

            plotOptions: {
                series: {
                    compare: 'percent'
                }
            },

            tooltip: {
                pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.change}%)<br/>',
                valueDecimals: 2
            },

            series: seriesOptions
        });
    }
    $.each(names, function (i, name) {

        $.getJSON('/admin/report/' + name,    function (data) {

            seriesOptions[i] = {
                name: name,
                data: data
            };
            seriesCounter += 1;

            if (seriesCounter === names.length) {
                createChart();
                // console.log(seriesOptions);
            }
        });
    });
    


        var seriesOptionscheque = [],
        seriesCountercheque = 0,
        checknames = ['requested', 'not requested','processed'];
        checknamesget = ['2', '1', '2'];
        checknamesget2 = ['0', '0', '1'];
        $.each(checknames, function (g, name2) {
        
        $.getJSON('/admin/report/paycheque/' + checknamesget[g] + "/" + checknamesget2[g] ,    function (data2) {

                seriesOptionscheque[g] = {
                    name: name2,
                    data: data2
                };
                seriesCountercheque += 1;
    
                if (seriesCountercheque == checknames.length) {
                    createpaycheckchart();
                    console.log(seriesOptionscheque);
                }
            });
        });
function createpaycheckchart() {
    $('#containerpaycheque').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Paycheque Report'
        },
        subtitle: {
            text: null
        },
        xAxis: {
            categories: [
                'Paycheque Report'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Amount'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} PHP</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: seriesOptionscheque
    });
}
        var seriesOptionsclaims = [],
        seriesCounterclaims = 0,
        checkclaims = ['processed', 'unclaimed'];
        checkclaimsget = ['processed', 'unclaimed'];
        $.each(checkclaims, function (f, name3) {
        
        $.getJSON('/admin/report/claims/' + checkclaimsget[f] ,    function (data3) {

                seriesOptionsclaims[f] = {
                    name: name3,
                    data: data3
                };
                seriesCounterclaims += 1;
    
                if (seriesCounterclaims == checkclaims.length) {
                    createclaimschart();
                    console.log(seriesOptionsclaims);
                }
            });
        });
 function createclaimschart() {
    $('#containerclaim').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Vouchers Report'
        },
        subtitle: {
            text: null
        },
        xAxis: {
            categories: [
                '<center>Claimable Codes</center>'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: seriesOptionsclaims
    });
}
</script>
@endsection
@extends('admin.layout')
@section('content')
<!-- HEADER -->
<input class="token" type="hidden" name="_token" value="{{{ csrf_token() }}}" />
<div class="header row">
    <div class="title col-md-8">
        <h2><i class="fa fa-table"></i> Product Inventory Reports</h2>
    </div>
    <div class="col-md-4 well">
      <button class="btn btn-primary col-md-6" onClick="excelreport()"><i class="fa fa-file-excel-o" aria-hidden="true"></i>Excel</button>
      <button class="btn btn-primary col-md-6" onClick="pdfreport()"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>PDF</button>
    </div>
</div>
<div class="form-container">
    <form id="add-product-form" method="post">
        <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
       
        <div class="form-group-container">
         <!--HEADER FILTER -->
        <div class="col-md-8">
          <select name="group-date" class="form-control">
            <option {{ $group == "daily" ? "selected='selected'" : '' }} value ="daily">DAILY</option>
            <option {{ $group == "monthly" ? "selected='selected'" : '' }} value ="monthly">MONTHLY</option>
            <option {{ $group == "yearly" ? "selected='selected'" : '' }} value ="yearly">YEARLY</option>
            <option {{ $group == "all" ? "selected='selected'" : '' }} value ="all">OVERALL</option>
          </select>
        </div>
        <div class="col-md-2 hide">
<!--           <input type="text" placeholder="From" name="from" value="{{ $from }}" class="form-control datepicker"> -->
        </div>
        <div class="col-md-2">
<!--           <input type="text" placeholder="To" name="to" value="{{ $to }}"  class="form-control datepicker"> -->
        </div>
        <div class="col-md-2">
          <button class="btn btn-primary"><i class="fa fa-area-chart"></i> Generate Report</button>
        </div>
      </div>
      <div class="col-md-12" style="margin-top: 30px">
          <table id="data-table" class="table table-bordered data-table">
              <thead>
                  <tr>
                      <th class="option-col">Product ID</th>
                      <th>Product Name</th>
                      <th>Quantity Sold</th>
                  </tr>
              </thead>
              <tbody>
              	@foreach($_inventory as $inventory)
                <tr>
                  <td>{{ $inventory->id }}</td>
                  <td>{{ $inventory->name }}</td>
                  <td>{{ $inventory->value*(-1)}}</td>
                </tr>
                @endforeach
              </tbody>
          </table>
      </div>


      </div>
    </form>
</div>  

@endsection

@section('script')
<link rel="stylesheet" type="text/css" href="resources/assets/jquery-ui/jquery-ui.css">
<script type="text/javascript" src="resources/assets/jquery-ui/jquery-ui.min.js"></script>
<script src="resources/assets/external/highchart.js"></script>
<script src="resources/assets/external/modules/exporting.js"></script>
<!-- Rerports Import-->
<script type="text/javascript" src="/assets/global/excel/jquery.table2excel.min.js"></script>
<script type="text/javascript" src="/assets/global/pdf/jspdf.debug.js"></script>
<script type="text/javascript" src="/assets/global/pdf/plugins/from_html.js"></script>
<script type="text/javascript" src="/assets/global/pdf/plugins/auto.js"></script>
<script type="text/javascript">
function excelreport(){
	$('#exclude').addClass('hide');
    	$("#data-table").table2excel({
    	exclude: ".noExl",
    	name: "Product Inventory Report",
    	exclude_links: true,
    	filename: "Product_inventory_report" //do not include extension
		});
	 $('#exclude').removeClass('hide');	
}
function pdfreport() {
  $('#exclude').addClass('hide');
  var pdfsize = 'a4';
  var pdf = new jsPDF('l', 'pt', pdfsize);

  var res = pdf.autoTableHtmlToJson(document.getElementById("data-table"));
  pdf.autoTable(res.columns, res.data, {
    startY: 60,
    styles: {
      overflow: 'linebreak',
      fontSize: 12,
      rowHeight: 17,
      columnWidth: 'auto'
    },
    columnStyles: {
      id: {columnWidth: 'auto'}
    }
  });

  pdf.save("Product_Sales_Report.pdf");
  $('#exclude').removeClass('hide');
};
</script>
@endsection
@extends('admin.layout')
@section('content')
  <div class="row">
    <div class="header">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="title col-md-8">
        <h2><i class="fa fa-users"></i> {{$title}}</h2>
      </div>
      <div class="col-md-4 well">
      <button class="btn btn-primary col-md-6" onClick="excelreport()"><i class="fa fa-file-excel-o" aria-hidden="true"></i>Excel</button>
      <button class="btn btn-primary col-md-6" onClick="pdfreport()"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>PDF</button>
    </div>
    </div>
    <div class="filters "></div>
  </div>
    </div>
  <form method="POST" form action="admin/maintenance/accounts" target="_blank">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <div class="col-md-12">
      <table id="table" class="table table-bordered">
        <thead>
          <tr class="text-center">
            <th class="option-col">Top</th> 
            <th class="option-col">Slot Id</th>      
            <th>Slot Owner</th>
            <th>Total Recruit</th>         
          </tr>
        </thead>
      </table>
  </div>
  </form>
@endsection

@section('script')
<script type="text/javascript">
$(function() {
   $accountTable = $('#table').DataTable({
        "order": [[ 2, "desc" ]],
        processing: true,
        serverSide: true,
         ajax:{
            url:'admin/reports/top_recruiter/get',
            data:{
                archived : "{{$archived = Request::input('archived') ? 1 : 0 }}"
               }
        },
        columns: [
            {data: 'ctr', name: 'ctr'},
            {data: 'slot_id', name: 'slot_id'},
            {data: 'account_name', name: 'account_name'},
            {data: 'count', name: 'count'},
        ],
        "lengthMenu": [[8, 10, 25, 50, -1], [10, 25, 50, "All"]],
        "oLanguage": 
          {
            "sSearch": "",
            "sProcessing": ""
          },            
        stateSave: true,
    });
});
</script>
<!-- Rerports Import-->
<script type="text/javascript" src="/assets/global/excel/jquery.table2excel.min.js"></script>
<script type="text/javascript" src="/assets/global/pdf/jspdf.debug.js"></script>
<script type="text/javascript" src="/assets/global/pdf/plugins/from_html.js"></script>
<script type="text/javascript" src="/assets/global/pdf/plugins/auto.js"></script>
<script type="text/javascript">
function excelreport(){
	$('#exclude').addClass('hide');
    	$("#table").table2excel({
    	exclude: ".noExl",
    	name: "Product Sales Report",
    	exclude_links: true,
    	filename: "Top_Recruiter" //do not include extension
		});
	 $('#exclude').removeClass('hide');	
}
function pdfreport() {
  $('#exclude').addClass('hide');
  var pdfsize = 'a4';
  var pdf = new jsPDF('l', 'pt', pdfsize);

  var res = pdf.autoTableHtmlToJson(document.getElementById("table"));
  pdf.autoTable(res.columns, res.data, {
    startY: 60,
    styles: {
      overflow: 'linebreak',
      fontSize: 12,
      rowHeight: 17,
      columnWidth: 'auto'
    },
    columnStyles: {
      id: {columnWidth: 'auto'}
    }
  });

  pdf.save("Top_Recruiter.pdf");
  $('#exclude').removeClass('hide');
};
</script>

@endsection
@extends('admin.layout')
@section('content')
<!-- HEADER -->
<input class="token" type="hidden" name="_token" value="{{{ csrf_token() }}}" />
<div class="header row">
    <div class="title col-md-8">
        <h2><i class="fa fa-table"></i> Audit Trail</h2>
    </div>
    <div class="col-md-4 well">
      <button class="btn btn-primary col-md-6" onClick="excelreport()"><i class="fa fa-file-excel-o" aria-hidden="true"></i>Excel</button>
      <button class="btn btn-primary col-md-6" onClick="pdfreport()"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>PDF</button>
    </div>
</div>
	
<div class="form-container">
	@if($_logs)
			<table id="table" class="table table-bordered">
				<thead>
					<tr class="text-center">
						<th>Admin Name</th>
						<th>Logs</th>
						<th>Date/Time</th>
						<th id="exclude" class=".noExl">View</th>
					</tr>
				</thead>
				<tbody>
					@foreach($_logs as $logs)
					<tr>
						<td>{{$logs->account_name}}</td>
						<td>{!! $logs->logs!!}</td>
						<td>{!! $logs->created_at!!}</td>
						<td id="exclude" class=".noExl"><a href='admin/reports/audit_trail/view?id={{$logs->admin_log_id}}' target="_blank">View</a></td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<table id="tableex" class="table table-bordered hide">
				<thead>
					<tr class="text-center">
						<th>Admin Name</th>
						<th>Logs</th>
						<th>Date/Time</th>
					</tr>
				</thead>
				<tbody>
					@foreach($_logs as $logs)
					<tr>
						<td>{{$logs->account_name}}</td>
						<td>{!! $logs->logs!!}</td>
						<td>{!! $logs->created_at!!}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<center>{!! $_logs->render() !!}</center>
	@endif	
</div>	

@endsection

@section('script')
<!-- Rerports Import-->
<script type="text/javascript" src="/assets/global/excel/jquery.table2excel.min.js"></script>
<script type="text/javascript" src="/assets/global/pdf/jspdf.debug.js"></script>
<script type="text/javascript" src="/assets/global/pdf/plugins/from_html.js"></script>
<script type="text/javascript" src="/assets/global/pdf/plugins/auto.js"></script>
<script type="text/javascript">
function excelreport(){
	$('#exclude').addClass('hide');
    	$("#tableex").table2excel({
    	exclude: ".noExl",
    	name: "Audit Trail",
    	exclude_links: true,
    	filename: "Logs" //do not include extension
		});
	 $('#exclude').removeClass('hide');	
}
function pdfreport() {
  $('#exclude').addClass('hide');
  var pdfsize = 'a4';
  var pdf = new jsPDF('l', 'pt', pdfsize);

  var res = pdf.autoTableHtmlToJson(document.getElementById("table"));
  pdf.autoTable(res.columns, res.data, {
    startY: 60,
    styles: {
      overflow: 'linebreak',
      fontSize: 12,
      rowHeight: 17,
      columnWidth: 'auto'
    },
    columnStyles: {
      id: {columnWidth: 'auto'}
    }
  });

  pdf.save("Logs.pdf");
  $('#exclude').removeClass('hide');
};
</script>
@endsection
@extends('admin.layout')
@section('content')
<div class="header row">
    <div class="title col-md-8">
        <h2><i class="fa fa-table"></i> Reports Summary</h2>
    </div>
    <div class="col-md-4 well">
      <button class="btn btn-primary col-md-6" onClick="excelreport()"><i class="fa fa-file-excel-o" aria-hidden="true"></i>Excel</button>
      <button class="btn btn-primary col-md-6" onClick="pdfreport()"><i class="fa fa-file-pdf-o" aria-hidden="true"></i>PDF</button>
    </div>
</div>
<div class="header row">
    <div class="title col-md-8">
        <h2><i class="fa fa-table"></i> Flushed out</h2>
    </div>
</div>
	
<div id="1" class="form-container">
			<table id="table1" class="table table-bordered">
				<thead>
					<tr class="text-center">
						<th>Name</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Total Flushed Out</td>
						<td>{{number_format($total_flushed,2)}}</td>
					</tr>	
				</tbody>
			</table>
</div>
<!-- HEADER -->

<div class="header row">
    <div class="title col-md-8">
        <h2><i class="fa fa-table"></i> Members Summary</h2>
    </div>
</div>
	

<div  class="form-container">
			<table id="table2" class="table table-bordered">
				<thead>
					<tr class="text-center">
						<th>Name</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody id="2">
					<tr>
						<td>PS</td>
						<td>{{number_format($ps,2)}}</td>
					</tr>
					<tr>
						<td>CD</td>
						<td>{{number_format($cd,2)}}</td>
					</tr>
					<tr>
						<td>FS</td>
						<td>{{number_format($fs,2)}}</td>
					</tr>
					<tr>
						<td>Total</td>
						<td>{{number_format($total_slot,2)}}</td>
					</tr>
				</tbody>
			</table>
</div>	

<div class="header row">
    <div class="title col-md-8">
        <h2><i class="fa fa-table"></i> Members Rebates Summary</h2>
    </div>
</div>
	
<div   class="form-container">
			<table id="table3" class="table table-bordered">
				<thead>
					<tr class="text-center">
						<th>Name</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody id="3">
					<tr>
						<td>Old Wallet</td>
						<td>{{number_format($old_wallet,2)}}</td>
					</tr>
					<tr>
						<td>Sponsor GC</td>
						<td>{{number_format($sponsor_gc,2)}}</td>
					</tr>
					<tr>
						<td>Match Sales GC</td>
						<td>{{number_format($matching_gc,2)}}</td>
					</tr>
					<tr>
						<td>Unilevel Bonus</td>
						<td>{{number_format($dynamic,2)}}</td>
					</tr>		
					<tr>
						<td>Unilevel Check Match</td>
						<td>{{number_format($checkmatch,2)}}</td>
					</tr>	
					<tr>
						<td>Leadership Bonus</td>
						<td>{{number_format($leadership,2)}}</td>
					</tr>	
					<tr>
						<td>Breakaway Bonus</td>
						<td>{{number_format($breakaway,2)}}</td>
					</tr>	
					<tr>
						<td>Global Pool Sharing</td>
						<td>{{number_format($gps,2)}}</td>
					</tr>				
					<tr>
						<td>Sponsor Bonus</td>
						<td>{{number_format($sponsor,2)}}</td>
					</tr>
					<tr>
						<td>Match Sales Bonus</td>
						<td>{{number_format($matching,2)}}</td>
					</tr>
					<tr>
						<td>Mentors Bonus</td>
						<td>{{number_format($mentor,2)}}</td>
					</tr>
					<tr>
						<td>Total Members Rebates</td>
						<td>{{number_format($total,2)}}</td>
					</tr>
				</tbody>
			</table>
</div>

<div  class="header row">
    <div class="title col-md-8">
        <h2><i class="fa fa-table"></i> Product Sales Summary</h2>
    </div>
</div>
	
<div  class="form-container">
			<table id="table4" class="table table-bordered">
				<thead>
					<tr class="text-center">
						<th>Name</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody id="4">
					<tr>
						<td>Total Order</td>
						<td>{{number_format($total_order,2)}}</td>
					</tr>
					<tr>
						<td>Total Items</td>
						<td>{{number_format($total_items,2)}}</td>
					</tr>
					<tr>
						<td>Total Product Sales</td>
						<td>{{number_format($total_sales,2)}}</td>
					</tr>
				</tbody>
			</table>
</div>

<div class="header row">
    <div class="title col-md-8">
        <h2><i class="fa fa-table"></i> Member Encashment Summary</h2>
    </div>
</div>
	
<div  class="form-container">
			<table id="table5" class="table table-bordered">
				<thead>
					<tr class="text-center">
						<th>Name</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody id="5">
					<tr>
						<td>Total Encashment Request(s)</td>
						<td>{{number_format($count_encash,2)}}</td>
					</tr>
					<tr>
						<td>Total Encashment</td>
						<td>{{number_format($total_encashment*(-1),2)}}</td>
					</tr>	
				</tbody>
			</table>
</div>


<div class="header row">
    <div class="title col-md-8">
        <h2><i class="fa fa-table"></i> Overall</h2>
    </div>
</div>
	
<div  class="form-container">
			<table id="table6" class="table table-bordered">
				<thead>
					<tr class="text-center">
						<th>Name</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody id="6">
					<tr>
						<td>Total Paid Slot Sales</td>
						<td>{{number_format($total_ps_price,2)}}</td>
					</tr>
					<tr>
						<td>Total Product Sales</td>
						<td>{{number_format($total_sales,2)}}</td>
					</tr>	
					<tr>
						<td>Total Company Sales</td>
						<td>{{number_format($company_subtotal,2)}}</td>
					</tr>
					<tr>
						<td>Less Total Member Rebates</td>
						<td>{{number_format($total*(-1),2)}}</td>
					</tr>	
					<tr>
						<td>Total Company Income</td>
						<td>{{number_format($company_total,2)}}</td>
					</tr>
				</tbody>
			</table>
</div>

<div class="header row">
    <div class="title col-md-8">
        <h2><i class="fa fa-table"></i> Membership Codes</h2>
    </div>
</div>
	
<div  class="form-container">
			<table id="table7" class="table table-bordered">
				<thead>
					<tr class="text-center">
						<th>Name</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody id="7">
					<tr>
						<td>Total Unused Codes</td>
						<td>{{number_format($total_avail_codes,2)}}</td>
					</tr>	
					<tr>
						<td>Total Used Codes</td>
						<td>{{number_format($total_used_codes,2)}}</td>
					</tr>
					<tr>
						<td>Total Codes</td>
						<td>{{number_format($total_codes,2)}}</td>
					</tr>
				</tbody>
			</table>
			<table id="data-table">
				
			</table>
</div>
@endsection

@section('script')
<!-- Rerports Import-->
<script type="text/javascript" src="/assets/global/excel/jquery.table2excel.min.js"></script>
<script type="text/javascript" src="/assets/global/pdf/jspdf.debug.js"></script>
<script type="text/javascript" src="/assets/global/pdf/plugins/from_html.js"></script>
<script type="text/javascript" src="/assets/global/pdf/plugins/auto.js"></script>
<script type="text/javascript">
function excelreport(){
		var tablesummary = document.getElementById('table1').innerHTML + document.getElementById('table2').innerHTML + document.getElementById('table3').innerHTML + document.getElementById('table4').innerHTML + document.getElementById('table5').innerHTML + document.getElementById('table6').innerHTML + document.getElementById('table7').innerHTML;
		$('#data-table').html("");
		$('#data-table').html(tablesummary);
		$("#data-table").table2excel({
    	exclude: " ",
    	name: "Product Sales Report",
    	filename: "Product_Sales_Report" //do not include extension
		});
    	
}
function pdfreport() {
  var tablesummary = document.getElementById('1').innerHTML + document.getElementById('2').innerHTML + document.getElementById('3').innerHTML + document.getElementById('4').innerHTML + document.getElementById('5').innerHTML + document.getElementById('6').innerHTML + document.getElementById('7').innerHTML;
  $('#data-table').html("");
  $('#data-table').html(tablesummary);
  var pdfsize = 'a4';
  var pdf = new jsPDF('l', 'pt', pdfsize);

  var res = pdf.autoTableHtmlToJson(document.getElementById("data-table"));
  pdf.autoTable(res.columns, res.data, {
    startY: 60,
    styles: {
      overflow: 'linebreak',
      fontSize: 12,
      rowHeight: 17,
      columnWidth: 'auto'
    },
    columnStyles: {
      id: {columnWidth: 'auto'}
    }
  });

  pdf.save("Product_Sales_Report.pdf");
};
</script>
@endsection

@extends('admin.layout')
@section('content')
<!--DIRECT-->
<div class="row pull-right" style="margin-bottom: 10px">
	<a target="_blank" class="btn btn-primary" href="/compute_royalty_bonus">Compute Royalty Bonus</a>
</div>

@if($rank->admin_position_id == 1)
<div class="row">
	<div class="header">
		<input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
		<div class="title col-md-8">
			<h2><i class="fa fa-diamond"></i> Royalty Bonus</h2>
		</div> 
		<div class="col-md-4">
			<a onclick="addModal()" class="pull-right btn btn-primary"><i class="fa fa-plus"></i> Add</a>
		</div>
	</div>
</div>
<div class="form-group">
	<table class="table table-bordered">
		<div class="col-md-12">
			Profit Sharing: <span class="txt"> {{$rb_profit->rb_profitsharing}} %</span>
			<a ><i class="fa fa-edit fa-2x edit-profit" onclick="editProffit()"></i></a>

			<input type="text" class="hidden old-txt" value="{{$rb_profit->rb_profitsharing}}">
			<input type="text" class="form-control edit-txt hidden" value="{{$rb_profit->rb_profitsharing}}" id="profit-sharing">

			<a><i class="fa fa-spinner fa-2x fa-spin spin-saving hidden" style="color:green" aria-hidden="true"></i></a>
			<a><i class="fa fa-save fa-2x save-profit hidden" onclick="saveProfit()"></i></a>
		</div>
	</table>
</div>
<div class="col-md-12">
	<table id="product-table" class="table table-bordered">
		<thead>
			<tr>
				<th class="option-col">ID</th>
				<th>Position</th>
				<th>PERCENTAGE</th>
				<th>REQUIREMENTS</th>
				<th>TERMS OF PROMOTION</th>
				<!--<th>MULTIPLIER</th>-->
				<th class="option-col"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($_rb as $rb)
			<tr>
				<td>{{ $rb->rb_id }}</td>
				<td>{{ $rb->rb_position }}</td>
				<td>{{ $rb->rb_value }} %</td>
				<td align="left">
					<strong>{{number_format($rb->rb_required_spv,2)}} SPV w/in the Month</strong> <br>
					@if($rb->rb_active_assoc_director != 0)
					{{$rb->rb_active_assoc_director}} Active Associate Director <br>
					@endif
					@if($rb->rb_active_director != 0)
					{{$rb->rb_active_director}} Active Director <br>
					@endif
					@if($rb->rb_active_avp != 0)
					{{$rb->rb_active_avp}} Active AVP <br>
					@endif
					@if($rb->rb_active_vp != 0)
					{{$rb->rb_active_vp}} Active VP <br>
					@endif
					@if($rb->rb_active_svp != 0)
					{{$rb->rb_active_svp}} Active SVP <br>
					@endif
					@if($rb->rb_active_evp != 0)
					{{$rb->rb_active_evp}} Active EVP <br>
					@endif
					@if($rb->rb_active_president != 0)
					{{$rb->rb_active_president}} President
					@endif
				</td>
				<td  align="left">
					<strong>{{number_format($rb->rbp_required_spv,2)}} SPV</strong> <br>
					<strong>{{number_format($rb->rbp_required_personal_spv,2)}} Personal SPV</strong> <br>
					@if($rb->rbp_active_assoc_director != 0)
					{{$rb->rbp_active_assoc_director}} Active Associate Director <br>
					@endif
					@if($rb->rbp_active_director != 0)
					{{$rb->rbp_active_director}} Active Director <br>
					@endif
					@if($rb->rbp_active_avp != 0)
					{{$rb->rbp_active_avp}} Active AVP <br>
					@endif
					@if($rb->rbp_active_vp != 0)
					{{$rb->rbp_active_vp}} Active VP <br>
					@endif
					@if($rb->rbp_active_svp != 0)
					{{$rb->rbp_active_svp}} Active SVP <br>
					@endif
					@if($rb->rbp_active_evp != 0)
					{{$rb->rbp_active_evp}} Active EVP <br>
					@endif
					@if($rb->rbp_active_president != 0)
					{{$rb->rbp_active_president}} President
					@endif
				</td>

				<!--<td>number_format($membership->multiplier) </td>-->
				<td><a href="admin/utilities/royalty_bonus/edit?id={{ $rb->rb_id }}">EDIT</a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
<!--END DIRECT-->
@endif
	

<div class="modal fade register-beneficiaries" id="add_modal" role="dialog">
      <div class="modal-dialog text-center" style="background-color:#fff">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div class="clearfix text-center">
            <div class="row text-center">
                <div class="col-md-12">
                   <strong>Add</strong>
                </div>
                <div class="col-md-12">
                    <img src="/resources/assets/frontend/img/sobranglupet.png">
                </div>
            </div>
            <form method="post" id="loginform" >
            <input type="hidden" value="{{ csrf_token() }}" name="_token" id="_token">
             <label class="error" style="display:none;"></label>
            <div class="content">
                <div class="form-group">
		             <div class="form-group col-md-12">
		                <label for="account_name">Position Name</label>
		                <input name="position_name" class="form-control" id="" placeholder="" type="text">
		            </div>  
		            <div class="form-group col-md-12">
		                <label for="account_name">Royaly Bonus (0-100%)</label>
		                <input name="rb_value"  class="form-control" id="" placeholder="" type="number">
		            </div>
		            <h3>Requirements</h3>
		            <div class="form-group col-md-6">
		                <label for="account_name">Required SPV w/in the Month</label>
		                <input name="rb_required_pv" class="form-control" id="" placeholder="" type="number">
		            </div>
		            <div class="form-group col-md-6">
		                <label for="account_name">Active Associate Director</label>
		                <input name="rb_a_assoc_director" class="form-control" id="" placeholder="" type="number">
		            </div>
		            <div class="form-group col-md-6">
		                <label for="account_name">Director</label>
		                <input name="rb_a_director" class="form-control" id="" placeholder="" type="number">
		            </div>
		            <div class="form-group col-md-6">
		                <label for="account_name">Active AVP</label>
		                <input name="rb_a_avp" class="form-control" id="" placeholder="" type="number">
		            </div>
		            <div class="form-group col-md-6">
		                <label for="account_name">Active VP</label>
		                <input name="rb_a_vp" class="form-control" id="" placeholder="" type="number">
		            </div>
		            <div class="form-group col-md-6">
		                <label for="account_name">Active SVP</label>
		                <input name="rb_a_svp" class="form-control" id="" placeholder="" type="number">
		            </div>
		            <div class="form-group col-md-6">
		                <label for="account_name">Active EVP</label>
		                <input name="rb_a_evp" class="form-control" id="" placeholder="" type="number">
		            </div>
		            <div class="form-group col-md-6">
		                <label for="account_name">Active President</label>
		                <input name="rb_a_president" class="form-control" id="" placeholder="" type="number">
		            </div>

		            <h3>Terms of Promotion</h3>
		            <div class="form-group col-md-6">
		                <label for="account_name">Required SPV</label>
		                <input name="rbp_required_pv" class="form-control" id="" placeholder="" type="number">
		            </div>
		            <div class="form-group col-md-6">
		                <label for="account_name">Required Personal SPV</label>
		                <input name="rbp_required_personal_pv" class="form-control" id="" placeholder="" type="number">
		            </div>
		            <div class="form-group col-md-6">
		                <label for="account_name">Active Associate Director</label>
		                <input name="rbp_a_assoc_director" class="form-control" id="" placeholder="" type="number">
		            </div>
		            <div class="form-group col-md-6">
		                <label for="account_name">Director</label>
		                <input name="rbp_a_director" class="form-control" id="" placeholder="" type="number">
		            </div>
		            <div class="form-group col-md-6">
		                <label for="account_name">Active AVP</label>
		                <input name="rbp_a_avp" class="form-control" id="" placeholder="" type="number">
		            </div>
		            <div class="form-group col-md-6">
		                <label for="account_name">Active VP</label>
		                <input name="rbp_a_vp" class="form-control" id="" placeholder="" type="number">
		            </div>
		            <div class="form-group col-md-6">
		                <label for="account_name">Active SVP</label>
		                <input name="rbp_a_svp" class="form-control" id="" placeholder="" type="number">
		            </div>
		            <div class="form-group col-md-6">
		                <label for="account_name">Active EVP</label>
		                <input name="rbp_a_evp" class="form-control" id="" placeholder="" type="number">
		            </div>
		            <div class="form-group col-md-6">
		                <label for="account_name">Active President</label>
		                <input name="rbp_a_president" class="form-control" id="" placeholder="" type="number">
		            </div>
                </div>
                
            </div>
            <div class="form-group">
              <button type="button" class="btn btn-primary form-control" onclick="saveAdd()" ><i class="fa fa-floppy-o"></i> ADD</button>
            </div>

            </form>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
<!--END Class Sequence-->
<!--<script type="text/javascript" src="resources/assets/chosen_v1.4.2/chosen.jquery.min.js"></script>-->
<script type="text/javascript" >
function addModal()
{
	$("#add_modal").modal();
}
function saveAdd()
{
	// alert(2324);
	$.ajax(
        {
          url: "/admin/utilities/royalty_bonus/add_position",
          type: "post",
          data: $("#loginform").serialize(),
          success: function(data)
          {
            console.log(data);
            if(data == 1)
            {
              location.reload();
              console.log("success");
            }
     //       if(data == 1)
     //       {
     //         $(".error").html("Username and Password didn't match");
     //         $(".error").attr("style", "text-align:right;color:red;");
			  //$("#add_modal").modal();
     //       }
          }
        });
	
}
function editProffit()
{
	$(".edit-txt").removeClass("hidden");
	$(".edit-profit").addClass("hidden");
	$(".txt").addClass("hidden");
	$(".save-profit").removeClass("hidden");
}
function saveProfit()
{
	var value = $(".edit-txt").val();
	var token = $("#_token").val();
	var old = $(".old-txt").val();
	$(".spin-saving").removeClass("hidden");
	$.ajax(
        {
          url: "/admin/utilities/royalty_bonus/save_profit",
          type: "post",
          data: {token:token, value:value,old:old},
          success: function(data)
          {
          	console.log(data);
			$(".spin-saving").addClass("hidden");
			location.reload();
          }
      });
}
</script>
@endsection
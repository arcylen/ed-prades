@extends('admin.layout')
@section('content')
<div class="row">
	<div class="header">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="title col-md-8">
			<h2><i class="fa fa-star-half-o"></i> PIPELINE BONUS PERCENTAGE PER MEMBERSHIP</h2>
		</div>
	</div>
</div>
<div class="col-md-12">
	<table id="product-table" class="table table-bordered">
		<thead>
			<tr>
				<th class="option-col">LEVEL</th>
				<th>PIPELINE GRADUATE AMOUNT</th>
				<th>PIPELINE REQUIREMENT</th>
				<th>PIPELINE BINARY POINTS</th>
				<th class="option-col"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($_pipeline as $pipeline)
			<tr>
				<td>{{ $pipeline->pipeline_label }}</td>
				<td>{{ $pipeline->pipeline_graduate_amount }}</td>
				<td>{{ $pipeline->pipeline_requirement }}</td>
				<td>{{ $pipeline->pipeline_binary_points}} </td>
				<td><a href="admin/utilities/pipeline/edit?id={{ $pipeline->pipeline_id }}">EDIT</a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>


<div class="remodal create-slot" data-remodal-id="process" data-remodal-options="hashTracking: false">
    <button data-remodal-action="close" class="remodal-close"></button>
    <div class="header">
        Encash All Wallet
    </div>
    <form class="form-horizontal" method="POST">

    <input type="hidden" class="token" name="_token" value="{{ csrf_token() }}">

    <button class="button" type="button" data-remodal-action="cancel">Cancel</button>
    <button class="c_slot button"  type="submit" name="encashall">Encash All Wallet</button>
    </form>
</div>
@endsection
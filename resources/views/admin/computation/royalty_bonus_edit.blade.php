@extends('admin.layout')
@section('content')
    <div class="row header">
        <div class="title col-md-8">
            <h2><i class="fa fa-diamond"></i> Royalty Bonus / Update</h2>
        </div>
        <div class="buttons col-md-4 text-right">
            <button onclick="location.href='admin/utilities/royalty_bonus'" type="button" class="btn btn-default"><i class="fa fa-angle-double-left"></i> Back</button>
            <button onclick="$('#country-add-form').submit();" type="button" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
        </div>
    </div>
    <div class="col-md-12 form-group-container">
        <form id="country-add-form" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
            <div class="form-group col-md-12">
                <label for="account_name">Position Name</label>
                <input disabled="disabled" value="{{ $data->rb_position }}" class="form-control" id="" placeholder="" type="text">
            </div>  
            <div class="form-group col-md-12">
                <label for="account_name">Royaly Bonus (0-100%)</label>
                <input name="rb_value" value="{{ $data->rb_value }}" class="form-control" id="" placeholder="" type="number">
            </div>
            <h3>Requirements</h3>
            <div class="form-group col-md-6">
                <label for="account_name">Required SPV w/in the Month</label>
                <input name="rb_required_pv" value="{{ $data->rb_required_spv }}" class="form-control" id="" placeholder="" type="number">
            </div>
            <div class="form-group col-md-6">
                <label for="account_name">Active Associate Director</label>
                <input name="rb_a_assoc_director" value="{{ $data->rb_active_assoc_director }}" class="form-control" id="" placeholder="" type="number">
            </div>
            <div class="form-group col-md-6">
                <label for="account_name">Director</label>
                <input name="rb_a_director" value="{{ $data->rb_active_director }}" class="form-control" id="" placeholder="" type="number">
            </div>
            <div class="form-group col-md-6">
                <label for="account_name">Active AVP</label>
                <input name="rb_a_avp" value="{{ $data->rb_active_avp }}" class="form-control" id="" placeholder="" type="number">
            </div>
            <div class="form-group col-md-6">
                <label for="account_name">Active VP</label>
                <input name="rb_a_vp" value="{{ $data->rb_active_vp }}" class="form-control" id="" placeholder="" type="number">
            </div>
            <div class="form-group col-md-6">
                <label for="account_name">Active SVP</label>
                <input name="rb_a_svp" value="{{ $data->rb_active_svp }}" class="form-control" id="" placeholder="" type="number">
            </div>
            <div class="form-group col-md-6">
                <label for="account_name">Active EVP</label>
                <input name="rb_a_evp" value="{{ $data->rb_active_evp }}" class="form-control" id="" placeholder="" type="number">
            </div>
            <div class="form-group col-md-6">
                <label for="account_name">Active President</label>
                <input name="rb_a_president" value="{{ $data->rb_active_president }}" class="form-control" id="" placeholder="" type="number">
            </div>

            <h3>Terms of Promotion</h3>
            <div class="form-group col-md-6">
                <label for="account_name">Required SPV</label>
                <input name="rbp_required_pv" value="{{ $data->rbp_required_spv }}" class="form-control" id="" placeholder="" type="number">
            </div>
            <div class="form-group col-md-6">
                <label for="account_name">Required Personal SPV</label>
                <input name="rbp_required_personal_pv" value="{{ $data->rbp_required_personal_spv }}" class="form-control" id="" placeholder="" type="number">
            </div>
            <div class="form-group col-md-6">
                <label for="account_name">Active Associate Director</label>
                <input name="rbp_a_assoc_director" value="{{ $data->rbp_active_assoc_director }}" class="form-control" id="" placeholder="" type="number">
            </div>
            <div class="form-group col-md-6">
                <label for="account_name">Director</label>
                <input name="rbp_a_director" value="{{ $data->rbp_active_director }}" class="form-control" id="" placeholder="" type="number">
            </div>
            <div class="form-group col-md-6">
                <label for="account_name">Active AVP</label>
                <input name="rbp_a_avp" value="{{ $data->rbp_active_avp }}" class="form-control" id="" placeholder="" type="number">
            </div>
            <div class="form-group col-md-6">
                <label for="account_name">Active VP</label>
                <input name="rbp_a_vp" value="{{ $data->rbp_active_vp }}" class="form-control" id="" placeholder="" type="number">
            </div>
            <div class="form-group col-md-6">
                <label for="account_name">Active SVP</label>
                <input name="rbp_a_svp" value="{{ $data->rbp_active_svp }}" class="form-control" id="" placeholder="" type="number">
            </div>
            <div class="form-group col-md-6">
                <label for="account_name">Active EVP</label>
                <input name="rbp_a_evp" value="{{ $data->rbp_active_evp }}" class="form-control" id="" placeholder="" type="number">
            </div>
            <div class="form-group col-md-6">
                <label for="account_name">Active President</label>
                <input name="rbp_a_president" value="{{ $data->rbp_active_president }}" class="form-control" id="" placeholder="" type="number">
            </div>

            <!--<div class="form-group col-md-12">-->
            <!--    <label for="multiplier">Multiplier</label>-->
            <!--    <input name="multiplier" value="$data->multiplier" required="required" class="form-control" id="" placeholder="" type="text">-->
            <!--</div>-->


            
        </form>
    </div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function()
    {
        $(".level-input").keyup(function(e)
        {
            $val = $(e.currentTarget).val();
            $(".level-container").html('');
            for($ctr = 0; $ctr < $val; $ctr++)
            {
                $level = $ctr + 1;
                $(".level-container").append(   '<div class="form-group col-md-12">' +
                                                    '<label for="account_contact">Level ' + $level + ' (0-100%)</label>' +
                                                    '<input name="level[' + $level + ']" value="0" required="required" class="form-control" id="" placeholder="" type="text">' +
                                                '</div>');
            }
        });
    });
</script>
@endsection
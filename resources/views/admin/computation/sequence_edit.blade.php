@extends('admin.layout')
@section('content')
    <div class="row header">
        <div class="title col-md-8">
            <h2><i class="fa fa-star-half-o"></i> Class Sequence / Update</h2>
        </div>
        <div class="buttons col-md-4 text-right">
            <button onclick="location.href='admin/utilities/class_sequence'" type="button" class="btn btn-default"><i class="fa fa-angle-double-left"></i> Back</button>
            <button onclick="$('#country-add-form').submit();" type="button" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
        </div>
    </div>
    <div class="col-md-12 form-group-container">
        <form id="country-add-form" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="type" value="{{$data->type }}">
            <div class="form-group col-md-12">
                <label for="account_name">Membership Name</label>
                <input disabled="disabled" value="{{ $data->sequence_name }}" class="form-control" id="" placeholder="" type="text">
            </div>  

            <div class="form-group col-md-12">
                <label for="account_name">Required SPV</label>
                <input name="required_pv" value="{{ $data->required_spv }}" class="form-control" id="" placeholder="" type="number">
            </div>  
            
            
            <div class="form-group col-md-12">
                <label for="account_name">Required Personal SPV</label>
                <input name="required_personal_pv" value="{{ $data->required_personal_spv }}" class="form-control" id="" placeholder="" type="number">
            </div>
            @if($data->type == 3)
            <div class="form-group col-md-12">
                <label for="account_name">Personal Bonus (0-100%)</label>
                <input name="personal_bonus" value="{{ $data->personal_bonus }}" class="form-control" id="" placeholder="" type="number">
            </div>
            <div class="form-group col-md-12">
                <label for="account_name">Active Consultant</label>
                <input name="a_consultant" value="{{ $data->active_consultant }}" class="form-control" id="" placeholder="" type="number">
            </div>
            <div class="form-group col-md-12">
                <label for="account_name">Active Manager</label>
                <input name="a_manager" value="{{ $data->active_manager }}" class="form-control" id="" placeholder="" type="number">
            </div>
            <div class="form-group col-md-12">
                <label for="account_name">Active Associate Director</label>
                <input name="a_assoc_director" value="{{ $data->active_assoc_director }}" class="form-control" id="" placeholder="" type="number">
            </div>
            <div class="form-group col-md-12">
                <label for="account_name">Director</label>
                <input name="a_director" value="{{ $data->active_director }}" class="form-control" id="" placeholder="" type="number">
            </div>
            @endif

            <!--<div class="form-group col-md-12">-->
            <!--    <label for="multiplier">Multiplier</label>-->
            <!--    <input name="multiplier" value="$data->multiplier" required="required" class="form-control" id="" placeholder="" type="text">-->
            <!--</div>-->
            
            <div class="form-group col-md-12">
                <label for="account_contact">Number of Levels</label>
                <input name="level_rep" value="{{ $data->level_rep }}" required="required" class="form-control level-input" id="" placeholder="" type="text">
            </div>

            <div class="level-container">
                @foreach($_level as $key => $level)
                <div class="form-group col-md-12">
                    <label for="account_contact">Level {{ $level->seq_level }} (0-100%)</label>
                    <input name="level[{{ $level->seq_level }}]" value="{{ $level->seq_value}}" required="required" class="form-control" id="" placeholder="" type="text">
                </div>
                @endforeach
            </div>
        </form>
    </div>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function()
    {
        $(".level-input").keyup(function(e)
        {
            $val = $(e.currentTarget).val();
            $(".level-container").html('');
            for($ctr = 0; $ctr < $val; $ctr++)
            {
                $level = $ctr + 1;
                $(".level-container").append(   '<div class="form-group col-md-12">' +
                                                    '<label for="account_contact">Level ' + $level + ' (0-100%)</label>' +
                                                    '<input name="level[' + $level + ']" value="0" required="required" class="form-control" id="" placeholder="" type="text">' +
                                                '</div>');
            }
        });
    });
</script>
@endsection
@extends('admin.layout')
@section('content')
    <div class="row header">
        <div class="title col-md-8">
            <h2><i class="fa fa-star-half-o"></i>  Pipeline Bonus/ Update</h2>
        </div>
        <div class="buttons col-md-4 text-right">
            <button onclick="location.href='admin/utilities/pipeline'" type="button" class="btn btn-default"><i class="fa fa-angle-double-left"></i> Back</button>
            <button onclick="$('#country-add-form').submit();" type="button" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
        </div>
    </div>
    <div class="col-md-12 form-group-container" id="getmembership" list="{{$pipeline}}">
        <form id="country-add-form" method="post" action="/admin/utilities/pipeline/edit/post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group col-md-12">
                <label for="account_name">Pipeline Graduate Amount</label>
                <input value="{{ $pipeline->pipeline_graduate_amount }}" class="form-control" id="pipeline_graduate_amount" name="pipeline_graduate_amount" placeholder="" type="text">
            </div>    

            <div class="form-group col-md-12">
                <label for="account_contact">Pipeline Requirement</label>
                <input name="pipeline_requirement" value="{{ $pipeline->pipeline_requirement }}" required="required" class="form-control level-input" id="pipeline_requirement" placeholder="" type="text">
            </div>
            <div class="form-group col-md-12">
                <label for="account_contact">Pipeline Binary Points</label>
                <input name="pipeline_binary_points" value="{{ $pipeline->pipeline_binary_points }}" required="required" class="form-control level-input" id="pipeline_binary_points" placeholder="" type="text">
                <input type="hidden" name="p_id" id="p_id" value="{{$pipeline->pipeline_id}}">
            <hr style="width: 100%; color: black; height: 1px; background-color:black;"/>
            </div>
        </form>
    </div>
@endsection

@extends('admin.layout')
@section('content')
<!--DIRECT-->
<div class="row">
	<div class="header">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="title col-md-8">
			<h2><i class="fa fa-star-half-o"></i> Direct Sales Bonus</h2>
		</div>
	</div>
</div>
<div class="col-md-12">
	<table id="product-table" class="table table-bordered">
		<thead>
			<tr>
				<th class="option-col">ID</th>
				<th>Position</th>
				<th>REQUIRED SPV</th>
				<th>REQUIRED PERSONAL SPV</th>
				<th>NO. of LEVELS</th>
				<!--<th>MULTIPLIER</th>-->
				<th class="option-col"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($_direct as $direct)
			<tr>
				<td>{{ $direct->sequence_id }}</td>
				<td>{{ $direct->sequence_name }}</td>
				<td>{{ number_format($direct->required_spv) }} SPV</td>
				<td>{{ number_format($direct->required_personal_spv) }} Personal SPV</td>
				<td>{{ number_format($direct->level_rep) }}</td>
				<!--<td>number_format($membership->multiplier) </td>-->
				<td><a href="admin/utilities/sequence/edit?id={{ $direct->sequence_id }}&type={{$direct->type}}">EDIT</a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
<!--END DIRECT-->
<!--INDIRECT-->
<div class="row">
	<div class="header">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="title col-md-8">
			<h2><i class="fa fa-star-half-o"></i> <i class="fa fa-star-half-o"></i> Indirect Sales Bonus</h2>
		</div>
	</div>
</div>
<div class="col-md-12">
	<table id="product-table" class="table table-bordered">
		<thead>
			<tr>
				<th class="option-col">ID</th>
				<th>Position</th>
				<th>REQUIRED SPV</th>
				<th>REQUIRED PERSONAL SPV</th>
				<th>NO. of LEVELS</th>
				<!--<th>MULTIPLIER</th>-->
				<th class="option-col"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($_indirect as $indirect)
			<tr>
				<td>{{ $indirect->sequence_id }}</td>
				<td>{{ $indirect->sequence_name }}</td>
				<td>{{ number_format($indirect->required_spv) }} SPV</td>
				<td>{{ number_format($indirect->required_personal_spv) }} Personal SPV</td>
				<td>{{ number_format($indirect->level_rep) }}</td>
				<!--<td>number_format($membership->multiplier) </td>-->
				<td><a href="admin/utilities/sequence/edit?id={{ $indirect->sequence_id }}&type={{$indirect->type}}">EDIT</a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
	
</div>
<!--END INDIRECT-->
<!--Class Sequence-->
<div class="row">
	<div class="header">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<div class="title col-md-8">
			<h2><i class="fa fa-star-half-o"></i> <i class="fa fa-star-half-o"></i> <i class="fa fa-star-half-o"></i> Group Sales Bonus</h2>
		</div>
		<div class="col-md-4">
			<a onclick="addModal()" class="pull-right btn btn-primary"><i class="fa fa-plus"></i> Add</a>
		</div>
	</div>
</div>
<div class="col-md-12">
	<table id="product-table" class="table table-bordered">
		<thead>
			<tr>
				<th class="option-col">ID</th>
				<th>Position</th>
				<th>REQUIRED SPV</th>
				<th>REQUIRED PERSONAL SPV</th>
				<th>PERSONAL BONUS</th>
				<th>REQUIRED ACTIVE MEMBERS</th>
				<th>NO. of LEVELS</th>
				<!--<th>MULTIPLIER</th>-->
				<th class="option-col"></th>
			</tr>
		</thead>
		<tbody>
			@foreach($_class_sequence as $class_sequence)
			<tr>
				<td>{{ $class_sequence->sequence_id }}</td>
				<td>{{ $class_sequence->sequence_name }}</td>
				<td>{{ number_format($class_sequence->required_spv) }} SPV</td>
				<td>{{ number_format($class_sequence->required_personal_spv) }} Personal SPV</td>
				<td>{{ number_format($class_sequence->personal_bonus) }} %</td>
				<td>
					@if($class_sequence->active_consultant != 0)
						{{$class_sequence->active_consultant}} Consultant<br>
					@endif

					@if($class_sequence->active_manager != 0)
						{{$class_sequence->active_manager}} Manager<br>
					@endif

					@if($class_sequence->active_assoc_director != 0)
					{{$class_sequence->active_assoc_director}} Associate Director<br>
					@endif

					@if($class_sequence->active_director != 0)
					{{$class_sequence->active_director}} Director
					@endif
				</td>
				<td>{{ number_format($class_sequence->level_rep) }}</td>
				<!--<td>number_format($membership->multiplier) </td>-->
				<td><a href="admin/utilities/sequence/edit?id={{ $class_sequence->sequence_id }}&type={{$class_sequence->type}}">EDIT</a></td>
			</tr>
			@endforeach
		</tbody>
	</table>
	
	

<div class="modal fade register-beneficiaries" id="add_modal" role="dialog">
      <div class="modal-dialog text-center" style="background-color:#fff">
        <div class="modal-body">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div class="clearfix text-center">
            <div class="row text-center">
                <div class="col-md-12">
                   <strong>Add</strong>
                </div>
                <div class="col-md-12">
                    <img src="/resources/assets/frontend/img/sobranglupet.png">
                </div>
            </div>
            <form method="POST" id="loginform" >
            <input type="hidden" value="{{ csrf_token() }}" name="_token" id="_token">
             <label class="error" style="display:none;"></label>
            <div class="content">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                          <input type="text" class="form-control sequence-name" placeholder="Position" name="sequence_name">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                          <input type="text" class="form-control required-spv" placeholder="Required SPV" name="required_spv">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                          <input type="text" class="form-control required-personal-spv" placeholder="Required Personal SPV" name="required_personal_spv">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
              <button class="btn btn-primary form-control" onclick="saveAdd()" ><i class="fa fa-floppy-o"></i> ADD</button>
            </div>
            </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</div>
<!--END Class Sequence-->
<!--<script type="text/javascript" src="resources/assets/chosen_v1.4.2/chosen.jquery.min.js"></script>-->
<script type="text/javascript" >
function addModal()
{
	$("#add_modal").modal();
}
function saveAdd()
{
	$.ajax(
        {
          url: "/admin/utilities/class_sequence/add_position",
          type: "post",
          data: $("#loginform").serialize(),
          success: function(data)
          {
            console.log(data);
            if(data == 0)
            {
              location.reload();
              console.log("success");
            }
     //       if(data == 1)
     //       {
     //         $(".error").html("Username and Password didn't match");
     //         $(".error").attr("style", "text-align:right;color:red;");
			  //$("#add_modal").modal();
     //       }
          }
        });
	
}
</script>
@endsection
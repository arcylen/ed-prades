@extends('admin.layout')
@section('content')
   <div class="row header">
        <div class="title col-md-8">
            <h2><i class="fa fa-star-half-o"></i> STAIRSTEP</h2>
        </div>
        
    </div>
    <div class="col-md-12">
	<table id="product-table" class="table table-bordered">
		<thead>
			<tr>
				<th>LEVEL</th>
				<th>STAIRSTEP BONUS</th>
				<th>PERSONAL PV REQUIREMENTS</th>
				<th>GROUP PV REQUIREMENTS</th>
				<th>UNILEVEL PERCENTAGE</th>
				<th>STAIR STEP <br> MAX PAIR</th>
				<th class="center">GROUP PV<br>REQUIREMENTS <br> PER LEG</th>
				<!--<th class="option-col"></th>-->
			</tr>
		</thead>
		<tbody>
			@foreach($stairstep as $stair)
			<tr>
				<td class="name{{ $stair['stairstep_id'] }}">{{ $stair['stairstep_name'] }}</td>
				<td class="bonus{{ $stair['stairstep_id'] }}">{{ $stair['stairstep_bonus'] }}</td>
				<td class="r_pv{{ $stair['stairstep_id'] }}">{{ $stair['stairstep_required_pv'] }}</td>
				<td class="r_gpv{{ $stair['stairstep_id'] }}">{{ $stair['stairstep_required_gv']}}</td>
				<td>{{ $stair['stairstep_unilvel_rate_percent']}} %</td>
				<td>{{$stair['stairstep_binary_max_pair']}}</td>
				<td>{{ $stair['stairstep_special'] }} / {{ $stair['stairstep_special_qty'] }}</td>
				<!--<td class="editbutton{{ $stair['stairstep_id'] }}"><button class="btn btn-primary" onClick="edit({{ $stair['stairstep_id'] }})">Edit</button></td>-->
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endsection

@section('script')
<script type="text/javascript">
    function edit(id){
        $('.editbutton' + id).html('');
        var htmltoadd = "<button class='btn btn-success' onClick='savestair("+id+")'>Save</button>";
        $('.editbutton' + id).html(htmltoadd);
        var name = $('.name' + id).html();
        var bonus = $('.bonus' + id).html();
        var r_pv = $('.r_pv' + id).html();
        var r_gpv = $('.r_gpv' + id).html();
        htmltoadd = "";
        var htmltoaddname = "<input type='text' class='form-control' value='"+name+"' id='nameinput"+id+"'>";
        var htmltoaddbonus = "<input type='number' class='form-control' value='"+bonus+"' id='bonusinput"+id+"'>";
        var htmltoaddr_pv = "<input type='number' class='form-control' value='"+r_pv+"' id='r_pvinput"+id+"'>";
        var htmltoaddr_gpv = "<input type='number' class='form-control' value='"+r_gpv+"' id='r_gpvinput"+id+"'>";
        $('.name' + id).html(htmltoaddname);
        $('.bonus' + id).html(htmltoaddbonus);
        $('.r_pv' + id).html(htmltoaddr_pv);
        $('.r_gpv' + id).html(htmltoaddr_gpv);
    }
    function savestair(id){
    var nameinput = $('#nameinput' + id).val();
    var bonusinput = $('#bonusinput' + id).val();
    var r_pvinput = $('#r_pvinput' + id).val();
    var r_gpvinput = $('#r_gpvinput' + id).val();
    }
</script>
@endsection
@extends('admin.layout')
@section('content')
    <div class="row header">
        <div class="title col-md-8">
            <h2><i class="fa fa-star-half-o"></i>  Pay Cheque Processing Update</h2>
        </div>
        <div class="buttons col-md-4 text-right">
            <button onclick="$('#country-add-form').submit();" type="button" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
        </div>
    </div>
    <div class="col-md-12 form-group-container">
        <form id="country-add-form" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            @foreach($_schedule as $schedule)
            <span class="computation-container">
                <div class="form-group col-md-2">
                    <label for="account_name">Computation Label</label>
                    <input disabled="disabled"  class="form-control" id="" value="{{ strtoupper($schedule->pay_schedule_key) }}" placeholder="" type="text">
                </div> 
                <div class="form-group col-md-3">
                    <label for="account_name">Last Execute Date</label>
                    <input disabled="disabled"  class="form-control" id="" value="{{ date('F d, Y @ h:i:A', strtotime($schedule->pay_schedule_last)) }}" placeholder="" type="text">
                </div> 
                <div class="form-group col-md-2">
                    <label for="account_name">{{ ucfirst($schedule->pay_schedule_key) }} Mode</label>
                    <select name="mode[{{ $schedule->pay_schedule_id  }}]" class="form-control check-mode">
                        <option {{ $schedule->pay_schedule_mode == "daily" ? "selected=selected" : "" }} value="daily">DAILY</option>
                        <option {{ $schedule->pay_schedule_mode == "weekly" ? "selected=selected" : "" }} value="weekly">WEEKLY</option>
                        <option {{ $schedule->pay_schedule_mode == "monthly" ? "selected=selected" : "" }} value="monthly">MONTHLY</option>
                    </select>
                </div>
                <div class="form-group col-md-2 weekly-mode">
                    <label for="account_name">Day of Week</label>
                    <select name="weekly[{{ $schedule->pay_schedule_id  }}]" class="form-control">
                        @foreach($_days as $key => $day)
                        <option {{ $schedule->pay_schedule_weekly == $day ? "selected=selected" : "" }} value="{{ $day }}">{{ strtoupper($day) }}</option>
                        @endforeach
                    </select>
                </div> 
                <div class="form-group col-md-2 monthly-mode hidden">
                    <label for="account_name">Day of Month</label>
                    <select name="monthly[{{ $schedule->pay_schedule_id  }}]" class="form-control">
                        @for($ctr=1; $ctr<=28; $ctr++)
                        <option {{ $schedule->pay_schedule_montly == $ctr ? "selected=selected" : "" }} value="{{ $ctr }}" >{{ ordinal($ctr) }}</option>
                        @endfor
                    </select>
                </div> 
                <div class="form-group col-md-2 daily-mode hidden">
                    <label for="account_name">&nbsp;</label>
                    <input type="text" disabled="disabled" class="form-control" name="">
                </div>
                <div class="form-group col-md-2">
                    <label for="account_name">Scheduled Time</label>
                    <select name="time[{{ $schedule->pay_schedule_id  }}]" class="form-control">
                        @foreach($_time as $key => $time)
                        <option {{ $schedule->pay_schedule_time == $key ? "selected=selected" : "" }} value="{{ $key }}">{{ $time }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-1">
                    <label for="account_name">&nbsp;</label>
                    <button link="/admin/utilities/paysched/execute?sked={{ $schedule->pay_schedule_id }}" type="button" class="btn btn-primary execute-process">Execute</button>
                </div> 
            </span>
            @endforeach 

        </form>
    </div>
@endsection

@section('script')
<script type="text/javascript" src="/resources/assets/admin/paysched.js"></script>
@endsection
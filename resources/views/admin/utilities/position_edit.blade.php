@extends('admin.layout')
@section('content')
    <div class="row header">
        <div class="title col-md-8">
            <h2><i class="fa fa-users"></i>  Edit Levels</h2>
        </div>
        <div class="buttons col-md-4 text-right">
            <button onclick="location.href='admin/utilities/position'" type="button" class="btn btn-default"><i class="fa fa-angle-double-left"></i> Back</button>
            <button onclick="$('#position-add-form').submit();" type="button" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
        </div>
    </div>
    <div class="col-md-12 form-group-container" style="overflow: hidden;">
                @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form id="position-add-form" class="form-horizontal" method="post">
            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="admin_position_id" id="rank_level" value="{{$position->admin_position_id}}">
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Position Name</label>
                
                <div class="col-sm-10">
                    <input type="text" class="form-control" id="admin_position_name" name="admin_position_name" value="{{ Session::get('_old_input')['admin_position_name'] ? Session::get('_old_input')['admin_position_name'] : $position->admin_position_name }}" required>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Position Level</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" id="admin_position_rank" name="admin_position_rank" value="{{ Session::get('_old_input')['admin_position_rank'] ? Session::get('_old_input')['admin_position_rank'] : $position->admin_position_rank }}" required>
                </div>
            </div> 
            <!--<div class="form-group">-->
            <!--    <label for="" class="col-sm-2 control-label">Modules</label>-->
            <!--    <div class="col-sm-10">-->
            <!--        @foreach($_module as $module)-->
            <!--        <div class="col-md-6">-->
            <!--            <label class="checkbox-inline">-->
            <!--                <input type="checkbox" name="module[]" value="{{$module->module_id}}" @if(in_array($module->module_id,$selected_position_module_array) && !in_array($module->module_id, (array)Request::old('module')) ) checked @endif  {{in_array($module->module_id, (array)Request::old('module'))  ? 'checked' : '' }}>{{$module->module_name}}-->
            <!--            </label>-->
            <!--        </div>-->
            <!--        @endforeach-->
            <!--    </div>-->
            <!--</div>-->
            <div class="form-group">
                <div class="col-md-12" style="margin:20px">
                <div class="page-header"><h3>Admin Permission</h3></div>
                     <?php $int =0;
                      ?>
                      @foreach($navs as $nav)
                      <ul class="col-md-3">
                        <li>{{$nav['name']}}<li><label class="checkbox-inline"><input type="checkbox" id="linkname{{$int}}" onclick="pageviewadd({{$int}})" name="" value="{{$nav['name']}}"
                          @foreach($admin_permission as $permission)
                          <?php $onee = $nav['name'];
                          $twoe = $permission->sub_page_name;
                          if($onee == $twoe){
                          echo 'checked';
                          break;
                          }
                          ?>
                          @endforeach
                        > Access 
                        @foreach($admin_permission as $permission)
                          <?php $onee = $nav['name'];
                          $twoe = $permission->sub_page_name;
                          if($onee == $twoe){
                          echo 'checked';
                          break;
                          }
                          ?>
                          @endforeach
                        
                        </label></li>
                        <ul>
                          @foreach($nav['submenu'] as $sub)
                          <li>{{$sub['label']}}:</li>
                          <ul>
                            @foreach($sub['action'] as $action)
                            
                            <li><label class="checkbox-inline"><input type="checkbox" id="permission{{$int}}" onclick="addpermission({{$int}})"name="checkboxes[name][{{$nav['name']}}][page][{{$sub['label']}}][action][{{$action}}]" value="{{$nav['name']}}|{{$sub['label']}}|{{$action}}|1|{{$sub['url']}}"
                              @foreach($admin_permission as $permission)
                              <?php
                              $one=$action;
                              $two =$permission->action;
                              $three = $nav['name'];
                              $four = $permission->page_name;
                              $five =  $sub['label'];
                              $six = $permission->sub_page_name;
                              if($five ==$six)
                              {
                              if($three== $four)
                              {
                              if ($one==$two)
                              {echo "checked";}
                              }
                              }
                              
                              
                              ?>
                              @endforeach
                            >{{$action}}</label></li>
                            <!-- <input type="hidden" value="{{$nav['name']}}|{{$sub['label']}}|{{$action}}|0|{{$sub['url']}}" name="checkboxesa[name][{{$nav['name']}}][page][{{$sub['label']}}][action][{{$action}}]"> -->
                            <input type="hidden" value="{{$nav['name']}}" id="page_name{{$int}}">
                            <input type="hidden" value="{{$action}}" id="action{{$int}}">
                            <input type="hidden" value="{{$sub['label']}}" id="page_link{{$int}}">
                            <input type="hidden" value="1" id="value{{$int}}">
                            <?php $int++ ?>
                            @endforeach
                          </ul>
                          @endforeach
                        </ul>
                      </li>
                    </ul>
                    @endforeach
                </div>
            </div>
        </form>
    </div>
@endsection
@section('script')
<script type="text/javascript" src="/resources/assets/admin/admin_level.js"></script>
@endsection
@extends('admin.layout')
@section('content')
	<div class="row">
		<div class="header">
			<div class="title col-md-6">
				<h2><i class="fa fa-share-alt"></i> Binary Processing</h2>
			</div>
			<div class="buttons col-md-6 text-right">
				<button onclick="location.href='/admin/transaction/binary/daily'" type="button" class="btn btn-default"><i class="fa fa-calendar-o"></i> Process Daily Binary</button>
			</div>
		</div>
	</div>
	<div class="col-md-12">
			<table id="table" class="table table-bordered">
				<thead>
					<tr class="text-center">
						<th>Date Processed</th>
						<th>Time</th>
						<th>Total Payout</th>
						<th>Total Flushout</th>
						<th>Number of Pairings</th>
						<th>View Information</th>
					</tr>
				</thead>
				<tbody>
					@if($_binary_admin_report->isEmpty())	
						<tr>
							<td colspan="6">NO PROCESSING YET</td>
						</tr>

					@else
						@foreach($_binary_admin_report as $binary_admin_report)
							<tr>
								<td>{{ date("F d, Y", strtotime($binary_admin_report->binary_admin_date)) }}</td>
								<td>{{ date("h:i A", strtotime($binary_admin_report->binary_admin_date)) }}</td>
								<td>{{ currency($binary_admin_report->binary_admin_amount) }}</td>
								<td>{{ currency($binary_admin_report->binary_admin_flushout) }}</td>
								<td>{{ number_format($binary_admin_report->binary_admin_pairing_count) }}</td>
								<td><a href="javascript:" onClick="breakdown({{$binary_admin_report->binary_admin_id}})">Breakdown</a></td>
							</tr>
						@endforeach
					@endif

				</tbody>
			</table>
	</div>
<div id="breakdown_modal" class="modal fade breakdown-modal" role="dialog">
    <div class="modal-dialog" style="width: 80%; max-height:80%; overflow-y: auto;">
        <!-- Modal content-->
        <div class="modal-content">
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    function breakdown(id)
    {
        $("#breakdown_modal").modal();
        $.ajax(
        {
          url: "/admin/transaction/binary/breakdown",
          type: "get",
          data: {id:id},
          success: function(data)
          {
            $(".modal-content").html(data);
            $(".modal-content").show();
          }
        })
        // alert(id);
    }
</script>
@endsection
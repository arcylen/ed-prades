@extends('admin.layout')
@section('content')
<div class="row">
	<div class="header">
		<div class="title col-md-6">
			<h2><i class="fa fa-share-alt"></i> Distribute SPV</h2>
		</div>
		<div class="buttons col-md-6 text-right">
			<!-- <button onclick="location.href='/admin/transaction/direct/daily'" type="button" class="btn btn-default"><i class="fa fa-calendar-o"></i> Process Daily Direct Referral</button> -->
		</div>
	</div>
</div>
<div class="col-md-12">
<input type="text" name="" value="1" class="hidden filter-class">
	<ul class="nav nav-tabs" id="myTab">
        <li class="nav active"><a data-content="0">Unused</a></li>
        <li class="nav"><a data-content="1">Used</a></li>
    </ul>
	<div class="load-table">
		
	</div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header" width="90%">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Voucher</h4>
      </div>
      <div class="modal-body">
		<iframe src="" id="showvoucheriframe"  frameborder="0" style="overflow:hidden;height:600px;width:100%;min-height: 100%; max-width: 100%;" ></iframe>
      </div>
    </div>
  </div>
</div>
@endsection
@section("script")
<script type="text/javascript" src="resources/assets/admin/distributespv.js"></script>
@endsection
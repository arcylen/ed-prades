<!DOCTYPE html>
<html>
<head> </head>
<body>
<div style="font-family: arial; font-size: 16px;">
    <div style="background-color: #fdfdfd; padding: 0px 40px;">
       
        @if(isset($_pay_cheque_processed))
            <table style="text-align: center; margin-bottom: 60px; font-size: 14px; border: 1px solid #000;" width="100%" border="0" cellpadding="5px" cellspacing="0">
                <thead style="background-color: #3689C9; color: #fff;">
                    <tr>
                        <td colspan="20"><div style="text-align: center; font-size: 20px; font-weight: bold;">SLOT NO. {{ formatslot($_pay_cheque_processed[0]['pay_cheque_slot']) }}</div></td>
                    </tr>
                </thead>
                <tbody>
                    @if(isset($_pay_cheque_batch_processed))
                        @foreach($_pay_cheque_batch_processed as $key => $value)
                         <tr >
                            <td colspan="4">{{ date("h:i A", strtotime($value['pay_cheque_batch_date'])) }}</td>
                            <td colspan="3">{{$value['pay_cheque_slot']}}</td>
                            <td colspan="3"></td>
                            <td colspan="3"></td>
                            <td colspan="4"></td>
                            <td colspan="3"></td>
                            <td></td>
                         </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
        @else
        <h4><center>No Report to be shown.</center></h4>
        @endif
    </div>
</div>
</body>
</html>

<div class="modal-body">
  <div class="details-header clearfix">
    <div class="pull-left">
      <!-- <img class="cart-icon" src="/assets/front/img/cart-but.png"> -->
      <div class="text">Pipeline Report</div>
    </div>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
  </div>
  <div class="table-holder">
  @if($_detail != null)
    <table class="table">
      <thead>
        <tr>
          <th>ACCOUNT NAME</th>
          <th>SLOT ID</th>
          <th>SETTINGS ID</th>
          <th>EARNED AMOUNT</th>
          <th>DATE</th>
          <th>ADMIN REPORT</th>
        </tr>
      </thead>
      <tbody>
        @foreach($_detail as $detail)
        <tr class="stripe">
          <td>{{$detail->account_name}}</td>
          <td>{{$detail->pipeline_slot_id}}</td>
          <td>{{$detail->pipeline_settings_id}}</td>
          <td>{{$detail->pipeline_earned_amount}}</td>
          <td>{{$detail->pipeline_date}}</td>
          <td>{{$detail->pipeline_admin_report}}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
    @else
      <h3>No data Available.</h3>
    @endif
  </div>
</div>
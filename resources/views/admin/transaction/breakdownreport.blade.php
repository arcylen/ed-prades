<div class="modal-body">
  <div class="details-header clearfix">
    <div class="pull-left">
      <!-- <img class="cart-icon" src="/assets/front/img/cart-but.png"> -->
      <div class="text">Breakdown Report</div>
    </div>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
  </div>
  <div class="table-holder">
  @if($_detail != null)
    <table class="table">
      <thead>
        <tr>
          <th>ACCOUNT NAME</th>
          <th>SLOT</th>
          <th>START LEFT</th>
          <th>START RIGHT</th>
          <th>TODAY LEFT</th>
          <th>TODAY RIGHT</th>
          <th>END LEFT</th>
          <th>END RIGHT</th>
          <th>FLUSH OUT</th>
          <th>EARNED</th>
          <th>DATE</th>
        </tr>
      </thead>
      <tbody>
        @foreach($_detail as $detail)
        <tr class="stripe">
          <td>{{$detail->account_name}}</td>
          <td>{{$detail->binary_slot_id}}</td>
          <td>{{$detail->binary_start_left}}</td>
          <td>{{$detail->binary_start_right}}</td>
          <td>{{$detail->binary_today_left}}</td>
          <td>{{$detail->binary_today_right}}</td>
          <td>{{$detail->binary_end_left}}</td>
          <td>{{$detail->binary_end_right}}</td>
          <td>{{currency($detail->binary_flushout)}}</td>
          <td>{{currency($detail->binary_earned)}}</td>
          <td>{{$detail->binary_date}}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
    @else
      <h3>No data Available.</h3>
    @endif
  </div>
</div>
<table id="table" class="table table-bordered">
	<thead>
		<tr class="text-center">
			<th>Date Processed</th>
			<th>Account Name</th>
			<th>Total SPV</th>
			<th>Total Amount</th>
			<th>Voucher</th>
			<th></th>
		</tr>
		@if($_voucher != null)
		@foreach($_voucher as $voucher)
		<tr>
			<td>{{date("F d, Y",strtotime($voucher->created_at))}}</td>
			<td>{{$voucher->account_name}}</td>
			<td>{{number_format($voucher->unilevel_pts,2)}}</td>
			<td>PHP {{$voucher->total_amount}}</td>
			<td><a style="cursor: pointer;" class="view-voucher" voucher-id="{{$voucher->voucher_id}}">View Voucher</a></td>
			@if($filter == 1)
			<td>Distributed</td>
			@else
			<td><a href="admin/transaction/distribute/{{$voucher->voucher_id}}" class="btn btn-primary">Distribute</a></td>
			@endif
		</tr>
		@endforeach
		@else
		<tr><td colspan="5" class="text-center"> No data available in table </td></tr>
		@endif
	</thead>
</table>
<center>{!! $_voucher->render() !!}</center>

<script type="text/javascript">
	  $('#table').on('click', '.view-voucher', function(event) {
   		event.preventDefault();
   		var v_id = $(this).attr('voucher-id');
   		$('.email-voucher').attr('voucher-id', v_id);
   		$('#voucher-prod-container').load('admin/transaction/sales/process/sale_or?voucher_id='+v_id);
   	// 	modal.open();
   		document.getElementById("showvoucheriframe").src="admin/transaction/sales/process/sale_or?voucher_id=" + v_id;
		 //alert(hash);
		$("#myModal").modal('toggle')
   });
</script>
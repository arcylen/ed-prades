@extends('admin.layout')
@section('content')
	<div class="row">
		<div class="header">
			<div class="title col-md-6">
				<h2><i class="fa fa-share-alt"></i> Indirect Referral Processing</h2>
			</div>
			<div class="buttons col-md-6 text-right">
				<button onclick="location.href='/admin/transaction/direct/daily'" type="button" class="btn btn-default"><i class="fa fa-calendar-o"></i> Process Daily Indirect Referral</button>
			</div>
		</div>
	</div>
	<div class="col-md-12">
			<table id="table" class="table table-bordered">
				<thead>
					<tr class="text-center">
						<th>Date Processed</th>
						<th>Time</th>
						<th>Total Payout</th>
						<th>View Information</th>
					</tr>
				</thead>
				<tbody>
					@if($_indirect_report == null)	
						<tr>
							<td colspan="5">NO PROCESSING YET</td>
						</tr>

					@else
						@foreach($_indirect_report as $direct_report)
							<tr>
								<td>{{ date("F d, Y", strtotime($direct_report->admin_report_date)) }}</td>
								<td>{{ date("h:i A", strtotime($direct_report->admin_report_date)) }}</td>
								<td>{{ currency($direct_report->admin_report_amount) }}</td>
								
								<td><a href="javascript:" onClick="breakdown({{$direct_report->admin_report_id}})">Breakdown</a></td>
							</tr>
						@endforeach
					@endif

				</tbody>
			</table>
	</div>
	<div id="breakdown_modal" class="modal fade breakdown-modal" role="dialog">
    <div class="modal-dialog" style="width: 80%; max-height:80%; overflow-y: auto;">
        <!-- Modal content-->
        <div class="modal-content">
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    function breakdown(id)
    {
    	$(".modal-content").html("");
        $("#breakdown_modal").modal();
        $.ajax(
        {
          url: "/admin/transaction/indirect/breakdown",
          type: "get",
          data: {id:id},
          success: function(data)
          {
            $(".modal-content").html(data);
            $(".modal-content").show();
          }
        })
        // alert(id);
    }
</script>
@endsection
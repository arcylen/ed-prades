<!DOCTYPE html>
<html>
<head >  
<div align="center" style="height: 70px;">
            <img style="height: 65px;object-fit: contain;"  src="http://{{$_SERVER['SERVER_NAME']}}/assets/front/img/logo.png">
        </div>
</head>
<body>
<div style="font-family: arial; font-size: 16px;">
    
    @if(isset($_slot))
    @foreach($_slot as $slot)
        @if($slot["_pay_cheque"])
            <div style="background-color: #fdfdfd; padding: 0px 40px;">
                
                <table style="text-align: center; margin-bottom: 60px; font-size: 14px; border: 1px solid #000;" width="100%" border="0" cellpadding="5px" cellspacing="0">

                <thead style="background-color: #3689C9; color: #fff;">

                    <tr>
                        <td colspan="20"><div style="text-align: center; font-size: 20px; font-weight: bold;">{{strtoupper($owner_name)}}</div></td>
                    </tr>
                    <tr>
                        <td colspan="20"><div style="text-align: center; font-size: 20px; font-weight: bold;">SLOT NO. {{ formatslot($slot["slot_id"]) }}</div></td>
                    </tr>
                </thead>

                @foreach($slot["_pay_cheque"] as $pay_cheque)
                    <!-- BINARY -->
                    @if($pay_cheque["pay_cheque_batch_reference"] == "binary")
                    
                        <tbody> 
                            <tr>
                                <td style="font-weight: bold; font-size: 16px; background-color: #aaa; color: #fff;" colspan="20">Binary Report</td>
                            </tr> <?php $sum = 0; ?> 
                            @foreach($pay_cheque["_binary_report"] as $binary_report)
                                    <tr style="background-color: #ddd;">
                                        <td colspan="3">Date</td>
                                        <td colspan="3">Earned Reason</td>
                                        <td colspan="2">Earned Left</td>
                                        <td colspan="2">Earned Left2</td>
                                        <td colspan="2">Earned Right</td>
                                        <td colspan="2">Earned Righ2</td>
                                        <td colspan="3">Account Name</td>
                                        <td colspan="3">SLOT NO. </td>
                                    </tr>
                                @foreach($binary_report["breakdown"] as $breakdown)
                                    <tr >
                                        <td colspan="3">{{ date("h:i A", strtotime($breakdown["breakdown_time"])) }}</td>
                                        <td colspan="3">{{ $breakdown["binary_earned_reason"] }}</td>
                                        <td colspan="2">{{ $breakdown["binary_earned_left"] }}</td>
                                        <td colspan="2">{{ $breakdown["binary_earned_left2"] }}</td>
                                        <td colspan="2">{{ $breakdown["binary_earned_right"] }}</td>
                                        <td colspan="2">{{ $breakdown["binary_earned_right2"] }}</td>
                                        <td colspan="3">{{ $breakdown["account_name"] }}</td>
                                        <td colspan="3">SLOT NO. {{ $breakdown["binary_slot_id_cause"] }}</td>
                                    </tr>
                                @endforeach
                                    <tr>
                                        <td colspan="20" style="background-color: white;"><br></td>
                                    </tr>
                                    <tr>
                                    <td style="font-weight: bold; font-size: 16px; background-color: #aaa; color: #fff;" colspan="20">Breakdown</td>
                                    </tr>
                                    <tr style="background-color: #ddd;">
                                        <td colspan="2">Date</td>
                                        <td colspan="2">START</td>
                                        <td colspan="2">START</td>
                                        <td colspan="2"><b>Today Left</b></td>
                                        <td colspan="2"><b>Today Right</b></td>
                                        <td colspan="2">END</td>
                                        <td colspan="2">END</td>
                                        <td colspan="3">FLUSHOUT</td>
                                        <td colspan="3" style="text-align: right; color: #000;">Earned</td>
                                    </tr>
                                @foreach($binary_report["breakdown"] as $breakdown)
                                    
                                    <tr >
                                        <td colspan="2">{{ date("F d, Y", strtotime($breakdown["breakdown_time"])) }}</td>
                                        <td colspan="2">LEFT <br>{{ $breakdown["binary_start_left"] }}</td>
                                        <td colspan="2">RIGHT <br>{{ $breakdown["binary_start_right"] }}</td>
                                        <td colspan="2"><b>- 0 -</b></td>
                                        <td colspan="2"><b>- 0 -</b></td>
                                        <td colspan="2">LEFT <br>{{ $breakdown["binary_end_left"] }}</td>
                                        <td colspan="2">RIGHT <br>{{ $breakdown["binary_end_right"] }}</td>
                                        <td colspan="3"></td>
                                        <td colspan="3" style="text-align: right; color: #000;"></td>
                                    </tr>
                                    <tr >
                                        <td colspan="2"></td>
                                        <td colspan="2">LEFT2 <br>{{ $breakdown["binary_start_left2"] }}</td>
                                        <td colspan="2">RIGHT2 <br>{{ $breakdown["binary_start_right2"] }}</td>
                                        <td colspan="2"><b>- 0 -</b></td>
                                        <td colspan="2"><b>- 0 -</b></td>
                                        <td colspan="2">LEFT2 <br>{{ $breakdown["binary_end_left2"] }}</td>
                                        <td colspan="2">RIGHT2 <br>{{ $breakdown["binary_end_right2"] }}</td>
                                        <td colspan="3">FLUSHOUT <br> <span style="color: red;">{{ currency(0) }}</span></td>
                                        <td colspan="3" style="text-align: right; color: #000;">{{ currency(0) }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="20" style="background-color: #ddd;"><hr></td>
                                    </tr>
                                @endforeach

                                <tr style="font-weight: bold; background-color: #eee;">
                                    <td colspan="2">{{ date("F d, Y", strtotime($binary_report["binary_date"])) }}</td>
                                    <td colspan="2">START LEFT <br>{{ $binary_report["binary_start_left"] }}</td>
                                    <td colspan="2">START RIGHT <br>{{ $binary_report["binary_start_right"] }}</td>
                                    <td colspan="2"><b>- {{ $binary_report["binary_today_left"] + $binary_report["binary_today_left2"] }} -</b></td>
                                    <td colspan="2"><b>- {{ $binary_report["binary_today_right"] + $binary_report["binary_today_right2"] }} -</b></td>
                                    <td colspan="2">END LEFT <br>{{ $binary_report["binary_end_left"] }}</td>
                                    <td colspan="2">END RIGHT <br>{{ $binary_report["binary_end_right"] }}</td>
                                    <td colspan="3">FLUSHOUT <br> <span style="color: red;">{{ currency($binary_report["binary_flushout"]) }}</span></td>
                                    <td colspan="3" style="text-align: right; color: #000;">{{ currency($binary_report["binary_earned"]) }}</td>
                                    <?php $sum =  $sum + $binary_report["binary_earned"]; ?>
                                </tr>
                            @endforeach
                            <tr style="background-color: #ddd;">
                                <td colspan="20" style="text-align: right; font-weight: bold;"><hr></td>
                            </tr>
                            
                            <tr style="background-color: #ddd;">
                                <td colspan="20" style="text-align: right; font-weight: bold;">{{ strtoupper($pay_cheque["pay_cheque_batch_reference"]) }} TOTAL: <u style="color: green">{{ currency($pay_cheque["pay_cheque_amount"]) }}</u></td>
                            </tr>
                        </tbody>
  
                    @endif
                    <!-- DIRECT -->
                    @if($pay_cheque["pay_cheque_batch_reference"] == "Referral Bonus")
                   
                        <tbody> 
                            <tr>
                                <td style="font-weight: bold; font-size: 16px; background-color: #aaa; color: #fff;" colspan="20">Referral Bonus Report</td>
                            </tr> <?php $sum = 0; ?>
                            @foreach($pay_cheque["_direct_report"] as $direct_report)
                                @if($direct_report)
                                <tr style="background-color: #eee;">
                                    <td colspan="5">{{ date("F d, Y", strtotime($direct_report["direct_date"])) }}</td>
                                    <td colspan="5">{{ $direct_report["account_name"] }}</td>
                                    <td colspan="5">SLOT NO. {{ $direct_report["direct_child"] }}</td>
                                    <td style="text-align: right; font-weight:bold;" colspan="5">{{ currency($direct_report["direct_amount"]) }}</td>
                                </tr><?php $sum = $sum + $direct_report["direct_amount"]; ?>
                                @endif
                            @endforeach
                            <tr style="background-color: #ddd;">
                                <td colspan="20" style="text-align: right; font-weight: bold;">{{ strtoupper($pay_cheque["pay_cheque_batch_reference"]) }} TOTAL: <u style="color: green">{{ currency($sum) }}</u></td>
                            </tr>
                        </tbody>
         
                    @endif
                    <!-- DSB -->
                    @if($pay_cheque["pay_cheque_batch_reference"] == "Direct Sales Bonus")
                   
                        <tbody> 
                            <tr>
                                <td style="font-weight: bold; font-size: 16px; background-color: #aaa; color: #fff;" colspan="20">Direct Sales Bonus Report</td>
                            </tr> <?php $sum = 0; ?>
                            @foreach($pay_cheque["_dsb_report"] as $dsb_report)
                                @if($dsb_report)
                                <tr style="background-color: #eee;">
                                    <td colspan="5">{{ date("F d, Y", strtotime($dsb_report["dsb_date"])) }}</td>
                                    <td colspan="5">{{ $dsb_report["account_name"] }}</td>
                                    <td colspan="5">SLOT NO. {{ $dsb_report["dsb_child"] }}</td>
                                    <td style="text-align: right; font-weight:bold;" colspan="5">{{ currency($dsb_report["dsb_amount"])}}</td>
                                </tr><?php $sum = $sum + $dsb_report["dsb_amount"]; ?>
                                @endif
                            @endforeach
                            <tr style="background-color: #ddd;">
                                <td colspan="20" style="text-align: right; font-weight: bold;">{{ strtoupper($pay_cheque["pay_cheque_batch_reference"]) }} TOTAL: <u style="color: green">{{ currency($sum) }}</u></td>
                            </tr>
                        </tbody>
         
                    @endif
                    <!-- INDIRECT -->
                    @if($pay_cheque["pay_cheque_batch_reference"] == "Indirect Sales Bonus")
                
                        <tbody> 
                            <tr>
                                <td style="font-weight: bold; font-size: 16px; background-color: #aaa; color: #fff;" colspan="20">Indirect Sales Bonus Report</td>
                            </tr> <?php $sum = 0; ?>
                            @foreach($pay_cheque["_isb_report"] as $isb_report)
                                @if($isb_report)
                                <tr style="background-color: #eee;">
                                    <td colspan="4">{{ date("F d, Y", strtotime($isb_report["isb_date"])) }}</td>
                                    <td colspan="4">{{ $isb_report["account_name"] }}</td>
                                    <td colspan="4">SLOT NO. {{ $isb_report["isb_child"] }}</td>
                                    <td colspan="4">GENERATION {{ $isb_report["isb_level"] }}</td>
                                    <td style="text-align: right; font-weight:bold;" colspan="4">{{ currency($isb_report["isb_amount"]) }}</td>
                                </tr><?php $sum = $sum + $isb_report["isb_amount"]; ?>
                                @endif
                            @endforeach
                            <tr style="background-color: #ddd;">
                                <td colspan="20" style="text-align: right; font-weight: bold;">{{ strtoupper($pay_cheque["pay_cheque_batch_reference"]) }} TOTAL: <u style="color: green">{{ currency($sum) }}</u></td>
                            </tr>
                        </tbody>
                    @endif
                    <!--ROYALTY BONUS-->

                    @if($pay_cheque["pay_cheque_batch_reference"] == "Royalty Bonus")
                
                        <tbody> 
                            <tr>
                                <td style="font-weight: bold; font-size: 16px; background-color: #aaa; color: #fff;" colspan="20">Royalty Bonus Report</td>
                            </tr> <?php $sum = 0; ?>
                            @foreach($pay_cheque["_rb_report"] as $rb_report)
                                @if($rb_report)
                                <tr style="background-color: #eee;">
                                    <td colspan="4">{{ date("F d, Y", strtotime($rb_report["rb_date"])) }}</td>
                                    <td colspan="4">{{ $rb_report["account_name"] }}</td>
                                    <td colspan="4">{{ $rb_report["rb_position"] }}</td>
                                    <td colspan="4">SLOT NO. {{ $rb_report["rb_sponsor_id"] }}</td>
                                    <td style="text-align: right; font-weight:bold;" colspan="4">{{ currency($rb_report["rb_bonus"]) }}</td>
                                </tr><?php $sum = $sum + $rb_report["rb_bonus"]; ?>
                                @endif
                            @endforeach
                            <tr style="background-color: #ddd;">
                                <td colspan="20" style="text-align: right; font-weight: bold;">{{ strtoupper($pay_cheque["pay_cheque_batch_reference"]) }} TOTAL: <u style="color: green">{{ currency($sum) }}</u></td>
                            </tr>
                        </tbody>
                    @endif
                     <!-- GSB -->
                    @if($pay_cheque["pay_cheque_batch_reference"] == "Group Sales Bonus")
                
                        <tbody> 
                            <tr>
                                <td style="font-weight: bold; font-size: 16px; background-color: #aaa; color: #fff;" colspan="20">Group Sales Bonus Report</td>
                            </tr> 
                            <tr>
                                <td style="font-weight: bold; font-size: 16px; background-color: #aaa; color: #fff;" colspan="20">{{$position}}</td>
                            </tr> <?php $sum = 0; ?>
                            @foreach($pay_cheque["_gsb_report"] as $gsb_report)
                                @if($gsb_report)
                                <tr style="background-color: #eee;">
                                    <!-- <td colspan="4"> date("F d, Y", strtotime($gsb_report["gsb_date"]))</td> -->
                                    <!-- <td colspan="4">{{ $gsb_report["account_name"] }}</td> -->
                                    <td colspan="4">LEVEL {{ $gsb_report["total_gsb_level"] }}</td>
                                    <td colspan="4">{{ $gsb_report["gsb_percentage"] }} %</td>
                                    <td colspan="4">{{ $gsb_report["total_no_of_slots"] }} Slots</td>
                                    <td colspan="4"> {{ number_format($gsb_report["total_spv"],2) }} SPV</td>
                                    <td style="text-align: right; font-weight:bold;" colspan="4">{{ currency($gsb_report["total_earned"]) }}</td>
                                </tr><?php $sum = $sum + $gsb_report["total_earned"]; ?>
                                @endif
                            @endforeach
                            <tr style="background-color: #ddd;">
                                <td colspan="20" style="text-align: right; font-weight: bold;">{{ strtoupper($pay_cheque["pay_cheque_batch_reference"]) }} TOTAL: <u style="color: green">{{ currency($sum) }}</u></td>
                            </tr>
                        </tbody>

                        <table style="text-align: center; margin-bottom: 60px; font-size: 14px; border: 1px solid #000;" width="100%" border="0" cellpadding="5px" cellspacing="0">
                            <thead style="background-color: #3689C9; color: #fff;">
                                <tr>
                                    <td colspan="20"><div style="text-align: center; font-size: 20px; font-weight: bold;">SLOT NO. {{ formatslot($slot["slot_id"]) }}</div></td>
                                </tr>
                            </thead>
                            <tbody > 
                                <tr>
                                    <td style="font-weight: bold; font-size: 16px; background-color: #aaa; color: #fff;" colspan="20">Group Sales Bonus History</td>
                                </tr> <?php $sum2 = 0; ?>
                                @foreach($pay_cheque["_gsb_history"] as $gsb_history)
                                    @if($gsb_history)
                                    <tr style="background-color: #eee;">
                                        <td colspan="3">LEVEL {{ $gsb_history["level"] }}</td>
                                        <td colspan="4">{{ $gsb_history["account_name"] }}  </td>
                                        <td colspan="2">{{ $gsb_history["percentage"] }} %</td>
                                        <td colspan="3">Slot #{{ $gsb_history["earned_from"] }} </td>
                                        <td colspan="4"> {{ number_format($gsb_history["gsb_spv"],2) }} SPV</td>
                                        <td style="text-align: right; font-weight:bold;" colspan="4">{{ currency($gsb_history["gsb_earned"]) }}</td>
                                    </tr><?php $sum2 = $sum2 + $gsb_history["gsb_earned"]; ?>
                                    @endif
                                @endforeach
                                <tr style="background-color: #ddd;">
                                    <td colspan="20" style="text-align: right; font-weight: bold;">{{ strtoupper($pay_cheque["pay_cheque_batch_reference"]) }} TOTAL: <u style="color: green">{{ currency($sum2) }}</u></td>
                                </tr>
                            </tbody>
                        </table>    

                    @endif

                    <!-- PIPELINE REPORT -->
                    @if($pay_cheque["pay_cheque_batch_reference"] == "pipeline")
               
                        <tbody> 
                            <tr>
                                <td style="font-weight: bold; font-size: 16px; background-color: #aaa; color: #fff;" colspan="20">Pipeline Report</td>
                            </tr> 
                            <?php $sum = 0; ?>
                            @foreach($pay_cheque["_pipeline_report"] as $pipeline_report)
                                @if($pipeline_report)
                                <tr style="background-color: #eee;">
                                    <td colspan="7">{{ date("F d, Y", strtotime($pipeline_report["pipeline_date"])) }}</td>
                                    <td colspan="6">Graduation from Pipeline No. {{ $pipeline_report["pipeline_label"] }}</td>
                                    <td style="text-align: right; font-weight:bold;" colspan="7">{{ currency($pipeline_report["pipeline_earned_amount"]) }}</td>
                                </tr>
                                 <?php $sum = $sum + $pipeline_report["pipeline_earned_amount"]; ?>
                                @endif
                            @endforeach
                            <tr style="background-color: #ddd;">
                                <td colspan="20" style="text-align: right; font-weight: bold;">{{ strtoupper($pay_cheque["pay_cheque_batch_reference"]) }} TOTAL: <u style="color: green">{{ currency($sum) }}</u></td>
                            </tr>
                        </tbody>

                    @endif
                    @if($pay_cheque["pay_cheque_batch_reference"] == "wallet")
               
                        <tbody> 
                            <tr>
                                <td style="font-weight: bold; font-size: 16px; background-color: #aaa; color: #fff;" colspan="20">Wallet Report</td>
                            </tr> 
                            <?php $sum = 0; ?>
                                <tr style="background-color: #eee;">
                                    <td colspan="5">Wallet Amount:</td>
                                    <td colspan="5">{{ currency($pay_cheque['pay_cheque_amount']) }}</td>
                                    <td colspan="5">Date:</td>
                                    <td colspan="5">{{ $pay_cheque['pay_cheque_processed_date'] }}</td>
                                </tr>
                                 <?php $sum = $sum + $pay_cheque["pay_cheque_amount"]; ?>
                            <tr style="background-color: #ddd;">
                                <td colspan="20" style="text-align: right; font-weight: bold;">{{ strtoupper($pay_cheque["pay_cheque_batch_reference"]) }} TOTAL: <u style="color: green">{{ currency($sum) }}</u></td>
                            </tr>
                        </tbody>

                    @endif
                @endforeach
                </table>
            </div>
        @endif
    @endforeach
    @endif
    @if($payment_option)
                
                <table style="text-align: center; margin-bottom: 60px; font-size: 14px; border: 1px solid #000;" width="100%" border="0" cellpadding="5px" cellspacing="0">
                <thead style="background-color: #3689C9; color: #fff;">
                    <tr>
                        <td colspan="20"><div style="text-align: center; font-size: 20px; font-weight: bold;">Encashment Details</div></td>
                    </tr>
                </thead>
                <tbody>
                    @if($payment_option->bank_type == 1)
                    <tr >
                        
                        
                       <td colspan="5"><div style="background-color: #eee;">{{$payment_option->bank_name}}</div></td>
                       <td colspan="5"><div style="background-color: #eee;">{{$payment_option->bank_branch}}</div></td>
                       <td colspan="5"><div style="background-color: #eee;">{{$payment_option->bank_account_name}}</div></td>
                       <td colspan="5"><div style="background-color: #eee;">{{$payment_option->bank_account_number}}</div></td>
                    </tr>
                    @else
                    <tr>
                       <td colspan="10"><div style="background-color: #eee;">Cheque</div></td>
                       <td colspan="10"><div style="background-color: #eee;">{{$payment_option->bank_account_name}}</div></td>
                    </tr>
                    @endif
                    <tr>
                       <td colspan="10"><div style="background-color: #eee;">Proccessing Fee:</div></td>
                       <td colspan="10"><div style="background-color: #eee;">PHP {{$payment_option->processing_fee}}</div></td>
                       
                    </tr>
                    <tr>
                        <td colspan="10"><div style="background-color: #eee;">Withholding Tax Fee:</div></td>
                       <td colspan="10"><div style="background-color: #eee;">PHP {{$payment_option->withholding_tax}}</div></td>
                    </tr>
                    <tr>
                        <td colspan="10"><div style="background-color: #eee;">Final Total:</div></td>
                       <td colspan="10"><div style="background-color: #eee;">PHP <?php $sum2 = $sum - $payment_option->processing_fee; $sum3 = $sum2 -$payment_option->withholding_tax; ?> {{$sum3}} </div></td>
                    </tr>
                </tbody>
    @endif
</div>
</body>
</html>

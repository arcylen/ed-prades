<div class="modal-body">
  <div class="details-header clearfix">
    <div class="pull-left">
      <!-- <img class="cart-icon" src="/assets/front/img/cart-but.png"> -->
      <div class="text">Indirect Report</div>
    </div>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
  </div>
  <div class="table-holder">
  
    <table class="table">
      <thead>
        <tr>
          <th>ACCOUNT NAME</th>
          <th>INDIRECT DATE</th>
          <th>AMOUNT</th>
          <th>REGISTREE</th>
        </tr>
      </thead>
      <tbody>
        @if($_detail == null)
        <tr>
          <td colspan="5">NO REPORT AVAILABLE</td>
        </tr>
        @else 
        @foreach($_detail as $detail)
        <tr class="stripe">
          <td>{{$detail->account_name}}</td>
          <td>{{$detail->indirect_date}}</td>
          <td>{{$detail->indirect_amount}}</td>
          <td>{{$detail->indirect_registree}}</td>
        </tr>
        @endforeach
        @endif
      </tbody>
    </table>
  </div>
</div>
@extends('admin.layout')
@section('content')
    <div class="row">
		<div class="header">
			<div class="title col-md-8">
				<h2><i class="fa fa-share-alt"></i> Unilevel Monthly Distribution</h2>
			</div>
			<div class="buttons col-md-4 text-right">
				<form method="POST">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
				
					<button type="submit" class="btn btn-primary hide" name="sbmt">Distribute Unilevel Points</button> 
				  
				</form>
			</div>
		</div>
	</div>
	<div class="row">
	    <!--@if($slot)	-->
     <!--   	<div class="col-md-12">-->
     <!--   			<table id="table" class="table table-bordered">-->
     <!--   				<thead>-->
     <!--   					<tr class="text-center">-->
     <!--   						<th>Slot #</th>-->
     <!--   						<th>Owner</th>-->
     <!--   						<th>Personal PV</th>-->
     <!--   						<th>Group PV</th>-->
     <!--   						<th>Multiplier</th>-->
     <!--   						<th>PV to Wallet</th>-->
     <!--   					</tr>-->
     <!--   				</thead>-->
     <!--   				<tbody>-->
     <!--   					@foreach($slot as $s)-->
     <!--   					<tr>-->
     <!--   						<td>{{$s->slot_id}}</td>-->
     <!--   						<td>{{$s->account_name}}</td>-->
     <!--   						<td>{{$s->slot_personal_points}}</td>-->
     <!--   						<td>{{$s->slot_group_points}}</td>-->
     <!--   						<td>{{$s->multiplier}}</td>-->
     <!--   						<td>{{($s->slot_group_points) * $s->multiplier}}</td>-->
     <!--   					</tr>-->
     <!--   					@endforeach-->
     <!--   				</tbody>-->
     <!--   			</table>-->
     <!--   			<center> {!! $slot->render() !!}</center>-->
     <!--   	</div>-->
     <!--   @endif-->
    			<div class="tableuni">
                    <table class="table table-striped">
                        <thead>
                        	<tr>
                        		<td colspan="3">
                        			<center>Group PV</center>
                        		</td>
                        	</tr>
                            <tr>
                                <td>Level</td>
                                <td>Percentage</td>
                                <td>Points Earned</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($unilevelsettings as $key => $value)
                            <tr>
                                <td>{{$value->level}}</td>
                                <td>{{$value->value}} %</td>
                                <td>{{$unilevel_log[$key]}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
	</div>
	<div class="row">
	    <table class="table">
	        <thead>
	            <tr>
	                <td colspan="3">Personal Pv</td>
	            </tr>
	            <tr>
                    <td>Points</td>
                    <td>Sponsor Slot#</td>
                    <td>Product Pin</td>
                </tr>
	        </thead>
	        <tbody>
	            @foreach($unilevel_log_personal as $key => $value)
	                <tr>
	                    <td>{{$value->unilevel_points}}</td>
	                    <td>{{$value->unilevel_recipeint}}</td>
	                    <td>{{$value->unilevel_product_pin}}</td>
	                </tr>
	            @endforeach
	        </tbody>
	    </table>
	    <center> {!! $unilevel_log_personal->render() !!}</center>
	</div>
@endsection
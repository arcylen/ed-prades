<div class="modal-body">
  <div class="details-header clearfix">
    <div class="pull-left">
      <!-- <img class="cart-icon" src="/assets/front/img/cart-but.png"> -->
      <div class="text">Direct Report</div>
    </div>
    <button type="button" class="close" data-dismiss="modal">&times;</button>
  </div>
  <div class="table-holder">
  
    <table class="table">
      <thead>
        <tr>
          <th>ACCOUNT NAME</th>
          <th>DIRECT DATE</th>
          <th>AMOUNT</th>
        </tr>
      </thead>
      <tbody>
        @if($_detail == null)
        <tr>
          <td colspan="5">NO REPORT AVAILABLE</td>
        </tr>
        @else 
        @foreach($_detail as $detail)
        <tr class="stripe">
          <td>{{$detail->account_name}}</td>
          <td>{{$detail->direct_date}}</td>
          <td>{{$detail->direct_amount}}</td>
        </tr>
        @endforeach
        @endif
      </tbody>
    </table>
  </div>
</div>
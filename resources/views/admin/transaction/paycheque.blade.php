@extends('admin.layout')
@section('content')
<div class="row">
    <div class="header">
        <div class="title col-md-6">
            <h2><i class="fa fa-share-alt"></i> Paycheque</h2>
        </div>
    </div>
</div>
<div class="row">
    <form method="post" action="/admin/transaction/paycheque/submit">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <table class="table">
            <tr>
                <td>Proccessing Fee:</td>
                <td><input type="number" name="processing_fee" class="form-control" value="{{$processing_fee}}" /></td>
                <td>withholding Tax(Percent):</td>
                <td><input type="number" name="withholding_tax" class="form-control" value="{{$withholding_tax}}" /></td>
                <td><input type="submit" class="btn btn-success" value="Update Proccessing Fee"></input></td>
            </tr>
        </table>
    </form>
</div>
<div class="container">
    <ul class="nav nav-tabs" id="myTab">
        <li class="nav active"><a href="#A" data-toggle="tab" onClick="clickas(#A)">Requested</a></li>
        <li class="nav"><a href="#B" data-toggle="tab">Not Requested</a></li>
        <li class="nav"><a href="#C" data-toggle="tab">Released</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane fade in active" id="A">
         
            <table id="table" class="table table-bordered">
                <thead>
                    <tr class="text-center">
                        <th>ID</th>
                        <th>Account</th>
                        <th>Account Name</th>
                        <th>Pay Cheque Amount</th>
                        <th>View Paycheque</th>
                        <th>Release</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                   <!--  @if($_account->isEmpty())   
                        <tr>
                            <td colspan="3">NO PROCESSING YET</td>
                        </tr>
                    @else
                        @foreach($_account as $key => $account)
                            <tr>
                                <td>{{ $account->account_name }}</td>
                                <td>{{ currency($account->account_paycheque_total) }}</td>
                                <td><a target="_blank" href="admin/transaction/paycheque/{{ $account->account_id }}">VIEW PAY CHEQUE</a></td>
                                <td><a  href="admin/transaction/paychequeprocess/{{ $_proccess_id[$key] }}">PROCESS</a></td>
                            </tr>
                        @endforeach
                    @endif -->
                     @if($_pay_cheque->isEmpty())   
                        <tr>
                            <td colspan="6">NO PROCESSING YET</td>
                        </tr>
                    @else
                        @foreach($_pay_cheque as $key => $account)
                            <tr>
                                <td>{{ $account->pay_cheque_id }}</td>
                                <td>{{ $account->account_name}}</td>
                                <td>{{ $account->pay_cheque_batch_reference }}</td>
                                <td>{{ currency($account->pay_cheque_amount) }}</td>
                                <td><a id="{{$account->pay_cheque_id}}" onClick="unclick({{$account->pay_cheque_id}})" target="_blank" href="admin/transaction/paycheque/{{ $account->pay_cheque_id }}/{{$account->pay_cheque_slot}}">VIEW PAY CHEQUE</a></td>
                                <td><a  href="admin/transaction/paychequeprocess/{{ $account->pay_cheque_id }}">Release</a></td>
                                <td><a  href="admin/transaction/paychequedeny/{{ $account->pay_cheque_id }}">Deny</a></td>
                            </tr>
                        @endforeach
                    @endif
                
                </tbody>
            </table>
             <center>{!! $_pay_cheque->render() !!}</center>
        </div>
        <div class="tab-pane fade" id="B">
            <table id="table" class="table table-bordered">
                <thead>
                    <tr class="text-center">
                        <th>ID</th>
                        <th>Account</th>
                        <th>Account Name</th>
                        <th>Pay Cheque Amount</th>
                        <th>View Paycheque</th>
                        <th>Release</th>
                        
                        <!--<th></th>-->
                    </tr>
                </thead>
                <tbody>
                     @if($_pay_cheque_notrequested->isEmpty())   
                        <tr>
                            <td colspan="6">NO PROCESSING YET</td>
                        </tr>
                    @else
                        @foreach($_pay_cheque_notrequested as $key => $account)
                            <tr>
                                <td>{{ $account->pay_cheque_id }}</td>
                                <td>{{$account->account_name}}</td>
                                <td>{{ $account->pay_cheque_batch_reference }}</td>
                                <td>{{ currency($account->pay_cheque_amount) }}</td>
                                <td  ><a target="_blank" href="admin/transaction/paycheque/{{ $account->pay_cheque_id }}/{{$account->pay_cheque_slot}}">VIEW PAY CHEQUE</a></td>
                                <!--<td class="btn btn-primary" id="{{$account->pay_cheque_id}}" onClick="unclick({{$account->pay_cheque_id}})">Process</td>-->
                                <td><a id="{{$account->pay_cheque_id}}" onClick="unclick({{$account->pay_cheque_id}})" href="admin/transaction/paychequeprocess/{{ $account->pay_cheque_id }}">Release</a></td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
               
            </table>
            
        </div>
        <div class="tab-pane fade" id="C">
            <table id="table" class="table table-bordered">
                <thead>
                    <tr class="text-center">
                        <th>ID</th>
                        <th>Account</th>
                        <th>Account Name</th>
                        <th>Pay Cheque Amount</th>
                        <th>View Paycheque</th>
                        <th>Release</th>
                    </tr>
                </thead>
                <tbody>
                     @if($_pay_cheque_processed->isEmpty())   
                        <tr>
                            <td colspan="4">NO PROCESSING YET</td>
                        </tr>
                    @else
                        @foreach($_pay_cheque_processed as $key => $account)
                            <tr>
                                <td>{{ $account->pay_cheque_id }}</td>
                                <td>{{$account->account_name}}</td>
                                <td>{{ $account->pay_cheque_batch_reference }}</td>
                                <td>{{ currency($account->pay_cheque_amount) }}</td>
                                <td><a id="{{$account->pay_cheque_id}}" onClick="unclick({{$account->pay_cheque_id}})" target="_blank" href="admin/transaction/paycheque/{{ $account->pay_cheque_id }}/{{$account->pay_cheque_slot}}">VIEW PAY CHEQUE</a></td>
                                <td ><span class="success">Already Released</span></td>
                            </tr>
                        @endforeach
                    @endif
                </tbody>
            </table>
             <center></center>
        </div>
    </div>
</div>

<script type="text/javascript">
   
    // $('#myTab a').click(function(e) {
    //   e.preventDefault();
    //   $(this).tab('show');
    // });
    
    // store the currently selected tab in the hash value
    // $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
    //   var id = $(e.target).attr("href").substr(1);
    //   window.location.hash = id;
    // });
    function unclick(id){
        // alert(id);
        $('#' + id).attr('disabled', 'disabled');
        $('#' + id).addClass('hide');
    }
    // on load of the page: switch to the currently selected tab
    // var hash = window.location.hash;
    // $('#myTab a[href="' + hash + '"]').tab('show');
</script>
@endsection

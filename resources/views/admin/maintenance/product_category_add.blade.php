@extends('admin.layout')
@section('content')
    <div class="row header">
        <div class="title col-md-8">
            <h2><i class="fa fa-flag-checkered"></i> Add Product Category</h2>
        </div>
        <div class="buttons col-md-4 text-right">
            <button onclick="location.href='admin/maintenance/product_category'" type="button" class="btn btn-default"><i class="fa fa-angle-double-left"></i> Back</button>
            <button onclick="$('#country-add-form').submit();" type="button" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
        </div>
    </div>
    <div class="col-md-12 form-group-container" style="overflow: hidden;">
        <form id="country-add-form" enctype="multipart/form-data" class="form-horizontal" action="admin/maintenance/product_category/add_submit" method="post">
            <div class="form-group col-md-12">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Product Category Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="title" name="title" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">Product Category Description</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="description" name="description" required></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Product Category Color</label>
                    <div class="col-sm-10">
                        <select name="color" class="form-control">
                            <option value="blue">Blue</option>
                            <option value="red">Red</option>
                            <option value="green">Green</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Product Category Icon (PNG File Type)</label>
                    <div class="col-sm-10">
                        <img src="/resources/assets/img/1428733091.jpg" class="img-responsive" style="border: 2px solid #ddd; margin-bottom: 7.5px;">
                        <label class="btn btn-primary">Upload Image<input type="file" style="display: none;" name="image_file"></label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Product Category Banner (JPG File Type)</label>
                    <div class="col-sm-10">
                        <img src="/resources/assets/img/1428733091.jpg" class="img-responsive" style="border: 2px solid #ddd; margin-bottom: 7.5px;">
                        <label class="btn btn-primary">Upload Image<input type="file" style="display: none;" name="image_banner"></label>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

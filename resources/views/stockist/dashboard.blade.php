@extends('stockist.layout')
@section('content')
<style type="text/css">
	.divider {
    text-align:center;
}

.divider hr {
    margin-left:auto;
    margin-right:auto;
    width:40%;

}

.left {
    float:left;
}

.right {
    float:right;
}
</style>
<div class="col-md-6 alert-info">
	<div class="divider col-md-12">
    <hr class="left"/>Inventory<hr class="right" />
	</div>
	<div class="col-md-6 well warning"><i class="fa fa-list  fa-4x col-md-6" aria-hidden="true"></i><h3 class="col-md-6">@if($_product) {{ $_product }} </h3>@endif <h4 class="col-md-12">Product Inventory</h4></div>
	<div class="col-md-6 well warning"><i class="fa fa-list  fa-4x col-md-6" aria-hidden="true"></i><h3 class="col-md-6">@if($_package) {{ $_package }} </h3>@endif <h4 class="col-md-12">Package Inventory</h4></div>
</div>
<div class="col-md-6 alert-info">
	<div class="divider col-md-12">
    <hr class="left"/>Sales<hr class="right" />

	</div>
	<div class="col-md-6 well success"><i class="fa fa-money fa-4x col-md-6" aria-hidden="true"></i><h3 class="col-md-6">@if($order_processed) {{ $order_processed }} @else 0 @endif </h3> <h4 class="col-md-12">Processed</h4></div>
	<div class="col-md-6 well success"><i class="fa fa-money fa-4x col-md-6" aria-hidden="true"></i><h3 class="col-md-6">@if($order_unclaimed) {{ $order_unclaimed }} @else 0 @endif </h3> <h4 class="col-md-12">Unclaimed</h4></div>
</div>
<div class="col-md-6 alert-info">
	<div class="divider col-md-12">
    <hr class="left"/>Codes<hr class="right" />
	</div>
	<div class="col-md-6 well primary"><i class="fa fa-code  fa-4x col-md-6" aria-hidden="true"></i><h3 class="col-md-6">@if($membership) {{ $membership }} @else 0 @endif </h3> <h4 class="col-md-12">Unused</h4></div>
    <div class="col-md-6 well primary"><i class="fa fa-code  fa-4x col-md-6" aria-hidden="true"></i><h3 class="col-md-6">@if($membership_code_used) {{ $membership_code_used }} </h3> @endif <h4 class="col-md-12">Used</h4></div>
</div>
<!--<div class="col-md-6 alert-info">-->
<!--	<div class="divider col-md-12">-->
<!--    <hr class="left"/>Graphs<hr class="right" />-->
<!--	</div>-->
<!--</div>-->
@endsection
@section('script')
@endsection
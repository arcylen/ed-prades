@extends("front.layout")
@section("content")
<div class="intro">
	<div class="container">
		<div class="text">
			<div class="name">{{ isset($content_data['Banner Title (Updates)']->about_description) ? $content_data['Banner Title (Updates)']->about_description : "" }}</div>
			<div class="desc">{{ isset($content_data['Banner Sub-title (Updates)']->about_description) ? $content_data['Banner Sub-title (Updates)']->about_description : "" }}</div>
		</div>
	</div>
</div>
<div class="main-content">
	<div class="container clearfix">
		<div class="title">News</div>
		<div class="sub">{{ isset($content_data['News Sub-title (Updates)']->about_description) ? $content_data['News Sub-title (Updates)']->about_description : "" }}</div>
		<div class="news-holder">
			@foreach($_news as $news)
			<div class="row clearfix" style="margin-bottom: 25px;">
				<div class="col-sm-6">
					<img class="img" src="{{ $news->image_file }}" style="border: 1px solid #ccc;">
				</div>
				<div class="col-sm-6">
					<div class="name">{{ $news->news_title }}</div>
					<div class="date">{{ date("m / d / Y", strtotime($news->news_date)) }}</div>
					<div class="desc" style="line-height: 25px;">{{ substr($news->news_description, 0, 300) }}... <a href="/news?id={{ $news->news_id }}">Read More</a></div>
				</div>
			</div>
			@endforeach
		</div>
		<div class="title">Promotions</div>
		<div class="sub">{{ isset($content_data['Promotions Sub-title (Updates)']->about_description) ? $content_data['Promotions Sub-title (Updates)']->about_description : "" }}</div>
		<div class="product-holder">
			@foreach($_promotions as $promotions)
			<div>
				<div class="row clearfix">
					<div class="col-md-6">
						<div class="product-name">{{ $promotions->product_name }}</div>
						<div class="product-sub">{{ $promotions->sub_name }}</div>
						<div class="product-desc">{{ $promotions->description }}</div>
						<div class="product-tag">{{ $promotions->tagline }}</div>
					</div>
					<div class="col-md-6">
						<img class="img-responsive" src="{{ $promotions->image_file }}">
					</div>	
				</div>
			</div>
			@endforeach
		</div>
	</div>
	<div class="subscribe">
		<div class="subscribe-title">Get regular updates!</div>
		<div class="subscribe-sub">Subscribe to ask the BOSS.</div>
		<form method="post" action="/newsletter_submit">
			<input type="hidden" type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="form-group">
				<input type="text" class="form-control" name="full_name" placeholder="YOUR NAME">
			</div>
			<div class="form-group">
				<input type="text" class="form-control" name="email_address" placeholder="YOUR EMAIL">
			</div>
			<div class="form-group">
				<input type="text" class="form-control" name="phone_number" placeholder="YOUR PHONE NUMBER">
			</div>
			<div>
				<button class="btn">SUBSCRIBE</button>
			</div>
		</form>
	</div>
</div>
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="/assets/front/css/update.css">
@endsection
@section('script')
<script type="text/javascript" src="/assets/front/js/updates.js"></script>
@endsection
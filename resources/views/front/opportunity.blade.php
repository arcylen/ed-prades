@extends('front.layout')
@section('content')
<div class="intro">
	<div class="container">
		<div class="text">
			<div class="name">{{ isset($content_data['Banner Title (Opportunity)']->about_description) ? $content_data['Banner Title (Opportunity)']->about_description : "" }}</div>
			<div class="desc">{{ isset($content_data['Banner Sub-title (Opportunity)']->about_description) ? $content_data['Banner Sub-title (Opportunity)']->about_description : "" }}</div>
		</div>
	</div>
</div>
<div class="main-content">
	<div class="container">
		<div class="row clearfix">
			<div class="col-sm-4">
				<div class="side-bar">
					<div class="holder active" show="quick-start">Quick Start</div>
					<div class="holder" show="getting-started">Getting Started</div>
					<div class="holder" show="compensation-plan">Compensation Plan</div>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="content-holder" show="quick-start">
					<div class="quick-start">
						<div class="row-no-padding">
							<div class="col-sm-7">
								<div class="honto">Buy 1 Bottle <strong>Mangostanax (30 capsule)</strong></div>
								<div class="honto">Or</div>
								<div class="honto"><strong>TurmerIC (30 capsule)</strong></div>
								<div class="dakedo"><strong style="font-weight: 600;">For only PHP 500.00</strong></div>
								<div class="divider"></div>
							</div>
						</div>
						<div class="row-no-padding">
							<div class="col-sm-7">
								<div class="img"><img style="margin: auto;" class="img-responsive" src="/assets/front/img/produkto.png"></div>
								<div class="honto"><strong>And enjoy</strong></div>
								<div class="dakedo"><strong>5%-30%</strong></div>
								<div class="dakedo"><strong>Members discounts</strong></div>
								<div class="honto"><strong>on future purchases</strong></div>
							</div>
							<div class="col-sm-5"><img style="margin: auto;" class="img-responsive" src="/assets/front/img/trolol.png"></div>
						</div>
					</div>
				</div>
				<div class="content-holder hide" show="getting-started">
					<div class="getting-started">
						<div class="title">Get Started</div>
						<div class="desc">An individual who is at least 18 (eighteen) years or older (may vary depending on the laws of every country) can join BOSS. However, BOSS Corp. reserves the right not to accept an application or reject/return an application after acceptance. After personally executing the Distributor Application Form and purchase of any product of BOSS, a prospect becomes an independent distributor of the Company. The distributor will be given a computer number, which the distributor will use worldwide. The distributor can now sponsor anybody into their organization and this organization will remain the same in every country where BOSS does business. Distributorship is “One Member-One Account Policy”. 

Signing up for distributorship of another person without the latter’s knowledge is strictly prohibited and therefore will be considered an invalid application. Perpetrators will be subject for termination from distributorship. Moreover, perpetrators may be violating civil or criminal acts punishable by law in most countries. A prospect/new distributor cannot put friends, family members or anyone between him/her and the sponsor who showed him/her the business when he/she signs up as this will lead to cutting off bonuses and stealing from the sponsor. In the event this happens, the situation will be rectified and will be corrected and all bonuses lost must be paid back to their uplines. Failing to do so, the distributorship will be terminated. No sponsor is allowed to give permission to any prospect/new distributor to put distributors between him/her and the new prospect/distributor who was shown the business as this will also affect the upline(s)’ bonuses. The distributorship of the said sponsor will be suspended for one (1) year for this wrong leadership. We encourage the new distributor to start using products to see how good they are and if he/she does not like them, to return them and refund the money back from the Company. Unless the distributor liked our products, this company is not for him/ her. 

We do not want people to join this company just because of the compensation program. Proper MLM is selling and wholesaling good products where the distributors are proud of to their customers and distributors. We are not in the business of selling a marketing plan. Product knowledge is mandatory for all distributors. You should know what you are selling. </div>
					</div>
				</div>
				<div class="content-holder hide" show="compensation-plan">
					<div class="compensation-plan">
						<div class="title">Compensation Plan</div>
						<div class="desc">The structure uses the stair step/breakaway compensation system. All distributors start as Consultants and upon reaching the required group sales from designated network members, as well as when their personal sales volume increase, they get PROMOTED to Manager, Associate Director and Director.  

Upon reaching the Director's rank, a distributor will be known as Assistant Vice President, Vice President, Senior Vice President, Executive Vice President and President depending on the number of personal recruits they helped to become division directors like them.</div>
						<img class="img-responsive" src="/assets/front/img/4-stages.png">
						<div class="desc"><strong>Members Price (Retail Profit).</strong> Members are classified into 4 different types of dealership, according to the total purchased volume and commission level received from the company. All MEMBERS gets 5%~30% Discounts on products.  

<strong>Direct Sales Bonus (DSB):</strong> Everyone earns 20% of the Sales Point Value (SPV) of their Direct Downlines. QUALIFICATION: Accumulate 1,500SPV  

<strong>Indirect Sales Bonus (ISB):</strong> Everyone earns 10% of the Sales Point Value (VP) of their Indirect Downlines. QUALIFICATION: Accumulate 5,000SPV  

<strong>Group Sales Bonus (GSB):</strong> rebates up to 100% of Group SPV will be distributed between qualified members according to their POSITIONS every week according to the rates. Individuals must first accumulate 10,000SPV in order to accumulate downline PVs. </div>
						<table class="table table-bordered table-striped table-hover table-condensed">
							<thead>
								<tr>
									<th>Position</th>
									<th>Manager</th>
									<th>Associate Director</th>
									<th>Director</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Personal</td>
									<td>15.00%</td>
									<td>20.00%</td>
									<td>25.00%</td>
								</tr>
								<tr>
									<td>1 LEVEL (DIRECT DOWNLINES)</td>
									<td>9.00%</td>
									<td>12.00%</td>
									<td>15.00%</td>
								</tr>
								<tr>
									<td>2 LEVEL (INDIRECT DOWNLINES)</td>
									<td>9.00%</td>
									<td>12.00%</td>
									<td>15.00%</td>
								</tr>
								<tr>
									<td>3 LEVEL</td>
									<td>9.00%</td>
									<td>12.00%</td>
									<td>15.00%</td>
								</tr>
								<tr>
									<td>4 LEVEL</td>
									<td>9.00%</td>
									<td>12.00%</td>
									<td>15.00%</td>
								</tr>
								<tr>
									<td>5 LEVEL</td>
									<td>9.00%</td>
									<td>12.00%</td>
									<td>15.00%</td>
								</tr>
								<tr>
									<td>TOTAL</td>
									<td>60.00%</td>
									<td>80.00%</td>
									<td>100.00%</td>
								</tr>
							</tbody>
						</table>
						<h4 class="text-center">NO INCOME LIMITS - NO FLUSH OUT - NO INCOME CONVERSION</h4>
						<h5 class="text-left" style="margin-bottom: 0; margin-top: 10px; font-weight: 700;">Classification and Requirements</h5>
						<table class="table table-bordered table-striped table-hover table-condensed" style="margin-top: 5px;">
							<thead>
								<tr>
									<th>Consultants</th>
									<th>Manager</th>
									<th>Associate Director</th>
									<th>Director</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Recruit as much as Consultants as possible. It becomes his frontliners or his Directo Downlines. All the recuirts from his Direct Drownlines becomes the Indirect Downlines.</td>
									<td>Accumulate a minumum of <strong>15,000SPV</strong> with a minimum ACTIVE member of 3 Consultans + <strong>personal 750SPV</strong></td>
									<td>Accumulate a minimum of <strong>45,000SPV</strong> with a minimum ACTIVE member of 3 Managers & 2 Consultants + personal 2,500PSV</td>
									<td>Accumulate a minimum of <strong>140,000SPV</strong> with a minimum ACTIVE member of 3 Associate Directors & 2 Managers + <strong>personal 7,000SPV</strong></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
<script type="text/javascript" src="/assets/front/js/opportunity.js"></script>
@endsection
@section('css')
<link rel="stylesheet" type="text/css" href="/assets/front/css/opportunity.css">
@endsection
<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>{{settings("company_name")}}</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="/assets/initializr/css/bootstrap.min.css">
        <link rel="stylesheet" href="/assets/initializr/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="/assets/initializr/css/main.css">
        <script src="/assets/initializr/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
        <!-- EXTERNAL CSS -->
        <link rel="stylesheet" type="text/css" href="/assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick.css">
        <link rel="stylesheet" type="text/css" href="/assets/slick/slick.css">
        <link rel="stylesheet" type="text/css" href="/resources/assets/frontend/lightslider/css/lightslider.css">
        <!-- CSS -->
        <link rel="stylesheet" type="text/css" href="/assets/front/css/global.css">
        @yield("css")
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <div class="loader-container">
      <div class="loader">
        <img src="/assets/front/img/loader.gif">
      </div>
    </div>
    <div class="header-nav">
      <div class="clearfix container">
        <div class="pull-left">
          <div class="text">CALL US NOW: <span>+999 99-99</span></div>
          <div class="text">|</div>
          <div class="text">EMAIL US: <span>youremail@here.com</span></div>
        </div>
        <div class="pull-right">
          <div class="text"><a href="member/login">LOGIN</a></div>
          <div class="text">|</div>
          <div class="text"><a href="member/register">SIGN UP</a></div>
        </div>
      </div>
    </div>
    <nav class="navbar" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="/"><img src="/assets/front/img/logo.png"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="{{ Request::segment(1) == '' ? 'active' : '' }}">
              <a href="/">HOME</a>
            </li>
            <li class="{{ Request::segment(1) == 'opportunity' ? 'active' : '' }}">
              <a href="/opportunity">OPPORTUNITY</a>
            </li>
            <li class="{{ Request::segment(1) == 'product' ? 'active' : '' }}">
              <a href="/product/category">PRODUCTS</a>
            </li>
            <li class="{{ Request::segment(1) == 'about' ? 'active' : '' }}">
              <a href="/about">COMPANY</a>
            </li>
            <li class="{{ Request::segment(1) == 'updates' ? 'active' : '' }}">
              <a href="/updates">UPDATES</a>
            </li>
            <li class="{{ Request::segment(1) == 'contact' ? 'active' : '' }}">
              <a href="/contact">SUPPORT</a>
            </li>
          </ul>
          <ul class="nav navbar-nav navbar-right">
             <li class="cart-holder clearfix" style="cursor: pointer;">
             </li>
          </ul>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>
    @yield("content")
    <footer>
      <div class="container">
        <div class="row clearfix">
          <div class="col-md-4 col-sm-6 match-height">
            <div class="title">INFORMATION</div>
            <div class="content">
              <ul>
                <li><a href="javascript:">HOME</a></li>
                <li><a href="javascript:">PRODUCTS</a></li>
                <li><a href="javascript:">ABOUT US</a></li>
                <li><a href="javascript:">CONTACT</a></li>
              </ul>
            </div>
          </div>
          <div class="col-md-4 col-sm-6 match-height">
            <div class="title">CONTACT US</div>
            <div class="content">
              <ul>
                <li><a href="javascript:">@if(isset($contactnumber->about_description)) {{$contactnumber->about_description}} @endif</a></li>
                <li><a href="javascript:">@if(isset($email->about_description)) {{$email->about_description}} @endif</a></li>
              </ul>
            </div>
          </div>
          <!-- <div class="col-md-4 col-sm-6 match-height">
            <div class="title text-center">NEWS LETTER</div>
            <div class="content">
              <p>@if(isset($newsletter->about_description)) {{$newsletter->about_description}} @endif</p>
              <div class="newsletter-button">
                <input type="email" class="form-control input-sm">
              </div>
            </div>
          </div> -->
          <div class="col-md-4 col-sm-6 match-height text-center">
            <div class="title">FOLLOW US ON</div>
            <div class="content">
              <div class="social-icon">
                <div class="holder">
                  <i class="fa fa-facebook"></i>
                </div>
                <div class="holder">
                  <i class="fa fa-twitter"></i>
                </div>
                <div class="holder">
                  <i class="fa fa-pinterest"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="copyright">
       @if(isset($rights->about_description)) {{$rights->about_description}} @endif
      </div>
    </footer>
    <!-- Modal -->
    <div id="account_modal" class="modal fade account-modal" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="color-header">
              <img src="/assets/front/img/color-header.jpg">
            </div>
            <div class="account-tab clearfix hide">
              <div class="holder active" type="login">LOGIN</div>
              <div class="holder" type="register">CREATE AN ACCOUNT</div>
            </div>
            <form method="POST" id="loginform">
            <input type="hidden" value="{{ csrf_token() }}" name="_token" id="_token">
            <div class="account-content" type="login">
              <div class="logo"><img src="/assets/front/img/logo.png"></div>
              <label class="error" style="display:none;"></label>
              <div class="form-group">
                <input type="text" class="form-control input-lg" name="username" id="username" placeholder="USER NAME">
              </div>
              <div class="form-group">
                <input type="password" class="form-control input-lg" name="password" id="password" placeholder="PASSWORD">
              </div>
              <div class="form-group text-right">
                <button class="btn btn-orange btn-lg" id="btnlogin">SIGN IN</button>
                
                <a href="/member/register" class="forgot" type="register">CREATE NEW ACCOUNT?</a>
              </div>
            </div>
            </form>
            <div class="account-content hide" type="register">
              <div class="logo"><img src="/assets/front/img/logo.png"></div>
              <div class="form-group">
                <input type="text" class="form-control input-lg" placeholder="FULL NAME">
              </div>
              <div class="form-group">
                <input type="text" class="form-control input-lg" placeholder="USER NAME">
              </div>
              <div class="form-group">
                <input type="text" class="form-control input-lg" placeholder="ACCOUNT EMAIL ADDRESS">
              </div>
              <div class="form-group">
                <select class="form-control input-lg">
                  <option>BIRTHDAY</option>
                </select>
              </div>
              <div class="form-group">
                <input type="text" class="form-control input-lg" placeholder="CONTACT NUMBER">
              </div>
              <div class="form-group">
                <input type="password" class="form-control input-lg" placeholder="PASSWORD">
              </div>
              <div class="form-group">
                <input type="password" class="form-control input-lg" placeholder="REPEAT PASSWORD">
              </div>
              <div class="form-group text-right">
                <button class="btn btn-orange btn-lg">CREATE</button>
                <a href="javascript:" class="forgot account-modal-button" type="login">ALREADY HAVE AN ACCOUNT?</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="cart_modal" class="modal fade cart-modal" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-body">
            <div class="cart-header clearfix">
              <div class="pull-left">
                <img class="cart-icon" src="/assets/front/img/cart-but.png">
                <div class="text">MY CART</div>
              </div>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="table-holder">
              <table class="table">
                <thead>
                  <tr>
                    <th>IMAGE</th>
                    <th>PRODUCT NAME</th>
                    <th>UNIT PRICE</th>
                    <th>QTY</th>
                    <th>TOTAL</th>
                  </tr>
                </thead>
                <tbody>
                  <tr class="stripe">
                    <td><img class="img" src="/assets/front/img/test-product.jpg"></td>
                    <td class="name">loremipsumdolor</td>
                    <td>P 900.00</td>
                    <td>1</td>
                    <td>P 900.00</td>
                  </tr>
                  <tr class="stripe">
                    <td><img class="img" src="/assets/front/img/test-product.jpg"></td>
                    <td class="name">loremipsumdolor</td>
                    <td>P 900.00</td>
                    <td>1</td>
                    <td>P 900.00</td>
                  </tr>
                  <tr class="stripe">
                    <td><img class="img" src="/assets/front/img/test-product.jpg"></td>
                    <td class="name">loremipsumdolor</td>
                    <td>P 900.00</td>
                    <td>1</td>
                    <td>P 900.00</td>
                  </tr>
                  <!-- Total -->
                  <tr>
                    <td colspan="4"></td>
                    <td>P 2,400.00</td>
                  </tr>
                  <tr>
                    <td colspan="4"></td>
                    <td><button class="btn btn-orange" type="button" onClick="location.href='/product/checkout'">CHECK OUT</button></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="/assets/initializr/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
    <script src="/assets/initializr/js/vendor/bootstrap.min.js"></script>
    <script src="/resources/assets/frontend/lightslider/js/lightslider.js"></script>
    <script src="/assets/initializr/js/main.js"></script>
    <script type="text/javascript" src="/assets/slick/slick.min.js"></script>
    <script type="text/javascript" src="/assets/front/js/match-height.js"></script>
    <script type="text/javascript" src="/assets/front/js/fit-text.js"></script>
    <script type="text/javascript" src="/assets/front/js/global.js"></script>
    <script type="text/javascript">
      $("#btnlogin").click(function(e)
      {
        e.preventDefault();
        var username = $("#username").val();
        var password = $("#password").val();
        var token = $("#_token").val();

        $.ajax(
        {
          url: "/front/login",
          type: "post",
          data: $("#loginform").serialize(),
          success: function(data)
          {
            if(data == 0)
            {
              location.href = "/distributor";
            }
            if(data == 1)
            {
              $(".error").html("Username and Password didn't match");
              $(".error").attr("style", "text-align:right;color:red;");
            }
            if(data == 2)
            {
              $(".error").html("Account didn't exist!");
              $(".error").attr("style", "text-align:right;color:red;");
            }
          }
        });
      });
      $(function() {
        $('.smooth-scroll[href*="#"]:not([href="#"])').click(function() {
          if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
              $('html, body').animate({
                scrollTop: target.offset().top
              }, 1000);
              return false;
            }
          }
        });
      });
    </script>

    @yield("script")
    </body>
</html>

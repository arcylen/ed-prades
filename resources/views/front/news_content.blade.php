@extends('front.layout')
@section('content')
<div class="post_content container" style="padding: 50px 0;">
    <div class="post post_main" style="border-bottom: 0;">                       
        <h2 class="rw bold">{{ $news->news_title }}</h2>  
        <h5 style="color: #a1a1a1; font-weight: 300;">{{ $news->month }} {{ $news->day }}, {{ $news->year }}</h5>                             
        <p class="op" style="white-space: pre-wrap; margin-top: 25px; color: #1a1a1a;">{{ $news->news_description }}</p>    
    </div>
</div>
@endsection
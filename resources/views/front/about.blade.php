@extends("front.layout")
@section("content")
<div class="intro">
	<div class="container">
		<div class="text">
			<div class="name">{{ isset($content_data['Banner Title (Company)']->about_description) ? $content_data['Banner Title (Company)']->about_description : "" }}</div>
			<div class="desc">{{ isset($content_data['Banner Sub-title (Company)']->about_description) ? $content_data['Banner Sub-title (Company)']->about_description : "" }}</div>
		</div>
	</div>
</div>
<div class="main-content">
	<div class="container">
		<div class="row clearfix">
			<div class="col-sm-4">
				<div class="side-bar">
					<div class="holder active" show="welcome">Welcome</div>
					<div class="holder" show="mission-vision">Mission and Vision</div>
					<div class="holder" show="commitment">Commitment</div>
				</div>
			</div>
			<div class="col-sm-8">
				<div class="content-holder" show="welcome">
					<div class="holder">
						<table class="heading">
							<td>
								<div class="title">Congratulations!</div>
							</td>
							<td>
								<img style="margin-left: auto;" class="img-responsive" src="/assets/front/img/congrats.jpg">
							</td>
						</table>
						<div class="desc">{{ isset($content_data['Welcome Text (Company)']->about_description) ? $content_data['Welcome Text (Company)']->about_description : "" }}</div>
					</div>	
				</div>
				<div class="content-holder hide" show="mission-vision">
					<div class="holder">
						<div class="title" style="margin-bottom: 15px;">Mission</div>
						<div class="desc">To help each individual uplift standard of living and attain financial freedom by providing bottomless income opportunities & quality consumer products that are deemed essential for good health and longevity.</div>
					</div>
					<div class="holder">
						<div class="title" style="margin-bottom: 15px;">Vision</div>
						<div class="desc">We envision to be a TOTAL direct sales company through dynamic, systematic distribution of products and proactive corporate management and staffs, and that helps alleviate the standards of living of our distributors by offering a realistic income opportunities.</div>
					</div>
					<img class="img-responsive" style="margin-left: auto; margin-bottom: -40px;" src="/assets/front/img/objective.jpg">
				</div>
				<div class="content-holder hide" show="commitment">
					<div class="clearfix holder">
						<div class="title" style="margin-bottom: 15px; color: #252525; font-size: 50px; font-weight: 700;">Our Commitment</div>
						<div class="desc" style="display: inline;">We are committed to provide our members a business based on honesty and integrity with the highest ethical business standards with a marketing plan that effectively works. 
We are committed to provide you with high quality and fairly priced products. We aims for total customer satisfaction. 

We are committed to provide you with professional training programs to help you achieve your dreams. It is composed of a wide array of professional training modules, literatures, brochures, etc. 

We are committed to market opportunities so that you will have a business with no boundaries. 

We are committed to become the best direct selling multi -level marketing company.
						<img style="shape-outside: url('/assets/front/img/wowe.png'); shape-image-threshold: 0.5; float: right;" src="/assets/front/img/wowe.png"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section("css")
<link rel="stylesheet" type="text/css" href="/assets/front/css/about.css">
@endsection
@section("script")
<script type="text/javascript" src="/assets/front/js/opportunity.js"></script>
@endsection
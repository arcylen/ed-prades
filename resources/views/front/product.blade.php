@extends("front.layout")
@section("content")
<div class="content">
	<div class="intro" style="background-image: url('{{ $product_category->product_category_banner }}')">
		<div class="overlay {{ $product_category->product_category_color }}"></div>
		<div class="container">
			<div class="text">
				<img src="{{ $product_category->image_file }}" class="img-responsive">
				<div class="name">{{ $product_category->product_category_name }}</div>
				<div class="desc">{{ $product_category->product_category_description }}</div>
			</div>
		</div>
	</div>
	<div class="container single-product">
		<div class="row clearfix">
			@foreach($_product as $product)
			<div class="col-md-4 col-sm-6">
				<div class="list">
					<div class="img"><img src="{{ $product->image }}"></div>
					<div class="info {{ $product_category->product_category_color }}">
						<div class="name">{{ $product->product_name }}</div>
						<div class="desc">{{ $product->product_info }}</div>
						<div class="row clearfix">
							<div class="col-xs-6">
								<!-- <div class="text">Product Category</div> -->
							</div>
							<div class="col-xs-6">
								<div class="text right" style="cursor: pointer;" onClick="location.href='/product/view/{{ $product->product_id }}'">Read More <img src="/assets/front/img/right-arrow.png"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			@endforeach
			<!-- <div class="col-md-4 col-sm-6">
				<div class="list">
					<div class="img"><img src="/assets/front/img/test-product.jpg"></div>
					<div class="info blue">
						<div class="name">Product Name</div>
						<div class="desc">Short product description</div>
						<div class="row clearfix">
							<div class="col-xs-6">
								<div class="text">Product Category</div>
							</div>
							<div class="col-xs-6">
								<div class="text right">Read More <img src="/assets/front/img/right-arrow.png"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="list">
					<div class="img"><img src="/assets/front/img/test-product.jpg"></div>
					<div class="info blue">
						<div class="name">Product Name</div>
						<div class="desc">Short product description</div>
						<div class="row clearfix">
							<div class="col-xs-6">
								<div class="text">Product Category</div>
							</div>
							<div class="col-xs-6">
								<div class="text right">Read More <img src="/assets/front/img/right-arrow.png"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="list">
					<div class="img"><img src="/assets/front/img/test-product.jpg"></div>
					<div class="info blue">
						<div class="name">Product Name</div>
						<div class="desc">Short product description</div>
						<div class="row clearfix">
							<div class="col-xs-6">
								<div class="text">Product Category</div>
							</div>
							<div class="col-xs-6">
								<div class="text right">Read More <img src="/assets/front/img/right-arrow.png"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="list">
					<div class="img"><img src="/assets/front/img/test-product.jpg"></div>
					<div class="info blue">
						<div class="name">Product Name</div>
						<div class="desc">Short product description</div>
						<div class="row clearfix">
							<div class="col-xs-6">
								<div class="text">Product Category</div>
							</div>
							<div class="col-xs-6">
								<div class="text right">Read More <img src="/assets/front/img/right-arrow.png"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="list">
					<div class="img"><img src="/assets/front/img/test-product.jpg"></div>
					<div class="info blue">
						<div class="name">Product Name</div>
						<div class="desc">Short product description</div>
						<div class="row clearfix">
							<div class="col-xs-6">
								<div class="text">Product Category</div>
							</div>
							<div class="col-xs-6">
								<div class="text right">Read More <img src="/assets/front/img/right-arrow.png"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="list">
					<div class="img"><img src="/assets/front/img/test-product.jpg"></div>
					<div class="info blue">
						<div class="name">Product Name</div>
						<div class="desc">Short product description</div>
						<div class="row clearfix">
							<div class="col-xs-6">
								<div class="text">Product Category</div>
							</div>
							<div class="col-xs-6">
								<div class="text right">Read More <img src="/assets/front/img/right-arrow.png"></div>
							</div>
						</div>
					</div>
				</div>
			</div> -->
		</div>
	</div>
</div>
@endsection
@section("css")
<link rel="stylesheet" type="text/css" href="/assets/front/css/product.css">
@endsection
@section("script")
<script type="text/javascript" src="/assets/front/js/product.js"></script>
@endsection
@extends("front.layout")
@section("content")
<div class="intro">
	<div class="container">
		<div class="text">
			<div class="name">At Your Service</div>
			<div class="desc">Say something about this page content.</div>
		</div>
	</div>
</div>
<div class="content">
	<div class="contact-container">
	    <div class="container">
	    	<div class="row clearfix">
	    		<div class="col-md-3">
	    			<div class="side-bar">
	    				<div class="holder active" show="howtoorder">How to Order</div>
	    				<div class="holder" show="deliverysystem">Delivery System</div>
	    				<div class="holder" show="productreturns">Product Returns</div>
	    				<div class="holder" show="responsoring">Re-sponsoring</div>
	    				<div class="holder" show="newsletter">Newsletter</div>
	    				<div class="holder" show="download">Download</div>
	    				<div class="holder" show="contact">Contact</div>
	    			</div>
	    		</div>
	    		<div class="col-md-9">
	    			<div class="content-holder" show="howtoorder">
	    				<div class="content-title">How to Order?</div>
	    				<div class="content-desc">All registered distributors are authorized to purchase directly from the Company. 

All distributors are only allowed to order BOSS products directly from his / her sponsor and uplines but not from crosslines. The sponsor or upline must make a purchase back of the same products from the Company using the distributor’s name and number on the same month. Please take note of this rule. 

A Crossline cannot sell to another Crossline. The reason for this rule is because some interests / benefits / profits might be jeopardized. Selling to Crossline is a serious offense and both distributorships are subject to termination. Please take note of this rule. 

Products shall be paid for in cash or other methods set by BOSS. All transactions shall be completed and consummated at the point of sale. 

Distributors can order products in bulk or order products individually. 

The Company will not temporarily store products for distributors. At the time of product pick-up, a distributor should check and confirm the accuracy of the orders thru the invoices. In case there is any discrepancy, the distributor should immediately request the staff of the designated distribution center to re-issue correct documents. In case there is a discrepancy between the products ordered and received, the distributor shall provide to the Company relevant invoices and product orders to request compensations within 3-days after the transaction. 

A distributor is encouraged to make further orders under the condition that he / she has sold more than 70% of initial product inventory. 

In the event that any distributor, using the same distributorship, orders excessive products in a month, the Company will review this abnormal situation to check if any game playing is ongoing. If any violation of operational regulations and Company Policies of the Company is established, the distributor’s qualification and benefits will be revoked. The distributor can then return the products and the distributorship will be terminated. If there is an instance when product orders are in excessive quantity for special occasions (Example: New Year gift-packs), approval for the sale transaction can be requested from the Company. </div>
	    			</div>
	    			<div class="content-holder hide" show="deliverysystem">
	    				<div class="content-title">Delivery System</div>
	    				<div class="content-desc"><strong style="font-size: 16.67px;">Service Guidelines:</strong>

1. Select the size of box you wish to avail.
2. Call or SMS BOSS Hotline (except weekends and holidays), place the order and get
confirmation. (02)-888-8888, 0917-888-8888. (Distributors Name & Number,
Complete Address, Contact Name & Number of Recipient).
3. Pay through (on or before cut-off time). BPI Account No.: 16463132-16465464. Fax
or email deposit slip to: (call or sms to confirm receipt of email). Email:
sales@thebossph.com
4. Delivery Schedule: Orders accepted from 10:00am to 6:00pm. Mondays to Fridays,
except holidays. Cut-off time 3:00PM. *Orders processed on or before cut-off will be
sent the following working day.
5. Expected delivery: Metro Manila & Luzon area: 1-3 days. Visayas & Mindanao: 2-5
days

No Deliveries on Weekend and Holidays

<strong style="font-size: 16.67px;">Boxes:</strong>

Small— P150.00 (360mm x 270mm x 230mm)
Medium— P200.00 (460mm x 310mm x 230mm)
Large— P250.00 (590mm x 360mm x 320mm </div>
	    			</div>
	    			<div class="content-holder hide" show="productreturns">
	    				<div class="content-title">Product Return & Exchange</div>
	    				<div class="content-desc">Our Assurance: To protect the interests of consumers and to minimize the investment risks of BOSS distributors, BOSS promulgates and implements a well-designed product return and exchange system.

If there is any defect in a BOSS product; a consumer or distributor is entitled to return the defective product. BOSS will replace the defective product with a new product of the same kind.

When a distributor desires to terminate his/her distributorship, for whatever reason, the remaining products or stocks in hand that are saleable can be returned to the Company for a refund in pursuant to relevant provisions of the Consumer Protection Law as well

<strong style="font-size: 16.67px;">Rules Of Product Return</strong>

For any defective product sold by a distributor, a customer is entitled to return the product and get a full refund from the distributor within thirty (30) days subsequent to the date of purchase. The distributor shall solely be responsible for this process of product return and refund. The Company will allow the distributor to exchange this defective product with a new same product.

At the time the customer applies for a refund, the customer shall return to the distributor the defective product and the delivery receipt or sales invoice. The delivery receipt or sales invoice must have the address and telephone number of the customer and the date of transaction clearly recorded on the invoice.

The distributor may return to the Company the defective product regardless whether the product was partially consumed or not re-saleable to other customers. The Company will provide a new same product to the distributor. Product fully consumed will not be accepted for a return. Distributors should also refuse a customer such a return, as this is an unreasonable request. Customers can also return an unopened product within thirty (30) days subsequent to the date of ordering of the product for a full refund. As soon as the distributor receives such request, he/she shall process accordingly.

In the event that a distributor refuse to accept the return of product and to refund the product price, the customer may directly contact the Company and forward the product delivery receipts to the Company within 30 days after the distributor refused to accept the product return. The Company will terminate a distributor who refuses to accept such product return from a customer. 

<strong style="font-size: 16.67px;">Product Return By the Distributor Who Bought A Starter Kit</strong>

All distributors who are serious with the business may start with a Starter Kit. They should treat the products in the Starter Kit as samples and use these products. We are not in the business of selling marketing plan and if a distributor does not like the products, he/she should return them and quit the business. If any distributor, who bought a Starter Kit, is not satisfied with the quality of products and decides to terminate his/her distributorship, he/she is entitled to return the Starter Kit to the Company for a full refund within 60 days. All bonuses paid will be deducted accordingly. We encourage distributors to start with the Starter Kit but we will also give our special BOSS 60 day full money back guarantee on it. 

The sponsor of the new distributor can also choose to accept the returned Starter Kit from the distributor and is entitled to replace the returned partially‐ consumed products with new ones from the Company or replace the Starter Kit with a new one.

<strong style="font-size: 16.67px;">Products Return By A Resigning Distributor</strong>

A distributor is entitled to resign his/her distributorship by delivering to the Company a written notice within fourteen (14) days subsequent to the execution of the agreement. Any distributor withdrawing from the marketing system of the Company must wait for a one (1) year period to be qualified to apply for reparticipation into the Company, if all conditions for re‐sponsoring are conformed.

The distributor, who resigns from the Company, shall within thirty (30) days apply for product return pursuant to relevant rules promulgated by the Company.

<strong>The following products cannot be returned:</strong>

Products that have been opened
Products that have been consumed
Products that cannot be resold
Product that BOSS no longer carries
Products that has new packaging
Products that are within 6 months from expiry date
Other non‐BOSS brand products 

The Company will buy back the products purchased within the last 180 days in the amount equal to 90% of the original product prices paid by the distributor.

The Company shall give a refund to the resigning distributor when the Company receives and confirms the accuracy of the documents as mentioned above.

The application for product return shall be submitted and processed by the distributor in person to the designated area distribution center that originally supplied the products.

After completion of the above procedures, the Company will then delete from the company record the name or level of the distributor concerned. All downline distributors originally sponsored by him/her will

Bonus Reclaim

The Company will deduct all bonus(es) paid, calculated upon a specific percentage of wholesale product prices (tax excluded), directly from the amount of the return products.

The Company will also reclaim the bonuses of upline distributors in the same line with the resigning distributor. The retrace will include the bonuses received by the distributors up to all applicable levels directly above the resigning distributor. If the product return causes any changes to other bonuses or benefits, including, but not limited to, leadership bonus, profit‐sharing bonus and other incentives, the Company will reclaim the over‐paid bonuses and benefits and will deduct the amount from the affected distributor(s) on the following month.

In case the product returns affects the level or other privileges or incentives earned by a distributor, the level and benefits will also be canceled.

Any amendments of the details and procedures, with respect to product returns, will be published from time to time.

<strong style="font-size: 16.67px;">The “Buy Back” Rule In Cases Of Game Playing</strong>

Purpose for the "Product Buy‐Back" rule.

When a distributor plays games, he/she normally orders products by the cases. He/she also returns the products by the cases when he/she wants to terminate his/ her distributorship when things do not work out or just wants his/her money back. Normally, this distributor does not have a solid organization.

Experience have taught us that distributors who overload products are normally encouraged to do so by their upline(s) because the upline(s) want to qualify for some benefit(s). In all cases, even if the upline(s) did not encourage this activity, they know about it. It is the responsibility of all distributors who have downlines to do MLM properly, to work within the laws of local government and to guide their downlines. We do not want people to play games in this Company. Please get the message or the Upline(s) will be held accountable.

The "Product Buy Back Rules" (Product Return) is designed for the following reasons:

Each sponsor is obliged to ensure sponsored downline distributor purchases and orders products prudently. We do not want inventory loading or game playing.

All distributors shall order products in a quantity based on his/her actual need. As recommended, distributors are encouraged to purchase additional products only after 70% of the initial inventory has been sold or otherwise disposed. This is our 30/70% rule.

In the event that the downline distributor insists to purchase products in quantities exceeding normal volumes, the sponsor shall try his/her best efforts to persuade the distributor to cease the activities. Failing this, the sponsor must inform BOSS, and all upline managers and distributors in this line, in writing within thirty (30) days of such activities. This will release him/her of all responsibilities. All uplines in turn must take similar action to release them of responsibility.

Failing to do the above, every sponsor must offer to buy back from any of his/her distributor who wishes to discontinue their business any unused and currently marketable products. Plea of ignorance is no defense and will not be entertained.

The responsibility to buy‐back the products passes up the line of sponsorship from distributor to distributor up to all managers in this line. If the sponsor fails or refuses to buy‐back the products, this responsibility goes up to his sponsor until someone in the line assumes responsibility. In the event no one assumes this responsibility, BOSS will buy them back according to the Buy‐Back Policy.

Uplines who are members of the Profit Sharing Bonus or President Award will lose their Profit Sharing bonus for one qualified year and all, or a portion, of their PAP at the discretion of the Executive Committee. There is no reason why a Profit Sharing Manager or a President’s Awardee, who encourage game playing or are not aware of game playing by members of their organization, to remain a Profit Sharing Manager or a President’s Awardee or go unpunished. Uplines who are not yet members of the Profit Sharing Bonus or President’s Award cannot qualify for these awards in the future. We hope all distributors know and can feel that BOSS does not really want distributors who play games to remain in the Company.

The whole organization of the departing distributor will go to the distributor who assumes the responsibility for buying back the saleable inventory from the departing distributor.

This "BUY BACK" rule will strictly be enforced. 
</div>
	    			</div>
	    			<div class="content-holder hide" show="responsoring">
	    				<div class="content-title">Re‐Sponsoring Procedure</div>
	    				<div class="content-desc">Re‐sponsoring
	    					
Any active distributor can re‐sponsor other distributors who did not renew their distributorships aŌer 1 year of inactivity. Distributors who requested termination or are terminated can also be re‐sponsored aŌer they have been inactive for 1 year. Any distributor being re‐sponsored will remain at the level according to the Company Policy, provided that his/her status is at, or higher than, the level prior to the re‐sponsor. A re‐ sponsored distributor cannot re‐sponsor any downline(s) in their former organization.

Any distributor and his/her upline(s) who directly or indirectly spur other distributors not to renew their distributorships in order to facilitate the purpose of re‐sponsoring will be penalized/disqualified for royalty bonus or be suspended for one year or terminated by the Company. For this offence, a distributor, who has not qualified as a AVP and above, will be penalized for one year when they qualify. Ignorance is no defense. We do not want upline(s) to claim they do not know that their downlines have committed this offence especially when the downlines are using their centers.

In the event that any distributor utilizes the name of his/her spouse or relative or any other persons to participate in another distribution line, then the whole distribution network of the illegitimate distributor will be allocated back to the original upline distributor who first sponsored the distributor into the sales system. All bonuses and benefits will be reclaimed and paid back to the original line. The distributor and his/her upline(s) will be suspended for one year or terminated by the Company or be penalized/disqualified for a Royalty Bonus. Ignorance is no defense. We do not want upline(s) to claim they do not know what is going on in their organization especially when the downlines are using their centers.

All downlines of a Distributor can only be re‐sponsored aŌer the downlines of the Distributor have been inactive for 3 years. 

<strong style="font-size: 16.67px;">Definition Of Inactivity</strong>

No products purchased from any upline, downline, other distributors or the Company within one year.

Not sponsoring any individual to participate into BOSS sales organization within one year.

Not attending any Company related meetings, including but not limited to, OPP, trainings, seminars, conferences, home meetings or home parties and other activities in connection with BOSS within one year. Any photos, persons, documents, records or videotapes will be valid evidence to prove the distributor’s participation in the activities.

Note: Any distributor applying for re‐sponsorship shall execute the Re‐Sponsor Application Form. AŌer confirmation by the Company, it becomes effective.

RESPONSORING PROCEDURE

Effective DECEMBER 1, 2016, an existing distributor who has not purchased any BOSS Products for a minimum of ONE (1) year, from the Company or from any other distributor, and has not received any other payments from a BOSS Distributor during that same time frame, will be considered for re‐sponsoring. The distributorship will be required to sign an affidavit under penalty of perjury with respect to the above policy before re‐ sponsoring will be allowed.

Instructions for Re‐Sponsorship Affidavit

There will be no exceptions granted to the requirements.

The re‐sponsoring affidavit must be completed and signed by the distributor requesting the re‐sponsorship.

The re‐sponsorship affidavit must be signed before a notary public in the jurisdiction where distributor is a resident.

The re‐sponsorship affidavit must be submitted to BOSS along with a new distributor application for verification and approval by Head Office. Once approved, the new application form together with the notarized re‐ sponsorship affidavit will be accepted and stamped. Distributor data will then be updated with the new sponsor’s data. </div>
	    			</div>
	    			<div class="content-holder hide" show="newsletter">
	    				<div class="content-title">Newsletter</div>
	    				<div class="content-desc" style="white-space: normal;">
	    					@foreach($_news as $news)
	    					<div class="holder" style="text-align: left; margin-bottom: 15px;">
	    						<div><a href="/news?id={{ $news->news_id }}"><strong style="font-size: 16.67px;">{{ $news->news_title }}</strong></a></div>
	    						<div style="margin-top: 15px; font-size: 14px;">{{ substr($news->news_description, 0, 200) }}... <a href="/news?id={{ $news->news_id }}">Read More</a></div>
	    					</div>
	    					@endforeach
	    				</div>
	    			</div>
	    			<div class="content-holder hide" show="download">
	    				<div class="content-title">Downloadables</div>
	    				<ul>@foreach($_downloads as $downloads)<li style="font-size: 14px;"><a href="/uploads/{{ $downloads->file }}" download style="color: #0000FF;">{{ $downloads->name }}</a></li>@endforeach</ul>
	    			</div>
	    			<div class="content-holder hide" show="contact">
	    				<div class="main-title">Contact Us</div>
				        <div class="row clearfix">
				            <div class="col-md-12">
				                <div class="title">Get In Touch With Us</div>
				                <div class="form-group-container">
				                    <div class="row clearfix">
				                        <div class="col-md-6">
				                            <div class="form-group">
				                                <input type="text" class="form-control" placeholder="First Name">
				                            </div>
				                        </div>
				                        <div class="col-md-6">
				                            <div class="form-group">
				                                <input type="text" class="form-control" placeholder="Last Name">
				                            </div>
				                        </div>
				                    </div>
				                    <div class="row clearfix">
				                        <div class="col-md-6">
				                            <div class="form-group">
				                                <input type="text" class="form-control" placeholder="Phone Number">
				                            </div>
				                        </div>
				                        <div class="col-md-6">
				                            <div class="form-group">
				                                <input type="text" class="form-control" placeholder="Email Address">
				                            </div>
				                        </div>
				                    </div>
				                    <div class="row clearfix">
				                        <div class="col-md-6">
				                            <div class="form-group">
				                                <input type="text" class="form-control" placeholder="Subject">
				                            </div>
				                        </div>
				                    </div>
				                    <div class="row clearfix">
				                        <div class="col-md-12">
				                            <div class="form-group">
				                                <textarea class="form-control" placeholder="Message"></textarea>
				                            </div>
				                        </div>
				                    </div>
				                    <div class="text-left">
				                        <button class="btn btn-primary">Send</button>
				                    </div>
				                </div>
				            </div>
				            <div class="col-md-12">
				                <div class="row clearfix">
				                	<div class="col-sm-6">
					                	<div class="title" style="margin-top: 50px;">Location</div>
						                <div class="info">
						                    <table>
						                        <tbody>
						                            <tr>
						                                <td class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i></td>
						                                <td class="text">{{isset($settings_data['company_name']->value) ? $settings_data['company_name']->value : 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. '}}</td>
						                            </tr>
						                            <tr>
						                                <td class="icon"><i class="fa fa-mobile" aria-hidden="true"></i></td>
						                                <td class="text">{{isset($settings_data['company_mobile']->value) ? $settings_data['company_mobile']->value : '+44 870 888 88 88'}}</td>
						                            </tr>
						                            <tr>
						                                <td class="icon"><i class="fa fa-envelope" aria-hidden="true"></i></td>
						                                <td class="text">{{isset($settings_data['company_email']->value) ? $settings_data['company_email']->value : 'customercare@theboss.com'}}</td>
						                            </tr>
						                        </tbody>
						                    </table>
						                </div>
					                </div>
					                <div class="col-sm-6">
					                	<div class="title" style="margin-top: 50px;">Business Hours</div>
						                <div class="info">
						                    <table>
						                        <tbody>
						                            <tr>
						                                <td class="icon"><i class="fa fa-clock-o" aria-hidden="true"></i></td>
						                                <td class="text">{{isset($content_data['Business Hours (Contact)']->about_description) ? $content_data['Business Hours (Contact)']->about_description : ''}}</td>
						                            </tr>
						                        </tbody>
						                    </table>
						                </div>
					                </div>
				                </div>
				            </div>
				        </div>
				        <div class="map-container">
							<div class="title">FIND US ON MAP</div>
							<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d3861.007280409033!2d120.97894086009421!3d14.598660949454503!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sph!4v1468556209677" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
						</div>
	    			</div>
	    		</div>
	    	</div>
	    </div>
	</div>
</div>
@endsection
@section("css")
<link rel="stylesheet" type="text/css" href="/assets/front/css/contact.css">
@endsection
@section('script')
<script type="text/javascript" src="/assets/front/js/opportunity.js"></script>
@endsection
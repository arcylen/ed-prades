@extends("front.layout")
@section("content")
<div class="intro">
	<div class="text"><img src="/assets/front/img/cart-big.png"> <span>CHECK OUT</span></div>
</div>
<div class="content">
	<div class="info">
		<div class="container">
			<div class="row clearfix">
				<div class="col-md-6">
					<div class="form-group">
						<label>FIRST NAME</label>
						<input type="text" class="form-control">
					</div>
					<div class="form-group">
						<label>LAST NAME</label>
						<input type="text" class="form-control">
					</div>
					<div class="form-group">
						<label>CONTACT NUMBER</label>
						<input type="text" class="form-control">
					</div>
					<div class="form-group">
						<label>OTHER CONTACT INFORMATION</label>
						<input type="text" class="form-control">
					</div>
					<div class="form-group">
						<label>SHIPPING METHOD</label>
						<select class="form-control">
							<option></option>
						</select>
					</div>
					<div class="form-group">
						<label>COMPLETE SHOPPING ADDRESS</label>
						<textarea class="form-control"></textarea>
					</div>
				</div>
				<div class="col-md-6">
					<div class="title">Order Summary</div>
					<div class="summary">
						<div class="c-qty">You have 1 item in your cart.</div>
						<table class="table">
							<thead>
								<tr>
									<th>PRODUCT</th>
									<th>QTY</th>
									<th>PRICE</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="name">loremipsum</td>
									<td class="qty">1</td>
									<td class="price">P 900.00</td>
								</tr>
								<tr>
									<td class="name">loremipsum</td>
									<td class="qty">1</td>
									<td class="price">P 900.00</td>
								</tr>
								<tr>
									<td class="name">loremipsum</td>
									<td class="qty">1</td>
									<td class="price">P 900.00</td>
								</tr>
							</tbody>
						</table>
						<div class="total">
							<table>
								<tbody>
									<tr>
										<td>Subtotal</td>
										<td>P 2,400.00</td>
									</tr>
									<tr>
										<td>Shipping Fee</td>
										<td>P 50.00</td>
									</tr>
									<tr>
										<td>TOTAL</td>
										<td>P 2,900.00</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="payment">
		<div class="title">CHOOSE MODE OF PAYMENT</div>
		<div class="container">
			<div class="row clearfix">
				<div class="col-md-4">
					<div class="image"><img src="/assets/front/img/payment/paypal.jpg"></div>	
					<div class="name"><input type="radio"> <span>PAYPAL</span></div>
					<div class="desc">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</div>
				</div>
				<div class="col-md-4">
					<div class="image"><img src="/assets/front/img/payment/bank.jpg"></div>	
					<div class="name"><input type="radio"> <span>BANK DEPOSIT</span></div>
					<div class="desc">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</div>
				</div>
				<div class="col-md-4">
					<div class="image"><img src="/assets/front/img/payment/remitance.jpg"></div>	
					<div class="name"><input type="radio"> <span>REMITANCE</span></div>
					<div class="desc">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor.</div>
				</div>
			</div>
			<div class="upload">
				<div class="text">UPLOAD PROOF OF PAYMENT</div>
				<input type="file">
				<button class="btn">PROCEED</button>
			</div>
		</div>
	</div>
</div>
@endsection
@section("css")
<link rel="stylesheet" type="text/css" href="/assets/front/css/checkout.css">
@endsection
@section("script")

@endsection
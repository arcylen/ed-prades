@extends("front.layout")
@section("content")
<div class="intro" style="background-image: url('{{ $product_category->product_category_banner }}')">
    <div class="overlay {{ $product_category->product_category_color }}"></div>
    <div class="container">
        <div class="text">
            <img src="{{ $product_category->image_file }}" class="img-responsive">
            <div class="name">{{ $product_category->product_category_name }}</div>
            <div class="desc">{{ $product_category->product_category_description }}</div>
            <div class="bred-cramb">
                <div class="holder"><a href="/product">Products</a></div>
                <div class="holder">></div>
                <div class="holder"><a href="/product?category={{ $product_category->product_category_id }}">Beauty & Wellness</a></div>
                <div class="holder">></div>
                <div class="holder"><strong>{{ $product->product_name }}</strong></div>
            </div>
        </div>
    </div>
</div>
<div class="product">
    <div class="container">
        <div class="row clearfix">
            <div class="col-md-6">
                <div class="image match-height">
                    <img src="{{$product->image_file}}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="info match-height">
                    <div class="name title"><h2>{{$product->product_name}}</h2></div>
                    <div class="desc">{{$product->product_info}}</div>
                    <div class="name"><h2>Price</h2></div>
                    <div class="desc">P {{number_format($product->price, 2)}}</div>
                    <div class="name"><h2>Category</h2></div>
                    <div class="desc">P {{ $product_category->product_category_name }}</div>
                    <!-- <div class="stock">In Stock</div> -->
                </div>
            </div>
        </div>
    </div>
</div>
<div class="related">
    <div class="container">
        <div class="title">Related Products</div>
        <div class="row clearfix">
            @foreach($_product as $product)
            <div class="col-md-4 col-sm-6">
                <div class="list">
                    <div class="img"><img src="{{ $product->image }}"></div>
                    <div class="info {{ $product->category->product_category_color }}">
                        <div class="name">{{ $product->product_name }}</div>
                        <div class="desc">{{ $product->product_info }}</div>
                        <div class="row clearfix">
                            <div class="col-xs-6">
                                <div class="text">{{ $product->category->product_category_name }}</div>
                            </div>
                            <div class="col-xs-6">
                                <div class="text right" style="cursor: pointer;" onClick="location.href='/product/view/{{ $product->product_id }}'">Read More <img src="/assets/front/img/right-arrow.png"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <!-- <div class="col-md-3">
                <div class="list">
                    <div class="img"><img src="/assets/front/img/test-product.jpg"></div>
                    <div class="name">NUTRA-CEUTICALS</div>
                    <div class="rate">
                        <img src="/assets/front/img/full-star.png">
                        <img src="/assets/front/img/full-star.png">
                        <img src="/assets/front/img/full-star.png">
                        <img src="/assets/front/img/full-star.png">
                        <img src="/assets/front/img/empty-star.png">
                    </div>
                    <div class="price">P 200.00</div>
                    <div class="btn-holder" onClick="location.href='/product/view'">
                        <div class="cart-but"><img src="/assets/front/img/cart-but.png"></div>
                        <button class="btn" type="button">Add to cart</button>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</div>
<div class="ending">
    <div class="container">
        <div class="row clearfix">
            <div class="col-md-8">Say something to motivate them and start your business. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</div>
            <div class="col-md-4"><button class="btn">START NOW!</button></div>
        </div>
    </div>
</div>
@endsection
@section("css")
<link rel="stylesheet" type="text/css" href="/assets/front/css/view_product.css">
@endsection
@section("script")

@endsection
@extends("front.layout")
@section("content")
<div class="intro">
	<div class="container">
		<div class="text">
			<div class="name">{{ isset($content_data['Banner Title (Product Category)']->about_description) ? $content_data['Banner Title (Product Category)']->about_description : "" }}</div>
			<div class="desc">{{ isset($content_data['Banner Sub-title (Product Category)']->about_description) ? $content_data['Banner Sub-title (Product Category)']->about_description : "" }}</div>
		</div>
	</div>
</div>
<div class="main-content">
	<div class="container">
		<div class="row-no-padding clearfix">
			@foreach($_category as $category)
			<div class="col-sm-4">
				<div class="holder {{ $category->product_category_color }}">
					<div class="icon"><img src="{{ $category->image_file }}"></div>
					<div class="name">{{ $category->product_category_name }}</div>
					<div class="desc">{{ $category->product_category_description }}</div>
					<button class="btn btn-lg" type="button" onClick="location.href='/product?category={{ $category->product_category_id }}'">View Products</button>
				</div>
			</div>
			@endforeach
			<!-- <div class="col-sm-4">
				<div class="holder blue">
					<div class="icon"><img src="/assets/front/img/category-1.png"></div>
					<div class="name">Beauty & Wellness</div>
					<div class="desc">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</div>
					<button class="btn btn-lg">View Products</button>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="holder red">
					<div class="icon"><img src="/assets/front/img/category-2.png"></div>
					<div class="name">Food & Beverages</div>
					<div class="desc">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</div>
					<button class="btn btn-lg">View Products</button>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="holder green">
					<div class="icon"><img src="/assets/front/img/category-3.png"></div>
					<div class="name">Business</div>
					<div class="desc">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu.</div>
					<button class="btn btn-lg">View Products</button>
				</div>
			</div> -->
		</div>
	</div>
</div>
@endsection
@section("css")
<link rel="stylesheet" type="text/css" href="/assets/front/css/product_category.css">
@endsection
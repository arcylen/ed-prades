@extends("front.layout")
@section("content")
<div class="intro">
	<div class="container">
		<div class="text">
			<div class="ichi">{{ isset($content_data['Introduction First Line (Front)']->about_description) ? $content_data['Introduction First Line (Front)']->about_description : "" }}</div>
			<div class="ni">{{ isset($content_data['Introduction Second Line (Front)']->about_description) ? $content_data['Introduction Second Line (Front)']->about_description : "" }}</div>
			<div class="san">{{ isset($content_data['Sub-Introduction (Front)']->about_description) ? $content_data['Sub-Introduction (Front)']->about_description : "" }}</div>
			<div class="intro-btn">
				<div class="line"></div>
				<div class="btn-container"><button class="btn">{{ isset($content_data['Introduction Button Text (Front)']->about_description) ? $content_data['Introduction Button Text (Front)']->about_description : "" }}</button></div>
			</div>
			<a class="bottom-arrow smooth-scroll" href="#second-row">
				<img src="/assets/front/img/bottom-arrow.png">
			</a>
		</div>
	</div>
</div>
<div class="container" id="second-row">
	<div class="about">
		<ul class="nav nav-tabs nav-justified">
		  <li class="active"><a data-toggle="tab" href="#home">Opportunity</a></li>
		  <li><a data-toggle="tab" href="#menu1">Mission</a></li>
		  <li><a data-toggle="tab" href="#menu2">Vision</a></li>
		  <li><a data-toggle="tab" href="#menu3">Commitment</a></li>
		</ul>
		<div class="tab-content">
		  <div id="home" class="tab-pane fade in active">
		    <div class="row clearfix">
		    	<div class="col-md-8">
		    		<h3>Opportunity for Better Life!</h3>
				    <p>{{ isset($content_data['Opportunity']->about_description) ? substr($content_data['Opportunity']->about_description, 0, 300) : "" }}...</p>
				    <button class="btn btn-lg" type="button" onClick="location.href='/opportunity'">Read More</button>
		    	</div>
		    	<div class="col-md-4">
		    		<img src="/assets/front/img/opportunity.png" class="img-responsive" style="margin: auto;">
		    	</div>
		    </div>
		  </div>
		  <div id="menu1" class="tab-pane fade">
		    <div class="row clearfix">
		    	<div class="col-md-8">
		    		<h3>Mission</h3>
				    <p>{{ isset($content_data['Mission']->about_description) ? substr($content_data['Mission']->about_description, 0, 300) : "" }}...</p>
				    <button class="btn btn-lg" type="button" onClick="location.href='/about'">Read More</button>
		    	</div>
		    	<div class="col-md-4">
		    		<img src="/assets/front/img/opportunity.png" class="img-responsive" style="margin: auto;">
		    	</div>
		    </div>
		  </div>
		  <div id="menu2" class="tab-pane fade">
		    <div class="row clearfix">
		    	<div class="col-md-8">
		    		<h3>Vision</h3>
				    <p>{{ isset($content_data['Vision']->about_description) ? substr($content_data['Vision']->about_description, 0, 300) : "" }}...</p>
				    <button class="btn btn-lg" type="button" onClick="location.href='/about'">Read More</button>
		    	</div>
		    	<div class="col-md-4">
		    		<img src="/assets/front/img/opportunity.png" class="img-responsive" style="margin: auto;">
		    	</div>
		    </div>
		  </div>
		  <div id="menu3" class="tab-pane fade">
		    <div class="row clearfix">
		    	<div class="col-md-8">
		    		<h3>Commitment</h3>
				    <p>{{ isset($content_data['Commitment']->about_description) ? substr($content_data['Commitment']->about_description, 0, 300) : "" }}...</p>
				    <button class="btn btn-lg" type="button" onClick="location.href='/about'">Read More</button>
		    	</div>
		    	<div class="col-md-4">
		    		<img src="/assets/front/img/opportunity.png" class="img-responsive" style="margin: auto;">
		    	</div>
		    </div>
		  </div>
		</div>
	</div>
</div>
<div class="product">
	<div class="container">
		<div class="title">{{ isset($content_data['Product Introduction (Home)']->about_description) ? $content_data['Product Introduction (Home)']->about_description : "" }}</div>
	</div>
	<div class="row-no-padding clearfix">
	@foreach($_product as $product)
	<div class="col-md-3 col-sm-6">
		<div class="list">
			<div class="img"><img src="{{ $product->image }}"></div>
			<div class="info {{ $product->category->product_category_color }}">
				<div class="name">{{ $product->product_name }}</div>
				<div class="desc">{{ $product->product_info }}</div>
				<div class="row clearfix">
					<div class="col-xs-6">
						<div class="text">{{ $product->category->product_category_name }}</div>
					</div>
					<div class="col-xs-6">
						<div class="text right" style="cursor: pointer;" onClick="location.href='/product/view/{{ $product->product_id }}'">Read More <img src="/assets/front/img/right-arrow.png"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endforeach
		<!-- <div class="col-md-3">
			<div class="list">
				<div class="img"><img src="/assets/front/img/product-1.jpg"></div>
				<div class="info blue">
					<div class="name">NUTRA-CEUTICALS</div>
					<div class="desc">Short product description</div>
					<div class="row clearfix">
						<div class="col-xs-6">
							<div class="text">Product Category</div>
						</div>
						<div class="col-xs-6">
							<div class="text right">Read More <img src="/assets/front/img/right-arrow.png"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="list">
				<div class="img"><img src="/assets/front/img/product-2.jpg"></div>
				<div class="info red">
					<div class="name">NUTRA-CEUTICALS</div>
					<div class="desc">Short product description</div>
					<div class="row clearfix">
						<div class="col-xs-6">
							<div class="text">Product Category</div>
						</div>
						<div class="col-xs-6">
							<div class="text right">Read More <img src="/assets/front/img/right-arrow.png"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="list">
				<div class="img"><img src="/assets/front/img/product-3.jpg"></div>
				<div class="info green">
					<div class="name">NUTRA-CEUTICALS</div>
					<div class="desc">Short product description</div>
					<div class="row clearfix">
						<div class="col-xs-6">
							<div class="text">Product Category</div>
						</div>
						<div class="col-xs-6">
							<div class="text right">Read More <img src="/assets/front/img/right-arrow.png"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="list">
				<div class="img"><img src="/assets/front/img/product-4.jpg"></div>
				<div class="info blue">
					<div class="name">NUTRA-CEUTICALS</div>
					<div class="desc">Short product description</div>
					<div class="row clearfix">
						<div class="col-xs-6">
							<div class="text">Product Category</div>
						</div>
						<div class="col-xs-6">
							<div class="text right">Read More <img src="/assets/front/img/right-arrow.png"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="list">
				<div class="img"><img src="/assets/front/img/product-1.jpg"></div>
				<div class="info red">
					<div class="name">NUTRA-CEUTICALS</div>
					<div class="desc">Short product description</div>
					<div class="row clearfix">
						<div class="col-xs-6">
							<div class="text">Product Category</div>
						</div>
						<div class="col-xs-6">
							<div class="text right">Read More <img src="/assets/front/img/right-arrow.png"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="list">
				<div class="img"><img src="/assets/front/img/product-6.jpg"></div>
				<div class="info green">
					<div class="name">NUTRA-CEUTICALS</div>
					<div class="desc">Short product description</div>
					<div class="row clearfix">
						<div class="col-xs-6">
							<div class="text">Product Category</div>
						</div>
						<div class="col-xs-6">
							<div class="text right">Read More <img src="/assets/front/img/right-arrow.png"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="list">
				<div class="img"><img src="/assets/front/img/product-7.jpg"></div>
				<div class="info blue">
					<div class="name">NUTRA-CEUTICALS</div>
					<div class="desc">Short product description</div>
					<div class="row clearfix">
						<div class="col-xs-6">
							<div class="text">Product Category</div>
						</div>
						<div class="col-xs-6">
							<div class="text right">Read More <img src="/assets/front/img/right-arrow.png"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="list">
				<div class="img"><img src="/assets/front/img/product-1.jpg"></div>
				<div class="info red">
					<div class="name">NUTRA-CEUTICALS</div>
					<div class="desc">Short product description</div>
					<div class="row clearfix">
						<div class="col-xs-6">
							<div class="text">Product Category</div>
						</div>
						<div class="col-xs-6">
							<div class="text right">Read More <img src="/assets/front/img/right-arrow.png"></div>
						</div>
					</div>
				</div>
			</div>
		</div> -->
	</div>
</div>
<div class="ending">
	<div class="container">
		<div class="row clearfix">
			<div class="col-md-8">{{ isset($content_data['Ending Footnote Text']->about_description) ? $content_data['Ending Footnote Text']->about_description : "" }}</div>
			<div class="col-md-4"><button class="btn">{{ isset($content_data['Ending Footnote Button Text']->about_description) ? $content_data['Ending Footnote Button Text']->about_description : "" }}</button></div>
		</div>
	</div>
</div>
@endsection
@section("script")
<script type="text/javascript" src="/assets/front/js/home.js"></script>
<script type="text/javascript" src="/resources/assets/frontend/js/lightslider.js"></script>
@endsection
@section("css")
<link rel="stylesheet" type="text/css" href="/assets/front/css/home.css">
@endsection
var opportunity = new opportunity();

function opportunity()
{
	init();
	function init()
	{
		$(document).ready(function()
		{
			document_ready();
		});
	}
	function document_ready()
	{
		event_sidebar_opportunity();
	}
	function event_sidebar_opportunity()
	{
		$('body').on('click', '.side-bar .holder', function(event) 
		{
			event.preventDefault();
			action_sidebar_opportunity(this);
		});
	}
	function action_sidebar_opportunity(x)
	{
		var show = $(x).attr("show");

		$('.side-bar .holder').removeClass('active');
		$('.side-bar .holder[show="'+show+'"]').addClass('active');
		$('.content-holder').addClass('hide');
		$('.content-holder[show="'+show+'"]').removeClass('hide');
	}
}
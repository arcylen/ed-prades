var distributespv = new distributespv();
var timer_search;
var filter = "0";
var page = 1;
var account_ajax_loading;
var search_keyword = "";
function distributespv()
{
	init();

	function init()
	{
		$(document).ready(function()
		{
			document_ready();
		});
	}

	function document_ready()
    {
        add_event_change_tab();
        add_event_pagination_click();
        action_load_table();
        add_search_event();
    }
    
    function add_search_event()
    {
        $(".search-keyword").keyup(function(e)
        {
            search_keyword = $(e.currentTarget).val();
            page = 1;
            action_load_table();
        });
    }
    function add_event_change_tab()
    {
        $(".nav-tabs a").unbind('click');
        $(".nav-tabs a").bind('click',function(e)
        {
            page = 1;
            filter = $(e.currentTarget).attr("data-content");
            action_load_table();
            $(".nav-tabs li").removeClass("active");
            $(e.currentTarget).closest("li").addClass("active");
            $(".filter-class").val(filter);
        });
    }
    function action_load_table()
    {
    	$(".show-loading").removeClass("hidden");

        var search_keyword = $(".search-keyword").val();
        var search_column = $(".search-col").val();
        var status = $(".search-keyword").val();

        if(account_ajax_loading)
        {
            table_ajax_loading.abort();
        }

        table_ajax_loading = $.ajax(
        {
            url: '/admin/transaction/distributespv/table',
            data: {page:page,filter:filter},
            traditional: true,
            type:"get",
            success: function(data)
            {
                $(".load-table").html(data);
                $(".show-loading").addClass("hidden");
            },
            error: function(data)
            {
                alert("ERROR!:" + data);
            }
        });
    }
    function add_event_pagination_click()
    {
        $("body").on("click", ".pagination a", function(e)
        {
            $link = $(e.currentTarget).attr("href");
            page = gup("page", $link);
            action_load_table();
            return false;
        });
    }

    function gup( name, url )
    {
          if (!url) url = location.href;
          name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
          var regexS = "[\\?&]"+name+"=([^&#]*)";
          var regex = new RegExp( regexS );
          var results = regex.exec( url );
          return results == null ? null : results[1];
    }
}
